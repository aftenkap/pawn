package raziel.pawn.persistence.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-12-30T17:05:18.930-0500")
@StaticMetamodel(DomainObject.class)
public class DomainObject_ {
	public static volatile SingularAttribute<DomainObject, Integer> id;
	public static volatile SingularAttribute<DomainObject, Long> version;
	public static volatile SingularAttribute<DomainObject, String> name;
}
