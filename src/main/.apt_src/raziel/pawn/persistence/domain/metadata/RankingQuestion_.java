package raziel.pawn.persistence.domain.metadata;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-12-30T17:05:19.075-0500")
@StaticMetamodel(RankingQuestion.class)
public class RankingQuestion_ extends Question_ {
	public static volatile SingularAttribute<RankingQuestion, ChoiceSet> choices;
	public static volatile ListAttribute<RankingQuestion, ChoiceQuestion> rankings;
}
