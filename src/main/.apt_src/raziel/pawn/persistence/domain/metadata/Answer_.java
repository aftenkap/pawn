package raziel.pawn.persistence.domain.metadata;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;
import raziel.pawn.persistence.domain.data.DataPointBase;

@Generated(value="Dali", date="2014-12-30T17:05:18.951-0500")
@StaticMetamodel(Answer.class)
public class Answer_ extends DomainObject_ {
	public static volatile SingularAttribute<Answer, Participant> participant;
	public static volatile SingularAttribute<Answer, Question> question;
	public static volatile SingularAttribute<Answer, DataPointBase> dataPoint;
	public static volatile SingularAttribute<Answer, String> value;
}
