package raziel.pawn.persistence.domain.metadata;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;
import raziel.pawn.persistence.domain.data.DataPointBase;

@Generated(value="Dali", date="2014-12-30T17:05:19.058-0500")
@StaticMetamodel(Question.class)
public class Question_ extends DomainObject_ {
	public static volatile SingularAttribute<Question, String> questionText;
	public static volatile SingularAttribute<Question, Boolean> isAnswerRequired;
	public static volatile SingularAttribute<Question, Integer> order;
	public static volatile SingularAttribute<Question, DataPointBase> dataPoint;
}
