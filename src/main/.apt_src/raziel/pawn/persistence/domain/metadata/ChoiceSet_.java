package raziel.pawn.persistence.domain.metadata;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;

@Generated(value="Dali", date="2014-12-30T17:05:19.006-0500")
@StaticMetamodel(ChoiceSet.class)
public class ChoiceSet_ extends DomainObject_ {
	public static volatile SetAttribute<ChoiceSet, Choice> choices;
}
