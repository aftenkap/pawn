package raziel.pawn.persistence.domain.metadata;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;
import raziel.pawn.persistence.domain.data.DataPointBase;

@Generated(value="Dali", date="2014-12-30T17:05:19.044-0500")
@StaticMetamodel(Participant.class)
public class Participant_ extends DomainObject_ {
	public static volatile SingularAttribute<Participant, DataPointBase> dataPoint;
}
