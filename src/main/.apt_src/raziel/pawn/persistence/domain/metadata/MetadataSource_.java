package raziel.pawn.persistence.domain.metadata;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;
import raziel.pawn.persistence.domain.data.DataSourceBase;

@Generated(value="Dali", date="2014-12-30T17:05:19.035-0500")
@StaticMetamodel(MetadataSource.class)
public class MetadataSource_ extends DomainObject_ {
	public static volatile ListAttribute<MetadataSource, Participant> participants;
	public static volatile SingularAttribute<MetadataSource, DataSourceBase> dataSource;
}
