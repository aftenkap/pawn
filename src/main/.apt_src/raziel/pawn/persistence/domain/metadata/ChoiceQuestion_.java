package raziel.pawn.persistence.domain.metadata;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-12-30T17:05:18.995-0500")
@StaticMetamodel(ChoiceQuestion.class)
public class ChoiceQuestion_ extends Question_ {
	public static volatile SingularAttribute<ChoiceQuestion, TextQuestion> alternativeChoice;
	public static volatile SingularAttribute<ChoiceQuestion, ChoiceSet> choices;
}
