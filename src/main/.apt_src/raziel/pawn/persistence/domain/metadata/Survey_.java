package raziel.pawn.persistence.domain.metadata;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-12-30T17:05:19.103-0500")
@StaticMetamodel(Survey.class)
public class Survey_ extends MetadataSource_ {
	public static volatile ListAttribute<Survey, Question> questions;
	public static volatile ListAttribute<Survey, Answer> answers;
}
