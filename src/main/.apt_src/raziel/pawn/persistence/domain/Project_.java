package raziel.pawn.persistence.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.data.DataSourceBase;
import raziel.pawn.persistence.domain.metadata.MetadataSource;
import raziel.pawn.persistence.domain.modeling.DataModel;

@Generated(value="Dali", date="2014-12-30T17:05:19.443-0500")
@StaticMetamodel(Project.class)
public class Project_ extends DomainObject_ {
	public static volatile SingularAttribute<Project, DataModel> dataModel;
	public static volatile ListAttribute<Project, MetadataSource> metadataSources;
	public static volatile ListAttribute<Project, DataSourceBase> dataSources;
}
