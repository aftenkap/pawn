package raziel.pawn.persistence.domain.modeling.time;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;

@Generated(value="Dali", date="2014-12-30T17:05:19.425-0500")
@StaticMetamodel(DurationOrder.class)
public class DurationOrder_ extends DomainObject_ {
	public static volatile SingularAttribute<DurationOrder, Duration> firstOperand;
	public static volatile SingularAttribute<DurationOrder, Duration> secondOperand;
	public static volatile SingularAttribute<DurationOrder, Operator> operator;
}
