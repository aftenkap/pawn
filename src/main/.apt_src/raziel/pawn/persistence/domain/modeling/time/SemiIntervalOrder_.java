package raziel.pawn.persistence.domain.modeling.time;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;

@Generated(value="Dali", date="2014-12-30T17:05:19.440-0500")
@StaticMetamodel(SemiIntervalOrder.class)
public class SemiIntervalOrder_ extends DomainObject_ {
	public static volatile SingularAttribute<SemiIntervalOrder, SemiInterval> firstOperand;
	public static volatile SingularAttribute<SemiIntervalOrder, SemiInterval> secondOperand;
	public static volatile SingularAttribute<SemiIntervalOrder, Operator> operator;
}
