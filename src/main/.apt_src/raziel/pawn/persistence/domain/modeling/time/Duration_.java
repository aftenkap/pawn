package raziel.pawn.persistence.domain.modeling.time;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;

@Generated(value="Dali", date="2014-12-30T17:05:19.402-0500")
@StaticMetamodel(Duration.class)
public class Duration_ extends DomainObject_ {
	public static volatile SingularAttribute<Duration, SemiInterval> start;
	public static volatile SingularAttribute<Duration, SemiInterval> end;
	public static volatile SingularAttribute<Duration, Date> duration;
}
