package raziel.pawn.persistence.domain.modeling;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;
import raziel.pawn.persistence.domain.data.DataPointBase;

@Generated(value="Dali", date="2014-12-30T17:05:19.126-0500")
@StaticMetamodel(Assertion.class)
public class Assertion_ extends DomainObject_ {
	public static volatile ListAttribute<Assertion, DataPointBase> support;
}
