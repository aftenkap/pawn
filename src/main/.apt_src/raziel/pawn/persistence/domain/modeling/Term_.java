package raziel.pawn.persistence.domain.modeling;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;
import raziel.pawn.persistence.domain.data.DataPointBase;

@Generated(value="Dali", date="2014-12-30T17:05:19.260-0500")
@StaticMetamodel(Term.class)
public class Term_ extends DomainObject_ {
	public static volatile ListAttribute<Term, DataPointBase> support;
	public static volatile ListAttribute<Term, Assertion> instances;
}
