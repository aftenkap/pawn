package raziel.pawn.persistence.domain.modeling.assertions;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.modeling.Assertion_;
import raziel.pawn.persistence.domain.modeling.time.Duration;

@Generated(value="Dali", date="2014-12-30T17:05:19.185-0500")
@StaticMetamodel(Entity.class)
public class Entity_ extends Assertion_ {
	public static volatile SingularAttribute<Entity, Duration> duration;
}
