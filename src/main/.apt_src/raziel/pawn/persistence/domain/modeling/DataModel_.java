package raziel.pawn.persistence.domain.modeling;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;
import raziel.pawn.persistence.domain.modeling.time.DurationOrder;
import raziel.pawn.persistence.domain.modeling.time.SemiIntervalOrder;

@Generated(value="Dali", date="2014-12-30T17:05:19.245-0500")
@StaticMetamodel(DataModel.class)
public class DataModel_ extends DomainObject_ {
	public static volatile ListAttribute<DataModel, Term> terms;
	public static volatile ListAttribute<DataModel, Assertion> assertions;
	public static volatile ListAttribute<DataModel, DurationOrder> durationOrders;
	public static volatile ListAttribute<DataModel, SemiIntervalOrder> semiIntervalOrders;
}
