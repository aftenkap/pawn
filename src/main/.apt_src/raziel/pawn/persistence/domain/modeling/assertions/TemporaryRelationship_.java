package raziel.pawn.persistence.domain.modeling.assertions;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.modeling.time.Duration;

@Generated(value="Dali", date="2014-12-30T17:05:19.240-0500")
@StaticMetamodel(TemporaryRelationship.class)
public class TemporaryRelationship_ extends Relationship_ {
	public static volatile SingularAttribute<TemporaryRelationship, Duration> duration;
}
