package raziel.pawn.persistence.domain.modeling.terminology;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.modeling.Term_;

@Generated(value="Dali", date="2014-12-30T17:05:19.293-0500")
@StaticMetamodel(Concept.class)
public class Concept_ extends Term_ {
	public static volatile ListAttribute<Concept, Concept> descendants;
}
