package raziel.pawn.persistence.domain.modeling.time;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;

@Generated(value="Dali", date="2014-12-30T17:05:19.437-0500")
@StaticMetamodel(SemiInterval.class)
public class SemiInterval_ extends DomainObject_ {
	public static volatile SingularAttribute<SemiInterval, Date> time;
}
