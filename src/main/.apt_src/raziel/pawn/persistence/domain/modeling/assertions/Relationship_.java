package raziel.pawn.persistence.domain.modeling.assertions;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.modeling.Assertion;
import raziel.pawn.persistence.domain.modeling.Assertion_;

@Generated(value="Dali", date="2014-12-30T17:05:19.217-0500")
@StaticMetamodel(Relationship.class)
public class Relationship_ extends Assertion_ {
	public static volatile ListAttribute<Relationship, Assertion> domainAssertions;
	public static volatile ListAttribute<Relationship, Assertion> rangeAssertions;
}
