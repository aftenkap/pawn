package raziel.pawn.persistence.domain.data;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import raziel.pawn.persistence.domain.DomainObject_;

@Generated(value="Dali", date="2014-12-30T17:05:18.786-0500")
@StaticMetamodel(DataSourceBase.class)
public class DataSourceBase_ extends DomainObject_ {
	public static volatile SingularAttribute<DataSourceBase, String> uri;
}
