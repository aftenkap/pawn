package raziel.pawn.persistence.domain.data;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-12-30T17:05:18.860-0500")
@StaticMetamodel(SpreadsheetCell.class)
public class TableCell_ extends DataPointBase_ {
	public static volatile SingularAttribute<SpreadsheetCell, Integer> row;
	public static volatile SingularAttribute<SpreadsheetCell, Integer> column;
	public static volatile SingularAttribute<SpreadsheetCell, Object> value;
}
