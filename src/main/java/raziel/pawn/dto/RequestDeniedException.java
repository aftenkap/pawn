package raziel.pawn.dto;


/**
 * The {@link RequestDeniedException} represents an exception thrown if a
 * request is denied.
 * 
 * @author Peter J. Radics
 * @version 0.0.1
 * @since 0.0.1
 */
public class RequestDeniedException
        extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -4594466596616301349L;

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     */
    public RequestDeniedException() {

        super();
    }

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     * 
     * @param message
     *            the message.
     */
    public RequestDeniedException(String message) {

        super(message);
    }

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     * 
     * @param cause
     *            the cause.
     */
    public RequestDeniedException(Throwable cause) {

        super(cause);
    }

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     * 
     * @param message
     *            the message.
     * @param cause
     *            the cause.
     */
    public RequestDeniedException(String message, Throwable cause) {

        super(message, cause);
    }

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     * 
     * @param message
     *            the message.
     * @param cause
     *            the cause.
     * @param enableSuppression
     *            whether suppression is enabled.
     * @param writableStackTrace
     *            whether the stack trace is writable.
     */
    public RequestDeniedException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {

        super(message, cause, enableSuppression, writableStackTrace);
    }

}
