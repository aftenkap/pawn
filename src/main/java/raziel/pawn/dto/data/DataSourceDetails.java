/**
 * 
 */
package raziel.pawn.dto.data;


import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class DataSourceDetails
        extends DomainObjectDTO {

    private String uri;


    /**
     * Returns the uri.
     * 
     * @return the uri.
     */
    public String getUri() {

        return uri;
    }


    /**
     * Sets the uri.
     * 
     * @param uri
     *            the uri.
     */
    public void setUri(String uri) {

        this.uri = uri;
    }

    /**
     * Creates a new instance of the {@link DataSourceDetails} class.
     */
    public DataSourceDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link DataSourceDetails} class.
     * 
     * @param id
     *            the id.
     */
    public DataSourceDetails(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link DataSourceDetails} class.
     * 
     * @param details
     */
    public <T extends DomainObjectDTO> DataSourceDetails(T details) {

        super(details);

        if (details instanceof DataSourceDetails) {

            DataSourceDetails dsdetails = (DataSourceDetails) details;

            this.setUri(dsdetails.getUri());
        }
    }

}
