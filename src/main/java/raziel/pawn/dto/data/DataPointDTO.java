package raziel.pawn.dto.data;


import raziel.pawn.dto.DomainObjectDTO;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class DataPointDTO
        extends DomainObjectDTO {


    /**
     * Creates a new instance of the {@link DataPointDTO} class.
     */
    public DataPointDTO() {

        this((Integer)null);
    }

    /**
     * Creates a new instance of the {@link DataPointDTO} class.
     * 
     * @param id
     *            the id.
     */
    public DataPointDTO(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link DataPointDTO} class. (Copy
     * Constructor)
     * 
     * @param details
     */
    public <T extends DomainObjectDTO> DataPointDTO(T details) {

        super(details);
    }
}
