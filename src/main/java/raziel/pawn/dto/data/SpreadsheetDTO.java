package raziel.pawn.dto.data;


import java.util.ArrayList;
import java.util.List;

import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public class SpreadsheetDTO
        extends DataSourceDetails {

    private final List<SpreadsheetCellDTO> cells;


    /**
     * Returns the cells.
     * 
     * @return the cells.
     */
    public List<SpreadsheetCellDTO> getCells() {

        return cells;
    }


    /**
     * Sets the cells.
     * 
     * @param cells
     *            the cells.
     */
    public void setCells(List<SpreadsheetCellDTO> cells) {

        if (cells != null) {

            this.cells.addAll(cells);
        }
    }

    /**
     * Creates a new instance of the {@link SpreadsheetDTO} class.
     */
    public SpreadsheetDTO() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link SpreadsheetDTO} class.
     * 
     * @param id
     *            the id.
     */
    public SpreadsheetDTO(Integer id) {

        super(id);

        this.cells = new ArrayList<>();
    }

    /**
     * Creates a new instance of the {@link SpreadsheetDTO} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <S extends DomainObjectDTO> SpreadsheetDTO(S details) {

        super(details);

        this.cells = new ArrayList<>();
    }

    /**
     * Creates a new instance of the {@link SpreadsheetDTO} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public SpreadsheetDTO(SpreadsheetDTO details) {

        super(details);

        this.cells = new ArrayList<>();
        this.setCells(details.getCells());
    }
}
