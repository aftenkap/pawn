package raziel.pawn.dto.data;



/**
 * The generic {@link SpreadsheetCellDTO} class is a container class for
 * elements that are referenced by two indices.
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 */

public class SpreadsheetCellDTO
        extends DataPointDTO {


    private int    row;
    private int    column;
    private String value;


    /**
     * Returns the row.
     * 
     * @return the row.
     */
    public int getRow() {

        return this.row;
    }

    /**
     * Sets the row.
     * 
     * @param row
     *            the row.
     */
    public void setRow(int row) {

        this.row = row;
    }

    /**
     * Returns the column.
     * 
     * @return the column.
     */
    public int getColumn() {

        return this.column;
    }

    /**
     * Sets the column.
     * 
     * @param column
     *            the column.
     */
    public void setColumn(int column) {

        this.column = column;
    }

    /**
     * Returns the value.
     * 
     * @return the value.
     */
    public String getValue() {

        return this.value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     *            the value.
     */
    public void setValue(String value) {

        this.value = value;
    }

    /**
     * Creates a new instance of the {@link SpreadsheetCellDTO} class.
     */
    public SpreadsheetCellDTO() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link SpreadsheetCellDTO} class.
     * 
     * @param id
     *            the id.
     */
    public SpreadsheetCellDTO(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link SpreadsheetCellDTO} class from the
     * provided DTO.
     * 
     * @param dto
     *            the DTO.
     */
    public <S extends DataPointDTO> SpreadsheetCellDTO(S dto) {

        super(dto);
    }

    /**
     * Creates a new instance of the {@link SpreadsheetCellDTO} class from the
     * provided DTO.
     * 
     * @param dto
     *            the DTO.
     */
    public SpreadsheetCellDTO(SpreadsheetCellDTO dto) {

        super(dto);

        this.setRow(dto.getRow());
        this.setColumn(dto.getColumn());
        this.setValue(dto.getValue());
    }

    @Override
    public String toString() {

        return this.getId() + "@" + this.getVersion() + ":" + this.getName()
                + " - " + this.getRow() + ", " + this.getColumn() + " = "
                + this.getValue();
    }
}
