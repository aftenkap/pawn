/**
 * 
 */
package raziel.pawn.dto;


import java.util.List;

import raziel.pawn.dto.data.DataSourceDetails;
import raziel.pawn.dto.metadata.MetadataSourceDetails;
import raziel.pawn.dto.modeling.DataModelDetails;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class ProjectDetails
        extends DomainObjectDTO {

    private DataModelDetails            dataModel;

    private List<MetadataSourceDetails> metadataSources;
    private List<DataSourceDetails>     dataSources;



    /**
     * Returns the dataModel.
     * 
     * @return the dataModel.
     */
    public DataModelDetails getDataModel() {

        return dataModel;
    }


    /**
     * Sets the dataModel.
     * 
     * @param dataModel
     *            the dataModel.
     */
    public void setDataModel(DataModelDetails dataModel) {

        this.dataModel = dataModel;
    }


    /**
     * Returns the metadataSources.
     * 
     * @return the metadataSources.
     */
    public List<MetadataSourceDetails> getMetadataSources() {

        return metadataSources;
    }


    /**
     * Sets the metadataSources.
     * 
     * @param metadataSources
     *            the metadataSources.
     */
    public void setMetadataSources(List<MetadataSourceDetails> metadataSources) {

        this.metadataSources = metadataSources;
    }


    /**
     * Returns the dataSources.
     * 
     * @return the dataSources.
     */
    public List<DataSourceDetails> getDataSources() {

        return dataSources;
    }


    /**
     * Sets the dataSources.
     * 
     * @param dataSources
     *            the dataSources.
     */
    public void setDataSources(List<DataSourceDetails> dataSources) {

        this.dataSources = dataSources;
    }

    /**
     * Creates a new instance of the {@link ProjectDetails} class.
     */
    public ProjectDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link ProjectDetails} class.
     * 
     * @param id
     *            the id.
     */
    public ProjectDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link ProjectDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> ProjectDetails(T details) {

        super(details);

    }


    /**
     * Creates a new instance of the {@link ProjectDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public ProjectDetails(ProjectDetails details) {

        super(details);

        this.setDataModel(details.getDataModel());
        this.setDataSources(details.getDataSources());
        this.setMetadataSources(details.getMetadataSources());
    }

}
