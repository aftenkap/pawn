package raziel.pawn.dto;


import raziel.pawn.persistence.domain.DomainObject;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class DomainObjectDTO {

    private final Integer id;
    private long      version;
    private String    name;


    /**
     * Returns the id.
     * 
     * @return the id.
     */
    public Integer getId() {

        return id;
    }


    /**
     * Returns the version.
     * 
     * @return the version.
     */
    public long getVersion() {

        return this.version;
    }


    /**
     * Sets the version.
     * 
     * @param version
     *            the version.
     */
    public void setVersion(long version) {

        this.version = version;
    }


    /**
     * Returns the name.
     * 
     * @return the name.
     */
    public String getName() {

        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the name.
     */
    public void setName(String name) {

        this.name = name;
    }


    /**
     * Creates a new instance of the {@link DomainObjectDTO} class.
     * <p/>
     * Note: constructor for creation of new {@link DomainObject DomainObjects}.
     */
    public DomainObjectDTO() {

        this((Integer)null);
    }

    /**
     * Creates a new instance of the {@link DomainObjectDTO} class.
     * 
     * 
     * @param id
     *            the id.
     */
    public DomainObjectDTO(Integer id) {


        this.id = id;
        this.name = "";
    }

    /**
     * Creates a new instance of the {@link DomainObjectDTO} class. (Copy
     * Constructor)
     * 
     * @param details
     *            the details to copy.
     */
    public <T extends DomainObjectDTO> DomainObjectDTO(final T details) {

        this.id = details.getId();
        this.version = details.getVersion();
        this.name = details.getName();
    }
}
