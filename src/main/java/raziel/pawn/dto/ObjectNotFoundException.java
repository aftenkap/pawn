package raziel.pawn.dto;


/**
 * The {@link ObjectNotFoundException} represents an exception thrown if an
 * object is not found.
 * 
 * @author Peter J. Radics
 * @version 0.0.1
 * @since 0.0.1
 */
public class ObjectNotFoundException
        extends Exception {


    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -4594466596616301349L;

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     */
    public ObjectNotFoundException() {

        super();
    }

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     * 
     * @param message
     *            the message.
     */
    public ObjectNotFoundException(String message) {

        super(message);
    }

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     * 
     * @param cause
     *            the cause.
     */
    public ObjectNotFoundException(Throwable cause) {

        super(cause);
    }

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     * 
     * @param message
     *            the message.
     * @param cause
     *            the cause.
     */
    public ObjectNotFoundException(String message, Throwable cause) {

        super(message, cause);
    }

    /**
     * Creates a new instance of the {@code RequestFailedException} class.
     * 
     * @param message
     *            the message.
     * @param cause
     *            the cause.
     * @param enableSuppression
     *            whether suppression is enabled.
     * @param writableStackTrace
     *            whether the stack trace is writable.
     */
    public ObjectNotFoundException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {

        super(message, cause, enableSuppression, writableStackTrace);
    }

}
