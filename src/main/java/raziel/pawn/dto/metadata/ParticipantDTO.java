/**
 * 
 */
package raziel.pawn.dto.metadata;


import raziel.pawn.dto.DomainObjectDTO;
import raziel.pawn.dto.data.DataPointDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class ParticipantDTO
        extends DomainObjectDTO {

    private DataPointDTO dataPoint;


    /**
     * Returns the dataPoint.
     * 
     * @return the dataPoint.
     */
    public DataPointDTO getDataPoint() {

        return dataPoint;
    }


    /**
     * Sets the dataPoint.
     * 
     * @param dataPoint
     *            the dataPoint.
     */
    public void setDataPoint(DataPointDTO dataPoint) {

        this.dataPoint = dataPoint;
    }

    /**
     * Creates a new instance of the {@link ParticipantDTO} class.
     */
    public ParticipantDTO() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link ParticipantDTO} class.
     * 
     * @param id
     *            the id.
     */
    public ParticipantDTO(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link ParticipantDTO} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> ParticipantDTO(T details) {

        super(details);
    }

    /**
     * Creates a new instance of the {@link ParticipantDTO} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public ParticipantDTO(ParticipantDTO details) {

        super(details);

        this.setDataPoint(details.getDataPoint());
    }

}
