package raziel.pawn.dto.metadata;


import raziel.pawn.dto.DomainObjectDTO;
import raziel.pawn.dto.data.DataPointDTO;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class QuestionDetails
        extends DomainObjectDTO {

    private String           questionText;
    private boolean          answerRequired;
    private int              order;

    private DataPointDTO dataPoint;

    /**
     * Returns the question text.
     * 
     * @return the question text.
     */
    public String getQuestionText() {

        return questionText;
    }

    /**
     * Sets the question text.
     * 
     * @param questionText
     *            the question text.
     */
    public void setQuestionText(String questionText) {

        this.questionText = questionText;
    }


    /**
     * Returns whether or not an answer is required.
     * 
     * @return {@code true}, if an answer is required; {@code false} otherwise.
     */
    public boolean isAnswerRequired() {

        return this.answerRequired;
    }


    /**
     * Sets the answerRequired.
     * 
     * @param answerRequired
     *            set to {@code true} if an answer is required; set to
     *            {@code false} otherwise.
     */
    public void setAnswerRequired(boolean answerRequired) {

        this.answerRequired = answerRequired;
    }


    /**
     * Returns the order of the question.
     * 
     * @return the order of the question.
     */
    public int getOrder() {

        return this.order;
    }

    /**
     * Sets the order of the question.
     * 
     * @param order
     *            the order of the question.
     */
    public void setOrder(int order) {

        this.order = order;
    }


    /**
     * Returns the dataPoint.
     * 
     * @return the dataPoint.
     */
    public DataPointDTO getDataPoint() {

        return dataPoint;
    }


    /**
     * Sets the dataPoint.
     * 
     * @param dataPoint
     *            the dataPoint.
     */
    public void setDataPoint(DataPointDTO dataPoint) {

        this.dataPoint = dataPoint;
    }

    /**
     * Creates a new instance of the {@link QuestionDetails} class.
     */
    public QuestionDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link QuestionDetails} class.
     * 
     * @param id
     *            the id.
     */
    public QuestionDetails(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link QuestionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> QuestionDetails(T details) {

        super(details);

    }


    /**
     * Creates a new instance of the {@link QuestionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public QuestionDetails(final QuestionDetails details) {

        super(details);

        this.setQuestionText(details.getQuestionText());
        this.setAnswerRequired(details.isAnswerRequired());
        this.setOrder(details.getOrder());
        this.setDataPoint(details.getDataPoint());

    }
}
