/**
 * 
 */
package raziel.pawn.dto.metadata;


import java.util.List;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class SurveyDetails
        extends MetadataSourceDetails {

    private List<QuestionDetails> questions;
    private List<AnswerDetails>   answers;


    /**
     * Returns the questions.
     * 
     * @return the questions.
     */
    public List<QuestionDetails> getQuestions() {

        return questions;
    }


    /**
     * Sets the questions.
     * 
     * @param questions
     *            the questions.
     */
    public void setQuestions(List<QuestionDetails> questions) {

        this.questions = questions;
    }


    /**
     * Returns the answers.
     * 
     * @return the answers.
     */
    public List<AnswerDetails> getAnswers() {

        return answers;
    }


    /**
     * Sets the answers.
     * 
     * @param answers
     *            the answers.
     */
    public void setAnswers(List<AnswerDetails> answers) {

        this.answers = answers;
    }

    /**
     * Creates a new instance of the {@link SurveyDetails} class.
     */
    public SurveyDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link SurveyDetails} class.
     * 
     * @param id
     *            the id.
     */
    public SurveyDetails(Integer id) {

        super(id);
    }



    /**
     * Creates a new instance of the {@link SurveyDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends MetadataSourceDetails> SurveyDetails(final T details) {

        super(details);
    }

    /**
     * Creates a new instance of the {@link SurveyDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public SurveyDetails(final SurveyDetails details) {

        super(details);

        this.setAnswers(details.getAnswers());
        this.setQuestions(details.getQuestions());
    }

}
