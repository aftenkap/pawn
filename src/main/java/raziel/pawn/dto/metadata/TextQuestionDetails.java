/**
 * 
 */
package raziel.pawn.dto.metadata;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class TextQuestionDetails
        extends QuestionDetails {

    /**
     * Creates a new instance of the {@link TextQuestionDetails} class.
     */
    public TextQuestionDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link SurveyDetails} class.
     * 
     * @param id
     *            the id.
     */
    public TextQuestionDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link TextQuestionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends QuestionDetails> TextQuestionDetails(final T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link TextQuestionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public TextQuestionDetails(final TextQuestionDetails details) {

        super(details);
    }
}
