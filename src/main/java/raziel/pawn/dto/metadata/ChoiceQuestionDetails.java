/**
 * 
 */
package raziel.pawn.dto.metadata;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class ChoiceQuestionDetails
        extends QuestionDetails {

    private TextQuestionDetails alternativeChoice;

    private ChoiceSetDetails    choices;


    /**
     * Returns the alternativeChoice.
     * 
     * @return the alternativeChoice.
     */
    public TextQuestionDetails getAlternativeChoice() {

        return alternativeChoice;
    }


    /**
     * Sets the alternativeChoice.
     * 
     * @param alternativeChoice
     *            the alternativeChoice.
     */
    public void setAlternativeChoice(TextQuestionDetails alternativeChoice) {

        this.alternativeChoice = alternativeChoice;
    }


    /**
     * Returns the choices.
     * 
     * @return the choices.
     */
    public ChoiceSetDetails getChoices() {

        return choices;
    }


    /**
     * Sets the choices.
     * 
     * @param choices
     *            the choices.
     */
    public void setChoices(ChoiceSetDetails choices) {

        this.choices = choices;
    }


    /**
     * Creates a new instance of the {@link ChoiceQuestionDetails} class.
     */
    public ChoiceQuestionDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link ChoiceQuestionDetails} class.
     * 
     * @param id
     *            the id.
     */
    public ChoiceQuestionDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link ChoiceQuestionDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends QuestionDetails> ChoiceQuestionDetails(final T details) {

        super(details);
    }



    /**
     * Creates a new instance of the {@link ChoiceQuestionDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public ChoiceQuestionDetails(final ChoiceQuestionDetails details) {

        super(details);
        
        this.setAlternativeChoice(details.getAlternativeChoice());
        this.setChoices(details.getChoices());
    }
}
