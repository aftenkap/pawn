/**
 * 
 */
package raziel.pawn.dto.metadata;


import java.util.List;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class RankingQuestionDetails
        extends QuestionDetails {

    private List<ChoiceQuestionDetails> rankings;
    private ChoiceSetDetails            choices;


    /**
     * Returns the rankings.
     * 
     * @return the rankings.
     */
    public List<ChoiceQuestionDetails> getRanks() {

        return this.rankings;
    }


    /**
     * Sets the rankings.
     * 
     * @param rankings
     *            the rankings.
     */
    public void setRankings(List<ChoiceQuestionDetails> rankings) {

        this.rankings = rankings;
    }


    /**
     * Returns the choices.
     * 
     * @return the choices.
     */
    public ChoiceSetDetails getChoices() {

        return choices;
    }


    /**
     * Sets the choices.
     * 
     * @param choices
     *            the choices.
     */
    public void setChoices(ChoiceSetDetails choices) {

        this.choices = choices;
    }


    /**
     * Creates a new instance of the {@link RankingQuestionDetails} class.
     */
    public RankingQuestionDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link RankingQuestionDetails} class.
     * 
     * @param id
     *            the id.
     */
    public RankingQuestionDetails(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link RankingQuestionDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details to copy.
     */
    public <T extends QuestionDetails> RankingQuestionDetails(final T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link RankingQuestionDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details to copy.
     */
    public RankingQuestionDetails(final RankingQuestionDetails details) {

        super(details);

        this.setRankings(details.getRanks());
        this.setChoices(details.getChoices());
    }
}
