/**
 * 
 */
package raziel.pawn.dto.metadata;


import java.util.List;

import raziel.pawn.dto.DomainObjectDTO;
import raziel.pawn.dto.data.DataSourceDetails;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class MetadataSourceDetails
        extends DomainObjectDTO {

    private DataSourceDetails        dataSource;

    private List<ParticipantDTO> participants;



    /**
     * Returns the dataSource.
     * 
     * @return the dataSource.
     */
    public DataSourceDetails getDataSource() {

        return dataSource;
    }


    /**
     * Sets the dataSource.
     * 
     * @param dataSource
     *            the dataSource.
     */
    public void setDataSource(DataSourceDetails dataSource) {

        this.dataSource = dataSource;
    }


    /**
     * Returns the participants.
     * 
     * @return the participants.
     */
    public List<ParticipantDTO> getParticipants() {

        return participants;
    }


    /**
     * Sets the participants.
     * 
     * @param participants
     *            the participants.
     */
    public void setParticipants(List<ParticipantDTO> participants) {

        this.participants = participants;
    }

    /**
     * Creates a new instance of the {@link MetadataSourceDetails} class.
     */
    public MetadataSourceDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link ChoiceSetDetails} class.
     * 
     * @param id
     *            the id.
     */
    public MetadataSourceDetails(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link MetadataSourceDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> MetadataSourceDetails(T details) {

        super(details);
    }

    /**
     * Creates a new instance of the {@link MetadataSourceDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public MetadataSourceDetails(final MetadataSourceDetails details) {

        super(details);

        this.setDataSource(details.getDataSource());
        this.setParticipants(details.getParticipants());
    }
}
