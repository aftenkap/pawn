/**
 * 
 */
package raziel.pawn.dto.metadata;


import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class ChoiceDetails
        extends DomainObjectDTO {

    /**
     * Creates a new instance of the {@link ChoiceDetails} class.
     */
    public ChoiceDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link ChoiceDetails} class.
     * 
     * @param id
     *            the id.
     */
    public ChoiceDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link ChoiceDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> ChoiceDetails(T details) {

        super(details);
    }

    
    /**
     * Creates a new instance of the {@link ChoiceDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */

    public ChoiceDetails(final ChoiceDetails details) {

        super(details);
    }
}
