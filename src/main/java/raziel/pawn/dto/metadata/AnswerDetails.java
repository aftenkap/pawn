/**
 * 
 */
package raziel.pawn.dto.metadata;


import raziel.pawn.dto.DomainObjectDTO;
import raziel.pawn.dto.data.DataPointDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class AnswerDetails
        extends DomainObjectDTO {

    private QuestionDetails    question;
    private ParticipantDTO participant;
    private DataPointDTO   dataPoint;

    private String             value;


    /**
     * Returns the question.
     * 
     * @return the question.
     */
    public QuestionDetails getQuestion() {

        return question;
    }


    /**
     * Sets the question.
     * 
     * @param question
     *            the question.
     */
    public void setQuestion(QuestionDetails question) {

        this.question = question;
    }

    /**
     * Returns the participant.
     * 
     * @return the participant.
     */
    public ParticipantDTO getParticipant() {

        return participant;
    }


    /**
     * Sets the participant.
     * 
     * @param participant
     *            the participant.
     */
    public void setParticipant(ParticipantDTO participant) {

        this.participant = participant;
    }

    /**
     * Returns the dataPoint.
     * 
     * @return the dataPoint.
     */
    public DataPointDTO getDataPoint() {

        return dataPoint;
    }


    /**
     * Sets the dataPoint.
     * 
     * @param dataPoint
     *            the dataPoint.
     */
    public void setDataPoint(DataPointDTO dataPoint) {

        this.dataPoint = dataPoint;
    }



    /**
     * Returns the value.
     * 
     * @return the value.
     */
    public String getValue() {

        return this.value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     *            the value.
     */
    public void setValue(String value) {

        this.value = value;
    }


    /**
     * Creates a new instance of the {@link AnswerDetails} class.
     */
    public AnswerDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link AnswerDetails} class.
     * 
     * @param id
     *            the id.
     */
    public AnswerDetails(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link AnswerDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> AnswerDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link AnswerDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public AnswerDetails(AnswerDetails details) {

        super(details);

        this.setQuestion(details.getQuestion());
        this.setParticipant(details.getParticipant());
        this.setDataPoint(details.getDataPoint());
    }
}
