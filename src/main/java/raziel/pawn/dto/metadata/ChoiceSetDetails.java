/**
 * 
 */
package raziel.pawn.dto.metadata;


import java.util.List;

import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class ChoiceSetDetails
        extends DomainObjectDTO {

    private List<ChoiceDetails> choices;


    /**
     * Returns the choices.
     * 
     * @return the choices.
     */
    public List<ChoiceDetails> getChoices() {

        return choices;
    }


    /**
     * Sets the choices.
     * 
     * @param choices
     *            the choices.
     */
    public void setChoices(List<ChoiceDetails> choices) {

        this.choices = choices;
    }

    /**
     * Creates a new instance of the {@link ChoiceSetDetails} class.
     */
    public ChoiceSetDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link ChoiceSetDetails} class.
     * 
     * @param id
     *            the id.
     */
    public ChoiceSetDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link ChoiceSetDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> ChoiceSetDetails(T details) {

        super(details);
    }



    /**
     * Creates a new instance of the {@link ChoiceSetDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public ChoiceSetDetails(ChoiceSetDetails details) {

        super(details);
        this.setChoices(details.getChoices());
    }
}
