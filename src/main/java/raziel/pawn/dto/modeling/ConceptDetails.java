/**
 * 
 */
package raziel.pawn.dto.modeling;


import java.util.List;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class ConceptDetails
        extends TermDTO {

    private List<ConceptDetails> descendants;


    /**
     * Returns the descendants.
     * 
     * @return the descendants.
     */
    public List<ConceptDetails> getDescendants() {

        return descendants;
    }


    /**
     * Sets the descendants.
     * 
     * @param descendants
     *            the descendants.
     */
    public void setDescendants(List<ConceptDetails> descendants) {

        this.descendants = descendants;
    }

    /**
     * Creates a new instance of the {@link ConceptDetails} class.
     */
    public ConceptDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link ConceptDetails} class.
     * 
     * @param id
     *            the id.
     */
    public ConceptDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link ConceptDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends TermDTO> ConceptDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link ConceptDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public ConceptDetails(final ConceptDetails details) {

        super(details);

        this.setDescendants(details.getDescendants());
    }

}
