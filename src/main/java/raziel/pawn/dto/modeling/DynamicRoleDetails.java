/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class DynamicRoleDetails
        extends RoleDetails {

    /**
     * Creates a new instance of the {@link DynamicRoleDetails} class.
     */
    public DynamicRoleDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link DynamicRoleDetails} class.
     * 
     * @param id
     */
    public DynamicRoleDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link DynamicRoleDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends RoleDetails> DynamicRoleDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link DynamicRoleDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public DynamicRoleDetails(final DynamicRoleDetails details) {

        super(details);
    }
}
