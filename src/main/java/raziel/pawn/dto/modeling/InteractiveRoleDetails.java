/**
 * 
 */
package raziel.pawn.dto.modeling;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class InteractiveRoleDetails
        extends DynamicRoleDetails {

    /**
     * Creates a new instance of the {@link InteractiveRoleDetails} class.
     */
    public InteractiveRoleDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link InteractiveRoleDetails} class.
     * 
     * @param id
     *            the id.
     */
    public InteractiveRoleDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link InteractiveRoleDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DynamicRoleDetails> InteractiveRoleDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link InteractiveRoleDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public InteractiveRoleDetails(final InteractiveRoleDetails details) {

        super(details);
    }

}
