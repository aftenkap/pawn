/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class AssociationDetails
        extends PermanentRelationshipDetails {

    /**
     * Creates a new instance of the {@link AssociationDetails} class.
     */
    public AssociationDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link AssociationDetails} class.
     * 
     * @param id
     *            the id.
     */
    public AssociationDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link AssociationDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends PermanentRelationshipDetails> AssociationDetails(
            final T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link AssociationDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public AssociationDetails(final AssociationDetails details) {

        super(details);
    }
}
