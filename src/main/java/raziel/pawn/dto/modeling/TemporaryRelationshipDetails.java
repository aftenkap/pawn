/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class TemporaryRelationshipDetails
        extends RelationshipDetails {

    private DurationDetails duration;


    /**
     * Returns the duration.
     * 
     * @return the duration.
     */
    public DurationDetails getDuration() {

        return duration;
    }


    /**
     * Sets the duration.
     * 
     * @param duration
     *            the duration.
     */
    public void setDuration(DurationDetails duration) {

        this.duration = duration;
    }

    /**
     * Creates a new instance of the {@link TemporaryRelationshipDetails} class.
     */
    public TemporaryRelationshipDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link TemporaryRelationshipDetails} class.
     * 
     * @param id
     *            the id.
     */
    public TemporaryRelationshipDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link TemporaryRelationshipDetails} class
     * from the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends RelationshipDetails> TemporaryRelationshipDetails(
            T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link TemporaryRelationshipDetails} class
     * from the provided details.
     * 
     * @param details
     *            the details.
     */
    public TemporaryRelationshipDetails(
            final TemporaryRelationshipDetails details) {

        super(details);

        this.setDuration(details.getDuration());
    }

}
