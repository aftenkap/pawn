/**
 * 
 */
package raziel.pawn.dto.modeling;


import java.util.List;

import raziel.pawn.dto.DomainObjectDTO;
import raziel.pawn.dto.data.DataPointDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class AssertionDetails
        extends DomainObjectDTO {

    private List<DataPointDTO> support;



    /**
     * Returns the support.
     * 
     * @return the support.
     */
    public List<DataPointDTO> getSupport() {

        return support;
    }


    /**
     * Sets the support.
     * 
     * @param support
     *            the support.
     */
    public void setSupport(List<DataPointDTO> support) {

        this.support = support;
    }


    /**
     * Creates a new instance of the {@link AssertionDetails} class.
     */
    public AssertionDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link AssertionDetails} class with the
     * provided id.
     * 
     * @param id
     *            the id.
     */
    public AssertionDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link AssertionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> AssertionDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link AssertionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public AssertionDetails(final AssertionDetails details) {

        super(details);

        this.setSupport(details.getSupport());
    }
}
