/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class ConstantDetails
        extends AssertionDetails {

    /**
     * Creates a new instance of the {@link ConstantDetails} class.
     */
    public ConstantDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link ConstantDetails} class.
     * 
     * @param id
     *            the id.
     */
    public ConstantDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link ConstantDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends AssertionDetails> ConstantDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link ConstantDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public ConstantDetails(final ConstantDetails details) {

        super(details);
    }

}
