package raziel.pawn.dto.modeling;


import java.util.ArrayList;
import java.util.List;

import raziel.pawn.dto.DomainObjectDTO;
import raziel.pawn.dto.data.DataPointDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class TermDTO
        extends DomainObjectDTO {

    private final List<DataPointDTO>     support;
    private final List<AssertionDetails> instances;


    /**
     * Returns the support.
     * 
     * @return the support.
     */
    public List<DataPointDTO> getSupport() {

        return support;
    }


    /**
     * Sets the support.
     * 
     * @param support
     *            the support.
     */
    public void setSupport(List<DataPointDTO> support) {

        if (support != null) {

            this.support.clear();
            this.support.addAll(support);
        }
    }


    /**
     * Returns the instances.
     * 
     * @return the instances.
     */
    public List<AssertionDetails> getInstances() {

        return instances;
    }


    /**
     * Sets the instances.
     * 
     * @param instances
     *            the instances.
     */
    public void setInstances(List<AssertionDetails> instances) {

        if (instances != null) {

            this.instances.clear();
            this.instances.addAll(instances);
        }
    }


    /**
     * Creates a new instance of the {@link TermDTO} class.
     */
    public TermDTO() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link TermDTO} class.
     * 
     * @param id
     *            the id.
     */
    public TermDTO(Integer id) {

        super(id);

        this.instances = new ArrayList<>();
        this.support = new ArrayList<>();
    }


    /**
     * Creates a new instance of the {@link TermDTO} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> TermDTO(T details) {

        super(details);

        this.instances = new ArrayList<>();
        this.support = new ArrayList<>();
    }


    /**
     * Creates a new instance of the {@link TermDTO} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public TermDTO(final TermDTO details) {

        this.instances = new ArrayList<>();
        this.support = new ArrayList<>();

        this.setSupport(details.getSupport());
        this.setInstances(details.getInstances());
    }

}
