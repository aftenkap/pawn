/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class PermanentRelationshipDetails
        extends RelationshipDetails {

    /**
     * Creates a new instance of the {@link PermanentRelationshipDetails} class.
     */
    public PermanentRelationshipDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link PermanentRelationshipDetails} class.
     * 
     * @param id
     *            the id.
     */
    public PermanentRelationshipDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link PermanentRelationshipDetails} class
     * from the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends RelationshipDetails> PermanentRelationshipDetails(
            T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link PermanentRelationshipDetails} class
     * from the provided details.
     * 
     * @param details
     *            the details.
     */
    public PermanentRelationshipDetails(
            final PermanentRelationshipDetails details) {

        super(details);
    }
}
