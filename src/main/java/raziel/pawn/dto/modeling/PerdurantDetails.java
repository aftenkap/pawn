/**
 * 
 */
package raziel.pawn.dto.modeling;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class PerdurantDetails
        extends ConceptDetails {

    /**
     * Creates a new instance of the {@link PerdurantDetails} class.
     */
    public PerdurantDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link PerdurantDetails} class.
     * 
     * @param id
     *            the id.
     */
    public PerdurantDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link PerdurantDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends ConceptDetails> PerdurantDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link PerdurantDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public PerdurantDetails(final PerdurantDetails details) {

        super(details);
    }

}
