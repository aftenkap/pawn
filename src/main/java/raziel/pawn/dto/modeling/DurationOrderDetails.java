/**
 * 
 */
package raziel.pawn.dto.modeling;


import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author peter
 * @version
 * @since
 *
 */
public class DurationOrderDetails
        extends DomainObjectDTO {

    private DurationDetails firstOperand;
    private DurationDetails secondOperand;
    private String          operator;


    /**
     * Returns the firstOperand.
     * 
     * @return the firstOperand.
     */
    public DurationDetails getFirstOperand() {

        return firstOperand;
    }

    /**
     * Sets the firstOperand.
     * 
     * @param firstOperand
     *            the firstOperand.
     */
    public void setFirstOperand(DurationDetails firstOperand) {

        this.firstOperand = firstOperand;
    }

    /**
     * Returns the secondOperand.
     * 
     * @return the secondOperand.
     */
    public DurationDetails getSecondOperand() {

        return secondOperand;
    }

    /**
     * Sets the secondOperand.
     * 
     * @param secondOperand
     *            the secondOperand.
     */
    public void setSecondOperand(DurationDetails secondOperand) {

        this.secondOperand = secondOperand;
    }

    /**
     * Returns the operator.
     * 
     * @return the operator.
     */
    public String getOperator() {

        return operator;
    }

    /**
     * Sets the operator.
     * 
     * @param operator
     *            the operator.
     */
    public void setOperator(String operator) {

        this.operator = operator;
    }

    /**
     * Creates a new instance of the {@link DurationOrderDetails} class.
     */
    public DurationOrderDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link DurationOrderDetails} class.
     * 
     * @param id
     *            the id.
     */
    public DurationOrderDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link DurationOrderDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> DurationOrderDetails(T details) {

        super(details);
    }

    /**
     * Creates a new instance of the {@link DurationOrderDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public DurationOrderDetails(final DurationOrderDetails details) {

        super(details);

        this.setFirstOperand(details.getFirstOperand());
        this.setSecondOperand(details.getSecondOperand());
        this.setOperator(details.getOperator());
    }

}
