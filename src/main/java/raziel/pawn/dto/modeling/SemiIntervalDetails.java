/**
 * 
 */
package raziel.pawn.dto.modeling;


import java.util.Date;

import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class SemiIntervalDetails
        extends DomainObjectDTO {

    private Date time;


    /**
     * Returns the time.
     * 
     * @return the time.
     */
    public Date getTime() {

        return time;
    }


    /**
     * Sets the time.
     * 
     * @param time
     *            the time.
     */
    public void setTime(Date time) {

        this.time = time;
    }

    /**
     * Creates a new instance of the {@link SemiIntervalDetails} class.
     */
    public SemiIntervalDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link SemiIntervalDetails} class with the
     * provided id.
     * 
     * @param id
     *            the id.
     */
    public SemiIntervalDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link SemiIntervalDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> SemiIntervalDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link SemiIntervalDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public SemiIntervalDetails(final SemiIntervalDetails details) {

        super(details);

        this.setTime(details.getTime());
    }
}
