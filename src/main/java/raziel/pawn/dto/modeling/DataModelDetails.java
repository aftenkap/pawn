/**
 * 
 */
package raziel.pawn.dto.modeling;


import java.util.List;

import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class DataModelDetails
        extends DomainObjectDTO {

    private List<TermDTO>              terms;
    private List<AssertionDetails>         assertions;
    private List<DurationOrderDetails>     durationOrders;
    private List<SemiIntervalOrderDetails> semiIntervalOrders;


    /**
     * Returns the terms.
     * 
     * @return the terms.
     */
    public List<TermDTO> getTerms() {

        return terms;
    }


    /**
     * Sets the terms.
     * 
     * @param terms
     *            the terms.
     */
    public void setTerms(List<TermDTO> terms) {

        this.terms = terms;
    }



    /**
     * Returns the assertions.
     * 
     * @return the assertions.
     */
    public List<AssertionDetails> getAssertions() {

        return assertions;
    }


    /**
     * Sets the assertions.
     * 
     * @param assertions
     *            the assertions.
     */
    public void setAssertions(List<AssertionDetails> assertions) {

        this.assertions = assertions;
    }


    /**
     * Returns the durationOrders.
     * 
     * @return the durationOrders.
     */
    public List<DurationOrderDetails> getDurationOrders() {

        return durationOrders;
    }


    /**
     * Sets the durationOrders.
     * 
     * @param durationOrders
     *            the durationOrders.
     */
    public void setDurationOrders(List<DurationOrderDetails> durationOrders) {

        this.durationOrders = durationOrders;
    }


    /**
     * Returns the semiIntervalOrders.
     * 
     * @return the semiIntervalOrders.
     */
    public List<SemiIntervalOrderDetails> getSemiIntervalOrders() {

        return semiIntervalOrders;
    }


    /**
     * Sets the semiIntervalOrders.
     * 
     * @param semiIntervalOrders
     *            the semiIntervalOrders.
     */
    public void setSemiIntervalOrders(
            List<SemiIntervalOrderDetails> semiIntervalOrders) {

        this.semiIntervalOrders = semiIntervalOrders;
    }

    /**
     * Creates a new instance of the {@link DataModelDetails} class.
     */
    public DataModelDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link DataModelDetails} class.
     * 
     * @param id
     *            the id.
     */
    public DataModelDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link DataModelDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> DataModelDetails(T details) {

        super(details);
    }

    /**
     * Creates a new instance of the {@link DataModelDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public DataModelDetails(final DataModelDetails details) {

        super(details);

        this.setTerms(details.getTerms());
        this.setAssertions(details.getAssertions());
        this.setDurationOrders(details.getDurationOrders());
        this.setSemiIntervalOrders(details.getSemiIntervalOrders());
    }
}
