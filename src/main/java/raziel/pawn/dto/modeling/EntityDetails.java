/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class EntityDetails
        extends AssertionDetails {

    private DurationDetails duration;


    /**
     * Returns the duration.
     * 
     * @return the duration.
     */
    public DurationDetails getDuration() {

        return duration;
    }


    /**
     * Sets the duration.
     * 
     * @param duration
     *            the duration.
     */
    public void setDuration(DurationDetails duration) {

        this.duration = duration;
    }

    /**
     * Creates a new instance of the {@link EntityDetails} class.
     */
    public EntityDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link EntityDetails} class.
     * 
     * @param id
     *            the id.
     */
    public EntityDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link EntityDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends AssertionDetails> EntityDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link EntityDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public EntityDetails(final EntityDetails details) {

        super(details);

        this.setDuration(details.getDuration());
    }

}
