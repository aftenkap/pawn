/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class StaticRoleDetails
        extends RoleDetails {

    /**
     * Creates a new instance of the {@link StaticRoleDetails} class.
     */
    public StaticRoleDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link StaticRoleDetails} class.
     * 
     * @param id
     *            the id.
     */
    public StaticRoleDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link StaticRoleDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends RoleDetails> StaticRoleDetails(final T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link StaticRoleDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */

    public StaticRoleDetails(final StaticRoleDetails details) {

        super(details);
    }

}
