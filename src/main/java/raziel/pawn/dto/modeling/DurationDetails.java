/**
 * 
 */
package raziel.pawn.dto.modeling;


import java.util.Date;

import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author peter
 * @version
 * @since
 *
 */
public class DurationDetails
        extends DomainObjectDTO {

    private SemiIntervalDetails start;
    private SemiIntervalDetails end;
    private Date                duration;


    /**
     * Returns the start.
     * 
     * @return the start.
     */
    public SemiIntervalDetails getStart() {

        return start;
    }


    /**
     * Sets the start.
     * 
     * @param start
     *            the start.
     */
    public void setStart(SemiIntervalDetails start) {

        this.start = start;
    }


    /**
     * Returns the end.
     * 
     * @return the end.
     */
    public SemiIntervalDetails getEnd() {

        return end;
    }

    /**
     * Sets the end.
     * 
     * @param end
     *            the end.
     */
    public void setEnd(SemiIntervalDetails end) {

        this.end = end;
    }


    /**
     * Returns the duration.
     * 
     * @return the duration.
     */
    public Date getDuration() {

        return duration;
    }

    /**
     * Sets the duration.
     * 
     * @param duration
     *            the duration.
     */
    public void setDuration(Date duration) {

        this.duration = duration;
    }


    /**
     * Creates a new instance of the {@link DurationDetails} class.
     */
    public DurationDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link DurationDetails} class with the
     * provided id.
     * 
     * @param id
     *            the id.
     */
    public DurationDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link DurationDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> DurationDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link DurationDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public DurationDetails(final DurationDetails details) {

        super(details);

        this.setStart(details.getStart());
        this.setEnd(details.getEnd());
        this.setDuration(details.getDuration());
    }

}
