/**
 * 
 */
package raziel.pawn.dto.modeling;


import raziel.pawn.dto.DomainObjectDTO;


/**
 * @author peter
 * @version
 * @since
 *
 */
public class SemiIntervalOrderDetails
        extends DomainObjectDTO {

    private SemiIntervalDetails firstOperand;
    private SemiIntervalDetails secondOperand;
    private String              operator;


    /**
     * Returns the firstOperand.
     * 
     * @return the firstOperand.
     */
    public SemiIntervalDetails getFirstOperand() {

        return firstOperand;
    }

    /**
     * Sets the firstOperand.
     * 
     * @param firstOperand
     *            the firstOperand.
     */
    public void setFirstOperand(SemiIntervalDetails firstOperand) {

        this.firstOperand = firstOperand;
    }

    /**
     * Returns the secondOperand.
     * 
     * @return the secondOperand.
     */
    public SemiIntervalDetails getSecondOperand() {

        return secondOperand;
    }

    /**
     * Sets the secondOperand.
     * 
     * @param secondOperand
     *            the secondOperand.
     */
    public void setSecondOperand(SemiIntervalDetails secondOperand) {

        this.secondOperand = secondOperand;
    }

    /**
     * Returns the operator.
     * 
     * @return the operator.
     */
    public String getOperator() {

        return operator;
    }

    /**
     * Sets the operator.
     * 
     * @param operator
     *            the operator.
     */
    public void setOperator(String operator) {

        this.operator = operator;
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrderDetails} class.
     */
    public SemiIntervalOrderDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrderDetails} class.
     * 
     * @param id
     *            the id.
     */
    public SemiIntervalOrderDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link SemiIntervalOrderDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DomainObjectDTO> SemiIntervalOrderDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link SemiIntervalOrderDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public SemiIntervalOrderDetails(final SemiIntervalOrderDetails details) {

        super(details);

        this.setFirstOperand(details.getFirstOperand());
        this.setSecondOperand(details.getSecondOperand());
        this.setOperator(details.getOperator());
    }
}
