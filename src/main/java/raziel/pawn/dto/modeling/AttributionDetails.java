/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class AttributionDetails
        extends TemporaryRelationshipDetails {


    /**
     * Creates a new instance of the {@link AttributionDetails} class.
     */
    public AttributionDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link AttributionDetails} class.
     * 
     * @param id
     *            the id.
     */
    public AttributionDetails(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link AttributionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends TemporaryRelationshipDetails> AttributionDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link AttributionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public AttributionDetails(final AttributionDetails details) {

        super(details);
    }

}
