/**
 * 
 */
package raziel.pawn.dto.modeling;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class AssociativeRoleDetails
        extends StaticRoleDetails {

    /**
     * Creates a new instance of the {@link AssociativeRoleDetails} class.
     */
    public AssociativeRoleDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link AssociativeRoleDetails} class.
     * 
     * @param id
     *            the id.
     */
    public AssociativeRoleDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link AssociativeRoleDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends StaticRoleDetails> AssociativeRoleDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link AssociativeRoleDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public AssociativeRoleDetails(final AssociativeRoleDetails details) {

        super(details);
    }

}
