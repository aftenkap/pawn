/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class EndurantDetails
        extends ConceptDetails {

    /**
     * Creates a new instance of the {@link EndurantDetails} class.
     */
    public EndurantDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link EndurantDetails} class.
     * 
     * @param id
     *            the id.
     */
    public EndurantDetails(Integer id) {

        super(id);
    }

    /**
     * Creates a new instance of the {@link EndurantDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends ConceptDetails> EndurantDetails(T details) {

        super(details);
    }

    /**
     * Creates a new instance of the {@link EndurantDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public EndurantDetails(final EndurantDetails details) {

        super(details);

    }
}
