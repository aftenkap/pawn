/**
 * 
 */
package raziel.pawn.dto.modeling;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class InteractionDetails
        extends TemporaryRelationshipDetails {


    /**
     * Creates a new instance of the {@link InteractionDetails} class.
     */
    public InteractionDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link InteractionDetails} class.
     * 
     * @param id
     *            the id.
     */
    public InteractionDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link InteractionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends TemporaryRelationshipDetails> InteractionDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link InteractionDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public InteractionDetails(final InteractionDetails details) {

        super(details);
    }

}
