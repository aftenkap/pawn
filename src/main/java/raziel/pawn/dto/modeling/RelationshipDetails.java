/**
 * 
 */
package raziel.pawn.dto.modeling;


import java.util.List;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class RelationshipDetails
        extends AssertionDetails {

    private List<AssertionDetails> domainAssertions;
    private List<AssertionDetails> rangeAssertions;


    /**
     * Returns the domainAssertions.
     * 
     * @return the domainAssertions.
     */
    public List<AssertionDetails> getDomainAssertions() {

        return domainAssertions;
    }

    /**
     * Sets the domainAssertions.
     * 
     * @param domainAssertions
     *            the domainAssertions.
     */
    public void setDomainAssertions(List<AssertionDetails> domainAssertions) {

        this.domainAssertions = domainAssertions;
    }


    /**
     * Returns the rangeAssertions.
     * 
     * @return the rangeAssertions.
     */
    public List<AssertionDetails> getRangeAssertions() {

        return rangeAssertions;
    }

    /**
     * Sets the rangeAssertions.
     * 
     * @param rangeAssertions
     *            the rangeAssertions.
     */
    public void setRangeAssertions(List<AssertionDetails> rangeAssertions) {

        this.rangeAssertions = rangeAssertions;
    }

    /**
     * Creates a new instance of the {@link RelationshipDetails} class.
     */
    public RelationshipDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link RelationshipDetails} class.
     * 
     * @param id
     *            the id.
     */
    public RelationshipDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link RelationshipDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends AssertionDetails> RelationshipDetails(T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link RelationshipDetails} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public RelationshipDetails(final RelationshipDetails details) {

        super(details);

        this.setDomainAssertions(details.getDomainAssertions());
        this.setRangeAssertions(details.getRangeAssertions());
    }

}
