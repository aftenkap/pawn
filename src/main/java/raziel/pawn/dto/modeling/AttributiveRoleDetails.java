/**
 * 
 */
package raziel.pawn.dto.modeling;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class AttributiveRoleDetails
        extends DynamicRoleDetails {

    /**
     * Creates a new instance of the {@link AttributiveRoleDetails} class.
     */
    public AttributiveRoleDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link AttributiveRoleDetails} class.
     * 
     * @param id
     *            the id.
     */
    public AttributiveRoleDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link AttributiveRoleDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public <T extends DynamicRoleDetails> AttributiveRoleDetails(
            final T details) {

        super(details);
    }

    /**
     * Creates a new instance of the {@link AttributiveRoleDetails} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public AttributiveRoleDetails(final AttributiveRoleDetails details) {

        super(details);
    }

}
