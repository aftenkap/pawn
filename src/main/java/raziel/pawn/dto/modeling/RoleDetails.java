/**
 * 
 */
package raziel.pawn.dto.modeling;


import java.util.List;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class RoleDetails
        extends TermDTO {

    private List<ConceptDetails> domainConcepts;
    private List<ConceptDetails> rangeConcepts;
    private List<RoleDetails>    descendants;

    /**
     * Returns the domainConcepts.
     * 
     * @return the domainConcepts.
     */
    public List<ConceptDetails> getDomainConcepts() {

        return domainConcepts;
    }

    /**
     * Sets the domainConcepts.
     * 
     * @param domainConcepts
     *            the domainConcepts.
     */
    public void setDomainConcepts(List<ConceptDetails> domainConcepts) {

        this.domainConcepts = domainConcepts;
    }


    /**
     * Returns the descendants.
     * 
     * @return the descendants.
     */
    public List<RoleDetails> getDescendants() {

        return descendants;
    }

    /**
     * Sets the descendants.
     * 
     * @param descendants
     *            the descendants.
     */
    public void setDescendants(List<RoleDetails> descendants) {

        this.descendants = descendants;
    }


    /**
     * Returns the rangeConcepts.
     * 
     * @return the rangeConcepts.
     */
    public List<ConceptDetails> getRangeConcepts() {

        return rangeConcepts;
    }

    /**
     * Sets the rangeConcepts.
     * 
     * @param rangeConcepts
     *            the rangeConcepts.
     */
    public void setRangeConcepts(List<ConceptDetails> rangeConcepts) {

        this.rangeConcepts = rangeConcepts;
    }

    /**
     * Creates a new instance of the {@link RoleDetails} class.
     */
    public RoleDetails() {

        this((Integer) null);
    }

    /**
     * Creates a new instance of the {@link RoleDetails} class.
     * 
     * @param id
     *            the id.
     */
    public RoleDetails(Integer id) {

        super(id);
    }


    /**
     * Creates a new instance of the {@link RoleDetails} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public <T extends TermDTO> RoleDetails(final T details) {

        super(details);
    }


    /**
     * Creates a new instance of the {@link RoleDetails} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public RoleDetails(final RoleDetails details) {

        super(details);

        this.setDomainConcepts(details.getDomainConcepts());
        this.setRangeConcepts(details.getRangeConcepts());
        this.setDescendants(details.getDescendants());
    }
}
