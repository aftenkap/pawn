/**
 * 
 */
package raziel.pawn.dto;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class RequestFailedException
        extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 8337581673761556715L;

    /**
     * Creates a new instance of the {@link RequestFailedException} class.
     */
    public RequestFailedException() {

        super();
    }

    /**
     * Creates a new instance of the {@link RequestFailedException} class with
     * the provided message.
     * 
     * @param message
     *            the message.
     */
    public RequestFailedException(String message) {

        super(message);
    }

    /**
     * Creates a new instance of the {@link RequestFailedException} class with
     * the provided cause.
     * 
     * @param cause
     *            the cause.
     */
    public RequestFailedException(Throwable cause) {

        super(cause);
    }

    /**
     * Creates a new instance of the {@link RequestFailedException} class with
     * the provided message and cause.
     * 
     * @param message
     *            the message.
     * @param cause
     *            the cause.
     */
    public RequestFailedException(String message, Throwable cause) {

        super(message, cause);
    }

    /**
     * Creates a new instance of the {@link RequestFailedException} class with
     * the provided message and cause. Furthermore, suppression of the exception
     * can be enabled and a writable stack trace created through flags.
     * 
     * @param message
     *            the message.
     * @param cause
     *            the cause.
     * @param enableSuppression
     *            whether or not to enable suppression of the event.
     * @param writableStackTrace
     *            whether or not the stack trace should be writable.
     */
    public RequestFailedException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {

        super(message, cause, enableSuppression, writableStackTrace);
    }
}
