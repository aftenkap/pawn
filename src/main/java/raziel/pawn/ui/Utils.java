package raziel.pawn.ui;


import java.util.Optional;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Dialog;
import javafx.stage.Window;

import org.controlsfx.dialog.ExceptionDialog;

import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.ui.controls.dialog.DomainObjectDialog;


/**
 * Utility class.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.2
 */
public class Utils {


    /**
     * Returns the {@link Window} of a {@link Node}.
     * 
     * @param node
     *            the {@link Node}.
     * @return the {@link Window} of the {@link Node}.
     */
    public static Window windowOf(final Node node) {

        if (node != null && node.getScene() != null) {

            return node.getScene().getWindow();
        }
        return null;
    }



    /**
     * Shows a confirmation {@link Alert} for the deletion of a
     * {@link IDomainObject DomainObject}.
     * 
     * @param objectToDelete
     *            the object to delete.
     * @param owner
     *            the owner of the {@link Alert}.
     * @return {@code true}, if deletion should continue; {@code false}
     *         otherwise.
     */
    public static boolean confirmDeletion(final IDomainObject objectToDelete,
            final Node owner) {

        return Utils.confirmDeletion(objectToDelete, Utils.windowOf(owner));
    }

    /**
     * Shows a confirmation {@link Alert} for the deletion of a
     * {@link IDomainObject DomainObject}.
     * 
     * @param objectToDelete
     *            the object to delete.
     * @param owner
     *            the owner of the {@link Alert}.
     * @return {@code true}, if deletion should continue; {@code false}
     *         otherwise.
     */
    public static boolean confirmDeletion(final IDomainObject objectToDelete,
            final Window owner) {

        if (objectToDelete != null) {
            Alert alert = new Alert(AlertType.CONFIRMATION,
                    "Are you sure you want to delete  "
                            + objectToDelete.getName() + "?");
            alert.initOwner(owner);

            return alert.showAndWait()
                    .filter(result -> result == ButtonType.OK).isPresent();
        }

        return false;
    }


    /**
     * Shows a confirmation {@link Alert} for deletion of a property of a
     * {@link IDomainObject DomainObject}.
     * 
     * @param propertyToDelete
     *            the property to delete.
     * @param propertyOwner
     *            the owner of the property.
     * @param owner
     *            the owner of the {@link Alert}.
     * @return {@code true}, if deletion should continue; {@code false}
     *         otherwise.
     */
    public static boolean confirmDeletionOfProperty(
            final IDomainObject propertyToDelete,
            final IDomainObject propertyOwner, final Node owner) {

        return Utils.confirmDeletionOfProperty(propertyToDelete, propertyOwner,
                Utils.windowOf(owner));
    }

    /**
     * Shows a confirmation {@link Alert} for the deletion of a property of a
     * {@link IDomainObject DomainObject}.
     * 
     * @param propertyToDelete
     *            the property to delete.
     * @param propertyOwner
     *            the owner of the property.
     * @param owner
     *            the owner of the {@link Alert}.
     * @return {@code true}, if deletion should continue; {@code false}
     *         otherwise.
     */
    public static boolean confirmDeletionOfProperty(
            final IDomainObject propertyToDelete,
            final IDomainObject propertyOwner, final Window owner) {

        if (propertyToDelete != null && propertyOwner != null) {
            Alert alert = new Alert(AlertType.CONFIRMATION,
                    "Are you sure you want to delete "
                            + propertyToDelete.getName() + " from "
                            + propertyOwner + "?");
            alert.initOwner(owner);

            return alert.showAndWait()
                    .filter(result -> result == ButtonType.OK).isPresent();
        }

        return false;
    }


    /**
     * Shows a {@link Dialog} for renaming the provided {@link IDomainObject
     * DomainObject}.
     * 
     * @param objectToRename
     *            the object to delete.
     * @param owner
     *            the owner of the {@link Dialog}.
     * @return {@code true}, if deletion should continue; {@code false}
     *         otherwise.
     */
    public static <T extends IDomainObject> Optional<T> renameDomainObject(
            final T objectToRename, final Node owner) {

        return Utils.renameDomainObject(objectToRename, Utils.windowOf(owner));
    }

    /**
     * Shows a {@link Dialog} for renaming the provided {@link IDomainObject
     * DomainObject}.
     * 
     * @param objectToRename
     *            the object to delete.
     * @param owner
     *            the owner of the {@link Dialog}.
     * @return {@code true}, if deletion should continue; {@code false}
     *         otherwise.
     */
    public static <T extends IDomainObject> Optional<T> renameDomainObject(
            final T objectToRename, final Window owner) {

        if (objectToRename != null) {

            return new DomainObjectDialog<>(owner, "Rename", objectToRename)
                    .showAndWait();
        }

        return Optional.empty();
    }


    /**
     * Shows an {@link ExceptionDialog} for the provided owner with the provided
     * content.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the dialog title.
     * @param content
     *            the dialog content.
     * @param exception
     *            the exception to display.
     */
    public static void showExceptionDialog(final Node owner,
            final String title, final String content, final Throwable exception) {

        Utils.showExceptionDialog(owner == null ? null : owner.getScene()
                .getWindow(), title, content, exception);
    }


    /**
     * Shows an {@link ExceptionDialog} for the provided owner with the provided
     * content.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the dialog title.
     * @param content
     *            the dialog content.
     * @param exception
     *            the exception to display.
     */
    public static void showExceptionDialog(final Window owner,
            final String title, final String content, final Throwable exception) {

        ExceptionDialog edlg = new ExceptionDialog(exception);

        edlg.initOwner(owner);
        edlg.setTitle(title);
        edlg.setContentText(content);

        edlg.showAndWait();
    }

}
