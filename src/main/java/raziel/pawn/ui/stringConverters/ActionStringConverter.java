package raziel.pawn.ui.stringConverters;
//package raziel.pawn.ui.stringConverters;
//
// FIXME: fix
//
//import raziel.pawn.core.domain.modeling.assertions.Action;
//
//
///**
// * The {@link ActionStringConverter} class provides a string converter for
// * {@link Action Actions}.
// * 
// * @author Peter J. Radics
// * @version 0.1
// */
//public class ActionStringConverter
//        extends DomainObjectStringConverter<Action> {
//
//
//    /**
//     * Creates a new instance of the {@link ActionStringConverter} class with
//     * the default configuration.
//     */
//    public ActionStringConverter() {
//
//        super();
//    }
//
//    /**
//     * Creates a new instance of the {@link ActionStringConverter} class with
//     * the provided configuration.
//     * 
//     * @param configuration
//     *            the configuration to use.
//     */
//    public ActionStringConverter(
//            DomainObjectStringConverterConfiguration configuration) {
//
//        super(configuration);
//    }
//
//
//    @Override
//    public Action fromString(String string) {
//
//        Action action = null;
//        switch (this.getConfiguration().getOutput()) {
//            case COMBINED:
//                action = new Action(string);
//                break;
//            case ID:
//                action = new Action(string);
//                break;
//            case NAME:
//
//                action = new Action(string);
//                break;
//            default:
//                break;
//        }
//
//        return action;
//    }
//
//
//
//    @Override
//    public boolean equals(Object obj) {
//
//        if (obj != null && obj instanceof ActionStringConverter) {
//
//            ActionStringConverter other = (ActionStringConverter) obj;
//
//            boolean sameConfiguration = this.getConfiguration().equals(
//                    other.getConfiguration());
//
//            return sameConfiguration;
//
//        }
//        return false;
//    }
//
//    @Override
//    public int hashCode() {
//
//        int hash = 7;
//
//        return hash *= this.getConfiguration().hashCode();
//    }
//}
