package raziel.pawn.ui.stringConverters;


import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.core.domain.metadata.IAnswer;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.IQuestion;


/**
 * The {@link AnswerStringConverter} class provides a string converter for
 * {@link DomainObject DomainObjects}.
 *
 * @author Peter J. Radics
 * @version 0.1
 * @param <T>
 *            the type of the StringConverter (subclass of {@link DomainObject}
 *            ).
 */
public class AnswerStringConverter<T extends IAnswer>
        extends DomainObjectStringConverter<T> {


    /**
     * Configuration using the name of the {@link IQuestion}.
     */
    public static final DomainObjectStringConverterConfiguration QUESTION    = new DomainObjectStringConverterConfiguration(
                                                                                     "QUESTION");
    /**
     * Configuration using the name of the {@link IParticipant}.
     */
    public static final DomainObjectStringConverterConfiguration PARTICIPANT = new DomainObjectStringConverterConfiguration(
                                                                                     "PARTICIPANT");

    /**
     * Creates a new instance of the {@link AnswerStringConverter} class with
     * the default configuration.
     */
    public AnswerStringConverter() {

        this(DomainObjectStringConverterConfiguration.DEFAULT);
    }

    /**
     * Creates a new instance of the {@link AnswerStringConverter} class with
     * the provided configuration.
     *
     * @param configuration
     *            the configuration to use.
     */
    public AnswerStringConverter(
            DomainObjectStringConverterConfiguration configuration) {

        super(configuration);
    }



    @Override
    public String toString(T item) {

        if (item == null) {

            return "";
        }
        else {

            switch (this.getConfiguration().getOutput()) {

                case "QUESTION":

                    return item.getQuestion().getName();

                case "PARTICIPANT":

                    return item.getParticipant().getName();

                case "COMBINED":

                    return item.getId() + ": " + item.getName();
                case "ID":

                    return "" + item.getId();
                default:
                case "NAME":

                    return item.getName();
            }
        }
    }

    @Override
    public boolean equals(Object obj) {

        if (obj != null && obj instanceof AnswerStringConverter<?>) {

            AnswerStringConverter<?> other = (AnswerStringConverter<?>) obj;

            boolean sameConfiguration = this.getConfiguration().equals(
                    other.getConfiguration());

            return sameConfiguration;

        }
        return false;
    }


    @Override
    public T fromString(String stringRepresentation) {

        throw new UnsupportedOperationException(
                "Cannot create object from string!");
    }
}
