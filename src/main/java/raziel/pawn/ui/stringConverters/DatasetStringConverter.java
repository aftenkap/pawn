package raziel.pawn.ui.stringConverters;


import raziel.pawn.core.domain.modeling.DataModel;


/**
 * The {@link DatasetStringConverter} class provides a string converter for
 * {@link DataModel Datasets}.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
public class DatasetStringConverter
        extends DomainObjectStringConverter<DataModel> {

    /**
     * Creates a new instance of the {@link DatasetStringConverter} class with
     * the default configuration.
     */
    public DatasetStringConverter() {

        super();
    }

    /**
     * Creates a new instance of the {@link DatasetStringConverter} class with
     * the provided configuration.
     * 
     * @param configuration
     *            the configuration to use.
     */
    public DatasetStringConverter(
            DomainObjectStringConverterConfiguration configuration) {

        super(configuration);
    }


    @Override
    public DataModel fromString(String string) {

        DataModel action = null;
        switch (this.getConfiguration().getOutput()) {
            case "COMBINED":
                action = new DataModel(string);
                break;
            case "ID":
                action = new DataModel(string);
                break;
            case "NAME":
                action = new DataModel(string);
                break;
            default:
                break;
        }

        return action;
    }



    @Override
    public boolean equals(Object obj) {

        if (obj != null && obj instanceof DatasetStringConverter) {

            DatasetStringConverter other = (DatasetStringConverter) obj;

            boolean sameConfiguration = this.getConfiguration().equals(
                    other.getConfiguration());

            return sameConfiguration;

        }
        return false;
    }

    @Override
    public int hashCode() {

        int hash = 7;

        return hash *= this.getConfiguration().hashCode();
    }
}
