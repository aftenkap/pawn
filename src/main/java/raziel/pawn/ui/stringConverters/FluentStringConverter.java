package raziel.pawn.ui.stringConverters;
//package raziel.pawn.ui.stringConverters;
//// FIXME: fix
//
//import raziel.pawn.core.domain.modeling.assertions.Fluent;
//
//
///**
// * The {@link FluentStringConverter} class provides a string converter for
// * {@link Fluent Fluents}.
// * 
// * @author Peter J. Radics
// * @version 0.1
// */
//public class FluentStringConverter
//        extends DomainObjectStringConverter<Fluent> {
//
//
//    /**
//     * Creates a new instance of the {@link FluentStringConverter} class with
//     * the default configuration.
//     */
//    public FluentStringConverter() {
//
//        super();
//    }
//
//    /**
//     * Creates a new instance of the {@link FluentStringConverter} class with
//     * the provided configuration.
//     * 
//     * @param configuration
//     *            the configuration to use.
//     */
//    public FluentStringConverter(
//            DomainObjectStringConverterConfiguration configuration) {
//
//        super(configuration);
//    }
//
//
//    @Override
//    public Fluent fromString(String string) {
//
//        throw new UnsupportedOperationException(
//                "Creating instance from string not supported!");
//    }
//
//
//
//    @Override
//    public boolean equals(Object obj) {
//
//        if (obj != null && obj instanceof FluentStringConverter) {
//
//            FluentStringConverter other = (FluentStringConverter) obj;
//
//            boolean sameConfiguration = this.getConfiguration().equals(
//                    other.getConfiguration());
//
//            return sameConfiguration;
//
//        }
//        return false;
//    }
//
//    @Override
//    public int hashCode() {
//
//        int hash = 7;
//
//        return hash *= this.getConfiguration().hashCode();
//    }
//}
