package raziel.pawn.ui.stringConverters;


import raziel.pawn.core.domain.Project;


/**
 * The {@link ProjectStringConverter} class provides a string converter for
 * {@link Project Projects}.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
public class ProjectStringConverter
        extends DomainObjectStringConverter<Project> {

    /**
     * Creates a new instance of the {@link ProjectStringConverter} class with
     * the default configuration.
     */
    public ProjectStringConverter() {

        super();
    }

    /**
     * Creates a new instance of the {@link ProjectStringConverter} class with
     * the provided configuration.
     * 
     * @param configuration
     *            the configuration to use.
     */
    public ProjectStringConverter(
            DomainObjectStringConverterConfiguration configuration) {

        super(configuration);
    }


    @Override
    public Project fromString(String string) {

        Project action = null;
        switch (this.getConfiguration().getOutput()) {
            case "COMBINED":
                action = new Project(string);
                break;
            case "ID":
                action = new Project(string);
                break;
            case "NAME":

                action = new Project(string);
                break;
            default:
                break;
        }

        return action;
    }



    @Override
    public boolean equals(Object obj) {

        if (obj != null && obj instanceof ProjectStringConverter) {

            ProjectStringConverter other = (ProjectStringConverter) obj;

            boolean sameConfiguration = this.getConfiguration().equals(
                    other.getConfiguration());

            return sameConfiguration;

        }
        return false;
    }

    @Override
    public int hashCode() {

        int hash = 7;

        return hash *= this.getConfiguration().hashCode();
    }
}
