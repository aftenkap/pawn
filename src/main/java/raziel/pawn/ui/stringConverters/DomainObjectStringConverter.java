package raziel.pawn.ui.stringConverters;


import org.jutility.javafx.stringconverter.IConfigurableStringConverter;
import org.jutility.javafx.stringconverter.IStringConverterConfiguration;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.util.StringConverter;
import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.core.domain.IDomainObject;


/**
 * The {@link DomainObjectStringConverter} class provides a string converter for
 * {@link DomainObject DomainObjects}.
 *
 * @author Peter J. Radics
 * @version 0.1
 * @param <T>
 *            the type of the StringConverter (subclass of {@link DomainObject}
 *            ).
 */
public class DomainObjectStringConverter<T extends IDomainObject>
        extends StringConverter<T>
        implements IConfigurableStringConverter<T> {

    private final ObjectProperty<DomainObjectStringConverterConfiguration> configuration;



    /**
     * Creates a new instance of the {@link DomainObjectStringConverter} class
     * with the default configuration.
     */
    public DomainObjectStringConverter() {

        this(DomainObjectStringConverterConfiguration.DEFAULT);
    }

    /**
     * Creates a new instance of the {@link DomainObjectStringConverter} class
     * with the provided configuration.
     *
     * @param configuration
     *            the configuration to use.
     */
    public DomainObjectStringConverter(
            DomainObjectStringConverterConfiguration configuration) {

        this.configuration = new SimpleObjectProperty<>(configuration);
    }

    @Override
    public ObjectProperty<DomainObjectStringConverterConfiguration> configuration() {

        return this.configuration;
    }

    @Override
    public void configure(IStringConverterConfiguration configuration) {

        if (configuration instanceof DomainObjectStringConverterConfiguration) {

            this.configuration
                    .set((DomainObjectStringConverterConfiguration) configuration);
        }
    }

    @Override
    public DomainObjectStringConverterConfiguration getConfiguration() {

        return this.configuration.get();
    }



    @Override
    public String toString(T item) {

        if (item == null) {
            return "";
        }
        else {
            switch (this.configuration.get().getOutput()) {

                case "COMBINED":

                    return item.getId() + ": " + item.getName();
                case "ID":

                    return "" + item.getId();
                default:
                case "NAME":

                    return item.getName();
            }
        }
    }

    @Override
    public boolean equals(Object obj) {

        if (obj != null && obj instanceof DomainObjectStringConverter<?>) {

            DomainObjectStringConverter<?> other = (DomainObjectStringConverter<?>) obj;

            boolean sameConfiguration = this.getConfiguration().equals(
                    other.getConfiguration());

            return sameConfiguration;

        }
        return false;
    }

    @Override
    public int hashCode() {

        int hash = 7;

        return hash *= this.configuration.hashCode();
    }

    @Override
    public T fromString(String stringRepresentation) {

        throw new UnsupportedOperationException(
                "Cannot create object from string!");
    }
}
