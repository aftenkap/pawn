package raziel.pawn.ui.stringConverters;


import org.jutility.javafx.stringconverter.IConfigurableStringConverter;
import org.jutility.javafx.stringconverter.IStringConverterConfiguration;

import raziel.pawn.core.domain.DomainObject;



/**
 * The {@link DomainObjectStringConverterConfiguration} provides configuration
 * options for {@link IConfigurableStringConverter StringConverters} that
 * support {@link DomainObject DatabaseObjects}.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
public class DomainObjectStringConverterConfiguration
        implements IStringConverterConfiguration {


    /**
     * The default configuration.
     */
    public static final DomainObjectStringConverterConfiguration DEFAULT  = new DomainObjectStringConverterConfiguration();
    /**
     * Configuration using the ID of the {@link DomainObject}.
     */
    public static final DomainObjectStringConverterConfiguration ID       = new DomainObjectStringConverterConfiguration(
                                                                                  "ID");
    /**
     * Configuration using the name of the {@link DomainObject}.
     */
    public static final DomainObjectStringConverterConfiguration NAME     = new DomainObjectStringConverterConfiguration(
                                                                                  "NAME");
    /**
     * Configuration using the a combination of name and ID of the
     * {@link DomainObject}.
     */
    public static final DomainObjectStringConverterConfiguration COMBINED = new DomainObjectStringConverterConfiguration(
                                                                                  "COMBINED");

    private final String                                         output;



    /**
     * Returns the output field.
     * 
     * @return the output field.
     */
    public String getOutput() {

        return output;
    }

    /**
     * Creates a new instance of the
     * {@link DomainObjectStringConverterConfiguration} class using the default
     * output field.
     */
    public DomainObjectStringConverterConfiguration() {

        this("NAME");
    }

    /**
     * Creates a new instance of the
     * {@link DomainObjectStringConverterConfiguration} class using the provided
     * output field.
     * 
     * @param output
     *            the output field.
     */
    public DomainObjectStringConverterConfiguration(String output) {

        this.output = output;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj != null
                && obj instanceof DomainObjectStringConverterConfiguration) {

            DomainObjectStringConverterConfiguration other = (DomainObjectStringConverterConfiguration) obj;

            return this.getOutput() == other.getOutput();
        }
        return false;
    }

    @Override
    public int hashCode() {

        return this.getOutput().hashCode();
    }



}
