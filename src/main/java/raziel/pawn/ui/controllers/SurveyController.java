package raziel.pawn.ui.controllers;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javafx.scene.Node;
import javafx.stage.Window;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import raziel.pawn.core.domain.metadata.Answer;
import raziel.pawn.core.domain.metadata.Choice;
import raziel.pawn.core.domain.metadata.ChoiceQuestion;
import raziel.pawn.core.domain.metadata.ChoiceSet;
import raziel.pawn.core.domain.metadata.IAnswer;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.core.domain.metadata.Participant;
import raziel.pawn.core.domain.metadata.RankingQuestion;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.core.domain.metadata.TextQuestion;
import raziel.pawn.core.services.SurveyService;
import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.metadata.AnswerDetails;
import raziel.pawn.dto.metadata.ChoiceDetails;
import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.ChoiceSetDetails;
import raziel.pawn.dto.metadata.ParticipantDTO;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;
import raziel.pawn.dto.metadata.SurveyDetails;
import raziel.pawn.dto.metadata.TextQuestionDetails;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.controls.dialog.ActionDialog;
import raziel.pawn.ui.controls.dialog.AnswerDialog;
import raziel.pawn.ui.controls.dialog.DomainObjectDialog;
import raziel.pawn.ui.controls.dialog.QuestionDialog;


/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
@Controller
public class SurveyController {

    private static Logger       LOG = LoggerFactory
                                            .getLogger(SurveyController.class);

    private final SurveyService surveyService;



    /**
     * Creates a new instance of the {@link SurveyController} class.
     * 
     * @param surveyService
     *            the survey service.
     */
    @Autowired
    public SurveyController(SurveyService surveyService) {

        this.surveyService = surveyService;
    }



    /**
     * Creates a new survey.
     * 
     * @param owner
     *            the calling {@link Node}.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> createSurvey(final Node owner) {

        return this.createSurvey(Utils.windowOf(owner));
    }

    /**
     * Creates a new survey.
     * 
     * @param owner
     *            the calling {@link Window}.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> createSurvey(Window owner) {

        DomainObjectDialog<Survey> dlg = new DomainObjectDialog<>(owner
                .getScene().getWindow(), "Create Survey", new Survey(
                "New Survey"));

        Optional<Survey> result = dlg.showAndWait();

        if (result.isPresent()) {

            Survey survey = result.get();
            Survey createdSurvey = null;

            try {

                createdSurvey = this.createSurvey(survey);
            }
            catch (RequestFailedException | RequestDeniedException e) {

                Utils.showExceptionDialog(owner, "Creating Survey Failed!",
                        "An exception occurred when attempting to create Survey "
                                + survey + "!", e);
            }

            return Optional.ofNullable(createdSurvey);
        }

        return Optional.empty();
    }

    private Survey createSurvey(Survey survey)
            throws RequestFailedException, RequestDeniedException {

        SurveyDetails result = this.surveyService.createSurvey(survey
                .toDTO());

        return Survey.fromDetails(result);
    }


    /**
     * Retrieves all {@link Survey Surveys}.
     * 
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<List<Survey>> retrieveAllSurveys(final Node owner) {

        return this.retrieveAllSurveys(Utils.windowOf(owner));
    }

    /**
     * Retrieves all {@link Survey Surveys}.
     * 
     * @param owner
     *            the calling {@link Window}.
     * @return the {@link Optional}{@code <}{@link List}{@code <}{@link Survey}
     *         {@code >>} containing the result of the action.
     */
    public Optional<List<Survey>> retrieveAllSurveys(final Window owner) {

        List<Survey> retrievedSurveys = null;

        try {

            retrievedSurveys = this.retrieveAllSurveys();
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Surveys Failed!",
                    "An exception occurred when attempting to retrieve all surveys!",
                    e);
        }

        return Optional.ofNullable(retrievedSurveys);
    }

    private List<Survey> retrieveAllSurveys()
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        List<SurveyDetails> result = this.surveyService.retrieveAllSurveys();

        return Survey.fromDetails(result);
    }


    /**
     * Retrieves a {@link Survey}.
     * 
     * @param id
     *            the id of the {@link Survey} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> retrieveSurvey(final int id, final Node owner) {

        return this.retrieveSurvey(id, Utils.windowOf(owner));
    }

    /**
     * Retrieves a {@link Survey}.
     * 
     * @param id
     *            the id of the {@link Survey} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> retrieveSurvey(final int id, final Window owner) {

        Survey retrievedSurvey = null;

        try {

            retrievedSurvey = this.retrieveSurvey(id);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Survey Failed!",
                    "An exception occurred when attempting to retrieve Survey with id "
                            + id + "!", e);
        }

        return Optional.ofNullable(retrievedSurvey);
    }


    private Survey retrieveSurvey(int id)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SurveyDetails result = this.surveyService.retrieveSurvey(id);

        return Survey.fromDetails(result);
    }



    /**
     * Retrieves the {@link Survey} containing the provided {@link IQuestion
     * Question} .
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> retrieveSurveyContainingQuestion(
            final IQuestion question, final Node owner) {

        return this.retrieveSurveyContainingQuestion(question,
                Utils.windowOf(owner));
    }

    /**
     * Retrieves the {@link Survey} containing the provided {@link IQuestion
     * Question} .
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> retrieveSurveyContainingQuestion(
            final IQuestion question, final Window owner) {

        Survey retrievedSurvey = null;

        try {

            retrievedSurvey = this.retrieveSurveyContainingQuestion(question);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Survey Failed!",
                    "An exception occurred when attempting to retrieve survey for question + "
                            + question + "!", e);
        }

        return Optional.ofNullable(retrievedSurvey);
    }

    private Survey retrieveSurveyContainingQuestion(final IQuestion question)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SurveyDetails result = this.surveyService
                .retrieveSurveyForQuestion(question.getId());

        return Survey.fromDetails(result);
    }


    /**
     * Retrieves the {@link Survey} containing the provided {@link IAnswer
     * Answer} .
     * 
     * @param answer
     *            the {@link IAnswer Answer}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> retrieveSurveyContainingAnswer(
            final IAnswer answer, final Node owner) {

        return this.retrieveSurveyContainingAnswer(answer,
                Utils.windowOf(owner));
    }

    /**
     * Retrieves the {@link Survey} containing the provided {@link IAnswer
     * Answer} .
     * 
     * @param answer
     *            the {@link IAnswer Answer}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> retrieveSurveyContainingAnswer(
            final IAnswer answer, final Window owner) {

        Survey retrievedSurvey = null;

        try {

            retrievedSurvey = this.retrieveSurveyContainingAnswer(answer);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Survey Failed!",
                    "An exception occurred when attempting to retrieve survey for answer + "
                            + answer + "!", e);
        }

        return Optional.ofNullable(retrievedSurvey);
    }

    private Survey retrieveSurveyContainingAnswer(final IAnswer answer)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SurveyDetails result = this.surveyService
                .retrieveSurveyForAnswer(answer.getId());

        return Survey.fromDetails(result);
    }

    /**
     * Retrieves the {@link Survey} containing the provided {@link IParticipant
     * Participant}.
     * 
     * @param participant
     *            the {@link IParticipant Participant}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> retrieveSurveyContainingParticipant(
            final IParticipant participant, final Node owner) {

        return this.retrieveSurveyContainingParticipant(participant,
                Utils.windowOf(owner));
    }

    /**
     * Retrieves the {@link Survey} containing the provided {@link IParticipant
     * Participant} .
     * 
     * @param participant
     *            the {@link IParticipant Participant}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> retrieveSurveyContainingParticipant(
            final IParticipant participant, final Window owner) {

        Survey retrievedSurvey = null;

        try {

            retrievedSurvey = this
                    .retrieveSurveyContainingParticipant(participant);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Survey Failed!",
                    "An exception occurred when attempting to retrieve survey for participant + "
                            + participant + "!", e);
        }

        return Optional.ofNullable(retrievedSurvey);
    }

    private Survey retrieveSurveyContainingParticipant(
            final IParticipant participant)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SurveyDetails result = this.surveyService
                .retrieveSurveyForParticipant(participant.getId());

        return Survey.fromDetails(result);
    }

    /**
     * Updates a {@link Survey}.
     * 
     * @param Survey
     *            the {@link Survey} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> updateSurvey(final Survey Survey, final Node owner) {

        return this.updateSurvey(Survey, Utils.windowOf(owner));
    }

    /**
     * Updates a {@link Survey}.
     * 
     * @param Survey
     *            the {@link Survey} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> updateSurvey(final Survey Survey, final Window owner) {

        Survey updatedSurvey = null;

        try {

            updatedSurvey = this.updateSurvey(Survey);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Updating Survey  Failed!",
                    "An exception occurred when attempting to update Survey set "
                            + Survey + "!", e);
        }

        return Optional.ofNullable(updatedSurvey);
    }


    private Survey updateSurvey(Survey survey)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SurveyDetails result = this.surveyService.updateSurvey(survey
                .toDTO());

        return Survey.fromDetails(result);
    }


    /**
     * Deletes a {@link Survey}.
     * 
     * @param survey
     *            the {@link Survey} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> deleteSurvey(final Survey survey, final Node owner) {

        return this.deleteSurvey(survey, Utils.windowOf(owner));
    }

    /**
     * Deletes a {@link Survey}.
     * 
     * @param survey
     *            the {@link Survey} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Survey}{@code >} containing
     *         the result of the action.
     */
    public Optional<Survey> deleteSurvey(final Survey survey, final Window owner) {

        Survey deletedSurvey = null;

        if (Utils.confirmDeletion(survey, owner)) {

            try {

                deletedSurvey = this.deleteSurvey(survey);
            }
            catch (RequestFailedException | RequestDeniedException
                    | ObjectNotFoundException e) {

                Utils.showExceptionDialog(owner, "Deleting Survey Failed!",
                        "An exception occurred when attempting to delete set "
                                + survey + "!", e);
            }
        }

        return Optional.ofNullable(deletedSurvey);
    }

    private Survey deleteSurvey(Survey survey)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SurveyDetails result = this.surveyService.deleteSurvey(survey.getId());

        return Survey.fromDetails(result);
    }

    /**
     * Creates an empty {@link IQuestion Question} of the provided type.
     * 
     * @param type
     *            the type of {@link IQuestion Question} to create (and
     *            persist).
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> createEmptyQuestion(Class<T> type,
            final Node owner) {

        return this.createEmptyQuestion(type, Utils.windowOf(owner));
    }

    /**
     * Creates an empty {@link IQuestion Question} of the provided type.
     * 
     * @param type
     *            the type of {@link IQuestion Question} to create (and
     *            persist).
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> createEmptyQuestion(Class<T> type,
            final Window owner) {

        return this.createEmptyQuestion(type, Integer.MAX_VALUE, owner);
    }

    /**
     * Creates an empty {@link IQuestion Question} of the provided type.
     * 
     * @param type
     *            the type of {@link IQuestion Question} to create (and
     *            persist).
     * @param order
     *            the initial order of the {@link IQuestion Question}.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> createEmptyQuestion(Class<T> type,
            final int order, final Node owner) {

        return this.createEmptyQuestion(type, order, Utils.windowOf(owner));
    }

    /**
     * Creates an empty {@link IQuestion Question} of the provided type.
     * 
     * @param type
     *            the type of {@link IQuestion Question} to create (and
     *            persist).
     * @param order
     *            the initial order of the {@link IQuestion Question}.
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> createEmptyQuestion(Class<T> type,
            final int order, final Window owner) {

        QuestionDialog<T> dialog = new QuestionDialog<>(owner.getScene()
                .getWindow(), "Create Question", this.createQuestion(type,
                "New " + type.getSimpleName()));


        return this.createQuestion(dialog.showAndWait(), order, owner);
    }

    /**
     * Creates and persists the provided {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to create (and persist).
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> createQuestion(final T question,
            final Node owner) {

        return this.createQuestion(question, Utils.windowOf(owner));
    }

    /**
     * Creates and persists the provided {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to create (and persist).
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> createQuestion(final T question,
            final Window owner) {

        return this.createQuestion(question, Integer.MAX_VALUE, owner);
    }

    /**
     * Creates and persists the provided {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to create (and persist).
     * @param order
     *            the initial order of the {@link IQuestion Question}.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> createQuestion(final T question,
            final int order, final Node owner) {

        return this.createQuestion(question, order, Utils.windowOf(owner));
    }

    /**
     * Creates and persists the provided {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to create (and persist).
     * @param order
     *            the initial order of the {@link IQuestion Question}.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> createQuestion(final T question,
            final int order, final Window owner) {

        Objects.requireNonNull(question,
                "Cannot create question: No question provided!");

        return this.createQuestion(new QuestionDialog<>(owner.getScene()
                .getWindow(), "Create Question", question).showAndWait(),
                order, owner);
    }


    /**
     * Creates and persists the provided {@link IQuestion Questions}.
     * 
     * @param questions
     *            the {@link IQuestion Questions}.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link List}{@code <}
     *         {@link IQuestion}{@code >>} containing the result of the action.
     */
    public <T extends IQuestion> Optional<List<T>> createQuestions(
            final List<T> questions, final Node owner) {

        return this.createQuestions(questions, Utils.windowOf(owner));
    }


    /**
     * Creates and persists the provided {@link IQuestion Questions}.
     * 
     * @param questions
     *            the {@link IQuestion Questions}.
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link List}{@code <}
     *         {@link IQuestion}{@code >>} containing the result of the action.
     */
    public <T extends IQuestion> Optional<List<T>> createQuestions(
            final List<T> questions, final Window owner) {

        return this.createQuestions(questions, Integer.MAX_VALUE, owner);
    }

    /**
     * Creates and persists the provided {@link IQuestion Questions}.
     * 
     * @param questions
     *            the {@link IQuestion Questions}.
     * @param offset
     *            the offset of the order of questions.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link List}{@code <}
     *         {@link IQuestion}{@code >>} containing the result of the action.
     */
    public <T extends IQuestion> Optional<List<T>> createQuestions(
            final List<T> questions, final int offset, final Node owner) {

        return this.createQuestions(questions, offset, Utils.windowOf(owner));
    }


    /**
     * Creates and persists the provided {@link IQuestion Questions}.
     * 
     * @param questions
     *            the {@link IQuestion Questions}.
     * @param offset
     *            the offset of the order of questions.
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link List}{@code <}
     *         {@link IQuestion}{@code >>} containing the result of the action.
     */
    public <T extends IQuestion> Optional<List<T>> createQuestions(
            final List<T> questions, final int offset, final Window owner) {

        Objects.requireNonNull(questions,
                "Cannot create questions: No questions provided!");

        ActionDialog<T> editListDialog = new ActionDialog<>(owner,
                "Create Questions",
                "Please confirm the questions to be created!",
                (T question) -> {

                    return new QuestionDialog<>(owner, "Edit Question",
                            question).showAndWait();
                }, "Edit", questions);

        return this
                .createQuestions(editListDialog.showAndWait(), offset, owner);
    }

    private <T extends IQuestion> Optional<List<T>> createQuestions(
            final Optional<List<T>> questions, final int offset,
            final Window owner) {

        List<T> createdQuestions = null;

        if (questions.isPresent()) {

            createdQuestions = new ArrayList<>();

            List<T> questionsToCreate = questions.get();

            LOG.debug("About to create " + questionsToCreate.size()
                    + " questions.");
            int order = offset;

            if (order == Integer.MAX_VALUE) {

                order -= questions.get().size();
            }
            LOG.debug("Initial order is: " + order);

            int i = 0;
            for (T question : questions.get()) {

                LOG.debug("Attempting to create question " + i + " with order "
                        + (order + i) + ".");
                question.setOrder(order + i);
                i++;
                try {

                    createdQuestions.add(this.createQuestion(question));
                }
                catch (RequestFailedException | RequestDeniedException e) {

                    Utils.showExceptionDialog(owner,
                            "Creating New Question Failed!",
                            "An exception occurred when "
                                    + "attempting to create new questions!", e);
                }
            }
        }

        return Optional.ofNullable(createdQuestions);
    }


    private <T extends IQuestion> Optional<T> createQuestion(
            final Optional<T> question, final int order, final Window owner) {

        T newQuestion = null;
        if (question.isPresent()) {

            question.get().setOrder(order);
            try {

                newQuestion = this.createQuestion(question.get());
            }
            catch (RequestFailedException | RequestDeniedException e) {

                Utils.showExceptionDialog(owner,
                        "Creating New Question Failed!",
                        "An exception occurred when "
                                + "attempting to create new question!", e);
            }
        }

        return Optional.ofNullable(newQuestion);
    }

    @SuppressWarnings("unchecked")
    private <T extends IQuestion> T createQuestion(T question)
            throws RequestFailedException, RequestDeniedException {

        if (question instanceof TextQuestion) {

            return (T) this.createTextQuestion((TextQuestion) question);
        }
        else if (question instanceof ChoiceQuestion) {

            return (T) this.createChoiceQuestion((ChoiceQuestion) question);
        }
        else if (question instanceof RankingQuestion) {

            return (T) this.createRankingQuestion((RankingQuestion) question);
        }
        throw new RequestFailedException("Question " + question
                + " could not be created!");
    }


    /**
     * Creates a new {@link TextQuestion}.
     * 
     * @param question
     *            the {@link TextQuestion} to create.
     * 
     * @return the new {@link TextQuestion}..
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     */
    private TextQuestion createTextQuestion(TextQuestion question)
            throws RequestFailedException, RequestDeniedException {

        TextQuestionDetails result = this.surveyService
                .createTextQuestion(question.toDTO());

        return TextQuestion.fromDetails(result);
    }

    /**
     * Creates a new {@link ChoiceQuestion}.
     * 
     * @param question
     *            the {@link ChoiceQuestion} to create.
     * 
     * @return the new {@link ChoiceQuestion}..
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     */
    private ChoiceQuestion createChoiceQuestion(ChoiceQuestion question)
            throws RequestFailedException, RequestDeniedException {

        ChoiceQuestionDetails result = this.surveyService
                .createChoiceQuestion(question.toDTO());

        return ChoiceQuestion.fromDetails(result);
    }

    /**
     * Creates a new {@link RankingQuestion}.
     * 
     * @param question
     *            the {@link RankingQuestion} to create.
     * 
     * @return the new {@link RankingQuestion}..
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     */
    private RankingQuestion createRankingQuestion(RankingQuestion question)
            throws RequestFailedException, RequestDeniedException {

        RankingQuestionDetails result = this.surveyService
                .createRankingQuestion(question.toDTO());

        return RankingQuestion.fromDetails(result);
    }



    private <T extends IQuestion> T createQuestion(final Class<T> type,
            final String name) {

        IQuestion question = null;

        if (type == TextQuestion.class) {

            question = new TextQuestion(name);
        }
        else if (type == ChoiceQuestion.class) {

            question = new ChoiceQuestion(name);
        }
        else if (type == RankingQuestion.class) {

            question = new RankingQuestion(name);
        }

        if (type.isAssignableFrom(question.getClass())) {

            return type.cast(question);
        }

        throw new IllegalArgumentException("Did not recognize question type "
                + type + "!");
    }

    /**
     * Updates a {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to update.
     * @param owner
     *            the {@link Node} that triggered the update.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> updateQuestion(final T question,
            final Node owner) {

        return this.updateQuestion(question, Utils.windowOf(owner));
    }

    /**
     * Updates a {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to update.
     * @param owner
     *            the {@link Node} that triggered the update.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public <T extends IQuestion> Optional<T> updateQuestion(final T question,
            final Window owner) {

        LOG.debug("Updating Question");
        T updatedQuestion = null;
        if (question != null) {

            try {

                updatedQuestion = this.updateQuestion(question);
            }
            catch (RequestFailedException | RequestDeniedException
                    | ObjectNotFoundException e) {

                Utils.showExceptionDialog(
                        owner,
                        "Updating Question Failed!",
                        "An exception occurred when attempting to update the question!",
                        e);
            }
        }

        return Optional.ofNullable(updatedQuestion);
    }

    /**
     * Updates a {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to update.
     * @return the updated {@link IQuestion Question}.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     * @throws RequestDeniedException
     */
    @SuppressWarnings("unchecked")
    private <T extends IQuestion> T updateQuestion(T question)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        if (question instanceof TextQuestion) {

            return (T) this.updateTextQuestion((TextQuestion) question);
        }
        else if (question instanceof ChoiceQuestion) {

            return (T) this.updateChoiceQuestion((ChoiceQuestion) question);
        }
        else if (question instanceof RankingQuestion) {

            return (T) this.updateRankingQuestion((RankingQuestion) question);
        }

        throw new RequestFailedException("Question " + question
                + " could not be updated!");
    }

    /**
     * Updates a {@link TextQuestion}.
     * 
     * @param question
     *            the {@link TextQuestion} to update.
     * @return the updated {@link TextQuestion}.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private TextQuestion updateTextQuestion(TextQuestion question)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        TextQuestionDetails result = this.surveyService
                .updateTextQuestion(question.toDTO());

        return TextQuestion.fromDetails(result);
    }

    /**
     * Updates a {@link ChoiceQuestion}.
     * 
     * @param question
     *            the {@link ChoiceQuestion} to update.
     * @return the updated {@link ChoiceQuestion}.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private ChoiceQuestion updateChoiceQuestion(ChoiceQuestion question)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ChoiceQuestionDetails result = this.surveyService
                .updateChoiceQuestion(question.toDTO());

        return ChoiceQuestion.fromDetails(result);
    }

    /**
     * Updates a {@link RankingQuestion}.
     * 
     * @param question
     *            the {@link RankingQuestion} to update.
     * @return the updated {@link RankingQuestion}.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private RankingQuestion updateRankingQuestion(RankingQuestion question)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        RankingQuestionDetails result = this.surveyService
                .updateRankingQuestion(question.toDTO());

        return RankingQuestion.fromDetails(result);
    }

    /**
     * Retrieves a {@link IQuestion Question}.
     * 
     * @param id
     *            the id of the {@link IQuestion Question} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public Optional<IQuestion> retrieveQuestion(final int id, final Node owner) {

        return this.retrieveQuestion(id, Utils.windowOf(owner));
    }

    /**
     * Retrieves a {@link IQuestion Question}.
     * 
     * @param id
     *            the id of the {@link IQuestion Question} to retrieve.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public Optional<IQuestion> retrieveQuestion(final int id, final Window owner) {

        IQuestion retrievedQuestion = null;

        try {

            retrievedQuestion = this.retrieveQuestion(id);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Question Failed!",
                    "An exception occurred when attempting to retrieve the question!",
                    e);
        }

        return Optional.ofNullable(retrievedQuestion);
    }

    /**
     * Retrieves a single {@link IQuestion Question}.
     * 
     * @param id
     *            the id.
     * @return the {@link IQuestion Question} with the provided id.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private IQuestion retrieveQuestion(int id)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {


        QuestionDetails details = this.surveyService.retrieveQuestion(id);

        if (details instanceof TextQuestionDetails) {

            return TextQuestion.fromDetails((TextQuestionDetails) details);
        }
        else if (details instanceof ChoiceQuestionDetails) {

            return ChoiceQuestion.fromDetails((ChoiceQuestionDetails) details);
        }
        else if (details instanceof RankingQuestionDetails) {

            return RankingQuestion
                    .fromDetails((RankingQuestionDetails) details);
        }


        throw new RequestFailedException("Could not retrieve question with id "
                + id + "!");
    }

    // /**
    // * Retrieves a single {@link TextQuestion}.
    // *
    // * @param id
    // * the id.
    // * @return the {@link TextQuestion} with the provided id.
    // *
    // * @throws RequestFailedException
    // * if the request fails.
    // * @throws RequestDeniedException
    // * if the request is denied.
    // * @throws ObjectNotFoundException
    // * if the object was not found.
    // */
    // private TextQuestion retrieveTextQuestion(int id)
    // throws RequestFailedException, RequestDeniedException,
    // ObjectNotFoundException {
    //
    //
    // TextQuestionDetails result = this.surveyService
    // .retrieveTextQuestion(id);
    //
    // return TextQuestion.fromDetails(result);
    // }
    //
    // /**
    // * Retrieves a single {@link ChoiceQuestion}.
    // *
    // * @param id
    // * the id.
    // * @return the {@link ChoiceQuestion} with the provided id.
    // *
    // * @throws RequestFailedException
    // * if the request fails.
    // * @throws RequestDeniedException
    // * if the request is denied.
    // * @throws ObjectNotFoundException
    // * if the object was not found.
    // */
    // private ChoiceQuestion retrieveChoiceQuestion(int id)
    // throws RequestFailedException, RequestDeniedException,
    // ObjectNotFoundException {
    //
    //
    // ChoiceQuestionDetails result = this.surveyService
    // .retrieveChoiceQuestion(id);
    //
    // return ChoiceQuestion.fromDetails(result);
    // }
    //
    // /**
    // * Retrieves a single {@link RankingQuestion}.
    // *
    // * @param id
    // * the id.
    // * @return the {@link RankingQuestion} with the provided id.
    // *
    // * @throws RequestFailedException
    // * if the request fails.
    // * @throws RequestDeniedException
    // * if the request is denied.
    // * @throws ObjectNotFoundException
    // * if the object was not found.
    // */
    // private RankingQuestion retrieveRankingQuestion(int id)
    // throws RequestFailedException, RequestDeniedException,
    // ObjectNotFoundException {
    //
    // RankingQuestionDetails result = this.surveyService
    // .retrieveRankingQuestion(id);
    //
    // return RankingQuestion.fromDetails(result);
    // }

    /**
     * Deletes a {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to delete.
     * @param owner
     *            the {@link Node} that triggered the deletion.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public Optional<IQuestion> deleteQuestion(final IQuestion question,
            final Node owner) {

        return this.deleteQuestion(question, Utils.windowOf(owner));
    }

    /**
     * Deletes a {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to delete.
     * @param owner
     *            the {@link Node} that triggered the deletion.
     * @return the {@link Optional}{@code <}{@link IQuestion Question}{@code >}
     *         containing the result of the action.
     */
    public Optional<IQuestion> deleteQuestion(final IQuestion question,
            final Window owner) {


        IQuestion deletedQuestion = null;

        if (Utils.confirmDeletion(question, owner)) {

            try {

                deletedQuestion = this.deleteQuestion(question);
            }
            catch (RequestFailedException | RequestDeniedException
                    | ObjectNotFoundException e) {

                Utils.showExceptionDialog(
                        owner,
                        "Deleting Question Failed!",
                        "An exception occurred when attempting to delete the question!",
                        e);
            }
        }
        return Optional.ofNullable(deletedQuestion);
    }

    /**
     * Deletes the {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question} to remove.
     * @return the removed {@link IQuestion Question}.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private IQuestion deleteQuestion(IQuestion question)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        QuestionDetails details = this.surveyService.deleteQuestion(question
                .getId());


        if (details instanceof TextQuestionDetails) {

            return TextQuestion.fromDetails((TextQuestionDetails) details);
        }
        else if (details instanceof ChoiceQuestionDetails) {

            return ChoiceQuestion.fromDetails((ChoiceQuestionDetails) details);
        }
        else if (details instanceof RankingQuestionDetails) {

            return RankingQuestion
                    .fromDetails((RankingQuestionDetails) details);
        }


        throw new RequestFailedException("question " + question
                + " could not be found!");

    }


    /**
     * Creates and persists an {@link IAnswer Answer} with the provided
     * {@link IQuestion Question} and {@link IParticipant Participant}
     * 
     * @param question
     *            the answered {@link IQuestion Question}.
     * @param participant
     *            the answering {@link IParticipant Participant}.
     * @param survey
     *            the {@link Survey} that is to contain the created
     *            {@link IAnswer Answer}.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> createAnswer(final IQuestion question,
            final IParticipant participant, final Survey survey,
            final Node owner) {

        return this.createAnswer(question, participant, survey,
                Utils.windowOf(owner));
    }

    /**
     * Creates and persists an {@link IAnswer Answer} with the provided
     * {@link IQuestion Question} and {@link IParticipant Participant}.
     * 
     * @param question
     *            the answered {@link IQuestion Question}.
     * @param participant
     *            the answering {@link IParticipant Participant}.
     * @param survey
     *            the {@link Survey} that is to contain the created
     *            {@link IAnswer Answer}.
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> createAnswer(final IQuestion question,
            final IParticipant participant, final Survey survey,
            final Window owner) {

        return this.createAnswer(
                new Answer("New Answer", question, participant), survey, owner);
    }


    /**
     * Creates and persists an empty {@link IAnswer Answer}.
     * 
     * @param survey
     *            the {@link Survey} that is to contain the created
     *            {@link IAnswer Answer}.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> createEmptyAnswer(final Survey survey,
            final Node owner) {

        return this.createEmptyAnswer(survey, Utils.windowOf(owner));
    }


    /**
     * Creates and persists an empty {@link IAnswer Answer}.
     * 
     * @param survey
     *            the {@link Survey} that is to contain the created
     *            {@link IAnswer Answer}.
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> createEmptyAnswer(final Survey survey,
            final Window owner) {

        return this.createAnswer(new Answer("New Answer", null, null), survey,
                owner);
    }

    /**
     * Creates an {@link IAnswer Answer}.
     * 
     * @param answer
     *            the answer to create.
     * @param survey
     *            the {@link Survey} that is to contain the created
     *            {@link IAnswer Answer}.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> createAnswer(final IAnswer answer,
            final Survey survey, final Node owner) {

        return this.createAnswer(answer, survey, Utils.windowOf(owner));
    }



    /**
     * Creates and persists the provided {@link IAnswer Answers}.
     * 
     * @param answers
     *            the {@link IAnswer Answers} to create.
     * @param survey
     *            the {@link Survey} that is to contain the created
     *            {@link IAnswer Answers}.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link List}{@code <}{@link Answer}
     *         {@code >>} containing the result of the action.
     */
    public Optional<List<IAnswer>> createAnswers(final List<IAnswer> answers,
            final Survey survey, final Node owner) {

        return this.createAnswers(answers, survey, Utils.windowOf(owner));
    }


    /**
     * Creates and persists the provided {@link IAnswer Answers}.
     * 
     * @param answers
     *            the {@link IAnswer Answers} to create.
     * @param survey
     *            the {@link Survey} that is to contain the created
     *            {@link IAnswer Answers}.
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link List}{@code <}{@link Answer}
     *         {@code >>} containing the result of the action.
     */
    public Optional<List<IAnswer>> createAnswers(final List<IAnswer> answers,
            final Survey survey, final Window owner) {

        Objects.requireNonNull(answers,
                "Cannot create Answers: No Answers provided!");
        Objects.requireNonNull(survey,
                "Cannot create Answers: No Survey provided!");

        ActionDialog<IAnswer> editListDialog = new ActionDialog<>(owner,
                "Create Answers", "Please confirm the Answers to be created!",
                (IAnswer answer) -> {

                    return new AnswerDialog(owner, "Edit Answer", answer,
                            survey).showAndWait();
                }, "Edit", answers);

        return this.createAnswers(editListDialog.showAndWait(), owner);
    }

    private Optional<List<IAnswer>> createAnswers(
            final Optional<List<IAnswer>> answers, final Window owner) {

        List<IAnswer> createdAnswers = null;

        if (answers.isPresent()) {

            createdAnswers = new ArrayList<>();

            List<IAnswer> answersToCreate = answers.get();

            LOG.debug("About to create " + answersToCreate.size() + " Answers.");


            int i = 0;
            for (IAnswer answer : answers.get()) {

                LOG.debug("Attempting to create Answer " + i + ".");
                try {

                    createdAnswers.add(this.createAnswer(answer));
                    i++;
                }
                catch (RequestFailedException | RequestDeniedException e) {

                    Utils.showExceptionDialog(owner,
                            "Creating New Answer Failed!",
                            "An exception occurred when "
                                    + "attempting to create new Answers!", e);
                    break;
                }

            }
        }

        return Optional.ofNullable(createdAnswers);
    }

    /**
     * Creates an {@link IAnswer Answer}.
     * 
     * @param answer
     *            the answer to create.
     * @param survey
     *            the {@link Survey} that is to contain the created
     *            {@link IAnswer Answer}.
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> createAnswer(final IAnswer answer,
            final Survey survey, final Window owner) {

        Objects.requireNonNull(answer);
        Objects.requireNonNull(survey);

        AnswerDialog dlg = new AnswerDialog(owner, "Create Answer", answer,
                survey);

        Optional<IAnswer> result = dlg.showAndWait();

        if (result.isPresent()) {

            IAnswer newAnswer = result.get();
            IAnswer createdAnswer = null;

            try {

                createdAnswer = this.createAnswer(newAnswer);
            }
            catch (RequestFailedException | RequestDeniedException e) {

                Utils.showExceptionDialog(owner, "Creating Answer Failed!",
                        "An exception occurred when attempting to create Answer "
                                + answer + "!", e);
            }

            return Optional.ofNullable(createdAnswer);
        }

        return Optional.empty();
    }


    private IAnswer createAnswer(IAnswer answer)
            throws RequestFailedException, RequestDeniedException {

        AnswerDetails result = this.surveyService.createAnswer(answer
                .toDTO());

        return Answer.fromDetails(result);
    }



    /**
     * Retrieves all {@link IAnswer} for the provided {@link IQuestion Question}
     * .
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IAnswer}{@code >} containing
     *         the result of the action.
     */
    public Optional<List<? extends IAnswer>> retrieveAllAnswersForQuestion(
            final IQuestion question, final Node owner) {

        return this.retrieveAllAnswersForQuestion(question,
                Utils.windowOf(owner));
    }

    /**
     * Retrieves all {@link IAnswer} for the provided {@link IQuestion Question}
     * .
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IAnswer}{@code >} containing
     *         the result of the action.
     */
    public Optional<List<? extends IAnswer>> retrieveAllAnswersForQuestion(
            final IQuestion question, final Window owner) {

        List<? extends IAnswer> retrievedAnswers = null;

        try {

            retrievedAnswers = this.retrieveAllAnswersForQuestion(question);
        }
        catch (RequestFailedException | RequestDeniedException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Answers Failed!",
                    "An exception occurred when attempting to retrieve all answers!",
                    e);
        }
        catch (ObjectNotFoundException e) {

            LOG.debug("Question " + question
                    + " has not been answered by any participants!");
        }

        return Optional.ofNullable(retrievedAnswers);
    }

    private List<? extends IAnswer> retrieveAllAnswersForQuestion(
            final IQuestion question)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        List<AnswerDetails> result = this.surveyService
                .retrieveAllAnswersForQuestion(question.getId());

        return Answer.fromDetails(result);
    }


    /**
     * Retrieves all {@link IAnswer} for the provided {@link IParticipant
     * Participant} .
     * 
     * @param participant
     *            the {@link IParticipant Participant}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IAnswer}{@code >} containing
     *         the result of the action.
     */
    public Optional<List<? extends IAnswer>> retrieveAllAnswersByParticipant(
            final IParticipant participant, final Node owner) {

        return this.retrieveAllAnswersByParticipant(participant,
                Utils.windowOf(owner));
    }

    /**
     * Retrieves all {@link IAnswer} for the provided {@link IParticipant
     * Participant} .
     * 
     * @param participant
     *            the {@link IParticipant Participant}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IAnswer}{@code >} containing
     *         the result of the action.
     */
    public Optional<List<? extends IAnswer>> retrieveAllAnswersByParticipant(
            final IParticipant participant, final Window owner) {

        List<? extends IAnswer> retrievedAnswers = null;

        try {

            retrievedAnswers = this
                    .retrieveAllAnswersByParticipant(participant);
        }
        catch (RequestFailedException | RequestDeniedException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Answers Failed!",
                    "An exception occurred when attempting to retrieve all answers!",
                    e);
        }
        catch (ObjectNotFoundException e) {

            LOG.debug("Participant " + participant
                    + " has not answered any questions!");
        }

        return Optional.ofNullable(retrievedAnswers);
    }

    private List<? extends IAnswer> retrieveAllAnswersByParticipant(
            final IParticipant participant)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        List<AnswerDetails> result = this.surveyService
                .retrieveAllAnswersForParticipant(participant.getId());

        return Answer.fromDetails(result);
    }



    /**
     * Retrieves the {@link IAnswer Answer} to the provided {@link IQuestion
     * Question} by the provided {@link IParticipant Participant}.
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param participant
     *            the {@link IParticipant Participant}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IAnswer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> retrieveAnswerToQuestionByParticipant(
            final IQuestion question, final IParticipant participant,
            final Node owner) {

        return this.retrieveAnswerToQuestionByParticipant(question,
                participant, Utils.windowOf(owner));
    }

    /**
     * Retrieves the {@link IAnswer Answer} to the provided {@link IQuestion
     * Question} by the provided {@link IParticipant Participant}.
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param participant
     *            the {@link IParticipant Participant}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IAnswer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> retrieveAnswerToQuestionByParticipant(
            final IQuestion question, final IParticipant participant,
            final Window owner) {

        IAnswer retrievedAnswer = null;

        try {

            retrievedAnswer = this.retrieveAnswerToQuestionByParticipant(
                    question, participant);
        }
        catch (RequestFailedException | RequestDeniedException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Answer Failed!",
                    "An exception occurred when attempting to retrieve answer to question "
                            + question + " by participant " + participant + "!",
                    e);
        }
        catch (ObjectNotFoundException e) {

            LOG.debug("Participant " + participant
                    + " has not answered question " + question + "!");
        }

        return Optional.ofNullable(retrievedAnswer);
    }

    private IAnswer retrieveAnswerToQuestionByParticipant(
            final IQuestion question, final IParticipant participant)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        AnswerDetails result = this.surveyService
                .retrieveAnswerToQuestionByParticipant(question.getId(),
                        participant.getId());

        return Answer.fromDetails(result);
    }



    /**
     * Retrieves an {@link IAnswer Answer}.
     * 
     * @param id
     *            the id of the {@link IAnswer Answer} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> retrieveAnswer(final int id, final Node owner) {

        return this.retrieveAnswer(id, Utils.windowOf(owner));
    }

    /**
     * Retrieves an {@link IAnswer Answer}.
     * 
     * @param id
     *            the id of the {@link IAnswer Answer} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> retrieveAnswer(final int id, final Window owner) {

        IAnswer retrievedAnswer = null;

        try {

            retrievedAnswer = this.retrieveAnswer(id);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Answer Failed!",
                    "An exception occurred when attempting to retrieve answer with id "
                            + id + "!", e);
        }

        return Optional.ofNullable(retrievedAnswer);
    }

    private Answer retrieveAnswer(int id)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        AnswerDetails result = this.surveyService.retrieveAnswer(id);

        return Answer.fromDetails(result);
    }

    /**
     * Updates an {@link IAnswer Answer}.
     * 
     * @param answer
     *            the {@link IAnswer Answer} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IAnswer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> updateAnswer(final IAnswer answer, final Node owner) {

        return this.updateAnswer(answer, Utils.windowOf(owner));
    }

    /**
     * Updates an {@link IAnswer Answer}.
     * 
     * @param answer
     *            the {@link IAnswer Answer} to update.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IAnswer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> updateAnswer(final IAnswer answer,
            final Window owner) {

        IAnswer updatedAnswer = null;

        try {

            updatedAnswer = this.updateAnswer(answer);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Updating Answer Failed!",
                    "An exception occurred when attempting to update answer set "
                            + answer + "!", e);
        }

        return Optional.ofNullable(updatedAnswer);
    }


    private IAnswer updateAnswer(IAnswer answer)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        AnswerDetails result = this.surveyService.updateAnswer(answer
                .toDTO());

        return Answer.fromDetails(result);
    }



    /**
     * Deletes an {@link IAnswer Answer}.
     * 
     * @param answer
     *            the {@link IAnswer Answer} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> deleteAnswer(final IAnswer answer, final Node owner) {

        return this.deleteAnswer(answer, Utils.windowOf(owner));
    }

    /**
     * Deletes an {@link IAnswer Answer}.
     * 
     * @param answer
     *            the {@link IAnswer Answer} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Answer}{@code >} containing
     *         the result of the action.
     */
    public Optional<IAnswer> deleteAnswer(final IAnswer answer,
            final Window owner) {

        IAnswer deletedAnswer = null;

        if (Utils.confirmDeletion(answer, owner)) {

            try {

                deletedAnswer = this.deleteAnswer(answer);
            }
            catch (RequestFailedException | RequestDeniedException
                    | ObjectNotFoundException e) {

                Utils.showExceptionDialog(owner, "Deleting Answer Failed!",
                        "An exception occurred when attempting to delete set "
                                + answer + "!", e);
            }
        }

        return Optional.ofNullable(deletedAnswer);
    }


    private IAnswer deleteAnswer(IAnswer answer)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        AnswerDetails result = this.surveyService.deleteAnswer(answer.getId());

        return Answer.fromDetails(result);
    }


    /**
     * Creates and persists an empty {@link IParticipant Participant}.
     * 
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IParticipant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> createParticipant(final Node owner) {

        return this.createParticipant(Utils.windowOf(owner));
    }


    /**
     * Creates and persists an empty {@link IParticipant Participant}.
     * 
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Participant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> createParticipant(final Window owner) {

        return this
                .createParticipant(new Participant("New Participant"), owner);
    }



    /**
     * Creates and persists the provided {@link IParticipant Participant}.
     * 
     * @param participant
     *            the {@link IParticipant Participant} to create.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link IParticipant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> createParticipant(
            final IParticipant participant, final Node owner) {

        return this.createParticipant(participant, Utils.windowOf(owner));
    }


    /**
     * Creates and persists the provided {@link IParticipant Participant}.
     * 
     * @param participant
     *            the {@link IParticipant Participant} to create.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Participant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> createParticipant(
            final IParticipant participant, final Window owner) {

        DomainObjectDialog<IParticipant> dlg = new DomainObjectDialog<>(owner
                .getScene().getWindow(), "Create Participant", participant);

        Optional<IParticipant> result = dlg.showAndWait();

        if (result.isPresent()) {

            IParticipant newParticipant = result.get();
            IParticipant createdParticipant = null;

            try {

                createdParticipant = this.createParticipant(newParticipant);
            }
            catch (RequestFailedException | RequestDeniedException e) {

                Utils.showExceptionDialog(owner,
                        "Creating Participant Failed!",
                        "An exception occurred when attempting to create Participant "
                                + participant + "!", e);
            }

            return Optional.ofNullable(createdParticipant);
        }

        return Optional.empty();
    }



    /**
     * Creates and persists the provided {@link IParticipant Participants}.
     * 
     * @param participants
     *            the {@link IParticipant Participants} to create.
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link List}{@code <}
     *         {@link Participant}{@code >>} containing the result of the
     *         action.
     */
    public Optional<List<IParticipant>> createParticipants(
            final List<IParticipant> participants, final Node owner) {

        return this.createParticipants(participants, Utils.windowOf(owner));
    }


    /**
     * Creates and persists the provided {@link IParticipant Participants}.
     * 
     * @param participants
     *            the {@link IParticipant Participants} to create.
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link List}{@code <}
     *         {@link Participant}{@code >>} containing the result of the
     *         action.
     */
    public Optional<List<IParticipant>> createParticipants(
            final List<IParticipant> participants, final Window owner) {

        Objects.requireNonNull(participants,
                "Cannot create Participants: No Participants provided!");

        ActionDialog<IParticipant> editListDialog = new ActionDialog<>(owner,
                "Create Participants",
                "Please confirm the Participants to be created!", (
                        IParticipant participant) -> {

                    return new DomainObjectDialog<>(owner, "Edit Participant",
                            participant).showAndWait();
                }, "Edit", participants);

        return this.createParticipants(editListDialog.showAndWait(), owner);
    }

    private Optional<List<IParticipant>> createParticipants(
            final Optional<List<IParticipant>> participants, final Window owner) {

        List<IParticipant> createdParticipants = null;

        if (participants.isPresent()) {

            createdParticipants = new ArrayList<>();

            List<IParticipant> participantsToCreate = participants.get();

            LOG.debug("About to create " + participantsToCreate.size()
                    + " Participants.");


            int i = 0;
            for (IParticipant participant : participants.get()) {

                LOG.debug("Attempting to create Participant " + i + ".");
                try {

                    createdParticipants
                            .add(this.createParticipant(participant));
                    i++;
                }
                catch (RequestFailedException | RequestDeniedException e) {

                    Utils.showExceptionDialog(owner,
                            "Creating New Participant Failed!",
                            "An exception occurred when "
                                    + "attempting to create new Participants!",
                            e);
                    break;
                }
            }
        }

        return Optional.ofNullable(createdParticipants);
    }



    private IParticipant createParticipant(IParticipant participant)
            throws RequestFailedException, RequestDeniedException {

        ParticipantDTO result = this.surveyService
                .createParticipant(participant.toDTO());

        return Participant.fromDetails(result);
    }


    /**
     * Retrieves an {@link IParticipant Participant}.
     * 
     * @param id
     *            the id of the {@link IParticipant Participant} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Participant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> retrieveParticipant(final int id,
            final Node owner) {

        return this.retrieveParticipant(id, Utils.windowOf(owner));
    }

    /**
     * Retrieves an {@link IParticipant Participant}.
     * 
     * @param id
     *            the id of the {@link IParticipant Participant} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Participant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> retrieveParticipant(final int id,
            final Window owner) {

        IParticipant retrievedParticipant = null;

        try {

            retrievedParticipant = this.retrieveParticipant(id);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Participant Failed!",
                    "An exception occurred when attempting to retrieve participant with id "
                            + id + "!", e);
        }

        return Optional.ofNullable(retrievedParticipant);
    }

    private IParticipant retrieveParticipant(int id)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {


        ParticipantDTO result = this.surveyService.retrieveParticipant(id);

        return Participant.fromDetails(result);
    }


    /**
     * Updates an {@link IParticipant Participant}.
     * 
     * @param participant
     *            the {@link IParticipant Participant} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IParticipant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> updateParticipant(
            final IParticipant participant, final Node owner) {

        return this.updateParticipant(participant, Utils.windowOf(owner));
    }

    /**
     * Updates an {@link IParticipant Participant}.
     * 
     * @param participant
     *            the {@link IParticipant Participant} to update.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link IParticipant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> updateParticipant(
            final IParticipant participant, final Window owner) {

        IParticipant updatedParticipant = null;

        try {

            updatedParticipant = this.updateParticipant(participant);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Updating Participant Failed!",
                    "An exception occurred when attempting to update participant set "
                            + participant + "!", e);
        }

        return Optional.ofNullable(updatedParticipant);
    }



    private IParticipant updateParticipant(IParticipant participant)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ParticipantDTO result = this.surveyService
                .updateParticipant(participant.toDTO());

        return Participant.fromDetails(result);
    }



    /**
     * Deletes an {@link IParticipant Participant}.
     * 
     * @param participant
     *            the {@link IParticipant Participant} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Participant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> deleteParticipant(
            final IParticipant participant, final Node owner) {

        return this.deleteParticipant(participant, Utils.windowOf(owner));
    }

    /**
     * Deletes an {@link IParticipant Participant}.
     * 
     * @param participant
     *            the {@link IParticipant Participant} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Participant}{@code >}
     *         containing the result of the action.
     */
    public Optional<IParticipant> deleteParticipant(
            final IParticipant participant, final Window owner) {

        IParticipant deletedParticipant = null;

        if (Utils.confirmDeletion(participant, owner)) {

            try {

                deletedParticipant = this.deleteParticipant(participant);
            }
            catch (RequestFailedException | RequestDeniedException
                    | ObjectNotFoundException e) {

                Utils.showExceptionDialog(owner,
                        "Deleting Participant Failed!",
                        "An exception occurred when attempting to delete set "
                                + participant + "!", e);
            }
        }

        return Optional.ofNullable(deletedParticipant);
    }


    private IParticipant deleteParticipant(IParticipant participant)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ParticipantDTO result = this.surveyService
                .deleteParticipant(participant.getId());

        return Participant.fromDetails(result);
    }


    /**
     * Creates a {@link Choice}.
     * 
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> createChoice(final Node owner) {

        return this.createChoice(Utils.windowOf(owner));
    }


    /**
     * Creates a {@link Choice}.
     * 
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> createChoice(final Window owner) {

        DomainObjectDialog<Choice> dlg = new DomainObjectDialog<>(owner
                .getScene().getWindow(), "Create Choice", new Choice(
                "New Choice"));

        Optional<Choice> result = dlg.showAndWait();

        if (result.isPresent()) {

            Choice choice = result.get();
            Choice createdChoice = null;

            try {

                createdChoice = this.createChoice(choice);
            }
            catch (RequestFailedException | RequestDeniedException e) {

                Utils.showExceptionDialog(owner, "Creating Choice Failed!",
                        "An exception occurred when attempting to create choice "
                                + choice + "!", e);
            }

            return Optional.ofNullable(createdChoice);
        }

        return Optional.empty();
    }

    /**
     * Creates a new choice.
     * 
     * @param choice
     *            the choice to create.
     * @return the new choice.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     */
    private Choice createChoice(Choice choice)
            throws RequestFailedException, RequestDeniedException {

        ChoiceDetails result = this.surveyService.createChoice(choice
                .toDTO());

        return Choice.fromDetails(result);
    }

    /**
     * Retrieves all {@link Choice}.
     * 
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Collection<Choice>> retrieveAllChoices(final Node owner) {

        return this.retrieveAllChoices(Utils.windowOf(owner));
    }

    /**
     * Retrieves all {@link Choice}.
     * 
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Collection<Choice>> retrieveAllChoices(final Window owner) {

        Collection<Choice> retrievedChoices = null;

        try {

            retrievedChoices = this.retrieveAllChoices();
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Choices Failed!",
                    "An exception occurred when attempting to retrieve all choices!",
                    e);
        }

        return Optional.ofNullable(retrievedChoices);
    }


    /**
     * Retrieves all choices.
     * 
     * @return a {@link List} of all choices.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private List<Choice> retrieveAllChoices()
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        List<ChoiceDetails> result = this.surveyService.retrieveAllChoices();

        return Choice.fromDetails(result);
    }

    /**
     * Retrieves a {@link Choice}.
     * 
     * @param id
     *            the id of the {@link Choice} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> retrieveChoice(final int id, final Node owner) {

        return this.retrieveChoice(id, Utils.windowOf(owner));
    }

    /**
     * Retrieves a {@link Choice}.
     * 
     * @param id
     *            the id of the {@link Choice} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> retrieveChoice(final int id, final Window owner) {

        Choice retrievedChoice = null;

        try {

            retrievedChoice = this.retrieveChoice(id);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Choice Failed!",
                    "An exception occurred when attempting to retrieve choice with id "
                            + id + "!", e);
        }

        return Optional.ofNullable(retrievedChoice);
    }

    /**
     * Retrieves the choice with the provided id.
     * 
     * @param id
     *            the id.
     * @return the choice with the provided id.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private Choice retrieveChoice(int id)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {


        ChoiceDetails result = this.surveyService.retrieveChoice(id);

        return Choice.fromDetails(result);
    }


    /**
     * Retrieves a {@link Choice}.
     * 
     * @param name
     *            the name of the {@link Choice} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> retrieveChoiceWithName(final String name,
            final Node owner) {

        return this.retrieveChoiceWithName(name, Utils.windowOf(owner));
    }


    /**
     * Retrieves a {@link Choice}.
     * 
     * @param name
     *            the name of the {@link Choice} to retrieve.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> retrieveChoiceWithName(final String name,
            final Window owner) {

        Choice retrievedChoice = null;

        try {

            retrievedChoice = this.retrieveChoiceWithName(name);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Choice Failed!",
                    "An exception occurred when attempting to retrieve choice with name "
                            + name + "!", e);
        }

        return Optional.ofNullable(retrievedChoice);
    }

    /**
     * Retrieves the choice with the provided name.
     * 
     * @param name
     *            the name.
     * @return the choice with the provided name.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private Choice retrieveChoiceWithName(String name)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ChoiceDetails result = this.surveyService.retrieveChoiceByName(name);

        return Choice.fromDetails(result);
    }


    /**
     * Updates a {@link Choice}.
     * 
     * @param choice
     *            the {@link Choice} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> updateChoice(final Choice choice, final Node owner) {

        return this.updateChoice(choice, Utils.windowOf(owner));
    }

    /**
     * Updates a {@link Choice}.
     * 
     * @param choice
     *            the {@link Choice} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> updateChoice(final Choice choice, final Window owner) {

        Choice updatedChoice = null;

        try {

            updatedChoice = this.updateChoice(choice);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Updating Choice  Failed!",
                    "An exception occurred when attempting to update choice set "
                            + choice + "!", e);
        }

        return Optional.ofNullable(updatedChoice);
    }

    private Choice updateChoice(Choice choice)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ChoiceDetails result = this.surveyService.updateChoice(choice
                .toDTO());

        return Choice.fromDetails(result);
    }

    /**
     * Deletes a {@link Choice}.
     * 
     * @param choice
     *            the {@link Choice} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> deleteChoice(final Choice choice, final Node owner) {

        return this.deleteChoice(choice, Utils.windowOf(owner));
    }

    /**
     * Deletes a {@link Choice}.
     * 
     * @param choice
     *            the {@link Choice} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Choice}{@code >} containing
     *         the result of the action.
     */
    public Optional<Choice> deleteChoice(final Choice choice, final Window owner) {

        Choice deletedChoice = null;

        if (Utils.confirmDeletion(choice, owner)) {

            try {

                deletedChoice = this.deleteChoice(choice);
            }
            catch (RequestFailedException | RequestDeniedException
                    | ObjectNotFoundException e) {

                Utils.showExceptionDialog(owner, "Deleting Choice Failed!",
                        "An exception occurred when attempting to delete set "
                                + choice + "!", e);
            }
        }

        return Optional.ofNullable(deletedChoice);
    }

    /**
     * Removes the choice.
     * 
     * @param choice
     *            the choice to remove.
     * @return the removed choice.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private Choice deleteChoice(Choice choice)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ChoiceDetails result = this.surveyService.deleteChoice(choice.getId());

        return Choice.fromDetails(result);
    }


    /**
     * Creates a {@link ChoiceSet}.
     * 
     * @param owner
     *            the {@link Node} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> createChoiceSet(final Node owner) {

        return this.createChoiceSet(Utils.windowOf(owner));
    }

    /**
     * Creates a {@link ChoiceSet}.
     * 
     * @param owner
     *            the {@link Window} that triggered the creation.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> createChoiceSet(final Window owner) {

        DomainObjectDialog<ChoiceSet> dlg = new DomainObjectDialog<>(owner
                .getScene().getWindow(), "Create Choice Set", new ChoiceSet(
                "New Choice Set"));

        Optional<ChoiceSet> result = dlg.showAndWait();

        if (result.isPresent()) {

            ChoiceSet choiceSet = result.get();
            ChoiceSet createdChoiceSet = null;

            try {

                createdChoiceSet = this.createChoiceSet(choiceSet);
            }
            catch (RequestFailedException | RequestDeniedException e) {

                Utils.showExceptionDialog(owner, "Creating Choice Set Failed!",
                        "An exception occurred when attempting to create choice set "
                                + choiceSet + "!", e);
            }

            return Optional.ofNullable(createdChoiceSet);
        }

        return Optional.empty();
    }

    /**
     * Creates a new {@link ChoiceSet}.
     * 
     * @param choiceSet
     *            the new {@link ChoiceSet}.
     * @return the created {@link ChoiceSet}.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     */
    private ChoiceSet createChoiceSet(ChoiceSet choiceSet)
            throws RequestFailedException, RequestDeniedException {

        ChoiceSetDetails result = this.surveyService.createChoiceSet(choiceSet
                .toDTO());

        return ChoiceSet.fromDetails(result);
    }

    /**
     * Retrieves a {@link ChoiceSet}.
     * 
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<List<ChoiceSet>> retrieveAllChoiceSets(final Node owner) {

        return this.retrieveAllChoiceSets(Utils.windowOf(owner));
    }

    /**
     * Retrieves a {@link ChoiceSet}.
     * 
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<List<ChoiceSet>> retrieveAllChoiceSets(final Window owner) {

        List<ChoiceSet> retrievedChoiceSets = null;

        try {

            retrievedChoiceSets = this.retrieveAllChoiceSets();
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Choice Sets Failed!",
                    "An exception occurred when attempting to retrieve all choice sets!",
                    e);
        }

        return Optional.ofNullable(retrievedChoiceSets);
    }


    /**
     * Retrieves all choiceSets.
     * 
     * @return a {@link List} of all choiceSets.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private List<ChoiceSet> retrieveAllChoiceSets()
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        List<ChoiceSetDetails> result = this.surveyService
                .retrieveAllChoiceSets();

        return ChoiceSet.fromDetails(result);
    }

    /**
     * Retrieves a {@link ChoiceSet}.
     * 
     * @param id
     *            the id of the {@link ChoiceSet} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> retrieveChoiceSet(final int id, final Node owner) {

        return this.retrieveChoiceSet(id, Utils.windowOf(owner));
    }


    /**
     * Retrieves a {@link ChoiceSet}.
     * 
     * @param id
     *            the id of the {@link ChoiceSet} to retrieve.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> retrieveChoiceSet(final int id,
            final Window owner) {

        ChoiceSet retrievedChoiceSet = null;

        try {

            retrievedChoiceSet = this.retrieveChoiceSet(id);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Choice Set Failed!",
                    "An exception occurred when attempting to retrieve choice set with id "
                            + id + "!", e);
        }

        return Optional.ofNullable(retrievedChoiceSet);
    }

    /**
     * Retrieves the choice set with the provided id.
     * 
     * @param id
     *            the id.
     * @return the choice set with the provided id.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private ChoiceSet retrieveChoiceSet(int id)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {


        ChoiceSetDetails result = this.surveyService.retrieveChoiceSet(id);

        return ChoiceSet.fromDetails(result);
    }


    /**
     * Retrieves a {@link ChoiceSet}.
     * 
     * @param name
     *            the name of the {@link ChoiceSet} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> retrieveChoiceSetWithName(final String name,
            final Node owner) {

        return this.retrieveChoiceSetWithName(name, Utils.windowOf(owner));
    }

    /**
     * Retrieves a {@link ChoiceSet}.
     * 
     * @param name
     *            the name of the {@link ChoiceSet} to retrieve.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> retrieveChoiceSetWithName(final String name,
            final Window owner) {

        ChoiceSet retrievedChoiceSet = null;

        try {

            retrievedChoiceSet = this.retrieveChoiceSetWithName(name);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Choice Set Failed!",
                    "An exception occurred when attempting to retrieve choice set with name "
                            + name + "!", e);
        }

        return Optional.ofNullable(retrievedChoiceSet);
    }

    /**
     * Retrieves the choice set with the provided name.
     * 
     * @param name
     *            the name.
     * @return the choice set with the provided name.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private ChoiceSet retrieveChoiceSetWithName(String name)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ChoiceSetDetails result = this.surveyService
                .retrieveChoiceSetByName(name);

        return ChoiceSet.fromDetails(result);
    }


    /**
     * Updates a {@link ChoiceSet}.
     * 
     * @param choiceSet
     *            the {@link ChoiceSet} to update.
     * @param owner
     *            the {@link Node} that triggered the update.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> updateChoiceSet(final ChoiceSet choiceSet,
            final Node owner) {

        return this.updateChoiceSet(choiceSet, Utils.windowOf(owner));
    }


    /**
     * Updates a {@link ChoiceSet}.
     * 
     * @param choiceSet
     *            the {@link ChoiceSet} to update.
     * @param owner
     *            the {@link Window} that triggered the update.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> updateChoiceSet(final ChoiceSet choiceSet,
            final Window owner) {

        ChoiceSet updatedChoiceSet = null;

        try {

            updatedChoiceSet = this.updateChoiceSet(choiceSet);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Updating Choice Set Failed!",
                    "An exception occurred when attempting to update choice set "
                            + choiceSet + "!", e);
        }

        return Optional.ofNullable(updatedChoiceSet);
    }


    /**
     * Updates the choice set.
     * 
     * @param choiceSet
     *            the choice set to update.
     * @return the updated choice set.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private ChoiceSet updateChoiceSet(ChoiceSet choiceSet)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ChoiceSetDetails result = this.surveyService.updateChoiceSet(choiceSet
                .toDTO());

        return ChoiceSet.fromDetails(result);
    }



    /**
     * Deletes a {@link ChoiceSet}.
     * 
     * @param choiceSet
     *            the {@link ChoiceSet} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> deleteChoiceSet(final ChoiceSet choiceSet,
            final Node owner) {

        return this.deleteChoiceSet(choiceSet, Utils.windowOf(owner));
    }


    /**
     * Deletes a {@link ChoiceSet}.
     * 
     * @param choiceSet
     *            the {@link ChoiceSet} to remove.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link ChoiceSet}{@code >}
     *         containing the result of the action.
     */
    public Optional<ChoiceSet> deleteChoiceSet(final ChoiceSet choiceSet,
            final Window owner) {

        ChoiceSet deletedChoiceSet = null;

        if (Utils.confirmDeletion(choiceSet, owner)) {

            try {

                deletedChoiceSet = this.deleteChoiceSet(choiceSet);
            }
            catch (RequestFailedException | RequestDeniedException
                    | ObjectNotFoundException e) {

                Utils.showExceptionDialog(owner, "Deleting Choice Set Failed!",
                        "An exception occurred when attempting to delete choice set "
                                + choiceSet + "!", e);
            }
        }

        return Optional.ofNullable(deletedChoiceSet);
    }

    /**
     * Removes the choice set.
     * 
     * @param choiceSet
     *            the choice set to remove.
     * @return the removed choice.
     * 
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    private ChoiceSet deleteChoiceSet(ChoiceSet choiceSet)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ChoiceSetDetails result = this.surveyService.deleteChoiceSet(choiceSet
                .getId());

        return ChoiceSet.fromDetails(result);
    }
}
