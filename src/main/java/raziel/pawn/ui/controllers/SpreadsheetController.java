package raziel.pawn.ui.controllers;


import java.util.List;
import java.util.Optional;

import javafx.scene.Node;
import javafx.stage.Window;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.data.SpreadsheetCell;
import raziel.pawn.core.services.SpreadsheetService;
import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.data.SpreadsheetDTO;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.controls.dialog.DomainObjectDialog;


/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 *
 */
@Controller
public class SpreadsheetController {

    private final SpreadsheetService spreadsheetService;



    /**
     * Creates a new instance of the {@link SpreadsheetController} class.
     * 
     * @param spreadsheetService
     *            the spreadsheet service.
     */
    @Autowired
    public SpreadsheetController(SpreadsheetService spreadsheetService) {

        this.spreadsheetService = spreadsheetService;
    }


    /**
     * Persists a {@link Spreadsheet}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to persist.
     * @param owner
     *            the calling {@link Node}.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> persistSpreadsheet(
            final Spreadsheet spreadsheet, final Node owner) {

        return this.persistSpreadsheet(spreadsheet, Utils.windowOf(owner));
    }

    /**
     * Persists a {@link Spreadsheet}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to persist.
     * @param owner
     *            the calling {@link Window}.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> persistSpreadsheet(
            final Spreadsheet spreadsheet, Window owner) {

        Spreadsheet createdSpreadsheet = null;

        try {

            createdSpreadsheet = this.createSpreadsheet(spreadsheet);
        }
        catch (RequestFailedException | RequestDeniedException e) {

            Utils.showExceptionDialog(owner, "Creating Spreadsheet Failed!",
                    "An exception occurred when attempting to create Spreadsheet "
                            + spreadsheet + "!", e);
        }

        return Optional.ofNullable(createdSpreadsheet);
    }


    /**
     * Creates a new spreadsheet.
     * 
     * @param owner
     *            the calling {@link Node}.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> createSpreadsheet(final Node owner) {

        return this.createSpreadsheet(Utils.windowOf(owner));
    }

    /**
     * Creates a new spreadsheet.
     * 
     * @param owner
     *            the calling {@link Window}.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> createSpreadsheet(Window owner) {

        DomainObjectDialog<Spreadsheet> dlg = new DomainObjectDialog<>(owner
                .getScene().getWindow(), "Create Spreadsheet", new Spreadsheet(
                "New Spreadsheet"));

        Optional<Spreadsheet> result = dlg.showAndWait();

        if (result.isPresent()) {

            Spreadsheet spreadsheet = result.get();
            Spreadsheet createdSpreadsheet = null;

            try {

                createdSpreadsheet = this.createSpreadsheet(spreadsheet);
            }
            catch (RequestFailedException | RequestDeniedException e) {

                Utils.showExceptionDialog(owner,
                        "Creating Spreadsheet Failed!",
                        "An exception occurred when attempting to create Spreadsheet "
                                + spreadsheet + "!", e);
            }

            return Optional.ofNullable(createdSpreadsheet);
        }

        return Optional.empty();
    }

    private Spreadsheet createSpreadsheet(Spreadsheet spreadsheet)
            throws RequestFailedException, RequestDeniedException {

        SpreadsheetDTO result = this.spreadsheetService
                .createSpreadsheet(spreadsheet.toDTO());

        return Spreadsheet.fromDTO(result);
    }


    /**
     * Retrieves all {@link Spreadsheet Spreadsheets}.
     * 
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<List<Spreadsheet>> retrieveAllSpreadsheets(final Node owner) {

        return this.retrieveAllSpreadsheets(Utils.windowOf(owner));
    }

    /**
     * Retrieves all {@link Spreadsheet Spreadsheets}.
     * 
     * @param owner
     *            the calling {@link Window}.
     * @return the {@link Optional}{@code <}{@link List}{@code <}
     *         {@link Spreadsheet} {@code >>} containing the result of the
     *         action.
     */
    public Optional<List<Spreadsheet>> retrieveAllSpreadsheets(
            final Window owner) {

        List<Spreadsheet> retrievedSpreadsheets = null;

        try {

            retrievedSpreadsheets = this.retrieveAllSpreadsheets();
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Spreadsheets Failed!",
                    "An exception occurred when attempting to retrieve all spreadsheets!",
                    e);
        }

        return Optional.ofNullable(retrievedSpreadsheets);
    }

    private List<Spreadsheet> retrieveAllSpreadsheets()
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        List<SpreadsheetDTO> result = this.spreadsheetService
                .retrieveAllSpreadsheets();

        return Spreadsheet.fromDTO(result);
    }


    /**
     * Retrieves a {@link Spreadsheet}.
     * 
     * @param id
     *            the id of the {@link Spreadsheet} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> retrieveSpreadsheet(final int id,
            final Node owner) {

        return this.retrieveSpreadsheet(id, Utils.windowOf(owner));
    }

    /**
     * Retrieves a {@link Spreadsheet}.
     * 
     * @param id
     *            the id of the {@link Spreadsheet} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> retrieveSpreadsheet(final int id,
            final Window owner) {

        Spreadsheet retrievedSpreadsheet = null;

        try {

            retrievedSpreadsheet = this.retrieveSpreadsheet(id);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Spreadsheet Failed!",
                    "An exception occurred when attempting to retrieve Spreadsheet with id "
                            + id + "!", e);
        }

        return Optional.ofNullable(retrievedSpreadsheet);
    }


    private Spreadsheet retrieveSpreadsheet(int id)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SpreadsheetDTO result = this.spreadsheetService.retrieveSpreadsheet(id);

        return Spreadsheet.fromDTO(result);
    }



    /**
     * Updates a {@link Spreadsheet}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> updateSpreadsheet(
            final Spreadsheet spreadsheet, final Node owner) {

        return this.updateSpreadsheet(spreadsheet, Utils.windowOf(owner));
    }

    /**
     * Updates a {@link Spreadsheet}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> updateSpreadsheet(
            final Spreadsheet spreadsheet, final Window owner) {

        Spreadsheet updatedSpreadsheet = null;

        try {

            updatedSpreadsheet = this.updateSpreadsheet(spreadsheet);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Updating Spreadsheet  Failed!",
                    "An exception occurred when attempting to update Spreadsheet set "
                            + spreadsheet + "!", e);
        }

        return Optional.ofNullable(updatedSpreadsheet);
    }


    private Spreadsheet updateSpreadsheet(Spreadsheet spreadsheet)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SpreadsheetDTO result = this.spreadsheetService
                .updateSpreadsheet(spreadsheet.toDTO());

        return Spreadsheet.fromDTO(result);
    }


    /**
     * Deletes a {@link Spreadsheet}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> deleteSpreadsheet(
            final Spreadsheet spreadsheet, final Node owner) {

        return this.deleteSpreadsheet(spreadsheet, Utils.windowOf(owner));
    }

    /**
     * Deletes a {@link Spreadsheet}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Spreadsheet}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> deleteSpreadsheet(
            final Spreadsheet spreadsheet, final Window owner) {

        if (spreadsheet != null) {
            Spreadsheet deletedSpreadsheet = null;

            if (Utils.confirmDeletion(spreadsheet, owner)) {

                try {

                    deletedSpreadsheet = this.deleteSpreadsheet(spreadsheet);
                }
                catch (RequestFailedException | RequestDeniedException
                        | ObjectNotFoundException e) {

                    Utils.showExceptionDialog(owner,
                            "Deleting Spreadsheet Failed!",
                            "An exception occurred when attempting to delete set "
                                    + spreadsheet + "!", e);
                }
            }

            return Optional.ofNullable(deletedSpreadsheet);
        }

        return Optional.empty();
    }

    private Spreadsheet deleteSpreadsheet(Spreadsheet spreadsheet)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        SpreadsheetDTO result = this.spreadsheetService
                .deleteSpreadsheet(spreadsheet.toDTO());

        return Spreadsheet.fromDTO(result);
    }


    /**
     * Creates a new spreadsheetCell.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to contain the {@link SpreadsheetCell}
     *            .
     * @param owner
     *            the calling {@link Node}.
     * @return the {@link Optional}{@code <}{@link SpreadsheetCell}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> createSpreadsheetCell(
            final Spreadsheet spreadsheet, final Node owner) {

        return this.createSpreadsheetCell(spreadsheet, Utils.windowOf(owner));
    }

    /**
     * Creates a new spreadsheetCell.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to contain the {@link SpreadsheetCell}
     *            .
     * @param owner
     *            the calling {@link Window}.
     * @return the {@link Optional}{@code <}{@link SpreadsheetCell}{@code >}
     *         containing the result of the action.
     */
    public Optional<Spreadsheet> createSpreadsheetCell(
            final Spreadsheet spreadsheet, Window owner) {

        // TODO: create spreadsheet cell dialog.
        // DomainObjectDialog<Spreadsheet> dlg = new DomainObjectDialog<>(owner
        // .getScene().getWindow(), "Create SpreadsheetCell", new Spreadsheet(
        // row, column, null));
        //
        // Optional<Spreadsheet> result = dlg.showAndWait();
        //
        // if (result.isPresent()) {
        //
        // Spreadsheet spreadsheetCell = result.get();
        // Spreadsheet createdSpreadsheetCell = null;
        //
        // try {
        //
        // createdSpreadsheetCell = this.createSpreadsheetCell(spreadsheetCell);
        // }
        // catch (RequestFailedException | RequestDeniedException e) {
        //
        // Utils.showExceptionDialog(owner, "Creating SpreadsheetCell Failed!",
        // "An exception occurred when attempting to create SpreadsheetCell "
        // + spreadsheetCell + "!", e);
        // }
        //
        // return Optional.ofNullable(createdSpreadsheetCell);
        // }

        return Optional.empty();
    }


    private SpreadsheetCell createSpreadsheetCell(
            SpreadsheetCell spreadsheetCell)
            throws RequestFailedException, RequestDeniedException {

        return SpreadsheetCell.fromDTO(this.spreadsheetService
                .createSpreadsheetCell(spreadsheetCell.toDTO()));
    }


    /**
     * Updates a {@link SpreadsheetCell}.
     * 
     * @param spreadsheetCell
     *            the {@link SpreadsheetCell} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link SpreadsheetCell}{@code >}
     *         containing the result of the action.
     */
    public Optional<SpreadsheetCell> updateSpreadsheetCell(
            final SpreadsheetCell spreadsheetCell, final Node owner) {

        return this.updateSpreadsheetCell(spreadsheetCell,
                Utils.windowOf(owner));
    }

    /**
     * Updates a {@link SpreadsheetCell}.
     * 
     * @param spreadsheetCell
     *            the {@link SpreadsheetCell} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link SpreadsheetCell}{@code >}
     *         containing the result of the action.
     */
    public Optional<SpreadsheetCell> updateSpreadsheetCell(
            final SpreadsheetCell spreadsheetCell, final Window owner) {

        SpreadsheetCell updatedSpreadsheetCell = null;

        try {

            updatedSpreadsheetCell = this
                    .updateSpreadsheetCell(spreadsheetCell);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner,
                    "Updating SpreadsheetCell  Failed!",
                    "An exception occurred when attempting to update SpreadsheetCell set "
                            + spreadsheetCell + "!", e);
        }

        return Optional.ofNullable(updatedSpreadsheetCell);
    }


    private SpreadsheetCell updateSpreadsheetCell(
            SpreadsheetCell spreadsheetCell)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        return SpreadsheetCell.fromDTO(this.spreadsheetService
                .updateSpreadsheetCell(spreadsheetCell.toDTO()));
    }


    /**
     * Deletes a {@link SpreadsheetCell}.
     * 
     * @param spreadsheetCell
     *            the {@link SpreadsheetCell} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link SpreadsheetCell}{@code >}
     *         containing the result of the action.
     */
    public Optional<SpreadsheetCell> deleteSpreadsheetCell(
            final SpreadsheetCell spreadsheetCell, final Node owner) {

        return this.deleteSpreadsheetCell(spreadsheetCell,
                Utils.windowOf(owner));
    }

    /**
     * Deletes a {@link SpreadsheetCell}.
     * 
     * @param spreadsheetCell
     *            the {@link SpreadsheetCell} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link SpreadsheetCell}{@code >}
     *         containing the result of the action.
     */
    public Optional<SpreadsheetCell> deleteSpreadsheetCell(
            final SpreadsheetCell spreadsheetCell, final Window owner) {

        if (spreadsheetCell != null) {

            SpreadsheetCell deletedSpreadsheetCell = null;

            if (Utils.confirmDeletion(spreadsheetCell, owner)) {

                try {

                    deletedSpreadsheetCell = this
                            .deleteSpreadsheetCell(spreadsheetCell);
                }
                catch (RequestFailedException | RequestDeniedException
                        | ObjectNotFoundException e) {

                    Utils.showExceptionDialog(owner,
                            "Deleting SpreadsheetCell Failed!",
                            "An exception occurred when attempting to delete set "
                                    + spreadsheetCell + "!", e);
                }
            }

            return Optional.ofNullable(deletedSpreadsheetCell);
        }

        return Optional.empty();
    }


    private SpreadsheetCell deleteSpreadsheetCell(
            SpreadsheetCell spreadsheetCell)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        if (spreadsheetCell != null) {

            SpreadsheetCellDTO result = this.spreadsheetService
                    .deleteSpreadsheetCell(spreadsheetCell.toDTO());


            return SpreadsheetCell.fromDTO(result);
        }

        return null;
    }
}
