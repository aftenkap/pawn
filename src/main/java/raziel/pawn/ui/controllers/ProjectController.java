package raziel.pawn.ui.controllers;


import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javafx.scene.Node;
import javafx.stage.Window;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import raziel.pawn.core.domain.Project;
import raziel.pawn.core.domain.data.IDataSource;
import raziel.pawn.core.domain.metadata.IMetadataSource;
import raziel.pawn.core.domain.modeling.DataModel;
import raziel.pawn.core.services.ProjectService;
import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.ProjectDetails;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.controls.dialog.DomainObjectDialog;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Controller
public class ProjectController {

    private final ProjectService projectService;

    /**
     * Creates a new instance of the {@link ProjectController} class.
     * 
     * @param projectService
     *            the project service.
     */
    @Autowired
    public ProjectController(ProjectService projectService) {

        this.projectService = projectService;
    }


    /**
     * Creates a new project.
     * 
     * @param owner
     *            the calling {@link Node}.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> createProject(final Node owner) {

        return this.createProject(Utils.windowOf(owner));
    }

    /**
     * Creates a new project.
     * 
     * @param owner
     *            the calling {@link Window}.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> createProject(Window owner) {

        DomainObjectDialog<Project> dlg = new DomainObjectDialog<>(owner
                .getScene().getWindow(), "Create Project", new Project(
                "New Project"));

        Optional<Project> result = dlg.showAndWait();

        if (result.isPresent()) {

            Project project = result.get();
            Project createdProject = null;

            try {

                createdProject = this.createProject(project);
            }
            catch (RequestFailedException | RequestDeniedException e) {

                Utils.showExceptionDialog(owner, "Creating Project Failed!",
                        "An exception occurred when attempting to create Project "
                                + project + "!", e);
            }

            return Optional.ofNullable(createdProject);
        }

        return Optional.empty();
    }

    private Project createProject(Project project)
            throws RequestFailedException, RequestDeniedException {

        return Project.fromDetails(this.projectService.createProject(project
                .toDTO()));
    }


    /**
     * Retrieves all {@link Project Projects}.
     * 
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<List<Project>> retrieveAllProjects(final Node owner) {

        return this.retrieveAllProjects(Utils.windowOf(owner));
    }

    /**
     * Retrieves all {@link Project Projects}.
     * 
     * @param owner
     *            the calling {@link Window}.
     * @return the {@link Optional}{@code <}{@link List}{@code <}{@link Project}
     *         {@code >>} containing the result of the action.
     */
    public Optional<List<Project>> retrieveAllProjects(final Window owner) {

        List<Project> retrievedProjects = null;

        try {

            retrievedProjects = this.retrieveAllProjects();
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Projects Failed!",
                    "An exception occurred when attempting to retrieve all projects!",
                    e);
        }

        return Optional.ofNullable(retrievedProjects);
    }

    private List<Project> retrieveAllProjects()
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        List<ProjectDetails> result = this.projectService.retrieveAllProjects();

        List<Project> projects = Project.fromDetails(result);

        return projects;
    }


    /**
     * Retrieves a {@link Project}.
     * 
     * @param id
     *            the id of the {@link Project} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> retrieveProject(final int id, final Node owner) {

        return this.retrieveProject(id, Utils.windowOf(owner));
    }

    /**
     * Retrieves a {@link Project}.
     * 
     * @param id
     *            the id of the {@link Project} to retrieve.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> retrieveProject(final int id, final Window owner) {

        Project retrievedProject = null;

        try {

            retrievedProject = this.retrieveProject(id);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Project Failed!",
                    "An exception occurred when attempting to retrieve Project with id "
                            + id + "!", e);
        }

        return Optional.ofNullable(retrievedProject);
    }

    private Project retrieveProject(final int id)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ProjectDetails result = this.projectService.retrieveProject(id);

        Project referenceProject = Project.fromDetails(result);

        return referenceProject;
    }



    /**
     * Retrieves the {@link Project} containing the provided {@link IDataSource
     * DataSource} .
     * 
     * @param dataSource
     *            the {@link IDataSource DataSource}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> retrieveProjectContainingDataSource(
            final IDataSource dataSource, final Node owner) {

        return this.retrieveProjectContainingDataSource(dataSource,
                Utils.windowOf(owner));
    }

    /**
     * Retrieves the {@link Project} containing the provided {@link IDataSource
     * DataSource} .
     * 
     * @param dataSource
     *            the {@link IDataSource DataSource}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> retrieveProjectContainingDataSource(
            final IDataSource dataSource, final Window owner) {

        Project retrievedProject = null;

        try {

            retrievedProject = this
                    .retrieveProjectContainingDataSource(dataSource);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Project Failed!",
                    "An exception occurred when attempting to retrieve project for dataSource + "
                            + dataSource + "!", e);
        }

        return Optional.ofNullable(retrievedProject);
    }

    private Project retrieveProjectContainingDataSource(
            final IDataSource dataSource)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ProjectDetails result = this.projectService
                .retrieveProjectForDataSource(dataSource.getId());

        return Project.fromDetails(result);
    }


    /**
     * Retrieves the {@link Project} containing the provided
     * {@link IMetadataSource MetaDataSource} .
     * 
     * @param metaDataSource
     *            the {@link IMetadataSource MetadataSource}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> retrieveProjectContainingMetadataSource(
            final IMetadataSource metaDataSource, final Node owner) {

        return this.retrieveProjectContainingMetadataSource(metaDataSource,
                Utils.windowOf(owner));
    }

    /**
     * Retrieves the {@link Project} containing the provided
     * {@link IMetadataSource MetadataSource} .
     * 
     * @param metaDataSource
     *            the {@link IMetadataSource MetadataSource}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> retrieveProjectContainingMetadataSource(
            final IMetadataSource metaDataSource, final Window owner) {

        Project retrievedProject = null;

        try {

            retrievedProject = this
                    .retrieveProjectContainingMetadataSource(metaDataSource);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(
                    owner,
                    "Retrieving Project Failed!",
                    "An exception occurred when attempting to retrieve project for metaDataSource + "
                            + metaDataSource + "!", e);
        }

        return Optional.ofNullable(retrievedProject);
    }

    private Project retrieveProjectContainingMetadataSource(
            final IMetadataSource metaDataSource)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ProjectDetails result = this.projectService
                .retrieveProjectForMetadataSource(metaDataSource.getId());

        return Project.fromDetails(result);
    }

    /**
     * Retrieves the {@link Project} containing the provided {@link DataModel}.
     * 
     * @param dataModel
     *            the {@link DataModel}.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> retrieveProjectContainingDataModel(
            final DataModel dataModel, final Node owner) {

        return this.retrieveProjectContainingDataModel(dataModel,
                Utils.windowOf(owner));
    }

    /**
     * Retrieves the {@link Project} containing the provided {@link DataModel} .
     * 
     * @param dataModel
     *            the {@link DataModel}.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> retrieveProjectContainingDataModel(
            final DataModel dataModel, final Window owner) {

        Project retrievedProject = null;

        try {

            retrievedProject = this
                    .retrieveProjectContainingDataModel(dataModel);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Retrieving Project Failed!",
                    "An exception occurred when attempting to retrieve project for dataModel + "
                            + dataModel + "!", e);
        }

        return Optional.ofNullable(retrievedProject);
    }

    private Project retrieveProjectContainingDataModel(final DataModel dataModel)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        ProjectDetails result = this.projectService
                .retrieveProjectForDataModel(dataModel.getId());

        return Project.fromDetails(result);
    }

    /**
     * Updates a {@link Project}.
     * 
     * @param Project
     *            the {@link Project} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> updateProject(final Project Project,
            final Node owner) {

        return this.updateProject(Project, Utils.windowOf(owner));
    }

    /**
     * Updates a {@link Project}.
     * 
     * @param Project
     *            the {@link Project} to update.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> updateProject(final Project Project,
            final Window owner) {

        Project updatedProject = null;

        try {

            updatedProject = this.updateProject(Project);
        }
        catch (RequestFailedException | RequestDeniedException
                | ObjectNotFoundException e) {

            Utils.showExceptionDialog(owner, "Updating Project  Failed!",
                    "An exception occurred when attempting to update Project set "
                            + Project + "!", e);
        }

        return Optional.ofNullable(updatedProject);
    }


    private Project updateProject(Project project)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        return Project.fromDetails(this.projectService.updateProject(project
                .toDTO()));
    }

    /**
     * Deletes a {@link Project}.
     * 
     * @param project
     *            the {@link Project} to remove.
     * @param owner
     *            the {@link Node} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> deleteProject(final Project project,
            final Node owner) {

        return this.deleteProject(project, Utils.windowOf(owner));
    }

    /**
     * Deletes a {@link Project}.
     * 
     * @param project
     *            the {@link Project} to remove.
     * @param owner
     *            the {@link Window} that triggered the retrieval.
     * @return the {@link Optional}{@code <}{@link Project}{@code >} containing
     *         the result of the action.
     */
    public Optional<Project> deleteProject(final Project project,
            final Window owner) {

        Project deletedProject = null;

        if (Utils.confirmDeletion(project, owner)) {

            try {

                deletedProject = this.deleteProject(project);
            }
            catch (RequestFailedException | RequestDeniedException
                    | ObjectNotFoundException e) {

                Utils.showExceptionDialog(owner, "Deleting Project Failed!",
                        "An exception occurred when attempting to delete set "
                                + project + "!", e);
            }
        }

        return Optional.ofNullable(deletedProject);
    }


    private Project deleteProject(final Project project)
            throws RequestFailedException, RequestDeniedException,
            ObjectNotFoundException {

        Objects.nonNull(project);

        ProjectDetails result = this.projectService.deleteProject(project
                .getId());

        Project removedProject = Project.fromDetails(result);

        return removedProject;
    }
}
