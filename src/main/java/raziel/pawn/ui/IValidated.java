package raziel.pawn.ui;


import org.controlsfx.validation.ValidationSupport;


/**
 * The {@code IValidated} interface provides the shared contract for validated
 * interface elements.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public interface IValidated {

    /**
     * Returns the {@link ValidationSupport} of the interface element.
     * 
     * @return the {@link ValidationSupport} of the interface element.
     */
    public abstract ValidationSupport validationSupport();
}
