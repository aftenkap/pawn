package raziel.pawn.ui;


import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;


/**
 * @author Peter J. Radics
 * @version 0.0.1
 * @since0.0.1
 *
 */
public class Helloworld
        extends Application {

    /**
     * @param args
     *            unused
     */
    public static void main(String[] args) {

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        String url = "file:///Users/peter/Dropbox/Research/Data/COPS/Focus%20Groups/fgd%201%20rackspace/audio.m4a";
        // create media player
        Media media = new Media(url);

        System.out.println("Source URI: " + media.getSource());
        System.out.println("Duration: " + media.getDuration());
        System.out.println("Metadata: " + media.getMetadata());
        System.out.println("Markers: " + media.getMarkers());
        System.out.println("Tracks: " + media.getTracks());

        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);

        System.out.println("Start Time: " + mediaPlayer.getStartTime());
        System.out.println("End Time: " + mediaPlayer.getStopTime());

        System.out.println("Duration: " + media.getDuration());



        primaryStage.setTitle("Embedded Media Player");
        Group root = new Group();
        Scene scene = new Scene(root, 540, 210);

        // create mediaView and add media player to the viewer
        MediaView mediaView = new MediaView(mediaPlayer);
        ((Group) scene.getRoot()).getChildren().add(mediaView);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}