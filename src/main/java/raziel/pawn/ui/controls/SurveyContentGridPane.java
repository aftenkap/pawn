package raziel.pawn.ui.controls;


import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.Survey;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public abstract class SurveyContentGridPane
        extends GridPane {

    private static Logger                LOG = LoggerFactory
                                                     .getLogger(SurveyContentGridPane.class);

    private final ObjectProperty<Survey> surveyProperty;

    /**
     * Returns the {@link Survey} property.
     * 
     * @return the {@link Survey} property.
     */
    public ObjectProperty<Survey> surveyProperty() {

        return this.surveyProperty;
    }

    /**
     * Returns the value of the {@link Survey} property.
     * 
     * @return the value of the {@link Survey} property.
     */
    public Survey getSurvey() {

        return this.surveyProperty.get();
    }

    /**
     * Sets the value of the {@link Survey} property.
     * 
     * @param survey
     *            the value of the {@link Survey} property.
     */
    public void setSurvey(final Survey survey) {

        this.surveyProperty.set(survey);
    }

    /**
     * Creates a new instance of the {@link SurveyContentGridPane} class.
     * 
     * @param survey
     *            the initial value for the {@link Survey} property.
     */
    public SurveyContentGridPane(final Survey survey) {

        LOG.debug("Creating survey content grid pane.");
        this.setPadding(new Insets(10, 10, 10, 10));
        this.setHgap(20);
        this.setVgap(10);

        this.surveyProperty = new SimpleObjectProperty<>(survey);

        this.setupEventHandlers();
    }

    private void setupEventHandlers() {

        this.surveyProperty.addListener((observable) -> {

            LOG.debug("Invalidation event received.");
            this.refreshView();
        });
    }

    /**
     * Refreshes the {@link Survey} from the database.
     */
    protected void refreshSurvey() {


        ApplicationContext.surveyController()
                .retrieveSurvey(this.getSurvey().getId(), this)
                .ifPresent(survey -> {

                    this.setSurvey(null);
                    this.setSurvey(survey);
                });
    }

    /**
     * Updates the {@link Survey} in the database.
     */
    protected void updateSurvey() {

        ApplicationContext.surveyController()
                .updateSurvey(this.getSurvey(), this).ifPresent(survey -> {

                    this.setSurvey(null);
                    this.setSurvey(survey);
                });
    }

    /**
     * Refreshes the view.
     */
    protected abstract void refreshView();
}
