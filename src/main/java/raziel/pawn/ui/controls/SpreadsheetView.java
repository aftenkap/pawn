package raziel.pawn.ui.controls;


import impl.org.controlsfx.spreadsheet.CellView;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TablePosition;

import org.controlsfx.control.spreadsheet.GridBase;
import org.controlsfx.control.spreadsheet.SpreadsheetCellType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.data.SpreadsheetCell;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 *
 */
public class SpreadsheetView
        extends org.controlsfx.control.spreadsheet.SpreadsheetView {

    private static Logger                     LOG = LoggerFactory
                                                          .getLogger(SpreadsheetView.class);

    private final ObjectProperty<Spreadsheet> spreadsheet;

    /**
     * Returns the {@link Spreadsheet} property.
     * 
     * @return the {@link Spreadsheet} property.
     */
    public ObjectProperty<Spreadsheet> spreadsheetProperty() {

        return this.spreadsheet;
    }

    /**
     * Returns the value of the {@link Spreadsheet} property.
     * 
     * @return the value of the {@link Spreadsheet} property.
     */
    public Spreadsheet getSpreadsheet() {

        return this.spreadsheetProperty().get();
    }

    /**
     * Sets the value of the {@link Spreadsheet} property.
     * 
     * @param spreadsheet
     *            the value of the {@link Spreadsheet} property.
     */
    public void setSpreadsheet(final Spreadsheet spreadsheet) {

        this.spreadsheetProperty().set(spreadsheet);
    }


    /**
     * Returns the default context menu.
     * 
     * @return the default context menu.
     */
    public ContextMenu defaultContextMenu() {

        return super.getSpreadsheetViewContextMenu();
    }


    /**
     * Create a menu on rightClick with two options: Copy/Paste This can be
     * overridden by developers for custom behavior.
     * 
     * @return the context menu.
     */
    @Override
    public ContextMenu getSpreadsheetViewContextMenu() {

        return this.getContextMenu();
    }

    /**
     * Returns the selected Cell (or the first selected cell, should multiple
     * cells be selected).
     * 
     * @return the selected Cell (or the first selected cell, should multiple
     *         cells be selected).
     */
    public SpreadsheetCell getSelectedCell() {

        for (TablePosition<?, ?> cell : this.getSelectionModel()
                .getSelectedCells()) {

            return this.getSpreadsheet().getCell(cell.getRow(),
                    cell.getColumn());
        }

        return null;
    }

    /**
     * Returns the selected cells.
     * 
     * @return the selected cells.
     */
    public List<SpreadsheetCell> getSelectedCells() {

        List<SpreadsheetCell> cells = new ArrayList<>(this.getSelectionModel()
                .getSelectedCells().size());

        for (TablePosition<?, ?> cell : this.getSelectionModel()
                .getSelectedCells()) {

            cells.add(this.getSpreadsheet().getCell(cell.getRow(),
                    cell.getColumn()));
        }

        return cells;
    }

    /**
     * Creates a new instance of the {@link SpreadsheetView} class.
     * 
     * @param spreadsheet
     *            the initial {@link Spreadsheet}.
     */
    public SpreadsheetView(Spreadsheet spreadsheet) {

        super();
        LOG.debug("Creating spreadsheet spreadsheet view.");


        this.spreadsheet = new SimpleObjectProperty<>(spreadsheet);

        this.setContextMenu(super.getSpreadsheetViewContextMenu());

        this.setupEventHandlers();
        this.refreshView();
    }



    private void setupEventHandlers() {

        this.spreadsheetProperty().addListener(observable -> {

            this.refreshView();
        });

        this.contextMenuProperty().addListener(
                (observable, oldValue, newValue) -> {

                    CellView.getValue(() -> {

                        setContextMenu(newValue);
                    });
                });
    }


    /**
     * Refreshes the view.
     */
    protected void refreshView() {

        this.setGrid(null);
        if (this.getSpreadsheet() != null) {

            int rowCount = this.getSpreadsheet().rows();
            int columnCount = this.getSpreadsheet().columns();

            GridBase grid = new GridBase(rowCount, columnCount);

            ObservableList<ObservableList<org.controlsfx.control.spreadsheet.SpreadsheetCell>> rows = FXCollections
                    .observableArrayList();
            for (int row = 0; row < grid.getRowCount(); ++row) {

                final ObservableList<org.controlsfx.control.spreadsheet.SpreadsheetCell> list = FXCollections
                        .observableArrayList();
                for (int column = 0; column < grid.getColumnCount(); ++column) {

                    if (this.getSpreadsheet().getCell(row, column) != null) {

                        list.add(SpreadsheetCellType.STRING.createCell(row,
                                column, 1, 1,
                                this.getSpreadsheet().get(row, column)
                                        .toString()));
                    }
                    else {

                        list.add(SpreadsheetCellType.STRING.createCell(row,
                                column, 1, 1, ""));
                    }
                }
                rows.add(list);
            }
            grid.setRows(rows);

            this.setGrid(grid);
        }
    }
}
