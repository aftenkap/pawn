package raziel.pawn.ui.controls;


import java.util.Collection;
import java.util.LinkedList;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import org.jutility.javafx.control.labeled.LabeledComboBox;

import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @author Peter J. Radics
 * 
 */
public class QuestionBox
        extends GridPane {


    private final ObservableList<IQuestion>  questions;


    private final LabeledComboBox<IQuestion> comboBox;

    private final TextArea                   questionText;


    /**
     * Returns the question property.
     * 
     * @return the question property.
     */
    public ObservableList<IQuestion> questions() {

        return this.questions;
    }

    /**
     * Returns the selected item property.
     * 
     * @return the selected item property.
     */
    public ReadOnlyObjectProperty<IQuestion> selectedItemProperty() {

        return comboBox.getSelectionModel().selectedItemProperty();
    }

    /**
     * Returns the selected item.
     * 
     * @return the selected item.
     */
    public IQuestion getSelectedItem() {

        return this.comboBox.getSelectionModel().getSelectedItem();
    }

    /**
     * Creates a new instance of the {@link QuestionBox} class.
     */
    public QuestionBox() {

        this(null);
    }

    /**
     * Creates a new instance of the {@link QuestionBox} class with the provided
     * {@link IQuestion Questions}.
     * 
     * @param questions
     *            the {@link IQuestion Questions} to display.
     */
    public QuestionBox(Collection<IQuestion> questions) {

        this.questions = FXCollections
                .observableList(new LinkedList<IQuestion>());
        if (questions != null) {

            this.questions.addAll(questions);
        }

        this.comboBox = new LabeledComboBox<>("Questions");
        this.comboBox
                .setConverter(new DomainObjectStringConverter<IQuestion>());
        GridPane.setHgrow(comboBox, Priority.SOMETIMES);

        this.questionText = new TextArea();
        GridPane.setHgrow(questionText, Priority.SOMETIMES);
        GridPane.setVgrow(questionText, Priority.SOMETIMES);

        this.add(comboBox, 0, 0);
        this.add(questionText, 0, 1);

        Bindings.bindContent(this.comboBox.getItems(), this.questions);

        this.setUpEventHandlers();
    }

    private void setUpEventHandlers() {

        this.comboBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.questionText.clear();
                    if (newValue != null) {

                        this.questionText.setText(newValue.getQuestionText());
                    }
                });

    }
}
