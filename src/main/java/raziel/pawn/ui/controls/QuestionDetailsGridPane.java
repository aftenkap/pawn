package raziel.pawn.ui.controls;



import java.util.Arrays;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import org.jutility.javafx.control.ListViewWithSearchPanel;
import org.jutility.javafx.control.labeled.LabeledTextField;
import org.jutility.javafx.control.wrapper.TextAreaWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.ChoiceQuestion;
import raziel.pawn.core.domain.metadata.ChoiceSet;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.core.domain.metadata.RankingQuestion;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.core.domain.metadata.TextQuestion;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.controls.dialog.DomainObjectSelectionDialog;
import raziel.pawn.ui.controls.dialog.EditLongTextDialog;
import raziel.pawn.ui.controls.dialog.QuestionDialog;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * The {@code QuestionDetailsGridPane} class provides a visual representation of
 * a {@link IQuestion Question}.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class QuestionDetailsGridPane
        extends MetadataDetailsGridPane<IQuestion> {


    private static Logger                                 LOG = LoggerFactory
                                                                      .getLogger(QuestionDetailsGridPane.class);

    private final TextAreaWrapper                         questionTextArea;
    private final CheckBox                                requiredCheckBox;

    private final ChoiceSetDetailsGridPane                choiceSetDetails;
    private final ListViewWithSearchPanel<ChoiceQuestion> ranks;
    private final LabeledTextField                        otherResponse;

    private Action                                        editQuestionText;
    private Action                                        setChoiceSet;
    private Action                                        clearChoiceSet;

    private Action                                        createOtherResponse;
    private Action                                        editOtherResponse;
    private Action                                        clearOtherResponse;

    private Action                                        editRank;

    /**
     * Creates a new instance of the {@link QuestionDetailsGridPane} class.
     */
    public QuestionDetailsGridPane() {

        this(null, null);
    }


    /**
     * Creates a new instance of the {@link QuestionDetailsGridPane} class with
     * the provided {@link IQuestion Question}.
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param survey
     *            the {@link Survey} containing the {@link IQuestion Question}.
     */
    public QuestionDetailsGridPane(final IQuestion question, final Survey survey) {

        this(question, survey, false);
    }

    /**
     * Creates a new instance of the {@link QuestionDetailsGridPane} class with
     * the provided {@link IQuestion Question} and {@link Survey}. Also
     * specifies whether the update of the {@link IQuestion Question} in the
     * database should be deferred.
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param survey
     *            the {@link Survey} containing the {@link IQuestion Question}.
     * @param deferUpdate
     *            whether or not the update of the {@link IQuestion Question} in
     *            the database is deferred.
     */
    public QuestionDetailsGridPane(final IQuestion question,
            final Survey survey, final boolean deferUpdate) {

        super(question, survey, deferUpdate);

        ColumnConstraints textColumnWidthConstraint = new ColumnConstraints();
        textColumnWidthConstraint.setPercentWidth(50);
        ColumnConstraints choiceColumnWidthConstraint = new ColumnConstraints();
        choiceColumnWidthConstraint.setPercentWidth(25);
        ColumnConstraints rankingColumnWidthConstraint = new ColumnConstraints();
        rankingColumnWidthConstraint.setPercentWidth(25);

        this.getColumnConstraints().addAll(textColumnWidthConstraint,
                choiceColumnWidthConstraint, rankingColumnWidthConstraint);


        this.requiredCheckBox = new CheckBox("Required");
        GridPane.setVgrow(this.requiredCheckBox, Priority.NEVER);
        GridPane.setValignment(this.requiredCheckBox, VPos.TOP);


        this.questionTextArea = new TextAreaWrapper();
        this.questionTextArea.setWrapText(true);
        GridPane.setVgrow(this.questionTextArea, Priority.ALWAYS);


        this.choiceSetDetails = new ChoiceSetDetailsGridPane("Choice Set",
                new DomainObjectStringConverter<>());
        this.choiceSetDetails.setPadding(new Insets(0));
        this.choiceSetDetails.getLabel().setPadding(new Insets(0, 0, 10, 0));
        GridPane.setVgrow(this.choiceSetDetails, Priority.ALWAYS);

        this.ranks = new ListViewWithSearchPanel<>("Ranks",
                new DomainObjectStringConverter<>());
        this.ranks.getLabel().setPadding(new Insets(0, 0, 10, 0));
        GridPane.setVgrow(this.ranks, Priority.ALWAYS);


        this.otherResponse = new LabeledTextField("Other Response",
                Pos.TOP_CENTER);
        this.otherResponse.setEditable(false);
        this.otherResponse.setVgap(10);
        GridPane.setVgrow(this.otherResponse, Priority.ALWAYS);

        this.add(this.requiredCheckBox, 0, 1);
        this.add(this.questionTextArea, 0, 2);

        this.add(this.choiceSetDetails, 1, 0, 1, 3);
        this.add(this.ranks, 2, 0, 1, 3);

        this.questionTextArea.setEditable(false);

        this.choiceSetDetails.setVisible(false);
        this.ranks.setVisible(false);


        this.setupValidation();
        this.setupContextMenus();
        this.setupEventHandlers();

        this.refreshView();
    }

    private void setupEventHandlers() {

        this.updateDeferredProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {

                            if (newValue) {

                                this.questionTextArea.getContextMenu()
                                        .getItems().clear();
                            }
                            else {

                                this.questionTextArea
                                        .getContextMenu()
                                        .getItems()
                                        .add(ActionUtils
                                                .createMenuItem(this.editQuestionText));
                            }
                        });



        this.requiredCheckBox.selectedProperty().addListener(
                (observable, oldValue, newValue) -> {

                    IQuestion question = this.getMetadata();

                    if (newValue != null && question != null
                            && !newValue.equals(question.isAnswerRequired())) {

                        this.getMetadata().setAnswerRequired(newValue);

                        this.update();
                    }
                });


        this.questionTextArea.editableProperty().bind(
                this.updateDeferredProperty());

        this.questionTextArea.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    if (this.getMetadata() != null && newValue != null
                            && !newValue.equals(oldValue)) {

                        this.getMetadata().setQuestionText(newValue);
                    }
                });


        this.ranks
                .setOnMouseClicked((event) -> {

                    if (event.getClickCount() == 2) {

                        ChoiceQuestion rank = this.ranks.getSelectionModel()
                                .getSelectedItem();

                        if (rank != null) {

                            new QuestionDialog<>(this, "Edit Rank", rank)
                                    .showAndWait()
                                    .ifPresent(
                                            modifiedRank -> {

                                                ApplicationContext
                                                        .surveyController()
                                                        .updateQuestion(
                                                                modifiedRank,
                                                                this)
                                                        .ifPresent(
                                                                updatedRank -> {

                                                                    RankingQuestion rankingQuestion = (RankingQuestion) this
                                                                            .getMetadata();
                                                                    rankingQuestion
                                                                            .getRanks()
                                                                            .set(rankingQuestion
                                                                                    .getRanks()
                                                                                    .indexOf(
                                                                                            rank),
                                                                                    updatedRank);

                                                                    this.update();
                                                                });
                                            });

                        }
                    }
                });

    }

    private void setupContextMenus() {

        this.editQuestionText = new Action("Edit", (event) -> {

            this.editQuestionText();
        });


        this.questionTextArea.setContextMenu(ActionUtils
                .createContextMenu(Arrays.asList(this.editQuestionText)));


        this.setChoiceSet = new Action(
                "Set Choice Set",
                (event) -> {
                    ApplicationContext
                            .surveyController()
                            .retrieveAllChoiceSets(this)
                            .ifPresent(
                                    choiceSets -> {

                                        DomainObjectSelectionDialog<ChoiceSet> dialog = new DomainObjectSelectionDialog<>(
                                                this, "Select Choice Set",
                                                choiceSets);
                                        dialog.showAndWait()
                                                .ifPresent(
                                                        choiceSet -> {

                                                            LOG.debug("Setting Choice Set");

                                                            IQuestion question = QuestionDetailsGridPane.this
                                                                    .getMetadata();

                                                            if (question instanceof TextQuestion) {

                                                                LOG.error("TextQuestion! This indicates a bug in disabling controls");
                                                            }
                                                            else {

                                                                if (question instanceof ChoiceQuestion) {

                                                                    LOG.debug("ChoiceQuestion");
                                                                    ((ChoiceQuestion) question)
                                                                            .setChoices(choiceSet);
                                                                }
                                                                else if (question instanceof RankingQuestion) {

                                                                    ((RankingQuestion) question)
                                                                            .setChoices(choiceSet);
                                                                }

                                                                this.update();

                                                                if (this.isUpdateDeferred()) {

                                                                    this.refreshChoiceSetDetails(choiceSet);
                                                                    this.refreshRankings();
                                                                }
                                                            }
                                                        });
                                    });

                });

        this.clearChoiceSet = new Action("Remove Choice Set", (event) -> {

            IQuestion question = this.getMetadata();

            boolean modified = false;

            if (question instanceof ChoiceQuestion) {

                ChoiceQuestion choiceQuestion = ((ChoiceQuestion) question);

                if (Utils.confirmDeletionOfProperty(
                        choiceQuestion.getChoices(), choiceQuestion, this)) {

                    choiceQuestion.setChoices(null);
                    modified = true;
                }
            }
            else if (question instanceof RankingQuestion) {

                RankingQuestion rankingQuestion = ((RankingQuestion) question);
                if (Utils.confirmDeletionOfProperty(
                        rankingQuestion.getChoices(), rankingQuestion, this)) {

                    rankingQuestion.setChoices(null);
                    modified = true;
                }
            }

            if (modified) {

                this.update();

                if (this.isUpdateDeferred()) {

                    this.refreshChoiceSetDetails(null);
                }
            }
        });


        this.choiceSetDetails.contextMenuActions().clear();
        this.choiceSetDetails.contextMenuActions().addAll(this.setChoiceSet,
                this.clearChoiceSet);


        this.createOtherResponse = new Action("Create", actionEvent -> {

            TextQuestion otherResponse = new TextQuestion(this.getMetadata()
                    .getName() + " [Other]", this.getMetadata()
                    .getQuestionText() + " [Other]");

            ApplicationContext
                    .surveyController()
                    .createQuestion(otherResponse, this)
                    .ifPresent(
                            other -> {

                                ((ChoiceQuestion) this.getMetadata())
                                        .setAlternativeChoice(other);

                                this.update();
                            });

        });

        this.editOtherResponse = new Action(
                "Edit",
                actionEvent -> {

                    if (this.getMetadata() != null
                            && this.getMetadata() instanceof ChoiceQuestion) {

                        ChoiceQuestion choiceQuestion = (ChoiceQuestion) this
                                .getMetadata();

                        new QuestionDialog<>(this, "Edit Other Response",
                                choiceQuestion.getAlternativeChoice())
                                .showAndWait()
                                .ifPresent(
                                        editedOtherResponse -> {

                                            ApplicationContext
                                                    .surveyController()
                                                    .updateQuestion(
                                                            editedOtherResponse,
                                                            this)
                                                    .ifPresent(
                                                            updatedOtherResponse -> {

                                                                choiceQuestion
                                                                        .setAlternativeChoice(updatedOtherResponse);

                                                                this.update();
                                                            });
                                        });
                    }
                });

        this.clearOtherResponse = new Action("Remove",
                actionEvent -> {

                    if (this.getMetadata() instanceof ChoiceQuestion) {

                        ChoiceQuestion choiceQuestion = (ChoiceQuestion) this
                                .getMetadata();
                        if (Utils.confirmDeletionOfProperty(
                                choiceQuestion.getAlternativeChoice(),
                                choiceQuestion, this)) {

                            choiceQuestion.setAlternativeChoice(null);
                        }
                    }
                });

        this.createOtherResponse.disabledProperty().bind(
                this.otherResponse.textProperty().isNotNull()
                        .and(this.otherResponse.textProperty().isNotEmpty()));

        this.editOtherResponse.disabledProperty().bind(
                this.otherResponse.textProperty().isNull()
                        .or(this.otherResponse.textProperty().isEmpty()));
        this.clearOtherResponse.disabledProperty().bind(
                this.otherResponse.textProperty().isNull()
                        .or(this.otherResponse.textProperty().isEmpty()));

        this.otherResponse.contextMenuActions().clear();
        this.otherResponse.contextMenuActions().addAll(
                this.createOtherResponse, this.editOtherResponse,
                this.clearOtherResponse);

        this.editRank = new Action(
                "Edit",
                actionEvent -> {

                    if (this.getMetadata() != null
                            && this.getMetadata() instanceof RankingQuestion) {

                        RankingQuestion rankingQuestion = (RankingQuestion) this
                                .getMetadata();

                        if (this.ranks.getSelectionModel().getSelectedItem() != null) {

                            ChoiceQuestion rank = this.ranks
                                    .getSelectionModel().getSelectedItem();

                            new QuestionDialog<>(this, "Edit Rank", rank)
                                    .showAndWait()
                                    .ifPresent(
                                            editedRank -> {

                                                ApplicationContext
                                                        .surveyController()
                                                        .updateQuestion(
                                                                editedRank,
                                                                this)
                                                        .ifPresent(
                                                                updatedRank -> {

                                                                    rankingQuestion
                                                                            .getRanks()
                                                                            .set(rankingQuestion
                                                                                    .getRanks()
                                                                                    .indexOf(
                                                                                            rank),
                                                                                    updatedRank);

                                                                    this.update();
                                                                });
                                            });
                        }

                    }
                });

        this.editRank.disabledProperty().bind(
                this.ranks.selectedItemProperty().isNull());

        this.ranks.contextMenuActions().clear();
        this.ranks.contextMenuActions().add(this.editRank);
    }

    private void setupValidation() {

        this.questionTextArea.registerValidator(Validator
                .createEmptyValidator("Question text cannot be empty!"));
        this.questionTextArea.setErrorDecorationEnabled(true);
        this.questionTextArea
                .setValidationDecorator(new GraphicValidationDecoration());

        this.validationGroup().registerSubValidation(this.choiceSetDetails,
                this.choiceSetDetails.validationSupport());
        this.validationGroup().registerSubValidation(this.questionTextArea,
                this.questionTextArea.validationSupport());
    }

    private void refreshChoiceSetDetails(ChoiceSet choiceSet) {

        this.validationGroup().registerSubValidation(this.choiceSetDetails,
                this.choiceSetDetails.validationSupport());
        this.choiceSetDetails.setVisible(true);

        boolean empty = choiceSet == null;

        for (Action action : this.choiceSetDetails.contextMenuActions()) {

            if (action != null && action.disabledProperty() != null) {

                action.disabledProperty().set(empty);
            }
        }

        if (empty) {

            this.setChoiceSet.disabledProperty().set(!empty);
            this.choiceSetDetails.setChoiceSet(null);
            this.choiceSetDetails.getLabel().setText("Choice Set");
        }
        else {

            ApplicationContext
                    .surveyController()
                    .retrieveChoiceSet(choiceSet.getId(), this)
                    .ifPresent(
                            retrievedChoiceSet -> {
                                this.choiceSetDetails
                                        .setChoiceSet(retrievedChoiceSet);
                                this.choiceSetDetails.getLabel().setText(
                                        "Choice Set: "
                                                + retrievedChoiceSet.getName());
                            });
        }
        LOG.debug("Revalidating view.");
        this.choiceSetDetails.validationSupport().revalidate();
    }

    private void refreshRankings() {

        if (this.getMetadata() instanceof RankingQuestion) {

            this.ranks.getItems().clear();
            this.ranks.getItems().addAll(
                    ((RankingQuestion) this.getMetadata()).getRanks());
        }
    }

    @Override
    protected void update() {

        if (!this.isUpdateDeferred()) {

            LOG.debug("Updating Question");
            IQuestion question = this.getMetadata();

            if (question != null) {

                ApplicationContext.surveyController()
                        .updateQuestion(question, this)
                        .ifPresent(updatedQuestion -> {

                            this.setMetadata(null);
                            this.setMetadata(updatedQuestion);
                        });
            }
        }
        else {

            LOG.debug("Update of question deferred!");
        }
    }

    @Override
    protected void refreshView() {

        super.refreshView();

        LOG.debug("Refreshing view.");
        this.choiceSetDetails.setChoiceSet(null);

        this.choiceSetDetails.setVisible(false);
        this.ranks.setVisible(false);
        this.otherResponse.setVisible(false);

        this.validationGroup().removeSubValidation(this.choiceSetDetails);

        if (this.getMetadata() != null) {

            LOG.debug("Setting question text to "
                    + this.getMetadata().getQuestionText());
            this.questionTextArea.setText(this.getMetadata().getQuestionText());

            this.requiredCheckBox.setSelected(this.getMetadata()
                    .isAnswerRequired());


            if (this.getMetadata() instanceof ChoiceQuestion) {

                LOG.debug("Choice question.");
                ChoiceQuestion choiceQuestion = (ChoiceQuestion) this
                        .getMetadata();

                this.refreshChoiceSetDetails(choiceQuestion.getChoices());

                if (this.getChildren().contains(this.ranks)) {

                    this.getChildren().remove(this.ranks);
                }

                if (!this.getChildren().contains(this.otherResponse)) {

                    this.add(this.otherResponse, 2, 0);
                }

                if (choiceQuestion.getAlternativeChoice() != null) {

                    this.otherResponse.setText(choiceQuestion
                            .getAlternativeChoice().getName());
                }
                else {

                    this.otherResponse.setText("");
                }
                this.otherResponse.setVisible(true);
            }
            else if (this.getMetadata() instanceof RankingQuestion) {

                LOG.debug("Ranking question.");
                this.refreshChoiceSetDetails(((RankingQuestion) this
                        .getMetadata()).getChoices());
                this.refreshRankings();


                if (this.getChildren().contains(this.otherResponse)) {

                    this.getChildren().remove(this.otherResponse);
                }

                if (!this.getChildren().contains(this.ranks)) {

                    this.add(this.ranks, 2, 0, 1, 3);
                }

                this.getChildren().remove(this.otherResponse);

                this.ranks.setVisible(true);
            }
        }
        else {

            this.questionTextArea.setText(null);
        }
    }

    private void editQuestionText() {

        EditLongTextDialog dlg = new EditLongTextDialog(this.getScene()
                .getWindow(), this.getMetadata().getQuestionText());

        dlg.showAndWait().ifPresent(result -> {


            this.getMetadata().setQuestionText(result);

            this.update();
        });
    }
}
