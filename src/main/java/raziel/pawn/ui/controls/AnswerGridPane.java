package raziel.pawn.ui.controls;


import java.util.List;
import java.util.Optional;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;

import org.controlsfx.control.action.Action;
import org.jutility.javafx.control.ListViewWithSearchPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.IAnswer;
import raziel.pawn.core.domain.metadata.IMetadata;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.stringConverters.AnswerStringConverter;


/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class AnswerGridPane
        extends SurveyContentGridPane {

    private static Logger                          LOG = LoggerFactory
                                                               .getLogger(AnswerGridPane.class);

    private final ObjectProperty<IMetadata>        filter;

    private final ListViewWithSearchPanel<IAnswer> answersListView;
    private final Label                            answerLabel;
    private final AnswerDetailsGridPane            answerDetails;

    private Action                                 addAnswer;
    private Action                                 removeAnswer;
    private Action                                 renameAnswer;

    /**
     * Returns the filter property.
     * 
     * @return the filter property.
     */
    public ObjectProperty<IMetadata> filter() {

        return this.filter;
    }

    /**
     * Returns the value of the filter property.
     * 
     * @return the value of the filter property.
     */
    public IMetadata getFilter() {

        return this.filter.get();
    }

    /**
     * Sets the value of the filter property.
     * 
     * @param filter
     *            the value of the filter property.
     */
    public void setFilter(final IMetadata filter) {

        this.filter.set(filter);
    }

    /**
     * Creates a new instance of the {@link AnswerGridPane} class.
     * 
     * @param survey
     *            the survey.
     */
    public AnswerGridPane(final Survey survey) {

        super(survey);

        ColumnConstraints listColumnWidthConstraint = new ColumnConstraints();
        listColumnWidthConstraint.setPercentWidth(25);
        ColumnConstraints detailsColumnWidthConstraint = new ColumnConstraints();
        detailsColumnWidthConstraint.setPercentWidth(75);

        this.getColumnConstraints().addAll(listColumnWidthConstraint,
                detailsColumnWidthConstraint);


        this.filter = new SimpleObjectProperty<>();

        this.answersListView = new ListViewWithSearchPanel<>();
        this.answersListView.setConverter(new AnswerStringConverter<>());

        this.add(this.answersListView, 0, 0, 1, 2);
        GridPane.setHgrow(this.answersListView, Priority.ALWAYS);
        GridPane.setVgrow(this.answersListView, Priority.ALWAYS);

        this.answerDetails = new AnswerDetailsGridPane();
        this.answerDetails.setVisible(false);

        this.answerLabel = new Label("Answer");
        this.answerLabel.setLabelFor(this.answerDetails);
        this.answerLabel.setFont(Font.font("verdana", 16));
        this.answerLabel.setVisible(false);

        this.add(this.answerLabel, 1, 0);
        this.add(this.answerDetails, 1, 1);

        GridPane.setHgrow(this.answerDetails, Priority.ALWAYS);
        GridPane.setVgrow(this.answerDetails, Priority.ALWAYS);
        GridPane.setHalignment(this.answerDetails, HPos.LEFT);

        this.setupContextMenus();
        this.setupEventHandlers();

        this.refreshView();
    }

    private void setupContextMenus() {

        this.addAnswer = new Action("New", (actionEvent) -> {

            this.create();
        });

        this.removeAnswer = new Action("Delete", (actionEvent) -> {

            this.delete(this.answersListView.getSelectedItem());
        });

        this.renameAnswer = new Action("Rename", (actionEvent) -> {

            IAnswer selectedItem = this.answersListView.getSelectedItem();

            this.rename(selectedItem);
        });

        this.answersListView.contextMenuActions().addAll(this.addAnswer,
                this.renameAnswer, this.removeAnswer);
    }

    private void setupEventHandlers() {

        this.filter.addListener((observable) -> {

            LOG.debug("Invalidation event received. (Filter)");
            this.refreshView();
        });

        this.answerDetails.surveyProperty().bindBidirectional(
                this.surveyProperty());

        this.answerDetails.visibleProperty().bind(
                this.answersListView.selectedItemProperty().isNotNull());
        this.answerLabel.visibleProperty().bind(
                this.answersListView.selectedItemProperty().isNotNull());

        this.removeAnswer.disabledProperty().bind(
                this.answersListView.selectedItemProperty().isNull());
        this.renameAnswer.disabledProperty().bind(
                this.answersListView.selectedItemProperty().isNull());

        this.answersListView.selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    LOG.debug("Selected answer changed to " + newValue);
                    if (newValue != null) {

                        LOG.debug("Retrieving answer " + newValue);
                        ApplicationContext.surveyController()
                                .retrieveAnswer(newValue.getId(), this)
                                .ifPresent(answer -> {

                                    this.answerDetails.setMetadata(answer);
                                });
                    }
                });

    }

    private void create() {

        IParticipant participant = null;
        IQuestion question = null;

        if (this.getFilter() != null) {

            IMetadata metadata = this.getFilter();

            if (metadata instanceof IQuestion) {

                question = (IQuestion) metadata;
            }

            else if (metadata instanceof IParticipant) {

                participant = (IParticipant) metadata;
            }
        }

        ApplicationContext.surveyController()
                .createAnswer(question, participant, this.getSurvey(), this)
                .ifPresent(newAnswer -> {

                    this.getSurvey().addAnswer(newAnswer);

                    this.updateSurvey();

                    this.answersListView.getSelectionModel().select(newAnswer);
                });
    }

    private void rename(IAnswer answer) {


        Utils.renameDomainObject(answer, this).ifPresent(
                renamedAnswer -> {

                    ApplicationContext
                            .surveyController()
                            .updateAnswer(renamedAnswer, this)
                            .ifPresent(
                                    updatedAnswer -> {

                                        this.answersListView.getItems().set(
                                                this.answersListView.getItems()
                                                        .indexOf(answer),
                                                updatedAnswer);
                                    });
                });

    }

    private void delete(IAnswer answer) {

        ApplicationContext.surveyController().deleteAnswer(answer, this)
                .ifPresent(deletedAnswer -> {

                    this.refreshSurvey();

                    LOG.debug("Clearing selection");
                    this.answersListView.getSelectionModel().clearSelection();
                });
    }

    @Override
    protected void refreshView() {

        LOG.debug("Refreshing view.");

        IAnswer selectedItem = this.answersListView.getSelectedItem();

        this.answersListView.clear();
        this.answersListView.getSelectionModel().clearSelection();

        if (this.getSurvey() != null) {

            Optional<List<? extends IAnswer>> answers = Optional.empty();

            if (this.getFilter() != null) {

                IMetadata metadata = this.getFilter();

                if (metadata instanceof IQuestion) {

                    answers = ApplicationContext.surveyController()
                            .retrieveAllAnswersForQuestion(
                                    (IQuestion) metadata, this);
                    this.answersListView
                            .setConverter(new AnswerStringConverter<>(
                                    AnswerStringConverter.PARTICIPANT));
                }
                else if (metadata instanceof IParticipant) {

                    LOG.debug("Retrieving answers by " + metadata);
                    answers = ApplicationContext.surveyController()
                            .retrieveAllAnswersByParticipant(
                                    (IParticipant) metadata, this);
                    this.answersListView
                            .setConverter(new AnswerStringConverter<>(
                                    AnswerStringConverter.QUESTION));
                }

            }
            else {

                answers = Optional.ofNullable(this.getSurvey().getAnswers());
                this.answersListView
                        .setConverter(new AnswerStringConverter<>());
            }

            answers.ifPresent(answerList -> {

                LOG.debug("Adding " + answerList.size() + " Answers to view.");
                this.answersListView.getItems().addAll(answerList);

                if (selectedItem != null
                        && this.answersListView.getItems().contains(
                                selectedItem)) {

                    this.answersListView.getSelectionModel().select(
                            selectedItem);
                }
            });
        }
    }
}
