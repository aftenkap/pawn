package raziel.pawn.ui.controls;


import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.util.StringConverter;

import org.controlsfx.control.action.Action;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import org.jutility.javafx.control.ListViewWithSearchPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.core.domain.metadata.Choice;
import raziel.pawn.core.domain.metadata.ChoiceSet;
import raziel.pawn.ui.controls.dialog.AddChoiceDialog;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public class ChoiceSetDetailsGridPane
        extends ListViewWithSearchPanel<Choice> {

    private static Logger                   LOG = LoggerFactory
                                                        .getLogger(ChoiceSetDetailsGridPane.class);

    private final ObjectProperty<ChoiceSet> choiceSet;
    private Action                          addChoice;
    private Action                          removeChoice;

    /**
     * Returns the choiceSet property.
     * 
     * @return the choiceSet property.
     */
    public ObjectProperty<ChoiceSet> choiceSet() {

        return this.choiceSet;
    }

    /**
     * Returns the {@link ChoiceSet}.
     * 
     * @return the {@link ChoiceSet}.
     */
    public ChoiceSet getChoiceSet() {

        return this.choiceSet.get();
    }

    /**
     * Sets the {@link ChoiceSet}.
     * 
     * @param choiceSet
     *            the {@link ChoiceSet}.
     */
    public void setChoiceSet(final ChoiceSet choiceSet) {

        this.choiceSet.set(choiceSet);
    }

    /**
     * Creates a new {@link ChoiceSetDetailsGridPane} with no title.
     */
    public ChoiceSetDetailsGridPane() {

        this(null);
    }

    /**
     * Creates a new {@link ChoiceSetDetailsGridPane} with the provided title.
     * 
     * @param title
     *            title of this panel
     */
    public ChoiceSetDetailsGridPane(final String title) {

        this(title, null);
    }


    /**
     * Creates a new {@link ListViewWithSearchPanel} with the provided title.
     * 
     * @param title
     *            title of this panel
     * @param stringConverter
     *            the string converter to use.
     */
    public ChoiceSetDetailsGridPane(final String title,
            StringConverter<Choice> stringConverter) {

        super(title, stringConverter);

        this.choiceSet = new SimpleObjectProperty<>();


        this.registerValidator(Validator.createPredicateValidator(
                (ObservableList<Choice> items) -> {
                    LOG.debug("Evaluating " + items + " not null "
                            + (items != null) + " && not empty "
                            + !items.isEmpty());
                    return items != null && !items.isEmpty();
                }, "Choice set cannot be empty!"));
        this.setErrorDecorationEnabled(true);
        this.setValidationDecorator(new GraphicValidationDecoration());

        this.setupContextMenus();
        this.setupEventHandlers();
    }

    private void setupContextMenus() {

        this.addChoice = new Action("Add Choice", actionEvent -> {

            AddChoiceDialog.showDialog(this.getScene().getWindow()).ifPresent(
                    choice -> {

                        this.getItems().add(choice);
                    });
        });

        this.removeChoice = new Action("Remove Choice", actionEvent -> {

            Choice selectedItem = this.getSelectedItem();

            Alert alert = new Alert(AlertType.CONFIRMATION,
                    "Are you sure you want to remove " + selectedItem.getName()
                            + "?");

            alert.showAndWait().filter(result -> result == ButtonType.OK)
                    .ifPresent(result -> {

                        this.remove(selectedItem);
                    });
        });

        this.contextMenuActions().addAll(this.addChoice, this.removeChoice);
    }

    private void setupEventHandlers() {

        this.choiceSet.addListener((observable) -> {

            LOG.debug("Clearing choice set.");
            this.clear();

            if (this.getChoiceSet() != null) {

                LOG.debug("Setting new choices.");
                this.getItems().addAll(this.getChoiceSet().getChoices());
            }
        });
    }
}
