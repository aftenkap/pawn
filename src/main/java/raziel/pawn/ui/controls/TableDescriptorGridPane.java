package raziel.pawn.ui.controls;


import java.util.Optional;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import org.controlsfx.control.action.Action;
import org.jutility.javafx.control.ListViewWithSearchPanel;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.core.domain.metadata.descriptors.IDataPointDescriptor;
import raziel.pawn.core.domain.metadata.descriptors.TableDescriptor;
import raziel.pawn.core.domain.metadata.descriptors.TableMetadataDescriptor;
import raziel.pawn.core.domain.metadata.descriptors.TableParticipantDescriptorBase;
import raziel.pawn.core.domain.metadata.descriptors.TableQuestionDescriptor;
import raziel.pawn.ui.controllers.SurveyController;
import raziel.pawn.ui.controls.dialog.TableMetadataDescriptorDialog;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class TableDescriptorGridPane
        extends GridPane {


    private final ObjectProperty<TableDescriptor>                                       tableDescriptor;

    private final ListViewWithSearchPanel<IDataPointDescriptor<Spreadsheet, IQuestion>> questionDescriptorListView;
    private final ListViewWithSearchPanel<IDataPointDescriptor<Spreadsheet, IQuestion>> participantDescriptorListView;
    private final ListViewWithSearchPanel<IDataPointDescriptor<Spreadsheet, IQuestion>> answerDescriptorListView;

    private final ObjectProperty<TableGridPane>                                         tableGridPane;

    private Action                                                                      addQuestionDescriptor;
    private Action                                                                      editDescriptor;
    private Action                                                                      renameDescriptor;
    private Action                                                                      removeDescriptor;



    private final SurveyController                                                      surveyController;



    /**
     * Returns the table link property.
     * 
     * @return the table descriptor property.
     */
    public ObjectProperty<TableDescriptor> tableDescriptor() {

        return this.tableDescriptor;
    }

    /**
     * Returns the value of the table descriptor property.
     * 
     * @return the value of the table descriptor property.
     */
    public TableDescriptor getTableDescriptor() {

        return this.tableDescriptor.get();
    }

    /**
     * Sets the value of the table descriptor property.
     * 
     * @param tableDescriptor
     *            the value of the table descriptor property.
     */
    public void setTableDescriptor(TableDescriptor tableDescriptor) {

        this.tableDescriptor.set(tableDescriptor);
    }

    /**
     * Creates a new instance of the {@link TableDescriptorGridPane} class.
     */
    public TableDescriptorGridPane() {

        this.setPadding(new Insets(10, 10, 10, 10));
        this.setHgap(20);
        this.setVgap(10);

        this.surveyController = ApplicationContext.surveyController();

        this.tableDescriptor = new SimpleObjectProperty<>();
        this.tableGridPane = new SimpleObjectProperty<>();


        this.questionDescriptorListView = new ListViewWithSearchPanel<>(
                "Question Descriptors");
        GridPane.setVgrow(this.questionDescriptorListView, Priority.ALWAYS);
        GridPane.setHgrow(this.questionDescriptorListView, Priority.ALWAYS);

        this.participantDescriptorListView = new ListViewWithSearchPanel<>(
                "Participant Descriptors");
        GridPane.setVgrow(this.participantDescriptorListView, Priority.ALWAYS);
        GridPane.setHgrow(this.participantDescriptorListView, Priority.ALWAYS);

        this.answerDescriptorListView = new ListViewWithSearchPanel<>(
                "Answer Descriptors");
        GridPane.setVgrow(this.answerDescriptorListView, Priority.ALWAYS);
        GridPane.setHgrow(this.answerDescriptorListView, Priority.ALWAYS);

        this.add(this.questionDescriptorListView, 0, 0);
        this.add(this.participantDescriptorListView, 1, 0);
        this.add(this.answerDescriptorListView, 2, 0);

        this.setupContextMenus();
        this.setupEventHandlers();
    }

    private void setupContextMenus() {

        this.addQuestionDescriptor = new Action(
                "Add",
                (actionEvent) -> {

                    ApplicationContext
                            .surveyController()
                            .retrieveAllChoiceSets(this)
                            .ifPresent(
                                    choiceSets -> {

                                        Optional<TableMetadataDescriptor<?>> result = TableMetadataDescriptorDialog
                                                .showDialog(this.getScene()
                                                        .getWindow(), this
                                                        .getTableDescriptor()
                                                        .getTable(), choiceSets);

                                        if (result.isPresent()) {

                                            TableMetadataDescriptor<?> descriptor = result
                                                    .get();

                                            if (descriptor instanceof TableQuestionDescriptor<?>) {

                                                this.getTableDescriptor()
                                                        .addDataPointDescriptor(
                                                                IQuestion.class,
                                                                (TableQuestionDescriptor<?>) descriptor);
                                            }
                                            else if (descriptor instanceof TableParticipantDescriptorBase<?>) {

                                                this.getTableDescriptor()
                                                        .addDataPointDescriptor(
                                                                IParticipant.class,
                                                                (TableParticipantDescriptorBase<?>) descriptor);
                                            }

                                        }
                                    });

                });

        this.editDescriptor = new Action("Edit", (actionEvent) -> {

            // TODO Auto-generated method stub

            });
        this.renameDescriptor = new Action("Rename", (actionEvent) -> {

            // TODO Auto-generated method stub

            });
        this.removeDescriptor = new Action("Delete", (actionEvent) -> {

            // TODO Auto-generated method stub

            });


        this.questionDescriptorListView.contextMenuActions().addAll(
                this.addQuestionDescriptor, this.editDescriptor,
                this.renameDescriptor, this.removeDescriptor);
    }

    private void setupEventHandlers() {

        this.tableDescriptor.addListener((observable, oldValue, newValue) -> {

            TableDescriptorGridPane.this.questionDescriptorListView.clear();

            if (this.tableGridPane != null) {

                TableDescriptorGridPane.this.getChildren().remove(
                        this.tableGridPane);
            }

            if (newValue != null) {

                TableDescriptorGridPane.this.tableGridPane
                        .set(new TableGridPane(newValue.getTable()));
            }

        });

        this.tableGridPane.addListener((
                ObservableValue<? extends TableGridPane> observable,
                TableGridPane oldValue, TableGridPane newValue) -> {

            if (oldValue != null) {

                TableDescriptorGridPane.this.getChildren().remove(oldValue);
            }

            if (newValue != null) {

                TableDescriptorGridPane.this.add(newValue, 0, 1, 3, 3);
            }
        });
    }
}
