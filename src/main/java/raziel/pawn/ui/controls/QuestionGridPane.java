package raziel.pawn.ui.controls;



import java.util.Optional;

import javafx.collections.ListChangeListener.Change;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;

import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionGroup;
import org.controlsfx.control.action.ActionUtils;
import org.jutility.javafx.control.ListViewWithSearchPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.ChoiceQuestion;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.core.domain.metadata.RankingQuestion;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.core.domain.metadata.TextQuestion;
import raziel.pawn.ui.controllers.SurveyController;
import raziel.pawn.ui.controls.dialog.DomainObjectDialog;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public class QuestionGridPane
        extends SurveyContentGridPane {


    private static Logger                            LOG = LoggerFactory
                                                                 .getLogger(QuestionGridPane.class);
    private final SurveyController                   surveyController;

    private final ListViewWithSearchPanel<IQuestion> questionsListView;
    private final Label                              questionTypeLabel;
    private final QuestionDetailsGridPane            questionDetails;


    private final Label                              answersLabel;
    private final AnswerGridPane                     answerGridPane;


    private ActionGroup                              addQuestion;
    private Action                                   addTextQuestion;
    private Action                                   addChoiceQuestion;
    private Action                                   addRankingQuestion;
    private Action                                   removeQuestion;
    private Action                                   renameQuestion;


    private Action                                   moveUp;
    private Action                                   moveDown;
    private Action                                   swap;



    /**
     * Creates a new instance of the {@link QuestionGridPane} class.
     * 
     * @param survey
     *            the survey.
     */
    public QuestionGridPane(final Survey survey) {

        super(survey);

        ColumnConstraints listColumnWidthConstraint = new ColumnConstraints();
        listColumnWidthConstraint.setPercentWidth(25);
        ColumnConstraints detailsColumnWidthConstraint = new ColumnConstraints();
        detailsColumnWidthConstraint.setPercentWidth(75);

        this.getColumnConstraints().addAll(listColumnWidthConstraint,
                detailsColumnWidthConstraint);


        this.surveyController = ApplicationContext.surveyController();


        this.questionsListView = new ListViewWithSearchPanel<>();
        this.questionsListView
                .setConverter(new DomainObjectStringConverter<>());
        this.questionsListView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE);

        this.add(this.questionsListView, 0, 0, 1, 4);
        GridPane.setHgrow(this.questionsListView, Priority.ALWAYS);
        GridPane.setVgrow(this.questionsListView, Priority.ALWAYS);

        this.questionDetails = new QuestionDetailsGridPane();
        this.questionDetails.setVisible(false);

        this.questionTypeLabel = new Label("Question");
        this.questionTypeLabel.setLabelFor(this.questionDetails);
        this.questionTypeLabel.setFont(Font.font("verdana", 16));
        this.questionTypeLabel.setVisible(false);

        this.add(this.questionTypeLabel, 1, 0);
        this.add(this.questionDetails, 1, 1);

        GridPane.setHgrow(this.questionDetails, Priority.ALWAYS);
        GridPane.setHalignment(this.questionDetails, HPos.LEFT);

        this.answersLabel = new Label("Answered by");
        this.answersLabel.setVisible(false);

        this.answerGridPane = new AnswerGridPane(survey);
        this.answerGridPane.setPadding(new Insets(0));
        this.answerGridPane.setVisible(false);
        GridPane.setHgrow(this.answerGridPane, Priority.ALWAYS);
        GridPane.setVgrow(this.answerGridPane, Priority.ALWAYS);
        GridPane.setHalignment(this.answerGridPane, HPos.LEFT);

        this.answersLabel.setLabelFor(this.answerGridPane);
        this.answersLabel.setFont(Font.font("verdana", 16));

        this.add(this.answersLabel, 1, 2);
        this.add(this.answerGridPane, 1, 3);

        this.questionDetails.surveyProperty().bindBidirectional(
                this.surveyProperty());
        this.answerGridPane.surveyProperty().bindBidirectional(
                this.surveyProperty());


        this.setupContextMenus();
        this.setupEventHandlers();

        this.refreshView();
    }


    private void setupContextMenus() {


        this.addTextQuestion = new Action("Text Question", (actionEvent) -> {

            this.create(TextQuestion.class);
        });
        this.addChoiceQuestion = new Action("Choice Question",
                (actionEvent) -> {

                    this.create(ChoiceQuestion.class);
                });
        this.addRankingQuestion = new Action("Ranking Question",
                (actionEvent) -> {

                    this.create(RankingQuestion.class);
                });

        this.addQuestion = new ActionGroup("New", this.addTextQuestion,
                this.addChoiceQuestion, this.addRankingQuestion);

        this.removeQuestion = new Action("Delete", (actionEvent) -> {

            this.delete(this.questionsListView.getSelectedItem());
        });


        this.renameQuestion = new Action("Rename", (actionEvent) -> {

            this.rename(this.questionsListView.getSelectedItem());
        });



        this.moveUp = new Action("Move Up", (actionEvent) -> {

            int selectedIndex = this.questionsListView.getSelectedIndex();

            IQuestion firstQuestion = this.questionsListView.getItems().get(
                    selectedIndex);
            IQuestion secondQuestion = this.questionsListView.getItems().get(
                    selectedIndex - 1);

            this.swap(firstQuestion, secondQuestion, false);
        });


        this.moveDown = new Action("Move Down", (actionEvent) -> {

            int selectedIndex = this.questionsListView.getSelectedIndex();

            IQuestion firstQuestion = this.questionsListView.getItems().get(
                    selectedIndex);
            IQuestion secondQuestion = this.questionsListView.getItems().get(
                    selectedIndex + 1);

            this.swap(firstQuestion, secondQuestion, false);
        });

        this.swap = new Action("Swap Questions", (actionEvent) -> {

            int first = this.questionsListView.getSelectedIndices().get(0);
            int second = this.questionsListView.getSelectedIndices().get(1);


            IQuestion firstQuestion = this.questionsListView.getItems().get(
                    first);
            IQuestion secondQuestion = this.questionsListView.getItems().get(
                    second);

            this.swap(firstQuestion, secondQuestion, true);
        });

        this.questionsListView.contextMenuActions().addAll(this.addQuestion,
                this.renameQuestion, this.removeQuestion,
                ActionUtils.ACTION_SEPARATOR, this.moveUp, this.swap,
                this.moveDown);
    }

    private void setupEventHandlers() {


        this.questionsListView.getSelectedIndices().addListener(
                (Change<? extends Integer> change) -> {

                    this.swap.disabledProperty()
                            .set(this.questionsListView.getSelectedIndices()
                                    .size() != 2);
                });

        this.questionsListView.selectedIndexProperty().addListener(
                (observable, oldValue, newValue) -> {

                    boolean multipleSelected = (this.questionsListView
                            .getSelectedIndices().size() != 1);

                    if (newValue != null) {

                        int value = newValue.intValue();

                        this.moveUp.disabledProperty().set(
                                value == 0 || multipleSelected);
                        this.moveDown.disabledProperty().set(
                                value == (this.questionsListView.getItems()
                                        .size() - 1) || multipleSelected);
                    }
                    else {

                        this.moveUp.disabledProperty().set(true);
                        this.moveDown.disabledProperty().set(true);
                    }
                });

        this.removeQuestion.disabledProperty().bind(
                this.questionsListView.selectedItemProperty().isNull());
        this.renameQuestion.disabledProperty().bind(
                this.questionsListView.selectedItemProperty().isNull());


        this.questionTypeLabel.visibleProperty().bind(
                this.questionsListView.selectedItemProperty().isNotNull());
        this.questionDetails.visibleProperty().bind(
                this.questionsListView.selectedItemProperty().isNotNull());

        this.answerGridPane.visibleProperty().bind(
                this.questionsListView.selectedItemProperty().isNotNull());
        this.answersLabel.visibleProperty().bind(
                this.questionsListView.selectedItemProperty().isNotNull());

        this.questionsListView
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {

                            if (newValue != null) {

                                this.surveyController
                                        .retrieveQuestion(newValue.getId(),
                                                this)
                                        .ifPresent(
                                                retrievedValue -> {

                                                    this.questionDetails
                                                            .setMetadata(retrievedValue);

                                                    this.answerGridPane
                                                            .setFilter(retrievedValue);

                                                    if (retrievedValue instanceof TextQuestion) {

                                                        QuestionGridPane.this.questionTypeLabel
                                                                .setText("Text Question");
                                                    }
                                                    else if (retrievedValue instanceof ChoiceQuestion) {

                                                        this.questionTypeLabel
                                                                .setText("Choice Question");
                                                    }
                                                    else if (retrievedValue instanceof RankingQuestion) {

                                                        this.questionTypeLabel
                                                                .setText("Ranking Question");
                                                    }
                                                    else {

                                                        this.questionTypeLabel
                                                                .setText("Question");
                                                    }
                                                });
                            }
                        });


    }

    private void create(Class<? extends IQuestion> type) {

        ApplicationContext
                .surveyController()
                .createEmptyQuestion(type,
                        this.getSurvey().getQuestions().size(), this)
                .ifPresent(
                        newQuestion -> {

                            this.getSurvey().addQuestion(newQuestion);

                            this.updateSurvey();

                            if (this.questionsListView.isFocused()) {

                                this.questionsListView.getSelectionModel()
                                        .select(newQuestion);
                            }
                        });
    }

    private void rename(IQuestion question) {

        if (question != null) {

            DomainObjectDialog<IQuestion> dlg = new DomainObjectDialog<>(this
                    .getScene().getWindow(), "Rename Question", question);

            dlg.showAndWait().ifPresent(
                    changedObject -> {

                        this.surveyController.updateQuestion(changedObject,
                                this).ifPresent(
                                updatedQuestion -> {

                                    this.refreshSurvey();
                                    this.questionsListView.getSelectionModel()
                                            .select(updatedQuestion);
                                });
                    });
        }
    }

    private void delete(IQuestion question) {

        if (question != null) {

            LOG.info("Deleting question " + question);
            this.surveyController.deleteQuestion(question, this).ifPresent(
                    deletedQuestion -> {
                        LOG.debug("Deleted question in db.");

                        LOG.debug("Clearing selection");
                        this.questionsListView.getSelectionModel()
                                .clearSelection();

                        LOG.debug("Removing question from survey.");
                        this.getSurvey().removeQuestion(question);

                        LOG.debug("Updating survey");
                        this.refreshSurvey();

                    });
        }
    }

    private void swap(IQuestion firstQuestion, IQuestion secondQuestion,
            boolean swap) {

        int secondOrder = secondQuestion.getOrder();

        secondQuestion.setOrder(firstQuestion.getOrder());
        firstQuestion.setOrder(secondOrder);


        Optional<IQuestion> updatedFirstQuestion = this.surveyController
                .updateQuestion(firstQuestion, this);
        Optional<IQuestion> updatedSecondQuestion = this.surveyController
                .updateQuestion(secondQuestion, this);

        if (updatedFirstQuestion.isPresent()
                && updatedSecondQuestion.isPresent()) {

            this.refreshSurvey();

            if (swap) {

                int firstIndex = this.questionsListView.getItems().indexOf(
                        updatedFirstQuestion.get());
                int secondIndex = this.questionsListView.getItems().indexOf(
                        updatedSecondQuestion.get());

                this.questionsListView.getSelectionModel().selectIndices(
                        firstIndex, secondIndex);
            }
            else {

                this.questionsListView.getSelectionModel().select(
                        updatedFirstQuestion.get());
            }
        }
    }



    /**
     * Refreshes the contents of this view.
     */
    @Override
    protected void refreshView() {

        LOG.debug("Refreshing view.");
        IQuestion selectedItem = this.questionsListView.getSelectedItem();

        this.questionsListView.clear();
        if (this.getSurvey() != null) {

            this.questionsListView.getItems().addAll(
                    this.getSurvey().getQuestions());

            if (selectedItem != null
                    && this.getSurvey().getQuestions().contains(selectedItem)) {

                this.questionsListView.getSelectionModel().select(selectedItem);
            }
        }
    }

}
