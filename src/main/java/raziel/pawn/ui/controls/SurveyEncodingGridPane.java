package raziel.pawn.ui.controls;


import java.util.Collections;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import org.jutility.common.datatype.util.StringWithNumberComparator;

import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.core.domain.modeling.DataModel;


/**
 * @author Peter J. Radics
 * @version 0.1
 * 
 */
public class SurveyEncodingGridPane
        extends GridPane {


    private final DatasetSidebar            datasetSidebar;
    private final ObjectProperty<DataModel> masterDataset;

    private final InstanceGridPane          instanceGridPane;

    // FIXME: fix
    // private final ListChangeListener<Fluent> fluentListener;
    // private final ListChangeListener<Constant> constantListener;

    /**
     * Returns the master {@link DataModel} property.
     * 
     * @return the master {@link DataModel} property.
     */
    public ObjectProperty<DataModel> masterDataset() {

        return this.masterDataset;
    }

    /**
     * Returns the master {@link DataModel}.
     * 
     * @return the master {@link DataModel}.
     */
    public DataModel getMasterDataset() {

        return this.masterDataset.get();
    }

    /**
     * Sets the master {@link DataModel}.
     * 
     * @param masterDataset
     *            the master {@link DataModel}.
     */
    public void setMasterDataset(DataModel masterDataset) {

        this.masterDataset.set(masterDataset);
    }

    /**
     * Creates a new instance of the {@link SurveyEncodingGridPane} class.
     * 
     * @param survey
     *            the survey to encode.
     */
    public SurveyEncodingGridPane(Survey survey) {

        this.setPadding(new Insets(10, 10, 10, 10));
        this.setHgap(20);
        this.setVgap(10);

        this.masterDataset = new SimpleObjectProperty<>();

        this.datasetSidebar = new DatasetSidebar();



        Collections.sort(survey.getParticipants(), (IParticipant o1,
                IParticipant o2) -> {

            String lhs = o1.getName();
            String rhs = o2.getName();

            return StringWithNumberComparator.compareTo(lhs, rhs);

        });

        this.datasetSidebar.participants().addAll(survey.getParticipants());
        this.datasetSidebar.questions().addAll(survey.getQuestions());
        // this.datasetSidebar.answers().addAll(survey.getAnswers());



        this.instanceGridPane = new InstanceGridPane();
        GridPane.setHgrow(this.instanceGridPane, Priority.ALWAYS);
        GridPane.setVgrow(this.instanceGridPane, Priority.ALWAYS);
        this.instanceGridPane.setDisable(true);


        this.add(this.datasetSidebar, 0, 0);
        this.add(this.instanceGridPane, 1, 0);


        // FIXME: fix
        // this.fluentListener = new ListChangeListener<Fluent>() {
        //
        // @Override
        // public void onChanged(Change<? extends Fluent> change) {
        //
        // while (change.next()) {
        //
        // IQuestion question = SurveyEncodingGridPane.this.datasetSidebar
        // .getSelectedQuestion();
        // Participant participant = SurveyEncodingGridPane.this.datasetSidebar
        // .getSelectedParticipant();
        //
        // IAnswer answer = SurveyEncodingGridPane.this.datasetSidebar
        // .getSelectedAnswer();
        // if (change.wasAdded()) {
        //
        // for (Fluent fluent : change.getAddedSubList()) {
        //
        // SurveyEncodingGridPane.this.addInstance(fluent);
        //
        // if (fluent instanceof Activity) {
        //
        // for (Fluent subject : ((Activity) fluent)
        // .getAction().getDomainInstances()) {
        //
        // System.out.println("Subject: " + subject);
        // SurveyEncodingGridPane.this
        // .addInstance(subject);
        // }
        //
        // for (Fluent object : ((Activity) fluent)
        // .getAction().getRangeInstances()) {
        //
        // System.out.println("Object: " + object);
        // SurveyEncodingGridPane.this
        // .addInstance(object);
        // }
        // }
        // else if (fluent instanceof Attribute) {
        //
        // for (Fluent subject : ((Attribute) fluent)
        // .getAttribution().getDomainInstances()) {
        //
        // System.out.println("Subject: " + subject);
        // SurveyEncodingGridPane.this
        // .addInstance(subject);
        // }
        //
        // for (Constant object : ((Attribute) fluent)
        // .getAttribution().getRangeInstances()) {
        //
        // System.out.println("Object: " + object);
        // SurveyEncodingGridPane.this
        // .addInstance(object);
        // }
        // }
        // }
        // }
        // if (change.wasRemoved()) {
        //
        // for (Fluent fluent : change.getRemoved()) {
        //
        // System.out.println("Removing fluent " + fluent
        // + " from datasets!");
        // // SurveyEncodingGridPane.this.getMasterDataset()
        // // .getFluents().remove(fluent);
        // // question.getDataset().getFluents().remove(fluent);
        // // participant.getDataset().getFluents()
        // // .remove(fluent);
        // // answer.getDataset().getFluents().remove(fluent);
        // }
        // }
        // }
        // }
        // };

        // FIXME: fix
        // this.constantListener = new ListChangeListener<Constant>() {
        //
        // @Override
        // public void onChanged(Change<? extends Constant> change) {
        //
        // while (change.next()) {
        //
        // IQuestion question = SurveyEncodingGridPane.this.datasetSidebar
        // .getSelectedQuestion();
        // Participant participant = SurveyEncodingGridPane.this.datasetSidebar
        // .getSelectedParticipant();
        //
        // IAnswer answer = SurveyEncodingGridPane.this.datasetSidebar
        // .getSelectedAnswer();
        // if (change.wasAdded()) {
        //
        // for (Constant constant : change.getAddedSubList()) {
        //
        // SurveyEncodingGridPane.this.addInstance(constant);
        //
        // if (constant instanceof Association) {
        // for (Constant subject : ((Association) constant)
        // .getAssociation().getDomainInstances()) {
        //
        // System.out.println("Subject: " + subject);
        // SurveyEncodingGridPane.this
        // .addInstance(subject);
        // }
        //
        // for (Constant object : ((Association) constant)
        // .getAssociation().getRangeInstances()) {
        //
        // System.out.println("Object: " + object);
        // SurveyEncodingGridPane.this
        // .addInstance(object);
        // }
        // }
        // }
        // }
        // if (change.wasRemoved()) {
        //
        // for (Constant constant : change.getRemoved()) {
        //
        // System.out.println("Removing constant " + constant
        // + " from datasets!");
        // // SurveyEncodingGridPane.this.getMasterDataset()
        // // .getConstants().remove(constant);
        // // question.getDataset().getConstants()
        // // .remove(constant);
        // // participant.getDataset().getConstants()
        // // .remove(constant);
        // // answer.getDataset().getConstants().remove(constant);
        // }
        // }
        // }
        // }
        // };

        this.setupEventHandlers();
        this.setupContextMenus();
    }

    // FIXME: fix
    // private void addInstance(Instance instance) {
    //
    // IQuestion question = SurveyEncodingGridPane.this.datasetSidebar
    // .getSelectedQuestion();
    // Participant participant = SurveyEncodingGridPane.this.datasetSidebar
    // .getSelectedParticipant();
    // IAnswer answer = SurveyEncodingGridPane.this.datasetSidebar
    // .getSelectedAnswer();
    //
    //
    // if (this.getMasterDataset() != null) {
    //
    // this.getMasterDataset().addInstance(instance);
    // }
    //
    // if (question != null) {
    //
    // // question.getDataset().addInstance(instance);
    // }
    //
    // if (participant != null) {
    //
    // // participant.getDataset().addInstance(instance);
    // }
    //
    // if (answer != null) {
    //
    // // answer.getDataset().addInstance(instance);
    // }
    //
    // }

    private void setupEventHandlers() {

        // FIXME: fix
        //
        //
        // this.instanceGridPane.fluents().addListener(this.fluentListener);
        // this.instanceGridPane.constants().addListener(this.constantListener);
        //
        //
        // this.masterDataset().addListener(new ChangeListener<DataModel>() {
        //
        // @Override
        // public void changed(ObservableValue<? extends DataModel> observable,
        // DataModel oldValue, DataModel newValue) {
        //
        // SurveyEncodingGridPane.this.datasetSidebar.datasets().remove(
        // oldValue);
        //
        // if (newValue != null) {
        //
        // SurveyEncodingGridPane.this.instanceGridPane.fluents()
        // .addAll(newValue.getFluents());
        //
        // System.out.println("Fluents in master dataset: "
        // + newValue.getFluents().size());
        // if (SurveyEncodingGridPane.this.datasetSidebar.datasets()
        // .isEmpty()) {
        //
        // SurveyEncodingGridPane.this.datasetSidebar.datasets()
        // .add(newValue);
        // }
        // else {
        //
        // SurveyEncodingGridPane.this.datasetSidebar.datasets()
        // .add(0, newValue);
        // }
        // }
        // }
        // });
        //
        // this.datasetSidebar.selectedDataset().addListener(
        // new ChangeListener<DataModel>() {
        //
        // @Override
        // public void changed(
        // ObservableValue<? extends DataModel> observable,
        // DataModel oldValue, DataModel newValue) {
        //
        // SurveyEncodingGridPane.this.instanceGridPane
        // .fluents()
        // .removeListener(
        // SurveyEncodingGridPane.this.fluentListener);
        //
        //
        // SurveyEncodingGridPane.this.instanceGridPane.fluents()
        // .clear();
        //
        // if (newValue != null) {
        // SurveyEncodingGridPane.this.instanceGridPane
        // .fluents().addAll(newValue.getFluents());
        // }
        //
        //
        // SurveyEncodingGridPane.this.instanceGridPane
        // .fluents()
        // .addListener(
        // SurveyEncodingGridPane.this.fluentListener);
        //
        //
        // SurveyEncodingGridPane.this.instanceGridPane
        // .constants()
        // .removeListener(
        // SurveyEncodingGridPane.this.constantListener);
        //
        //
        // SurveyEncodingGridPane.this.instanceGridPane
        // .constants().clear();
        //
        // if (newValue != null) {
        // SurveyEncodingGridPane.this.instanceGridPane
        // .constants()
        // .addAll(newValue.getConstants());
        // }
        //
        //
        // SurveyEncodingGridPane.this.instanceGridPane
        // .constants()
        // .addListener(
        // SurveyEncodingGridPane.this.constantListener);
        //
        // SurveyEncodingGridPane.this.validate();
        // }
        // });

        this.datasetSidebar.selectedParticipant().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });

        this.datasetSidebar.selectedQuestion().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });
    }

    private void validate() {

        this.instanceGridPane.setDisable(this.datasetSidebar
                .getSelectedQuestion() == null
                || this.datasetSidebar.getSelectedDataset() == null
                || this.datasetSidebar.getSelectedParticipant() == null);

    }


    private void setupContextMenus() {

        // TODO: Implement
    }

}
