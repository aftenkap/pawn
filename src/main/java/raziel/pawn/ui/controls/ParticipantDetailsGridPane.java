package raziel.pawn.ui.controls;



import javafx.scene.layout.ColumnConstraints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.Survey;


/**
 * The {@code ParticipantDetailsGridPane} class provides a visual representation
 * of a {@link IParticipant Participant}.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class ParticipantDetailsGridPane
        extends MetadataDetailsGridPane<IParticipant> {


    private static Logger LOG = LoggerFactory
                                      .getLogger(ParticipantDetailsGridPane.class);


    /**
     * Creates a new instance of the {@link ParticipantDetailsGridPane} class.
     */
    public ParticipantDetailsGridPane() {

        this(null, null);
    }


    /**
     * Creates a new instance of the {@link ParticipantDetailsGridPane} class
     * with the provided {@link IParticipant Participant} and {@link Survey}.
     * 
     * @param participant
     *            the {@link IParticipant Participant}.
     * @param survey
     *            the {@link Survey} containing the {@link IParticipant
     *            Participant}.
     */
    public ParticipantDetailsGridPane(final IParticipant participant,
            final Survey survey) {

        this(participant, survey, false);
    }


    /**
     * Creates a new instance of the {@link ParticipantDetailsGridPane} class
     * with the provided {@link IParticipant Participant} and {@link Survey}.
     * Also specifies whether the update of the {@link IParticipant Participant}
     * in the database should be deferred.
     * 
     * @param participant
     *            the {@link IParticipant Participant}.
     * @param survey
     *            the {@link Survey} containing the {@link IParticipant
     *            Participant}.
     * @param deferUpdate
     *            whether or not the update of the {@link IParticipant
     *            Participant} in the database is deferred.
     */
    public ParticipantDetailsGridPane(final IParticipant participant,
            final Survey survey, final boolean deferUpdate) {

        super(participant, survey, deferUpdate);

        ColumnConstraints textColumnWidthConstraint = new ColumnConstraints();
        textColumnWidthConstraint.setPercentWidth(50);

        this.getColumnConstraints().addAll(textColumnWidthConstraint);


        this.setupEventHandlers();
        this.setupContextMenus();
    }

    private void setupEventHandlers() {

        // No event handlers
    }

    private void setupContextMenus() {

        // No context menu
    }


    @Override
    protected void update() {

        if (!this.isUpdateDeferred()) {

            IParticipant participant = this.getMetadata();

            if (participant != null) {

                LOG.debug("Updating participant " + participant);
                ApplicationContext.surveyController()
                        .updateParticipant(participant, this)
                        .ifPresent(updatedParticipant -> {

                            this.setMetadata(null);
                            this.setMetadata(updatedParticipant);
                        });

            }

        }
        else {

            LOG.debug("Update of answer deferred!");
        }
    }
}
