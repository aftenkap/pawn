package raziel.pawn.ui.controls;


import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.core.domain.Project;
import raziel.pawn.core.domain.data.IDataSource;
import raziel.pawn.core.domain.metadata.IMetadataSource;
import raziel.pawn.core.domain.modeling.DataModel;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class ProjectTreeView
        extends GridPane {

    private static String                                  DATASOURCES     = "DataSources";
    private static String                                  METADATASOURCES = "MetadataSources";
    private static String                                  DATAMODEL       = "DataModel";

    private final ObservableList<Project>                  projects;

    private final Map<IDomainObject, DomainObjectTreeItem> treeItems;

    private TreeItem<String>                               root;
    private final TreeView<String>                         treeView;

    private final Label                                    projectLabel;

    /**
     * Returns the projects property.
     * 
     * @return the projects property.
     */
    public ObservableList<Project> projects() {

        return this.projects;
    }


    /**
     * Returns the selected item property.
     * 
     * @return the selected item property.
     */
    public ReadOnlyObjectProperty<TreeItem<String>> selectedItemProperty() {

        return this.treeView.getSelectionModel().selectedItemProperty();
    }

    /**
     * Returns the selected item.
     * 
     * @return the selected item.
     */
    public TreeItem<String> getSelectedItem() {

        return this.selectedItemProperty().get();
    }

    /**
     * Returns the selected index.
     * 
     * @return the selected index.
     */
    public int getSelectedIndex() {

        return this.treeView.getSelectionModel().getSelectedIndex();
    }

    /**
     * Returns the selected domainObject (if applicable).
     * 
     * @return the selected domainObject or {@code null}.
     */
    public IDomainObject getSelectedDomainObject() {

        TreeItem<String> selectedTreeItem = this.getSelectedItem();

        if (selectedTreeItem != null
                && selectedTreeItem instanceof DomainObjectTreeItem) {

            DomainObjectTreeItem treeItem = (DomainObjectTreeItem) selectedTreeItem;

            IDomainObject domainObject = treeItem.getObject();

            return domainObject;
        }

        return null;
    }

    /**
     * Selects the {@link IDomainObject DomainObject} in the tree.
     * 
     * @param domainObject
     *            the {@link IDomainObject DomainObject} to select.
     */
    public void selectDomainObject(final IDomainObject domainObject) {

        if (this.treeItems.containsKey(domainObject)) {

            this.getSelectionModel().select(this.treeItems.get(domainObject));
        }
    }

    /**
     * Returns the {@link MultipleSelectionModel SelectionModel}.
     * 
     * @return the {@link MultipleSelectionModel SelectionModel}.
     */
    public MultipleSelectionModel<TreeItem<String>> getSelectionModel() {

        return this.treeView.getSelectionModel();
    }

    /**
     * Returns the context menu property.
     * 
     * @return the context menu property.
     */
    public ObjectProperty<ContextMenu> contextMenuProperty() {

        return this.treeView.contextMenuProperty();
    }

    /**
     * Creates a new instance of the {@link ProjectTreeView} class.
     */
    public ProjectTreeView() {

        this.setPadding(new Insets(10));
        this.setVgap(5);

        this.projects = FXCollections.observableList(new LinkedList<>());

        this.treeItems = new LinkedHashMap<>();

        this.root = new TreeItem<>("Projects");
        this.root.setExpanded(true);

        this.projectLabel = new Label("Projects");

        this.treeView = new TreeView<>(root);
        this.treeView.setShowRoot(false);

        this.projectLabel.setLabelFor(this.treeView);

        this.add(this.projectLabel, 0, 0);
        this.add(this.treeView, 0, 1);

        GridPane.setHgrow(treeView, Priority.ALWAYS);
        GridPane.setVgrow(treeView, Priority.ALWAYS);

        this.setupEventHandlers();
    }

    private void setupEventHandlers() {

        this.projects.addListener((Change<? extends Project> change) -> {

            while (change.next()) {

                if (change.wasRemoved()) {

                    for (Project project : change.getRemoved()) {

                        this.remove(project);
                    }
                }
                else if (change.wasAdded()) {

                    for (Project project : change.getAddedSubList()) {

                        this.addProject(project);
                    }
                }
            }
        });
    }

    private void addProject(Project project) {

        DomainObjectTreeItem projectNode = new DomainObjectTreeItem(project);

        this.treeItems.put(project, projectNode);
        this.root.getChildren().add(projectNode);
    }

    /**
     * Sets the value of the property onMouseClicked of the contained treeview.
     * 
     * @param value
     *            the handler.
     */
    public void setOnMouseClickedTreeView(EventHandler<? super MouseEvent> value) {

        this.treeView.setOnMouseClicked(value);
    }

    /**
     * Removes a domain object from the tree view.
     * 
     * @param domainObject
     *            the domain object to remove.
     */
    public void remove(IDomainObject domainObject) {

        DomainObjectTreeItem objectNode = this.treeItems.get(domainObject);

        this.remove(objectNode);
    }


    private void remove(TreeItem<String> treeItem) {

        if (treeItem != null) {

            if (treeItem instanceof DomainObjectTreeItem) {

                this.treeItems.remove(((DomainObjectTreeItem) treeItem)
                        .getObject());
            }

            for (TreeItem<String> child : treeItem.getChildren()) {

                this.remove(child);
            }
            treeItem.getChildren().clear();
        }
    }

    /**
     * Returns whether or not the project has been opened.
     * 
     * @param project
     *            the project to check.
     * @return {@code true}, if the project has been opened; {@code false}
     *         otherwise.
     */
    public boolean isOpen(Project project) {


        DomainObjectTreeItem projectNode = this.treeItems.get(project);

        if (projectNode != null && !projectNode.isLeaf()) {

            return true;
        }

        return false;
    }

    /**
     * Opens the project.
     * 
     * @param project
     *            the project.
     */
    public void openProject(Project project) {

        DomainObjectTreeItem projectNode = this.treeItems.get(project);

        if (projectNode != null) {

            TreeItem<String> dataSourceRootNode = new TreeItem<>(
                    ProjectTreeView.DATASOURCES);
            TreeItem<String> metadataSourceRootNode = new TreeItem<>(
                    ProjectTreeView.METADATASOURCES);
            TreeItem<String> dataModelRootNode = new TreeItem<>(
                    ProjectTreeView.DATAMODEL);

            projectNode.getChildren().add(dataSourceRootNode);
            projectNode.getChildren().add(metadataSourceRootNode);
            projectNode.getChildren().add(dataModelRootNode);

            for (IDataSource dataSource : project.getDataSources()) {

                DomainObjectTreeItem dataSourceNode = new DomainObjectTreeItem(
                        dataSource);
                dataSourceRootNode.getChildren().add(dataSourceNode);
                this.treeItems.put(dataSource, dataSourceNode);
            }

            for (IMetadataSource metadataSource : project.getMetadataSources()) {

                DomainObjectTreeItem metadataSourceNode = new DomainObjectTreeItem(
                        metadataSource);
                metadataSourceRootNode.getChildren().add(metadataSourceNode);
                this.treeItems.put(metadataSource, metadataSourceNode);
            }

            DataModel dataModel = project.getDataModel();
            if (dataModel != null) {

                DomainObjectTreeItem dataModelNode = new DomainObjectTreeItem(
                        dataModel);

                dataModelRootNode.getChildren().add(dataModelNode);
                this.treeItems.put(dataModel, dataModelNode);
            }

            projectNode.setExpanded(true);
            dataSourceRootNode.setExpanded(true);
            metadataSourceRootNode.setExpanded(true);
            dataModelRootNode.setExpanded(true);

            this.treeView.getSelectionModel().clearSelection();
            this.treeView.getSelectionModel().select(projectNode);
        }
    }

    /**
     * Closes the project.
     * 
     * @param project
     *            the project to close.
     */
    public void closeProject(Project project) {


        DomainObjectTreeItem projectNode = this.treeItems.get(project);

        for (TreeItem<String> child : projectNode.getChildren()) {

            this.remove(child);
        }
        projectNode.getChildren().clear();
    }

    /**
     * Returns the parent {@link Project} of the currently selected item.
     * 
     * @return the parent {@link Project} or {@code null}, if not applicable.
     */
    public Project getParentProject() {

        TreeItem<String> current = this.getSelectedItem();

        while (current != null) {

            if (current instanceof DomainObjectTreeItem) {

                DomainObjectTreeItem treeItem = (DomainObjectTreeItem) current;

                IDomainObject domainObject = treeItem.getObject();
                if (domainObject instanceof Project) {

                    return (Project) domainObject;
                }
            }

            current = current.getParent();
        }

        return null;
    }

    /**
     * Updates a tree item with a new value.
     * 
     * @param oldValue
     *            the old value.
     * @param newValue
     *            the new value.
     */
    public void updateTreeItem(IDomainObject oldValue, IDomainObject newValue) {

        DomainObjectTreeItem treeItem = this.treeItems.get(oldValue);

        if (treeItem != null) {

            treeItem.setObject(newValue);
            this.treeItems.remove(oldValue);
            this.treeItems.put(newValue, treeItem);

            if (treeItem.getObject() instanceof Project && !treeItem.isLeaf()) {

                Project project = (Project) treeItem.getObject();
                for (TreeItem<String> descriptionNode : treeItem.getChildren()) {

                    if (ProjectTreeView.DATASOURCES.equals(descriptionNode
                            .getValue())) {

                        for (IDataSource dataSource : project.getDataSources()) {

                            DomainObjectTreeItem dataSourceNode = this.treeItems
                                    .get(dataSource);
                            if (dataSourceNode == null) {

                                dataSourceNode = new DomainObjectTreeItem(
                                        dataSource);

                                descriptionNode.getChildren().add(
                                        dataSourceNode);
                            }
                            else if (dataSourceNode.getObject().getVersion() < dataSource
                                    .getVersion()) {

                                dataSourceNode.setObject(dataSource);
                                this.treeItems.put(dataSource, dataSourceNode);
                            }
                        }
                    }
                    else if (ProjectTreeView.METADATASOURCES
                            .equals(descriptionNode.getValue())) {

                        for (IMetadataSource metadataSource : project
                                .getMetadataSources()) {
                            DomainObjectTreeItem metadataSourceNode = this.treeItems
                                    .get(metadataSource);
                            if (metadataSourceNode == null) {

                                metadataSourceNode = new DomainObjectTreeItem(
                                        metadataSource);

                                descriptionNode.getChildren().add(
                                        metadataSourceNode);
                            }
                            else if (metadataSourceNode.getObject()
                                    .getVersion() < metadataSource.getVersion()) {

                                metadataSourceNode.setObject(metadataSource);
                                this.treeItems.put(metadataSource,
                                        metadataSourceNode);
                            }
                        }
                    }
                    else if (ProjectTreeView.DATAMODEL.equals(descriptionNode
                            .getValue())) {

                        DataModel dataModel = project.getDataModel();
                        DomainObjectTreeItem dataModelNode = this.treeItems
                                .get(dataModel);
                        if (dataModelNode == null) {

                            dataModelNode = new DomainObjectTreeItem(dataModel);

                            descriptionNode.getChildren().add(dataModelNode);
                        }
                        else if (dataModelNode.getObject().getVersion() < dataModel
                                .getVersion()) {

                            dataModelNode.setObject(dataModel);
                            this.treeItems.put(dataModel, dataModelNode);
                        }
                    }
                }
            }
        }
    }
}
