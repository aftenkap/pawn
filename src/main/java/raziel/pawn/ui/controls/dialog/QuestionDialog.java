package raziel.pawn.ui.controls.dialog;


import javafx.scene.Node;
import javafx.stage.Window;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.controls.QuestionDetailsGridPane;


/**
 * The {@code QuestionDialog} class provides a validated
 * {@link javafx.scene.control.Dialog Dialog} for editing a {@link IQuestion
 * Question}.
 * 
 * @param <T>
 *            the concrete type of the {@link IQuestion Question}.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class QuestionDialog<T extends IQuestion>
        extends DomainObjectDialog<T> {


    private final QuestionDetailsGridPane questionDetails;


    /**
     * Creates a new instance of the {@link QuestionDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param question
     *            the question to rename or {@code null} to create a new
     *            {@link IQuestion}.
     */
    public QuestionDialog(final Node owner, String title, T question) {

        this(Utils.windowOf(owner), title, question);
    }

    /**
     * Creates a new instance of the {@link QuestionDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param question
     *            the question to rename or {@code null} to create a new
     *            {@link IQuestion}.
     */
    public QuestionDialog(Window owner, String title, T question) {

        super(owner, title, question);

        this.questionDetails = new QuestionDetailsGridPane();
        this.questionDetails.setUpdateDeferred(true);

        this.questionDetails.setMetadata(question);

        this.content().add(this.questionDetails, 0, 1);

        this.setupValidation();
    }

    private void setupValidation() {

        this.validationGroup().registerSubValidation(this.questionDetails,
                this.questionDetails.validationGroup());
    }
}
