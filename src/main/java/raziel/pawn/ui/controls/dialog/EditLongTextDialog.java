package raziel.pawn.ui.controls.dialog;


import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;



/**
 * A dialog for editing long text.
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class EditLongTextDialog
        extends Dialog<String> {

    private TextArea textArea;
    private GridPane content;



    /**
     * Creates a new instance of the {@link EditLongTextDialog} class.
     * 
     * @param owner
     *            the owner of this dialog.
     * @param text
     *            the text to edit.
     */
    public EditLongTextDialog(Window owner, String text) {

        super();

        this.initOwner(owner);
        this.setTitle("Edit");


        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(this.content);

        this.textArea = new TextArea();
        if (text != null) {

            this.textArea.setText(text);
        }
        this.textArea.setPromptText("Cannot be empty");

        this.textArea.setMaxHeight(Double.MAX_VALUE);
        this.textArea.setMaxWidth(Double.MAX_VALUE);

        this.content.add(this.textArea, 0, 0);
        GridPane.setHgrow(this.textArea, Priority.ALWAYS);

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.setResultConverter((param) -> {

            if (param == ButtonType.OK) {

                return this.textArea.getText();
            }

            return null;
        });

        this.setupEventHandlers();
        this.validate();

        Platform.runLater(() -> {

            this.textArea.requestFocus();
        });

    }

    private void setupEventHandlers() {

        this.textArea.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });
    }

    private void validate() {

        this.getDialogPane()
                .lookupButton(ButtonType.OK)
                .disableProperty()
                .set(this.textArea.getText() == null
                        || this.textArea.getText().trim().isEmpty());

    }
}
