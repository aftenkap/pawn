package raziel.pawn.ui.controls.dialog;


import java.util.Optional;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Window;

import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionUtils;
import org.jutility.javafx.control.CellRangeGridPane;

import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.descriptors.TableDescriptor;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class CreateTableLinkDialog
        extends Dialog<TableDescriptor> {



    private GridPane                content;

    private final Spreadsheet       spreadsheet;

    private TableDescriptor         link;

    private ToggleGroup             toggleGroup;
    private RadioButton             textQuestion;
    private RadioButton             choiceQuestion;
    private RadioButton             rankingQuestion;
    private RadioButton             participant;

    private VBox                    toggleVBox;
    private HBox                    contentDescriptorHBox;

    private Action                  add;

    private final CellRangeGridPane range;


    /**
     * Creates a new instance of the {@link CreateTableLinkDialog} class.
     * 
     * @param title
     *            the title.
     * @param spreadsheet
     *            the {@link Spreadsheet}.
     * @param owner
     *            the owner.
     */
    public CreateTableLinkDialog(String title, Spreadsheet spreadsheet,
            Window owner) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.link = new TableDescriptor(CreateTableLinkDialog.this.spreadsheet);
        this.spreadsheet = spreadsheet;
        this.link = null;

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);

        this.getDialogPane().setContent(content);

        this.toggleGroup = new ToggleGroup();

        this.textQuestion = new RadioButton("Text Question");
        this.choiceQuestion = new RadioButton("Choice Question");
        this.rankingQuestion = new RadioButton("Ranking Question");
        this.participant = new RadioButton("Participant");

        this.textQuestion.setToggleGroup(toggleGroup);
        this.textQuestion.setSelected(true);
        this.choiceQuestion.setToggleGroup(toggleGroup);
        this.rankingQuestion.setToggleGroup(toggleGroup);
        this.participant.setToggleGroup(toggleGroup);

        Label addLabel = new Label("Add Content Descriptor");
        addLabel.setFont(Font.font("verdana", 16));

        this.toggleVBox = new VBox(15);
        this.toggleVBox.setPadding(new Insets(5));

        this.toggleVBox.getChildren().add(this.textQuestion);
        this.toggleVBox.getChildren().add(this.choiceQuestion);
        this.toggleVBox.getChildren().add(this.rankingQuestion);
        this.toggleVBox.getChildren().add(this.participant);

        addLabel.setLabelFor(this.toggleVBox);

        this.contentDescriptorHBox = new HBox(10);

        this.range = new CellRangeGridPane(spreadsheet.cellRange());

        this.contentDescriptorHBox.getChildren().add(this.toggleVBox);
        this.contentDescriptorHBox.getChildren().add(this.range);


        this.add = new Action("Add", event -> {
            CreateTableLinkDialog.this.range.setCellRange(null);
        });


        this.content.add(addLabel, 0, 0);
        this.content.add(this.contentDescriptorHBox, 0, 1);

        this.content.add(ActionUtils.createButton(this.add), 0, 2);

        this.setResultConverter((result) -> {

            return this.link;
        });

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .bind(this.range.invalidProperty());

        this.setupEventHandlers();

        Platform.runLater(() -> {

            this.range.requestFocus();
        });
    }


    private void setupEventHandlers() {

        this.add.disabledProperty().bind(this.range.invalidProperty());
    }


    /**
     * Creates a lightweight dialog for the creation of a
     * {@link TableDescriptor}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to link to.
     * @return the new {@link TableDescriptor}.
     */
    public static Optional<TableDescriptor> showDialog(Spreadsheet spreadsheet) {

        return CreateTableLinkDialog.showDialog(spreadsheet, null);
    }

    /**
     * Creates a dialog for the creation of a {@link TableDescriptor}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to link to.
     * @param owner
     *            the owner of the dialog.
     * @return the new {@link TableDescriptor}.
     */
    public static Optional<TableDescriptor> showDialog(Spreadsheet spreadsheet,
            Window owner) {

        CreateTableLinkDialog dialog = new CreateTableLinkDialog(
                "Create Spreadsheet Descriptor", spreadsheet, owner);

        return dialog.showAndWait();
    }
}
