package raziel.pawn.ui.controls.dialog;


import java.util.Collection;
import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;

import org.jutility.javafx.control.labeled.LabeledComboBox;

import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @param <T>
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class QuestionSelectionDialog<T extends IQuestion>
        extends Dialog<T> {

    private final LabeledComboBox<T> questionsCB;

    private final GridPane           content;

    /**
     * Creates a new instance of the {@link QuestionSelectionDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param questions
     *            the questions from which to select.
     */
    public QuestionSelectionDialog(Window owner, String title,
            Collection<T> questions) {

        super();
        this.initOwner(owner);
        this.setTitle(title);

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(content);

        this.questionsCB = new LabeledComboBox<>("Select a Question:");
        this.questionsCB.setConverter(
        new DomainObjectStringConverter<T>());

        this.questionsCB.getItems().addAll(questions);

        this.questionsCB.setMaxWidth(Double.MAX_VALUE);


        this.content.add(this.questionsCB, 0, 0);
        GridPane.setHgrow(this.questionsCB, Priority.ALWAYS);

        this.setResultConverter(param -> {

            if (param == ButtonType.OK) {

                return this.questionsCB.getSelectionModel().getSelectedItem();
            }

            return null;
        });

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .set(true);

        this.setupEventHandlers();


        Platform.runLater(() -> {

            this.questionsCB.requestFocus();
        });


    }


    private void setupEventHandlers() {

        this.questionsCB.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });
    }


    private void validate() {

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .set(this.questionsCB.getSelectionModel().getSelectedItem() == null);

    }

    /**
     * Displays a non-modal question dialog.
     * 
     * @param questions
     *            the questions from which to select.
     * @return the selected {@link IQuestion} or {@code null}.
     */
    public static <T extends IQuestion> Optional<T> showDialog(
            Collection<T> questions) {

        return QuestionSelectionDialog.showDialog(null, questions);
    }

    /**
     * Displays a modal question dialog (if an owner is provided).
     * 
     * @param owner
     *            the owner of the dialog.
     * 
     * @param questions
     *            the questions from which to select.
     * @return the selected {@link IQuestion} or {@code null}.
     */
    public static <T extends IQuestion> Optional<T> showDialog(Window owner,
            Collection<T> questions) {

        QuestionSelectionDialog<T> dialog;

        dialog = new QuestionSelectionDialog<>(owner, "Select a Question",
                questions);

        return dialog.showAndWait();
    }
}
