package raziel.pawn.ui.controls.dialog;


import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;

import org.jutility.javafx.control.labeled.LabeledTextField;

import raziel.pawn.core.domain.metadata.ChoiceSet;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
public class ChoiceSetDialog
        extends Dialog<ChoiceSet> {

    private final LabeledTextField choiceSetNameTF;

    private final GridPane         content;

    private ChoiceSet              choiceSet;

    /**
     * Creates a new instance of the {@link ChoiceSetDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param choiceSet
     *            the choiceSet to rename or {@code null} to create a new
     *            {@link ChoiceSet}.
     */
    public ChoiceSetDialog(Window owner, String title, ChoiceSet choiceSet) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.choiceSet = choiceSet;

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(content);

        final boolean choiceSetProvided = choiceSet != null;


        this.choiceSetNameTF = new LabeledTextField("ChoiceSet Name");
        this.choiceSetNameTF
                .setPromptText("Enter ChoiceSet Name (cannot be empty)");
        if (choiceSetProvided) {

            choiceSetNameTF.setText(choiceSet.getName());
        }

        choiceSetNameTF.setMaxHeight(Double.MAX_VALUE);
        choiceSetNameTF.setMaxWidth(Double.MAX_VALUE);

        GridPane.setHgrow(choiceSetNameTF, Priority.ALWAYS);

        this.content.add(choiceSetNameTF, 0, 0);

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.setResultConverter((param) -> {

            if (param == ButtonType.OK) {

                if (this.choiceSet != null) {

                    this.choiceSet.setName(this.choiceSetNameTF.getText());
                }
                else {

                    this.choiceSet = new ChoiceSet(this.choiceSetNameTF
                            .getText());
                }
                return this.choiceSet;
            }

            return null;
        });

        this.getDialogPane().lookupButton(ButtonType.OK)
                .setDisable(!choiceSetProvided);

        this.setupEventHandlers();


        Platform.runLater(() -> {

            this.choiceSetNameTF.requestFocus();
        });
    }


    private void setupEventHandlers() {


        this.choiceSetNameTF.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });
    }


    private void validate() {

        this.getDialogPane()
                .lookupButton(ButtonType.OK)
                .setDisable(
                        (this.choiceSetNameTF.getText() == null || this.choiceSetNameTF
                                .getText().trim().isEmpty()));
    }

    /**
     * Displays a non-modal choiceSet dialog.
     * 
     * @param choiceSet
     *            the choiceSet to rename or {@code null} to create a new
     *            choiceSet.
     * @return the renamed or new {@link ChoiceSet}.
     */
    public static Optional<ChoiceSet> showDialog(ChoiceSet choiceSet) {

        return ChoiceSetDialog.showDialog(null, choiceSet);
    }

    /**
     * Displays a modal choiceSet dialog (if an owner is provided).
     * 
     * @param owner
     *            the owner of the dialog.
     * 
     * @param choiceSet
     *            the choiceSet to rename or {@code null} to create a new
     *            choiceSet.
     * @return the renamed or new {@link ChoiceSet}.
     */
    public static Optional<ChoiceSet> showDialog(Window owner,
            ChoiceSet choiceSet) {

        ChoiceSetDialog dialog;

        if (choiceSet == null) {

            dialog = new ChoiceSetDialog(owner, "Create New ChoiceSet",
                    choiceSet);
        }
        else {

            dialog = new ChoiceSetDialog(owner, "Edit ChoiceSet", choiceSet);
        }

        return dialog.showAndWait();
    }
}
