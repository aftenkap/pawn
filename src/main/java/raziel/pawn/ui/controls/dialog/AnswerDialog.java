package raziel.pawn.ui.controls.dialog;


import java.util.Objects;

import javafx.stage.Window;
import raziel.pawn.core.domain.metadata.IAnswer;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.ui.controls.AnswerDetailsGridPane;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public class AnswerDialog
        extends DomainObjectDialog<IAnswer> {


    private final AnswerDetailsGridPane answerDetails;


    /**
     * Creates a new instance of the {@link AnswerDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param answer
     *            the question to rename or {@code null} to create a new
     *            {@link IAnswer}.
     * @param survey
     *            the {@link Survey} containing the answer.
     */
    public AnswerDialog(final Window owner, final String title,
            final IAnswer answer, final Survey survey) {

        super(owner, title, answer);

        Objects.requireNonNull(survey, "Cannot edit answer without survey.");

        this.answerDetails = new AnswerDetailsGridPane();

        this.answerDetails.setUpdateDeferred(true);

        this.answerDetails.setSurvey(survey);
        this.answerDetails.setMetadata(answer);

        this.content().add(this.answerDetails, 0, 1);

        this.setupValidation();
    }

    private void setupValidation() {

        this.validationGroup().registerSubValidation(this.answerDetails,
                this.answerDetails.validationGroup());
    }
}
