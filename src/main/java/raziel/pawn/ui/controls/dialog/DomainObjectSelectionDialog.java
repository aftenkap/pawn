package raziel.pawn.ui.controls.dialog;


import java.util.Collection;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.Priority;
import javafx.stage.Window;

import org.controlsfx.validation.Validator;
import org.jutility.javafx.control.labeled.LabeledComboBox;

import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * The {@code DomainObjectSelectionDialog} class provides a validated
 * {@link Dialog} for selecting a {@link IDomainObject DomainObject} from a
 * collection of {@link IDomainObject DomainObjects}.
 * 
 * @param <T>
 *            the concrete type of the {@link IDomainObject DomainObject}.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class DomainObjectSelectionDialog<T extends IDomainObject>
        extends Dialog<T> {

    private final LabeledComboBox<T> domainObjectCB;


    /**
     * Creates a new instance of the {@code DomainObjectSelectionDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param domainObjects
     *            the {@link IDomainObject DomainObjects} from which to choose.
     */
    public DomainObjectSelectionDialog(final Node owner, final String title,
            final Collection<T> domainObjects) {

        this(Utils.windowOf(owner), title, domainObjects);
    }

    /**
     * Creates a new instance of the {@code DomainObjectSelectionDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param domainObjects
     *            the {@link IDomainObject DomainObjects} from which to choose.
     */
    public DomainObjectSelectionDialog(final Window owner, final String title,
            final Collection<T> domainObjects) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.domainObjectCB = new LabeledComboBox<>("Please select an item!");
        this.domainObjectCB.setConverter(new DomainObjectStringConverter<>());

        this.domainObjectCB.getItems().addAll(domainObjects);

        this.domainObjectCB.getLabel().setPadding(new Insets(0, 10, 0, 0));

        this.domainObjectCB.setMaxWidth(Double.MAX_VALUE);
        this.domainObjectCB.setControlHGrow(Priority.ALWAYS);

        this.setupValidation();

        this.getDialogPane().setContent(this.domainObjectCB);

        this.setResultConverter((param) -> {

            if (param == ButtonType.OK) {

                return this.domainObjectCB.getValue();
            }
            return null;
        });

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .bind(this.domainObjectCB.invalidProperty());

        Platform.runLater(() -> {

            this.domainObjectCB.requestFocus();
        });
    }

    private void setupValidation() {


        this.domainObjectCB.registerValidator(Validator
                .createPredicateValidator((T value) -> {

                    return value != null;
                }, "You need to select one of the choices!"));
        this.domainObjectCB.setErrorDecorationEnabled(true);

        this.domainObjectCB.validationSupport().revalidate();
    }
}
