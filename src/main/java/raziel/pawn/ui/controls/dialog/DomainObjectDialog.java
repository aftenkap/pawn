package raziel.pawn.ui.controls.dialog;


import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;

import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import org.jutility.javafx.control.labeled.LabeledTextField;
import org.jutility.javafx.control.validation.ValidationGroup;

import raziel.pawn.core.domain.IDomainObject;


/**
 * The {@code DomainObjectDialog} class provides a {@link Dialog} for creating
 * and editing {@link IDomainObject DomainObjects}.
 * 
 * @param <T>
 *            the actual type of the {@link IDomainObject DomainObject}.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 *
 */
public class DomainObjectDialog<T extends IDomainObject>
        extends Dialog<T> {

    private final T                domainObject;

    private final LabeledTextField nameTF;
    private final GridPane         content;

    private final ValidationGroup  validationGroup;

    /**
     * Returns the {@link ValidationGroup} of this dialog.
     * 
     * @return the {@link ValidationGroup} of this dialog.
     */
    protected ValidationGroup validationGroup() {

        return this.validationGroup;
    }

    /**
     * The content of this {@link DomainObjectDialog}.
     * 
     * @return the content of this {@link DomainObjectDialog}.
     */
    protected GridPane content() {

        return this.content;
    }

    /**
     * Returns the value of the name text field.
     * 
     * @return the value of the name text field.
     */
    protected String getName() {

        return this.nameTF.getText();
    }

    /**
     * Creates a new instance of the {@link DomainObjectDialog} class.
     * 
     * @param owner
     *            the owner of this dialog.
     * @param title
     *            the title of this dialog.
     * @param domainObject
     *            the domain object.
     */
    public DomainObjectDialog(Window owner, String title, T domainObject) {

        super();
        this.initOwner(owner);
        this.setTitle(title);

        if (domainObject == null) {

            throw new IllegalArgumentException(
                    "Dialog requires non-null object!");
        }
        this.domainObject = domainObject;

        this.validationGroup = new ValidationGroup();

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(content);

        this.nameTF = new LabeledTextField("Name");
        this.nameTF.setPromptText("Enter Name (cannot be empty)");
        this.nameTF.setHgap(10);

        this.nameTF.setText(domainObject.getName());

        this.nameTF.setMaxHeight(Double.MAX_VALUE);
        this.nameTF.setMaxWidth(Double.MAX_VALUE);


        this.content.add(this.nameTF, 0, 0);
        GridPane.setHgrow(this.nameTF, Priority.ALWAYS);

        this.setResultConverter((param) -> {

            return this.convertResult(param);
        });

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .bind(this.validationGroup.invalidProperty());

        this.setupValidation();
        this.setupEventHandlers();

        Platform.runLater(() -> {

            this.nameTF.requestFocus();
        });
    }

    private void setupEventHandlers() {

        this.nameTF.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    if (this.domainObject != null && newValue != null
                            && !newValue.equals(oldValue)) {

                        this.domainObject.setName(this.nameTF.getText());
                    }
                });
    }

    private void setupValidation() {

        this.nameTF.registerValidator(Validator
                .createEmptyValidator("Name cannot be empty!"));
        this.nameTF.setValidationDecorator(new GraphicValidationDecoration());

        this.validationGroup.registerSubValidation(this.nameTF,
                this.nameTF.validationSupport());
    }

    /**
     * Converts the result from {@link ButtonType} into the type of the
     * {@link IDomainObject DomainObject}.
     * 
     * @param buttonType
     *            the button type.
     * @return the equivalent {@link IDomainObject DomainObject}.
     */
    protected T convertResult(final ButtonType buttonType) {

        if (buttonType == ButtonType.OK) {

            return this.domainObject;
        }

        return null;
    }
}
