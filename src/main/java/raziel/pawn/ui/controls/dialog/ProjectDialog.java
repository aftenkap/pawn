package raziel.pawn.ui.controls.dialog;



import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;
import raziel.pawn.core.domain.Project;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 *
 */
public class ProjectDialog
        extends Dialog<Project> {

    private Project   project;
    private TextField projectNameTF;
    private GridPane  content;



    /**
     * Creates a new instance of the {@link ProjectDialog} class.
     * 
     * @param owner
     * @param title
     * @param project
     */
    public ProjectDialog(Window owner, String title, Project project) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.project = project;

        final boolean projectProvided = project != null;

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(content);

        this.projectNameTF = new TextField();
        this.projectNameTF
                .setPromptText("Enter Project Name (cannot be empty)");
        if (projectProvided) {

            this.projectNameTF.setText(project.getName());
        }

        this.projectNameTF.setMaxHeight(Double.MAX_VALUE);
        this.projectNameTF.setMaxWidth(Double.MAX_VALUE);

        this.content.add(new Label("Project Name"), 0, 0);
        this.content.add(this.projectNameTF, 1, 0);
        GridPane.setHgrow(this.projectNameTF, Priority.ALWAYS);


        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);


        this.setResultConverter((param) -> {

            if (param == ButtonType.OK) {

                if (projectProvided) {

                    this.project.setName(this.projectNameTF.getText());
                }
                else {

                    this.project = new Project(this.projectNameTF.getText());
                }

                return this.project;
            }

            return null;
        });

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .set(!projectProvided);

        this.setupEventHandlers();

        Platform.runLater(() -> {

            this.projectNameTF.requestFocus();
        });

    }

    private void setupEventHandlers() {

        this.projectNameTF.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });

    }

    private void validate() {

        this.getDialogPane()
                .lookupButton(ButtonType.OK)
                .disableProperty()
                .set(this.projectNameTF.getText() == null
                        || this.projectNameTF.getText().trim().isEmpty());

    }

    /**
     * @param owner
     * @param project
     * @return the created or edited {@link Project}.
     */
    public static Optional<Project> showDialog(Window owner, Project project) {

        ProjectDialog dialog;

        if (project == null) {

            dialog = new ProjectDialog(owner, "Create New Project", project);
        }
        else {

            dialog = new ProjectDialog(owner, "Edit Project", project);
        }

        return dialog.showAndWait();
    }
}
