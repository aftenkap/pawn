package raziel.pawn.ui.controls.dialog;


import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Window;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.ui.controls.SpreadsheetView;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.0
 */
public class SpreadsheetViewDialog
        extends Dialog<ButtonType> {



    private final SpreadsheetView content;


    /**
     * Creates a new instance of the {@link SpreadsheetViewDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param spreadsheet
     *            the {@link Spreadsheet}.
     */
    public SpreadsheetViewDialog(Window owner, String title,
            Spreadsheet spreadsheet) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.content = new SpreadsheetView(spreadsheet);
        this.getDialogPane().setContent(this.content);


        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);
    }



    /**
     * @param spreadsheet
     *            the {@link Spreadsheet}.
     * 
     */
    public static void showDialog(final Spreadsheet spreadsheet) {

        SpreadsheetViewDialog.showDialog(spreadsheet, null);
    }

    /**
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet}.
     * @param owner
     *            the owner of the dialog.
     * 
     */
    public static void showDialog(final Spreadsheet spreadsheet, Window owner) {

        SpreadsheetViewDialog dialog = new SpreadsheetViewDialog(owner,
                "Edit Spreadsheet", spreadsheet);

        dialog.showAndWait();
    }
}
