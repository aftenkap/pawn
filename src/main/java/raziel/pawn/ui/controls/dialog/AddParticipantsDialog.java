package raziel.pawn.ui.controls.dialog;



import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;



/**
 *
 *
 * @author Peter J. Radics
 * @version 0.1
 */
public class AddParticipantsDialog
        extends Dialog<Integer> {

    private TextField numberOfParticipantsTF;
    private GridPane  content;



    /**
     * Creates a new instance of the {@link AddParticipantsDialog} class with
     * the provided title.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title.
     */
    public AddParticipantsDialog(Window owner, String title) {

        super();
        this.initOwner(owner);
        this.setTitle(title);


        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(content);

        numberOfParticipantsTF = new TextField();
        numberOfParticipantsTF
                .setPromptText("Enter Number of Participants to create (cannot be empty)!");

        numberOfParticipantsTF.setMaxHeight(Double.MAX_VALUE);
        numberOfParticipantsTF.setMaxWidth(Double.MAX_VALUE);

        content.add(new Label("Number of Participants to Add"), 0, 0);
        content.add(numberOfParticipantsTF, 1, 0);
        GridPane.setHgrow(numberOfParticipantsTF, Priority.ALWAYS);

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.setResultConverter((param) -> {

            if (param == ButtonType.OK) {

                return this.parseInteger();
            }

            return null;
        });

        this.getDialogPane().lookupButton(ButtonType.OK).setDisable(true);

        Platform.runLater(() -> {

            this.numberOfParticipantsTF.requestFocus();
        });

        this.setupEventHandlers();
    }

    private void setupEventHandlers() {

        this.numberOfParticipantsTF.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });
    }

    private void validate() {

        this.getDialogPane().lookupButton(ButtonType.OK)
                .setDisable(this.parseInteger() == null);
    }

    private Integer parseInteger() {

        try {

            Integer number = Integer.parseInt(this.numberOfParticipantsTF
                    .getText());

            if (number != null && number > 0) {

                return number;
            }
        }
        catch (NumberFormatException ex) {

            return null;
        }

        return null;
    }

    /**
     * Shows the Add Participant Dialog.
     * 
     * @param owner
     *            the owner.
     *
     * @return the number of participants to add.
     */
    public static Optional<Integer> showDialog(Window owner) {

        AddParticipantsDialog dialog = new AddParticipantsDialog(owner,
                "Enter Number of Participants to Create");

        return dialog.showAndWait();
    }
}
