package raziel.pawn.ui.controls.dialog;


import java.util.List;
import java.util.Optional;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Window;

import org.jutility.common.datatype.table.CellRange;
import org.jutility.javafx.control.CellRangeGridPane;
import org.jutility.javafx.control.labeled.LabeledComboBox;

import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.ChoiceSet;
import raziel.pawn.core.domain.metadata.descriptors.TableChoiceQuestionDescriptor;
import raziel.pawn.core.domain.metadata.descriptors.TableDescriptor;
import raziel.pawn.core.domain.metadata.descriptors.TableMetadataDescriptor;
import raziel.pawn.core.domain.metadata.descriptors.TableParticipantDescriptor;
import raziel.pawn.core.domain.metadata.descriptors.TableRankingQuestionDescriptor;
import raziel.pawn.core.domain.metadata.descriptors.TableTextQuestionDescriptor;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class TableMetadataDescriptorDialog
        extends Dialog<TableMetadataDescriptor<?>> {

    private GridPane                   content;

    private final Spreadsheet          spreadsheet;

    private TableMetadataDescriptor<?> descriptor;

    private ToggleGroup                toggleGroup;
    private RadioButton                textQuestion;
    private RadioButton                choiceQuestion;
    private RadioButton                rankingQuestion;
    private RadioButton                participant;

    private LabeledComboBox<ChoiceSet> choices;

    private VBox                       toggleVBox;
    private HBox                       contentDescriptorHBox;

    private final CellRangeGridPane    range;


    /**
     * Creates a new instance of the {@link TableMetadataDescriptorDialog}
     * class.
     * 
     * @param title
     *            the title of the dialog.
     * @param spreadsheet
     *            the spreadsheet to describe.
     * @param owner
     *            the owner of the dialog.
     * @param choices
     *            the choice sets questions can use.
     */
    public TableMetadataDescriptorDialog(String title, Window owner,
            Spreadsheet spreadsheet, List<ChoiceSet> choices) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.spreadsheet = spreadsheet;

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);

        this.getDialogPane().setContent(this.content);

        this.toggleGroup = new ToggleGroup();

        this.textQuestion = new RadioButton("Text Question");
        this.choiceQuestion = new RadioButton("Choice Question");
        this.rankingQuestion = new RadioButton("Ranking Question");
        this.participant = new RadioButton("Participant");

        this.textQuestion.setToggleGroup(toggleGroup);
        this.textQuestion.setSelected(true);
        this.choiceQuestion.setToggleGroup(toggleGroup);
        this.rankingQuestion.setToggleGroup(toggleGroup);
        this.participant.setToggleGroup(toggleGroup);


        this.toggleVBox = new VBox(15);
        this.toggleVBox.setPadding(new Insets(5));

        this.toggleVBox.getChildren().add(this.textQuestion);
        this.toggleVBox.getChildren().add(this.choiceQuestion);
        this.toggleVBox.getChildren().add(this.rankingQuestion);
        this.toggleVBox.getChildren().add(this.participant);


        this.contentDescriptorHBox = new HBox(10);

        this.range = new CellRangeGridPane(this.spreadsheet.cellRange());

        this.choices = new LabeledComboBox<>("Choice Set");
        this.choices.setConverter(new DomainObjectStringConverter<>());

        this.choices.getItems().addAll(choices);
        this.choices.setVgap(15);
        this.choices.setPadding(new Insets(5));
        this.choices
                .getLabel()
                .setStyle(
                        "-fx-font-size: 14; -fx-font-family: sans-serif; -fx-font-weight: bold;");

        this.choices.setDisable(true);

        this.contentDescriptorHBox.getChildren().add(this.toggleVBox);
        this.contentDescriptorHBox.getChildren().add(this.range);
        this.contentDescriptorHBox.getChildren().add(this.choices);



        this.content.add(this.contentDescriptorHBox, 0, 0);


        this.setResultConverter(param -> {

            if (param == ButtonType.OK) {

                Toggle selectedToggle = this.toggleGroup.getSelectedToggle();

                CellRange range = this.range.getCellRange();
                ChoiceSet choiceSet = this.choices.getSelectionModel()
                        .getSelectedItem();

                if (selectedToggle == this.textQuestion) {

                    this.descriptor = new TableTextQuestionDescriptor(range);
                }
                else if (selectedToggle == this.choiceQuestion) {

                    this.descriptor = new TableChoiceQuestionDescriptor(range,
                            choiceSet);
                }
                else if (selectedToggle == this.rankingQuestion) {

                    this.descriptor = new TableRankingQuestionDescriptor(range,
                            choiceSet);
                }
                else if (selectedToggle == this.participant) {

                    this.descriptor = new TableParticipantDescriptor(range);
                }

                return this.descriptor;
            }

            return null;
        });
        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .bind(this.range.invalidProperty());

        this.setupEventHandlers();

        Platform.runLater(() -> {

            this.range.requestFocus();
        });
    }

    private void setupEventHandlers() {


        this.toggleGroup
                .selectedToggleProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {

                            boolean choiceBased = (newValue == this.choiceQuestion || newValue == this.rankingQuestion);

                            this.choices.setDisable(!choiceBased);
                        });
    }

    /**
     * Creates a lightweight dialog for the creation of a
     * {@link TableDescriptor}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to link to.
     * @param choices
     *            the {@link ChoiceSet ChoiceSets} to be used for choices.
     * @return the new {@link TableDescriptor}.
     */
    public static Optional<TableMetadataDescriptor<?>> showDialog(
            Spreadsheet spreadsheet, List<ChoiceSet> choices) {

        return TableMetadataDescriptorDialog.showDialog(null, spreadsheet,
                choices);
    }

    /**
     * Creates a dialog for the creation of a {@link TableDescriptor}.
     * 
     * @param spreadsheet
     *            the {@link Spreadsheet} to link to.
     * @param owner
     *            the owner of the dialog.
     * @param choices
     *            the {@link ChoiceSet ChoiceSets} to be used for choices.
     * @return the new {@link TableDescriptor}.
     */
    public static Optional<TableMetadataDescriptor<?>> showDialog(Window owner,
            Spreadsheet spreadsheet, List<ChoiceSet> choices) {

        TableMetadataDescriptorDialog dialog = new TableMetadataDescriptorDialog(
                "Create Data Point Descriptor", owner, spreadsheet, choices);

        return dialog.showAndWait();
    }
}
