package raziel.pawn.ui.controls.dialog;


import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Window;

import org.jutility.javafx.control.labeled.LabeledComboBox;
import org.jutility.javafx.control.labeled.LabeledTextField;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.Choice;
import raziel.pawn.core.domain.metadata.ChoiceSet;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class AddChoiceDialog
        extends Dialog<Choice> {

    private final LabeledTextField        choiceNameTF;

    private final LabeledComboBox<Choice> globalChoicesCB;


    private final ToggleGroup             toggleGroup;
    private final RadioButton             addExistingChoiceRB;
    private final RadioButton             addNewChoiceRB;


    private final GridPane                content;

    /**
     * Creates a new instance of the {@link AddChoiceDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     */
    public AddChoiceDialog(Window owner, String title) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(this.content);

        this.toggleGroup = new ToggleGroup();


        this.addExistingChoiceRB = new RadioButton();
        this.addNewChoiceRB = new RadioButton();

        this.addExistingChoiceRB.setToggleGroup(this.toggleGroup);
        this.addNewChoiceRB.setToggleGroup(this.toggleGroup);
        this.toggleGroup.selectToggle(this.addExistingChoiceRB);

        this.globalChoicesCB = new LabeledComboBox<>("Existing Choice");
        this.globalChoicesCB
                .setConverter(new DomainObjectStringConverter<Choice>());
        this.globalChoicesCB.setMaxHeight(Double.MAX_VALUE);
        this.globalChoicesCB.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(this.globalChoicesCB, Priority.ALWAYS);


        ApplicationContext.surveyController().retrieveAllChoices(owner)
                .ifPresent(choices -> {

                    this.globalChoicesCB.getItems().addAll(choices);

                });
        this.choiceNameTF = new LabeledTextField("New Choice");
        this.choiceNameTF.setPromptText("Enter Choice (cannot be empty)");
        this.choiceNameTF.setMaxHeight(Double.MAX_VALUE);
        this.choiceNameTF.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(this.choiceNameTF, Priority.ALWAYS);


        HBox existingHBox = new HBox(10);
        existingHBox.getChildren().addAll(this.addExistingChoiceRB,
                this.globalChoicesCB);
        GridPane.setHgrow(existingHBox, Priority.ALWAYS);

        HBox newHBox = new HBox(10);
        newHBox.getChildren().addAll(this.addNewChoiceRB, this.choiceNameTF);
        GridPane.setHgrow(newHBox, Priority.ALWAYS);


        this.content.add(existingHBox, 0, 0);
        this.content.add(newHBox, 0, 1);

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.setResultConverter((param) -> {

            if (param == ButtonType.OK) {
                if (this.toggleGroup.getSelectedToggle() == addNewChoiceRB) {

                    return new Choice(this.choiceNameTF.getText());
                }
                else {

                    return this.globalChoicesCB.getSelectionModel()
                            .getSelectedItem();
                }
            }

            return null;
        });

        this.getDialogPane().lookupButton(ButtonType.OK).setDisable(true);

        this.setupEventHandlers();


        Platform.runLater(() -> {

            this.globalChoicesCB.requestFocus();
        });
    }

    private void setupEventHandlers() {

        this.toggleGroup.selectedToggleProperty().addListener(
                (observable, oldValue, newValue) -> {

                    boolean newSelected = newValue == this.addNewChoiceRB;

                    this.choiceNameTF.setDisable(!newSelected);
                    this.globalChoicesCB.setDisable(newSelected);

                    if (newSelected) {

                        this.choiceNameTF.requestFocus();
                    }
                    else {

                        this.globalChoicesCB.requestFocus();
                    }

                    this.validate();

                });

        this.globalChoicesCB.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {

                    this.validate();
                });

        this.choiceNameTF.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });
    }

    private void validate() {

        boolean isInvalid = false;


        if (this.toggleGroup.getSelectedToggle() == addNewChoiceRB) {

            isInvalid = (this.choiceNameTF.getText() == null || this.choiceNameTF
                    .getText().trim().isEmpty());
        }
        else {

            isInvalid = this.globalChoicesCB.getSelectionModel()
                    .getSelectedItem() == null;
        }

        this.getDialogPane().lookupButton(ButtonType.OK).setDisable(isInvalid);
    }

    /**
     * Displays a non-modal add {@link Choice} dialog.
     * 
     * @return the existing or new {@link ChoiceSet}.
     */
    public static Optional<Choice> showDialog() {

        return AddChoiceDialog.showDialog(null);
    }

    /**
     * Displays a modal add {@link Choice} dialog (if an owner is provided).
     * 
     * @param owner
     *            the owner of the dialog.
     * 
     * @return the existing or new {@link Choice}.
     */
    public static Optional<Choice> showDialog(Window owner) {

        AddChoiceDialog dialog;

        dialog = new AddChoiceDialog(owner, "Add Choice");

        return dialog.showAndWait();
    }
}
