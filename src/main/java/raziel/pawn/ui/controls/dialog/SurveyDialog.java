package raziel.pawn.ui.controls.dialog;



import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;
import raziel.pawn.core.domain.metadata.Survey;


/**
 * @author peter
 * @version
 * @since
 *
 */
public class SurveyDialog
        extends Dialog<Survey> {

    private Survey    survey;
    private TextField surveyNameTF;
    private GridPane  content;



    /**
     * Creates a new instance of the {@link SurveyDialog} class.
     * 
     * @param owner
     * @param title
     * @param survey
     */
    public SurveyDialog(Window owner, String title, Survey survey) {

        super();
        this.initOwner(owner);
        this.setTitle(title);

        this.survey = survey;

        final boolean surveyProvided = survey != null;

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(content);

        this.surveyNameTF = new TextField();
        this.surveyNameTF.setPromptText("Enter Survey Name (cannot be empty)");
        if (surveyProvided) {

            this.surveyNameTF.setText(survey.getName());
        }

        this.surveyNameTF.setMaxHeight(Double.MAX_VALUE);
        this.surveyNameTF.setMaxWidth(Double.MAX_VALUE);

        this.content.add(new Label("Survey Name"), 0, 0);
        this.content.add(this.surveyNameTF, 1, 0);
        GridPane.setHgrow(this.surveyNameTF, Priority.ALWAYS);

        this.setResultConverter(param -> {

            if (param == ButtonType.OK) {

                if (surveyProvided) {

                    this.survey.setName(this.surveyNameTF.getText());
                }
                else {

                    this.survey = new Survey(this.surveyNameTF.getText());
                }

                return this.survey;
            }

            return null;
        });

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .set(!surveyProvided);

        this.setupEventHandlers();

        Platform.runLater(() -> {

            this.surveyNameTF.requestFocus();
        });
    }

    private void setupEventHandlers() {


        this.surveyNameTF.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });
    }

    private void validate() {

        this.getDialogPane()
                .lookupButton(ButtonType.OK)
                .disableProperty()
                .set(surveyNameTF.getText() == null
                        || surveyNameTF.getText().trim().isEmpty());

    }

    /**
     * @param owner
     * @param survey
     * @return the created or edited {@link Survey}.
     */
    public static Optional<Survey> showDialog(Window owner, Survey survey) {

        SurveyDialog dialog;

        if (survey == null) {

            dialog = new SurveyDialog(owner, "Create New Survey", survey);
        }
        else {

            dialog = new SurveyDialog(owner, "Edit Survey", survey);
        }

        return dialog.showAndWait();
    }
}
