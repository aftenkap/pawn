package raziel.pawn.ui.controls.dialog;


import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Window;

import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.validation.Validator;
import org.jutility.javafx.control.ListViewWithSearchPanel;

import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @param <T>
 *            the concrete type of the {@link IDomainObject DomainObject}.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.2
 */
public class ActionDialog<T extends IDomainObject>
        extends Dialog<List<T>> {

    private final ListViewWithSearchPanel<T> listView;

    private final Function<T, Optional<T>>   action;
    private final String                     actionName;

    private Action                           applyAction;
    private Action                           remove;

    /**
     * Returns the {@link ListViewWithSearchPanel}.
     * 
     * @return the {@link ListViewWithSearchPanel}.
     */
    protected ListViewWithSearchPanel<T> getListView() {

        return this.listView;
    }


    /**
     * Creates a new instance of the {@code ActionDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param action
     *            the action available on the {@link IDomainObject
     *            DomainObjects}.
     * @param actionName
     *            the name of the action.
     * @param headerText
     *            the header text.
     * @param domainObjects
     *            the {@link IDomainObject DomainObjects}
     */
    public ActionDialog(final Node owner, final String title,
            final String headerText, final Function<T, Optional<T>> action,
            final String actionName, final Collection<T> domainObjects) {

        this(Utils.windowOf(owner), title, headerText, action, actionName,
                domainObjects);
    }

    /**
     * Creates a new instance of the {@code ActionDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title of the dialog.
     * @param headerText
     *            the header text.
     * @param action
     *            the action available on the {@link IDomainObject
     *            DomainObjects}.
     * @param actionName
     *            the name of the action.
     * @param domainObjects
     *            the {@link IDomainObject DomainObjects}.
     */
    public ActionDialog(final Window owner, final String title,
            final String headerText, final Function<T, Optional<T>> action,
            final String actionName, final Collection<T> domainObjects) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.action = action;
        this.actionName = actionName;

        this.listView = new ListViewWithSearchPanel<>(
                FXCollections.observableArrayList(domainObjects), "",
                new DomainObjectStringConverter<>());

        this.setupValidation();
        this.setupContextMenu();

        this.getDialogPane().setHeaderText(headerText);
        this.getDialogPane().setContent(this.listView);

        this.setResultConverter((param) -> {

            if (param == ButtonType.OK) {

                return this.listView.getItems();
            }

            return null;
        });

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .bind(this.listView.invalidProperty());

        Platform.runLater(() -> {

            this.listView.requestFocus();
        });
    }

    
    
    private void setupContextMenu() {

        this.applyAction = new Action(this.actionName, e -> {

            T object = this.listView.getSelectedItem();

            this.action.apply(object).ifPresent(
                    changedObject -> {

                        this.listView.getItems().set(
                                this.listView.getItems().indexOf(object),
                                changedObject);
                    });
        });

        this.remove = new Action("Remove", e -> {

            T object = this.listView.getSelectedItem();

            this.listView.getItems().remove(object);
        });

        this.applyAction.disabledProperty().bind(
                this.listView.selectedItemProperty().isNull());
        this.remove.disabledProperty().bind(
                this.listView.selectedItemProperty().isNull());

        this.listView.setContextMenu(ActionUtils.createContextMenu(Arrays
                .asList(this.applyAction, this.remove)));
    }


    private void setupValidation() {


        this.listView.registerValidator(Validator.createPredicateValidator((
                List<T> value) -> {

            return !value.isEmpty();
        }, "List cannot be empty!"));
        this.listView.setErrorDecorationEnabled(true);

        this.listView.validationSupport().revalidate();
    }
}
