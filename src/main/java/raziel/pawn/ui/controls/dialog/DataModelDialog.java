package raziel.pawn.ui.controls.dialog;



import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;
import raziel.pawn.core.domain.modeling.DataModel;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class DataModelDialog
        extends Dialog<DataModel> {

    private DataModel dataModel;

    private TextField dataModelNameTF;
    private GridPane  content;



    /**
     * Creates a new instance of the {@link DataModelDialog} class.
     * 
     * @param owner
     *            the owner of the dialog.
     * @param title
     *            the title.
     * @param dataModel
     *            the {@link DataModel} to edit or {@code null} to create a new
     *            {@link DataModel}.
     */
    public DataModelDialog(Window owner, String title, DataModel dataModel) {

        super();

        this.initOwner(owner);
        this.setTitle(title);

        this.setResult(dataModel);

        final boolean dataModelProvided = dataModel != null;

        this.content = new GridPane();
        this.content.setHgap(10);
        this.content.setVgap(10);
        this.getDialogPane().setContent(content);

        this.dataModelNameTF = new TextField();
        this.dataModelNameTF
                .setPromptText("Enter DataModel Name (cannot be empty)");
        if (dataModelProvided) {

            this.dataModelNameTF.setText(dataModel.getName());
        }

        this.dataModelNameTF.setMaxHeight(Double.MAX_VALUE);
        this.dataModelNameTF.setMaxWidth(Double.MAX_VALUE);

        this.content.add(new Label("DataModel Name"), 0, 0);
        this.content.add(this.dataModelNameTF, 1, 0);
        GridPane.setHgrow(this.dataModelNameTF, Priority.ALWAYS);

        this.setResultConverter((param) -> {

            if (param == ButtonType.OK) {

                if (dataModelProvided) {

                    this.dataModel.setName(this.dataModelNameTF.getText());

                }
                else {

                    this.dataModel = new DataModel(this.dataModelNameTF
                            .getText());
                }

                return this.dataModel;
            }

            return null;
        });

        this.getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);
        this.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
                .set(!dataModelProvided);

        this.setupEventHandlers();

        Platform.runLater(() -> {

            this.dataModelNameTF.requestFocus();
        });

    }

    private void setupEventHandlers() {

        this.dataModelNameTF.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });
    }

    private void validate() {

        this.getDialogPane()
                .lookupButton(ButtonType.OK)
                .disableProperty()
                .set(this.dataModelNameTF.getText() == null
                        || this.dataModelNameTF.getText().trim().isEmpty());

    }

    /**
     * Creates or edits a {@link DataModel}.
     * 
     * @param owner
     *            the owner .
     * @param dataset
     *            the {@link DataModel} to edit or {@code null} to create a new
     *            {@link DataModel}.
     * @return the updated or created {@link DataModel}.
     */
    public static Optional<DataModel> showDatasetDialog(Window owner,
            DataModel dataset) {

        DataModelDialog dialog;

        if (dataset == null) {

            dialog = new DataModelDialog(owner, "Create New DataModel", dataset);
        }
        else {
            dialog = new DataModelDialog(owner, "Edit DataModel", dataset);
        }

        return dialog.showAndWait();
    }
}
