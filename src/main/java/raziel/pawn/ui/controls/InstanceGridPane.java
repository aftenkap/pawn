package raziel.pawn.ui.controls;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionUtils;
import org.jutility.javafx.control.ListViewWithSearchPanel;
import org.jutility.javafx.control.labeled.LabeledTextField;

import raziel.pawn.core.domain.modeling.assertions.Constant;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @author Peter J. Radics
 * @version 1.0
 * @since 0.5
 * 
 */
public class InstanceGridPane
        extends GridPane {

    // FIXME: fix
    // private final ObservableList<Fluent> fluents;
    // private final ObservableList<Constant> constants;
    // FIXME: fix
    // private final ListViewWithSearchPanel<Fluent> subjectFluentList;
    // private final ListViewWithSearchPanel<Fluent> objectFluentList;

    private final ListViewWithSearchPanel<Constant> subjectConstantList;
    private final ListViewWithSearchPanel<Constant> objectConstantList;

    private final ToggleButton                      entities;
    private final ToggleButton                      activities;
    private final ToggleButton                      attributes;
    private final ToggleButton                      values;
    private final ToggleButton                      associations;
    private final ToggleGroup                       toggleGroup;

    private final LabeledTextField                  inputField;

    // private org.controlsfx.control.action.Action editFluent;
    // private org.controlsfx.control.action.Action removeFluent;


    private org.controlsfx.control.action.Action    addInstance;
    private org.controlsfx.control.action.Action    clearInputField;


    private ButtonBar                               entityButtonBar;


    // FIXME: fix
    // /**
    // * Provides access to the fluent list of this grid pane.
    // *
    // * @return the fluent list property.
    // */
    // public ObservableList<Fluent> fluents() {
    //
    // return this.fluents;
    // }
    //
    // /**
    // * Provides access to the constant list of this grid pane.
    // *
    // * @return the constant list property.
    // */
    // public ObservableList<Constant> constants() {
    //
    // return this.constants;
    // }

    /**
     * Creates a new instance of the {@link InstanceGridPane} class.
     * 
     */
    public InstanceGridPane() {

        this.setPadding(new Insets(10, 10, 10, 10));
        this.setHgap(20);
        this.setVgap(30);

        // FIXME: fix
        // this.fluents = FXCollections.observableList(new
        // LinkedList<Fluent>());
        // this.constants = FXCollections
        // .observableList(new LinkedList<Constant>());
        //
        // this.subjectFluentList = new ListViewWithSearchPanel<>(
        // "Subjects (Fluent)");
        // this.objectFluentList = new ListViewWithSearchPanel<>(
        // "Objects (Fluent)");

        this.subjectConstantList = new ListViewWithSearchPanel<>(
                "Subjects (Constant)");
        this.objectConstantList = new ListViewWithSearchPanel<>(
                "Objects (Constant)");

        // FIXME: fix
        // this.subjectFluentList.stringConverter().set(
        // new DomainObjectStringConverter<Fluent>());
        // this.objectFluentList.stringConverter().set(
        // new DomainObjectStringConverter<Fluent>());
        // this.objectFluentList.setSelectionMode(SelectionMode.MULTIPLE);


        this.subjectConstantList
                .setConverter(new DomainObjectStringConverter<Constant>());
        this.objectConstantList
                .setConverter(new DomainObjectStringConverter<Constant>());
        this.objectConstantList.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE);



        HBox toggleBox = new HBox(0);
        VBox editorBox = new VBox(10);
        // FIXME: fix
        this.add(toggleBox, 0, 0, 3, 1);
        // this.add(this.subjectFluentList, 0, 1);
        this.add(editorBox, 1, 1);
        // this.add(this.objectFluentList, 2, 1);


        this.toggleGroup = new ToggleGroup();


        this.entities = new ToggleButton("Entities");
        this.activities = new ToggleButton("Activities");
        this.attributes = new ToggleButton("Attributes");
        this.values = new ToggleButton("Constant Values");
        this.associations = new ToggleButton("Associations");


        this.entities
                .setStyle("-fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
        this.activities
                .setStyle("-fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
        this.attributes
                .setStyle("-fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
        this.values
                .setStyle("-fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");
        this.associations
                .setStyle("-fx-border-radius: 0 0 0 0; -fx-background-radius: 0 0 0 0;");


        this.entities.setToggleGroup(this.toggleGroup);
        this.activities.setToggleGroup(this.toggleGroup);
        this.attributes.setToggleGroup(this.toggleGroup);
        this.values.setToggleGroup(this.toggleGroup);
        this.associations.setToggleGroup(this.toggleGroup);

        this.toggleGroup.selectToggle(this.entities);

        this.entities.setMaxWidth(Double.MAX_VALUE);
        this.activities.setMaxWidth(Double.MAX_VALUE);
        this.attributes.setMaxWidth(Double.MAX_VALUE);
        this.values.setMaxWidth(Double.MAX_VALUE);
        this.associations.setMaxWidth(Double.MAX_VALUE);


        this.inputField = new LabeledTextField("Name:", Pos.TOP_CENTER);

        inputField.setPadding(new Insets(10, 0, 10, 0));
        inputField.setHgap(20);
        inputField.setVgap(10);
        inputField.setMaxWidth(Double.MAX_VALUE);


        this.entityButtonBar = new ButtonBar();
        this.entityButtonBar.setMaxWidth(Double.MAX_VALUE);


        toggleBox.getChildren().addAll(this.entities, this.activities,
                this.attributes, this.values, this.associations);


        editorBox.getChildren().addAll(this.inputField, this.entityButtonBar);


        // FIXME: fix
        // GridPane.setVgrow(this.subjectFluentList, Priority.ALWAYS);
        // GridPane.setHgrow(this.subjectFluentList, Priority.ALWAYS);
        //
        // GridPane.setVgrow(this.objectFluentList, Priority.ALWAYS);
        // GridPane.setHgrow(this.objectFluentList, Priority.ALWAYS);



        GridPane.setVgrow(this.subjectConstantList, Priority.ALWAYS);
        GridPane.setHgrow(this.subjectConstantList, Priority.ALWAYS);

        GridPane.setVgrow(this.objectConstantList, Priority.ALWAYS);
        GridPane.setHgrow(this.objectConstantList, Priority.ALWAYS);

        GridPane.setHgrow(toggleBox, Priority.ALWAYS);
        GridPane.setVgrow(editorBox, Priority.ALWAYS);
        GridPane.setHgrow(editorBox, Priority.ALWAYS);

        HBox.setHgrow(entities, Priority.ALWAYS);
        HBox.setHgrow(activities, Priority.ALWAYS);
        HBox.setHgrow(attributes, Priority.ALWAYS);
        HBox.setHgrow(values, Priority.ALWAYS);
        HBox.setHgrow(associations, Priority.ALWAYS);


        GridPane.setHgrow(this.entityButtonBar, Priority.ALWAYS);

        this.setupEventHandlers();
        this.setupContextMenus();


    }

    private void setupEventHandlers() {


        // FIXME: fix
        // Bindings.bindContentBidirectional(this.subjectFluentList.items(),
        // this.fluents);
        // Bindings.bindContentBidirectional(this.objectFluentList.items(),
        // this.fluents);
        //
        // Bindings.bindContentBidirectional(this.subjectConstantList.items(),
        // this.constants);
        // Bindings.bindContentBidirectional(this.objectConstantList.items(),
        // this.constants);

        // FIXME: fix
        // this.toggleGroup.selectedToggleProperty().addListener(
        // new ChangeListener<Toggle>() {
        //
        // @Override
        // public void changed(
        // ObservableValue<? extends Toggle> observable,
        // Toggle oldValue, Toggle newValue) {
        //
        // if (newValue == null) {
        //
        // InstanceGridPane.this.toggleGroup
        // .selectToggle(oldValue);
        // }
        // else {
        //
        // if (newValue == InstanceGridPane.this.entities) {
        //
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.subjectFluentList);
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.objectFluentList);
        // InstanceGridPane.this.subjectFluentList
        // .getSelectionModel().setSelectionMode(
        // SelectionMode.SINGLE);
        // InstanceGridPane.this.objectFluentList
        // .setDisable(true);
        // }
        // else if (newValue == InstanceGridPane.this.activities) {
        //
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.subjectFluentList);
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.objectFluentList);
        // InstanceGridPane.this.subjectFluentList
        // .getSelectionModel().setSelectionMode(
        // SelectionMode.MULTIPLE);
        // InstanceGridPane.this.objectFluentList
        // .setDisable(false);
        // }
        // else if (newValue == InstanceGridPane.this.attributes) {
        //
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.subjectFluentList);
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.objectConstantList);
        // InstanceGridPane.this.subjectFluentList
        // .getSelectionModel().setSelectionMode(
        // SelectionMode.MULTIPLE);
        // InstanceGridPane.this.objectConstantList
        // .setDisable(false);
        // }
        // else if (newValue == InstanceGridPane.this.values) {
        //
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.subjectConstantList);
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.objectConstantList);
        // InstanceGridPane.this.subjectConstantList
        // .getSelectionModel().setSelectionMode(
        // SelectionMode.SINGLE);
        // InstanceGridPane.this.objectConstantList
        // .setDisable(true);
        // }
        // else if (newValue == InstanceGridPane.this.associations) {
        //
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.subjectConstantList);
        // InstanceGridPane.this
        // .switchToList(InstanceGridPane.this.objectConstantList);
        // InstanceGridPane.this.subjectConstantList
        // .getSelectionModel().setSelectionMode(
        // SelectionMode.MULTIPLE);
        // InstanceGridPane.this.objectConstantList
        // .setDisable(false);
        // }
        // }
        // }
        // });
        //
        //
        // // Setting values of the action and attributes boxes based on the
        // newly
        // // selected fluent.
        // this.subjectFluentList.getSelectionModel().selectedItemProperty()
        // .addListener(new ChangeListener<Fluent>() {
        //
        // @Override
        // public void changed(
        // ObservableValue<? extends Fluent> observable,
        // Fluent oldValue, Fluent newValue) {
        //
        // InstanceGridPane.this.validate();
        // }
        // });
        // // Setting values of the action and attributes boxes based on the
        // newly
        // // selected fluent.
        // this.objectFluentList.getSelectionModel().selectedItemProperty()
        // .addListener(new ChangeListener<Fluent>() {
        //
        // @Override
        // public void changed(
        // ObservableValue<? extends Fluent> observable,
        // Fluent oldValue, Fluent newValue) {
        //
        // InstanceGridPane.this.validate();
        // }
        // });


        this.subjectConstantList.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {

                    this.validate();
                });

        this.objectConstantList.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {

                    this.validate();
                });


        this.inputField.textProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.validate();
                });

        this.inputField.setOnAction((actionEvent) -> {


            if (!this.addInstance.disabledProperty().get()) {

                this.addInstance.handle(actionEvent);
            }
        });


        this.addInstance = new Action("Add", actionEvent -> {

            // FIXME: fix
            // InstanceGridPane.this.createInstance();
            });

        this.clearInputField = new Action("Clear", actionEvent -> {

            this.inputField.clear();
        });

        Button createButton = ActionUtils.createButton(this.addInstance);
        ButtonBar.setButtonData(createButton, ButtonData.OK_DONE);

        Button clearButton = ActionUtils.createButton(this.clearInputField);
        ButtonBar.setButtonData(clearButton, ButtonData.CANCEL_CLOSE);



        this.entityButtonBar.getButtons().addAll(createButton, clearButton);

    }

    // FIXME: fix
    private void setupContextMenus() {

        //
        //
        // this.editFluent = new AbstractAction("Edit Fluent") {
        //
        // @Override
        // public void handle(ActionEvent actionEvent) {
        //
        // Fluent editedFluent = FluentDialog.showFluentDialog(
        // InstanceGridPane.this.getScene().getWindow(),
        // InstanceGridPane.this.subjectFluentList
        // .getSelectedItem());
        //
        //
        // if (editedFluent != null) {
        //
        // InstanceGridPane.this.subjectFluentList.requestLayout();
        // InstanceGridPane.this.objectFluentList.requestLayout();
        // }
        // }
        // };
        //
        //
        // this.removeFluent = new AbstractAction("Remove Fluent") {
        //
        // @Override
        // public void handle(ActionEvent actionEvent) {
        //
        // Fluent fluentToRemove = InstanceGridPane.this.subjectFluentList
        // .getSelectedItem();
        // org.controlsfx.control.action.Action result = Dialogs
        // .create()
        // .title("Confirm removal!")
        // .message(
        // "Are you sure you want to remove fluent "
        // + fluentToRemove.toString() + "?")
        // .showConfirm();
        //
        // if (result == Dialog.Actions.YES) {
        //
        // InstanceGridPane.this.fluents.remove(fluentToRemove);
        // }
        // }
        // };
        //
        //
        // this.subjectFluentList.contextMenuActions().addAll(this.editFluent,
        // this.removeFluent);
        //
        //
        //
        // }
        //
        // private void createInstance() {
        //
        // Toggle selectedToggle = this.toggleGroup.getSelectedToggle();
        //
        // String name = this.inputField.getText();
        //
        // if (selectedToggle == this.entities) {
        //
        // Entity entity = new Entity(name);
        // this.fluents.add(entity);
        // }
        // else if (selectedToggle == this.activities) {
        //
        // Action action = new Action(name);
        //
        // String activityDescription = this.createName(action,
        // this.subjectFluentList.getSelectedItems(),
        // this.objectFluentList.getSelectedItems());
        //
        //
        // Activity activity = new Activity(activityDescription, action);
        // this.fluents.add(activity);
        //
        // }
        // else if (selectedToggle == this.attributes) {
        //
        // Attribution attribution = new Attribution(name);
        //
        // String description = this.createName(attribution,
        // this.subjectFluentList.getSelectedItems(),
        // this.objectConstantList.getSelectedItems());
        //
        // Attribute attribute = new Attribute(description, attribution);
        // this.fluents.add(attribute);
        //
        // }
        // else if (selectedToggle == this.values) {
        //
        // Value value = new Value(name);
        // this.constants.add(value);
        // }
        // else if (selectedToggle == this.associations) {
        //
        // Associant associant = new Associant(name);
        //
        //
        // String description = this.createName(associant,
        // this.subjectConstantList.getSelectedItems(),
        // this.objectConstantList.getSelectedItems());
        //
        //
        // Association association = new Association(description, associant);
        // this.constants.add(association);
        //
        // this.inputField.clear();
        // }
        //
        //
        // this.inputField.clear();
    }

    //
    //
    // private <T extends Instance, S extends Instance> String createName(
    // Relationship<T, S> relationship, List<T> subjects, List<S> objects) {
    //
    // String description = "";
    // int i = 0;
    // int andIndex = subjects.size() - 1;
    //
    // for (T subject : subjects) {
    //
    // if (i > 0) {
    //
    // if (i != andIndex) {
    //
    // description += ", ";
    // }
    // else {
    // if (i > 2) {
    //
    // description += ",";
    // }
    // description += " and ";
    // }
    //
    // }
    //
    // description += subject.getName();
    // relationship.addDomainInstance(subject);
    // i++;
    // }
    //
    // description += " " + relationship.getName() + " ";
    //
    // i = 0;
    // andIndex = objects.size() - 1;
    // for (S object : objects) {
    //
    // if (i > 0) {
    //
    // if (i != andIndex) {
    //
    // description += ", ";
    // }
    // else {
    // if (i > 2) {
    //
    // description += ",";
    // }
    // description += " and ";
    // }
    //
    // }
    //
    // description += object.getName();
    // relationship.addRangeInstance(object);
    // i++;
    // }
    //
    // return description;
    // }



    private void validate() {

        boolean isInvalid = true;

        boolean inputFieldEmpty = this.inputField.getText() == null
                || this.inputField.getText().trim().isEmpty();

        Toggle selectedToggle = this.toggleGroup.getSelectedToggle();

        if (selectedToggle == entities) {

            isInvalid = inputFieldEmpty;
        }
        // FIXME: fix
        //
        // else if (selectedToggle == activities) {
        //
        // int participants = this.subjectFluentList.getSelectedItems().size();
        //
        // participants += this.objectFluentList.getSelectedItems().size();
        //
        // isInvalid = inputFieldEmpty || participants < 2;
        // }
        // else if (selectedToggle == attributes) {
        //
        // int participants = this.subjectFluentList.getSelectedItems().size();
        //
        // participants += this.objectConstantList.getSelectedItems().size();
        //
        // isInvalid = inputFieldEmpty || participants < 2;
        // }
        else if (selectedToggle == values) {

            isInvalid = inputFieldEmpty;
        }
        else if (selectedToggle == associations) {

            int participants = this.subjectConstantList.getSelectedItems()
                    .size();

            participants += this.objectConstantList.getSelectedItems().size();

            isInvalid = inputFieldEmpty || participants < 2;
        }

        this.addInstance.disabledProperty().set(isInvalid);
    }

    // private void switchToList(ListViewWithSearchPanel<?> listview) {
    //
    // FIXME: fix
    // if (!this.getChildren().contains(listview)) {
    //
    // if (listview == this.subjectFluentList) {
    //
    // this.getChildren().remove(this.subjectConstantList);
    // this.add(this.subjectFluentList, 0, 1);
    //
    // }
    // else if (listview == this.subjectConstantList) {
    //
    // this.getChildren().remove(this.subjectFluentList);
    // this.add(this.subjectConstantList, 0, 1);
    // }
    // else if (listview == this.objectFluentList) {
    //
    // this.getChildren().remove(this.objectConstantList);
    // this.add(this.objectFluentList, 2, 1);
    // }
    // else if (listview == this.objectConstantList) {
    //
    // this.getChildren().remove(this.objectFluentList);
    // this.add(this.objectConstantList, 2, 1);
    // }
    // }
    //
    // }
}
