package raziel.pawn.ui.controls;



import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.HPos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import org.controlsfx.control.action.Action;
import org.jutility.javafx.control.labeled.LabeledTextField;
import org.jutility.javafx.control.validation.ValidationGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.core.domain.metadata.IMetadata;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.ui.controls.dialog.DomainObjectSelectionDialog;


/**
 * The abstract {@code MetadataDetailsGridPane} class provides common
 * functionality for the visual representation of {@link IMetadata Metadata}.
 * 
 * @param <T>
 *            the concrete type of the {@link IMetadata Metadata}.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public abstract class MetadataDetailsGridPane<T extends IMetadata>
        extends GridPane {


    private static Logger                LOG = LoggerFactory
                                                     .getLogger(MetadataDetailsGridPane.class);

    private final ValidationGroup        validationGroup;

    private final ObjectProperty<Survey> surveyProperty;

    private final ObjectProperty<T>      metadata;
    private final BooleanProperty        updateDeferredProperty;

    private final LabeledTextField       dataPointTextField;

    private Action                       setDataPoint;
    private Action                       clearDataPoint;



    /**
     * Returns the {@link ValidationGroup}.
     * 
     * @return the {@link ValidationGroup}.
     */
    public ValidationGroup validationGroup() {

        return this.validationGroup;
    }

    /**
     * Returns the {@link Survey} property.
     * 
     * @return the {@link Survey} property.
     */
    public ObjectProperty<Survey> surveyProperty() {

        return this.surveyProperty;
    }

    /**
     * Returns the value of the {@link Survey} property.
     * 
     * @return the value of the {@link Survey} property.
     */
    public Survey getSurvey() {

        return this.surveyProperty.get();
    }

    /**
     * Sets the value of the {@link Survey} property.
     * 
     * @param survey
     *            the value of the {@link Survey} property.
     */
    public void setSurvey(final Survey survey) {

        this.surveyProperty.set(survey);
    }

    /**
     * Returns the metadata property.
     * 
     * @return the metadata property.
     */
    public ObjectProperty<T> metadata() {

        return this.metadata;
    }

    /**
     * Returns the {@link IMetadata Metadata}.
     * 
     * @return the {@link IMetadata Metadata}.
     */
    public T getMetadata() {

        return this.metadata.get();
    }

    /**
     * Sets the {@link IMetadata Metadata}.
     * 
     * @param metadata
     *            the {@link IMetadata Metadata}.
     */
    public void setMetadata(T metadata) {

        this.metadata.set(metadata);
    }

    /**
     * Returns the update deferred property.<br/>
     * This property determines whether changes to the contained
     * {@link IMetadata Metadata} are immediately persisted to the database or
     * deferred to a later point in time. By default, updates are immediately
     * persisted.
     * 
     * @return the update deferred property.
     */
    public BooleanProperty updateDeferredProperty() {

        return this.updateDeferredProperty;
    }

    /**
     * Returns the value of the update deferred property.
     * 
     * @return whether or not updates are deferred.
     */
    public boolean isUpdateDeferred() {

        return this.updateDeferredProperty.get();
    }

    /**
     * Sets the value of the update deferred property.
     * 
     * @param value
     *            whether or not updates should be deferred.
     */
    public void setUpdateDeferred(final boolean value) {

        this.updateDeferredProperty.set(value);
    }

    /**
     * Creates a new instance of the {@link MetadataDetailsGridPane} class.
     */
    public MetadataDetailsGridPane() {

        this(null, null);
    }

    /**
     * Creates a new instance of the {@link MetadataDetailsGridPane} class with
     * the provided {@link IMetadata Metadata}, disabling direct editing by
     * default.
     * 
     * @param metadata
     *            the {@link IMetadata Metadata}.
     * @param survey
     *            the {@link Survey} containing the {@link IMetadata Metadata}.
     */
    public MetadataDetailsGridPane(final T metadata, final Survey survey) {

        this(metadata, survey, false);
    }

    /**
     * Creates a new instance of the {@link MetadataDetailsGridPane} class with
     * the provided {@link IMetadata Metadata} and {@link Survey}. Also
     * specifies whether the update of the {@link IMetadata Metadata} in the
     * database should be deferred.
     * 
     * @param metadata
     *            the {@link IMetadata Metadata}.
     * @param survey
     *            the {@link Survey} containing the {@link IMetadata Metadata}.
     * @param deferUpdate
     *            whether or not the update of the {@link IMetadata Metadata} in
     *            the database is deferred.
     */
    public MetadataDetailsGridPane(final T metadata, final Survey survey,
            final boolean deferUpdate) {

        super();

        this.setHgap(20);
        this.setVgap(10);

        this.validationGroup = new ValidationGroup();

        this.surveyProperty = new SimpleObjectProperty<>(survey);

        this.metadata = new SimpleObjectProperty<>(metadata);
        this.updateDeferredProperty = new SimpleBooleanProperty(deferUpdate);


        this.dataPointTextField = new LabeledTextField("Data Point");
        this.dataPointTextField.setHgap(20);
        GridPane.setVgrow(this.dataPointTextField, Priority.NEVER);
        GridPane.setHalignment(this.dataPointTextField, HPos.LEFT);

        this.add(this.dataPointTextField, 0, 0);

        this.dataPointTextField.setEditable(false);

        this.setupEventHandlers();
        this.setupContextMenus();
    }

    private void setupEventHandlers() {

        this.surveyProperty().addListener(observable -> {

            this.refreshView();
        });

        this.metadata.addListener(observable -> {

            this.refreshView();
        });
    }

    private void setupContextMenus() {

        this.setDataPoint = new Action("Set Data Point", actionEvent -> {

            if (this.getSurvey() != null
                    && this.getSurvey().getDataSource() != null) {

                new DomainObjectSelectionDialog<>(this, "Select Data Point",
                        this.getSurvey().getDataSource().getDataPoints())
                        .showAndWait().ifPresent(
                                dataPoint -> {

                                    LOG.info("Setting data point of "
                                            + this.getMetadata() + " to "
                                            + dataPoint + ".");

                                    this.getMetadata().setDataPoint(dataPoint);
                                    this.update();
                                });
            }
        });

        this.clearDataPoint = new Action("Clear Data Point", actionEvent -> {

            Alert alert = new Alert(AlertType.CONFIRMATION,
                    "Are you sure you want to clear the data point?");

            alert.showAndWait()
                    .filter(result -> result == ButtonType.OK)
                    .ifPresent(
                            result -> {

                                LOG.info("Removing data point "
                                        + this.getMetadata().getDataPoint()
                                        + " from " + this.getMetadata() + ".");

                                this.getMetadata().setDataPoint(null);
                                this.update();
                            });

        });

        this.dataPointTextField.contextMenuActions().addAll(this.setDataPoint,
                this.clearDataPoint);
    }

    /**
     * Updates the {@link IMetadata Metadata}.
     */
    protected abstract void update();

    /**
     * Refreshes the view.
     */
    protected void refreshView() {

        LOG.debug("Refreshing view.");

        MetadataDetailsGridPane.this.dataPointTextField.clear();

        boolean isDataPointPresent = this.getMetadata() != null
                && this.getMetadata().getDataPoint() != null;
        boolean isDataSourcePresent = this.getSurvey() != null
                && this.getSurvey().getDataSource() != null;

        if (isDataPointPresent) {

            MetadataDetailsGridPane.this.dataPointTextField.setText(this
                    .getMetadata().getDataPoint().toString());
        }

        this.setDataPoint.setDisabled(!isDataSourcePresent);
        this.clearDataPoint.setDisabled(!isDataPointPresent);


    }
}
