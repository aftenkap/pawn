package raziel.pawn.ui.controls;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionGroup;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.control.spreadsheet.SpreadsheetColumn;
import org.jutility.common.datatype.table.CellLocation;
import org.jutility.common.datatype.table.ICell;
import org.jutility.common.datatype.util.StringWithNumberComparator;
import org.jutility.common.reflection.ReflectionUtils;
import org.jutility.javafx.control.ListViewWithSearchPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.data.SpreadsheetCell;
import raziel.pawn.core.domain.metadata.Answer;
import raziel.pawn.core.domain.metadata.ChoiceQuestion;
import raziel.pawn.core.domain.metadata.IAnswer;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.core.domain.metadata.Participant;
import raziel.pawn.core.domain.metadata.RankingQuestion;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.core.domain.metadata.TextQuestion;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.1
 */
public class DataLinkGridPane
        extends SurveyContentGridPane {

    private static Logger                               LOG = LoggerFactory
                                                                    .getLogger(DataLinkGridPane.class);


    private final TabPane                               tabPane;
    private final Tab                                   questionTab;
    private final Tab                                   participantTab;
    private final Tab                                   answerTab;

    private final ListViewWithSearchPanel<IQuestion>    questionListView;
    private final ListViewWithSearchPanel<IParticipant> participantListView;
    private final ListViewWithSearchPanel<IAnswer>      answerListView;

    private final SpreadsheetView                       spreadsheetView;

    private final Map<IQuestion, SpreadsheetCell>       linkedQuestions;
    private final Map<IParticipant, SpreadsheetCell>    linkedParticipants;
    private final Map<IAnswer, SpreadsheetCell>         linkedAnswers;

    private ContextMenu                                 questionMenu;
    private ActionGroup                                 createQuestion;
    private Action                                      createTextQuestion;
    private Action                                      createChoiceQuestion;
    private Action                                      createRankingQuestion;
    private Action                                      linkQuestion;
    private Action                                      autodetectQuestions;

    private ContextMenu                                 participantMenu;
    private Action                                      createParticipant;
    private Action                                      linkParticipant;
    private Action                                      autodetectParticipants;

    private ContextMenu                                 answerMenu;
    private Action                                      createAnswer;
    private Action                                      linkAnswer;
    private Action                                      autodetectAnswers;



    /**
     * Creates a new instance of the {@link DataLinkGridPane} class.
     * 
     * @param survey
     *            the survey that contains the metadata to be linked to the
     *            table.
     */
    public DataLinkGridPane(final Survey survey) {

        super(survey);

        LOG.debug("Creating DataLinkGridPane.");

        ColumnConstraints tabPaneColumnConstraint = new ColumnConstraints();
        tabPaneColumnConstraint.setPercentWidth(25);
        ColumnConstraints spreadsheetViewColumnConstraint = new ColumnConstraints();
        spreadsheetViewColumnConstraint.setPercentWidth(75);

        this.getColumnConstraints().addAll(tabPaneColumnConstraint,
                spreadsheetViewColumnConstraint);

        this.spreadsheetView = new SpreadsheetView(null);
        this.spreadsheetView.setEditable(false);
        this.spreadsheetView.getSpreadsheetViewContextMenu().getItems().clear();
        this.spreadsheetView.getSelectionModel().cellSelectionEnabledProperty()
                .set(true);

        this.linkedQuestions = new LinkedHashMap<>();
        this.linkedParticipants = new LinkedHashMap<>();
        this.linkedAnswers = new LinkedHashMap<>();


        this.questionListView = new ListViewWithSearchPanel<>(null,
                new DomainObjectStringConverter<>());
        this.questionTab = new Tab("Questions");
        this.questionTab.setContent(this.questionListView);
        this.questionTab.setClosable(false);

        this.participantListView = new ListViewWithSearchPanel<>(null,
                new DomainObjectStringConverter<>());
        this.participantTab = new Tab("Participants");
        this.participantTab.setContent(this.participantListView);
        this.participantTab.setClosable(false);

        this.answerListView = new ListViewWithSearchPanel<>(null,
                new DomainObjectStringConverter<>());
        this.answerTab = new Tab("Answers");
        this.answerTab.setContent(this.answerListView);
        this.answerTab.setClosable(false);

        this.tabPane = new TabPane();
        this.tabPane.getTabs().addAll(this.questionTab, this.participantTab,
                this.answerTab);

        GridPane.setVgrow(this.tabPane, Priority.ALWAYS);
        GridPane.setVgrow(this.spreadsheetView, Priority.ALWAYS);

        this.add(this.tabPane, 0, 0);
        this.add(this.spreadsheetView, 1, 0);

        this.setupContextMenus();
        this.setupEventHandlers();

        this.refreshView();
    }

    private void setupContextMenus() {

        this.createTextQuestion = new Action("Text Question",
                (actionEvent) -> {

                    this.createTextQuestion();
                });

        this.createChoiceQuestion = new Action("Choice Question",
                (actionEvent) -> {

                    this.createChoiceQuestion();
                });

        this.createRankingQuestion = new Action("Ranking Question", (
                actionEvent) -> {

            this.createRankingQuestion();
        });

        this.createQuestion = new ActionGroup("Create Question",
                createTextQuestion, createChoiceQuestion, createRankingQuestion);



        this.linkQuestion = new Action("Link Question", (actionEvent) -> {

            this.linkQuestion();
        });
        this.linkQuestion.setDisabled(true);

        this.autodetectQuestions = new Action("Automatically Link Questions",
                actionEvent -> {

                    // TODO: implement
            });
        this.autodetectQuestions.setDisabled(true);

        this.questionMenu = ActionUtils.createContextMenu(Arrays.asList(
                this.createQuestion, this.linkQuestion,
                this.autodetectQuestions));



        this.createParticipant = new Action("Create Participant",
                actionEvent -> {

                    this.createParticipant();
                });

        this.linkParticipant = new Action("Link Participant",
                (actionEvent) -> {

                    this.linkParticipant();
                });
        this.linkParticipant.setDisabled(true);

        this.autodetectParticipants = new Action(
                "Automatically Link Participants", actionEvent -> {

                    // TODO: implement
            });
        this.autodetectParticipants.setDisabled(true);

        this.participantMenu = ActionUtils.createContextMenu(Arrays.asList(
                this.createParticipant, this.linkParticipant,
                this.autodetectParticipants));



        this.createAnswer = new Action("Create Answer", actionEvent -> {

            this.createAnswer();

        });

        this.linkAnswer = new Action("Link Answer", (actionEvent) -> {

            this.linkAnswer();
        });
        this.linkAnswer.setDisabled(true);

        this.autodetectAnswers = new Action("Automatically Create Answers",
                actionEvent -> {

                    this.autodetectAnswers();
                });
        this.autodetectAnswers.setDisabled(true);

        this.answerMenu = ActionUtils.createContextMenu(Arrays.asList(
                this.createAnswer, this.linkAnswer, this.autodetectAnswers));

        this.spreadsheetView.setContextMenu(this.questionMenu);
    }

    private void setupEventHandlers() {

        this.tabPane
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {

                            LOG.debug("Selected tab changed to " + newValue);
                            this.spreadsheetView.setContextMenu(null);

                            if (newValue == this.questionTab) {

                                LOG.debug("QuestionTab");
                                this.spreadsheetView
                                        .setContextMenu(this.questionMenu);
                            }
                            else if (newValue == this.participantTab) {

                                LOG.debug("ParticipantTab");
                                this.spreadsheetView
                                        .setContextMenu(this.participantMenu);
                            }
                            else if (newValue == this.answerTab) {

                                LOG.debug("AnswerTab");
                                this.spreadsheetView
                                        .setContextMenu(this.answerMenu);
                            }
                        });


        this.spreadsheetView
                .getSelectionModel()
                .getSelectedCells()
                .addListener(
                        (Observable observable) -> {

                            LOG.debug("Selected cell of SpreadsheetView changed!");
                            int size = this.spreadsheetView.getSelectedCells()
                                    .size();
                            LOG.debug("Selection size: " + size);
                            LOG.debug("Selection: "
                                    + this.spreadsheetView.getSelectedCells());

                            this.createTextQuestion.setDisabled(size != 1);
                            this.createChoiceQuestion.setDisabled(size <= 0
                                    || size >= 3);
                            this.createRankingQuestion.setDisabled(size <= 2);
                        });



        this.questionListView
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {

                            boolean isEmpty = newValue == null;

                            this.linkQuestion.setDisabled(isEmpty);

                            if (isEmpty) {

                                LOG.debug("Selected question changed to null.");
                            }
                            else {

                                LOG.debug("Selected question changed to "
                                        + newValue.getName() + ".");

                                if (this.linkedQuestions.containsKey(newValue)
                                        || newValue instanceof RankingQuestion) {

                                    List<SpreadsheetCell> cells = new ArrayList<>();

                                    SpreadsheetCell cell = this.linkedQuestions
                                            .get(newValue);
                                    LOG.debug("Selected question is linked to "
                                            + cell + "!");
                                    if (cell != null) {

                                        cells.add(cell);
                                    }

                                    if (newValue instanceof ChoiceQuestion) {

                                        ChoiceQuestion choiceQuestion = (ChoiceQuestion) newValue;

                                        if (choiceQuestion
                                                .getAlternativeChoice() != null
                                                && choiceQuestion
                                                        .getAlternativeChoice()
                                                        .getDataPoint() != null
                                                && choiceQuestion
                                                        .getAlternativeChoice()
                                                        .getDataPoint() instanceof SpreadsheetCell) {

                                            cells.add((SpreadsheetCell) choiceQuestion
                                                    .getAlternativeChoice()
                                                    .getDataPoint());
                                        }

                                    }
                                    else if (newValue instanceof RankingQuestion) {

                                        RankingQuestion rankingQuestion = (RankingQuestion) newValue;

                                        for (ChoiceQuestion choiceQuestion : rankingQuestion
                                                .getRanks()) {

                                            if (choiceQuestion.getDataPoint() != null
                                                    && choiceQuestion
                                                            .getDataPoint() instanceof SpreadsheetCell) {

                                                cells.add((SpreadsheetCell) choiceQuestion
                                                        .getDataPoint());
                                            }

                                        }
                                    }

                                    this.selectTableCells(cells);
                                }
                            }
                        });


        this.participantListView.selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    boolean isEmpty = newValue == null;

                    this.linkParticipant.setDisabled(isEmpty);

                    if (isEmpty) {

                        LOG.debug("Selected participant changed to null.");
                    }
                    else {

                        LOG.debug("Selected participant changed to "
                                + newValue.getName() + ".");

                        if (this.linkedParticipants.containsKey(newValue)) {

                            List<SpreadsheetCell> cells = new ArrayList<>();

                            SpreadsheetCell cell = this.linkedParticipants
                                    .get(newValue);
                            LOG.debug("Selected participant is linked to "
                                    + cell + "!");
                            cells.add(cell);

                            this.selectTableCells(cells);
                        }
                    }
                });


        this.answerListView.selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    boolean isEmpty = newValue == null;

                    this.linkAnswer.setDisabled(isEmpty);

                    if (isEmpty) {

                        LOG.debug("Selected answer changed to null.");
                    }
                    else {

                        LOG.debug("Selected answer changed to "
                                + newValue.getName() + ".");

                        if (this.linkedAnswers.containsKey(newValue)) {

                            List<SpreadsheetCell> cells = new ArrayList<>();

                            SpreadsheetCell cell = this.linkedAnswers
                                    .get(newValue);
                            LOG.debug("Selected answer is linked to " + cell
                                    + "!");
                            cells.add(cell);

                            this.selectTableCells(cells);
                        }
                    }
                });

        this.questionListView
                .getItems()
                .addListener(
                        (Observable observable) -> {

                            this.autodetectQuestions.setDisabled(this
                                    .getSurvey() == null
                                    || this.getSurvey().getQuestions()
                                            .isEmpty());

                            this.autodetectAnswers.setDisabled(this.getSurvey() == null
                                    || this.getSurvey().getQuestions()
                                            .isEmpty()
                                    || this.getSurvey().getParticipants()
                                            .isEmpty());
                        });

        this.participantListView
                .getItems()
                .addListener(
                        (Observable observable) -> {

                            this.autodetectParticipants.setDisabled(this
                                    .getSurvey() == null
                                    || this.getSurvey().getParticipants()
                                            .isEmpty());

                            this.autodetectAnswers.setDisabled(this.getSurvey() == null
                                    || this.getSurvey().getQuestions()
                                            .isEmpty()
                                    || this.getSurvey().getParticipants()
                                            .isEmpty());
                        });
    }

    private void linkQuestion() {

        IQuestion question = this.questionListView.getSelectedItem();
        SpreadsheetCell cell = this.spreadsheetView.getSelectedCell();

        question.setDataPoint(cell);
        this.linkedQuestions.put(question, cell);
    }

    private void linkParticipant() {

        IParticipant participant = this.participantListView.getSelectedItem();
        SpreadsheetCell cell = this.spreadsheetView.getSelectedCell();

        participant.setDataPoint(cell);
        this.linkedParticipants.put(participant, cell);
    }

    private void linkAnswer() {

        IAnswer answer = this.answerListView.getSelectedItem();
        SpreadsheetCell cell = this.spreadsheetView.getSelectedCell();

        answer.setDataPoint(cell);
        this.linkedAnswers.put(answer, cell);
    }

    private void autodetectAnswers() {

        Spreadsheet table = this.spreadsheetView.getSpreadsheet();

        List<IAnswer> answers = new ArrayList<>();

        for (IParticipant participant : this.linkedParticipants.keySet()) {

            SpreadsheetCell participantDataCell = this.linkedParticipants
                    .get(participant);

            for (IQuestion question : this.linkedQuestions.keySet()) {

                SpreadsheetCell questionDataCell = this.linkedQuestions
                        .get(question);

                int maxRow = Math.max(participantDataCell.getRow(),
                        questionDataCell.getRow());
                int maxColumn = Math.max(participantDataCell.getColumn(),
                        questionDataCell.getColumn());

                CellLocation answerLocation = null;

                if (table.cellRange().contains(maxRow, maxColumn)) {

                    answerLocation = new CellLocation(maxRow, maxColumn);
                }
                else {

                    int minRow = Math.min(participantDataCell.getRow(),
                            questionDataCell.getRow());
                    int minColumn = Math.min(participantDataCell.getColumn(),
                            questionDataCell.getColumn());


                    if (table.cellRange().contains(minRow, minColumn)) {

                        answerLocation = new CellLocation(minRow, minColumn);
                    }
                }

                LOG.debug("AnswerLocation: " + answerLocation);

                if (answerLocation != null) {

                    SpreadsheetCell answerDataCell = table
                            .getCell(answerLocation);

                    if (answerDataCell != null
                            && answerDataCell.getValue() != null
                            && !answerDataCell.getValue().isEmpty()) {

                        LOG.debug("Creating Answer " + participant.getName()
                                + " - " + question.getName() + " for cell "
                                + answerDataCell);
                        IAnswer answer = new Answer(participant.getName()
                                + " - " + question.getName(), question,
                                participant);
                        answer.setDataPoint(answerDataCell);

                        answer.setValue(answerDataCell.getValue());

                        answers.add(answer);
                    }
                }

            }
        }

        this.createAnswers(answers);
    }


    private List<SpreadsheetCell> getSelectedCells() {

        LOG.debug("Unsorted Selected cells: "
                + this.spreadsheetView.getSelectedCells());
        List<SpreadsheetCell> selectedCells = new ArrayList<>(
                this.spreadsheetView.getSelectedCells());

        LOG.debug("Copied Unsorted Selected cells: " + selectedCells);
        selectedCells.sort((lhs, rhs) -> {

            return ICell.rowMajorOrder.compare(lhs, rhs);
        });

        LOG.debug("Sorted Selected cells: " + selectedCells);
        return selectedCells;
    }


    private void createTextQuestion() {

        SpreadsheetCell cell = this.spreadsheetView.getSelectedCell();
        this.createQuestion(new TextQuestion("New Text Question"), cell,
                cell.getValue().toString()).ifPresent(textQuestion -> {

            this.linkedQuestions.put(textQuestion, cell);
            this.addQuestionToSurvey(textQuestion);
        });
    }

    private void createChoiceQuestion() {

        List<SpreadsheetCell> cells = this.getSelectedCells();

        SpreadsheetCell cell = cells.get(0);

        LOG.debug("Selected Cells: " + this.spreadsheetView.getSelectedCells());

        this.createQuestion(new ChoiceQuestion("New Choice Question"), cell,
                cell.getValue().toString())
                .ifPresent(
                        choiceQuestion -> {

                            this.linkedQuestions.put(choiceQuestion, cell);
                            if (cells.size() == 2) {

                                SpreadsheetCell otherCell = cells.get(1);

                                this.createQuestion(
                                        new TextQuestion(choiceQuestion
                                                .getName() + " [Other]"),
                                        otherCell,
                                        otherCell.getValue().toString())
                                        .ifPresent(
                                                textQuestion -> {

                                                    this.linkedQuestions.put(
                                                            textQuestion,
                                                            otherCell);
                                                    choiceQuestion
                                                            .setAlternativeChoice(textQuestion);
                                                });
                            }

                            this.addQuestionToSurvey(choiceQuestion);
                        });
    }

    private void createRankingQuestion() {

        List<SpreadsheetCell> cells = this.getSelectedCells();
        SpreadsheetCell cell = cells.get(0);

        this.createQuestion(new RankingQuestion("New Ranking Question"), null,
                cell.getValue().toString()).ifPresent(
                rankingQuestion -> {

                    this.linkedQuestions.put(rankingQuestion, cell);
                    int index = 0;
                    if (cells.size() == rankingQuestion.getRanks().size()) {

                        for (ChoiceQuestion choiceQuestion : rankingQuestion
                                .getRanks()) {

                            choiceQuestion.setDataPoint(cells.get(index));
                            this.linkedQuestions.put(choiceQuestion,
                                    cells.get(index));

                            ApplicationContext.surveyController()
                                    .updateQuestion(choiceQuestion, this);
                            index++;
                        }
                    }

                    this.addQuestionToSurvey(rankingQuestion);
                });

    }

    private <T extends IQuestion> Optional<T> createQuestion(final T question,
            final SpreadsheetCell cell, final String questionText) {


        question.setQuestionText(questionText);
        question.setDataPoint(cell);

        return ApplicationContext.surveyController().createQuestion(question,
                this);
    }

    private void addQuestionToSurvey(final IQuestion newQuestion) {

        newQuestion.setOrder(this.getSurvey().getQuestions().size());

        ApplicationContext
                .surveyController()
                .updateQuestion(newQuestion, this)
                .ifPresent(
                        updatedQuestion -> {

                            this.getSurvey().addQuestion(updatedQuestion);

                            this.updateSurvey();

                            this.questionListView.getSelectionModel().select(
                                    updatedQuestion);
                        });
    }

    private void createParticipant() {


        List<SpreadsheetCell> cells = this.getSelectedCells();

        List<IParticipant> participants = new ArrayList<>();
        for (SpreadsheetCell cell : cells) {

            String participantId = "Participant " + cell.getValue().toString();

            Participant participant = new Participant(participantId);
            participant.setDataPoint(cell);

            participants.add(participant);
        }

        this.createParticipants(participants);
    }


    private void createParticipants(List<IParticipant> participants) {

        ApplicationContext
                .surveyController()
                .createParticipants(participants, this)
                .ifPresent(
                        newParticipants -> {

                            for (IParticipant newParticipant : newParticipants) {

                                this.getSurvey().addParticipant(newParticipant);

                                this.linkedParticipants.put(newParticipant,
                                        (SpreadsheetCell) newParticipant
                                                .getDataPoint());
                            }

                            if (!newParticipants.isEmpty()) {

                                this.participantListView.getSelectionModel()
                                        .select(newParticipants.get(0));
                                this.updateSurvey();
                            }
                        });
    }

    private void createAnswer() {

        List<SpreadsheetCell> cells = this.getSelectedCells();
        List<IAnswer> answers = new ArrayList<>();

        int i = 1;
        for (SpreadsheetCell cell : cells) {

            IAnswer answer = new Answer("New Answer " + i, null, null);
            answer.setDataPoint(cell);

            answers.add(answer);
        }

        this.createAnswers(answers);
    }


    private void createAnswers(List<IAnswer> answers) {

        LOG.debug("Creating " + answers.size() + " new answers.");

        ApplicationContext
                .surveyController()
                .createAnswers(answers, this.getSurvey(), this)
                .ifPresent(
                        newAnswers -> {

                            for (IAnswer newAnswer : newAnswers) {
                                this.getSurvey().addAnswer(newAnswer);

                                this.answerListView.getSelectionModel().select(
                                        newAnswer);
                                this.linkedAnswers.put(newAnswer,
                                        (SpreadsheetCell) newAnswer
                                                .getDataPoint());
                            }

                            if (!newAnswers.isEmpty()) {

                                this.answerListView.getSelectionModel().select(
                                        newAnswers.get(0));
                                this.updateSurvey();
                            }
                        });
    }

    private void findLinkedMetadata() {

        LOG.debug("Detecting linked metadata.");

        for (IQuestion question : this.getSurvey().getQuestions()) {

            if (question.getDataPoint() != null
                    && question.getDataPoint() instanceof SpreadsheetCell) {

                LOG.debug("Question " + question + " is linked!");
                this.linkedQuestions.put(question,
                        (SpreadsheetCell) question.getDataPoint());
            }
        }

        for (IParticipant participant : this.getSurvey().getParticipants()) {

            if (participant.getDataPoint() != null
                    && participant.getDataPoint() instanceof SpreadsheetCell) {

                this.linkedParticipants.put(participant,
                        (SpreadsheetCell) participant.getDataPoint());
            }
        }

        for (IAnswer answer : this.getSurvey().getAnswers()) {

            if (answer.getDataPoint() != null
                    && answer.getDataPoint() instanceof SpreadsheetCell) {

                this.linkedAnswers.put(answer,
                        (SpreadsheetCell) answer.getDataPoint());
            }
        }
    }

    private void selectTableCells(final List<SpreadsheetCell> cells) {

        this.spreadsheetView.getSelectionModel().clearSelection();

        for (SpreadsheetCell cell : cells) {
            try {

                SpreadsheetColumn column = this.spreadsheetView.getColumns()
                        .get(cell.getColumn());

                TableColumn<ObservableList<org.controlsfx.control.spreadsheet.SpreadsheetCell>, org.controlsfx.control.spreadsheet.SpreadsheetCell> tableColumn = ReflectionUtils
                        .getValue(column, "column", TableColumn.class);

                this.spreadsheetView.getSelectionModel().select(cell.getRow(),
                        tableColumn);
            }
            catch (Exception e) {

                LOG.error(
                        "Could not select (" + cell.getRow() + ", "
                                + cell.getColumn() + ")", e);
            }
        }
    }

    @Override
    protected void refreshView() {

        this.questionListView.clear();
        this.participantListView.clear();
        this.answerListView.clear();

        this.linkedQuestions.clear();
        this.linkedParticipants.clear();
        this.linkedQuestions.clear();

        if (this.getSurvey() != null) {

            this.questionListView.getItems().addAll(
                    this.getSurvey().getQuestions());
            this.participantListView.getItems().addAll(
                    this.getSurvey().getParticipants());
            this.participantListView.getItems().sort(
                    (lhs, rhs) -> {
                        return StringWithNumberComparator.compareTo(
                                lhs.getName(), rhs.getName());
                    });

            this.answerListView.getItems()
                    .addAll(this.getSurvey().getAnswers());

            if (this.getSurvey().getDataSource() != null
                    && this.getSurvey().getDataSource() instanceof Spreadsheet) {

                this.spreadsheetView.setSpreadsheet((Spreadsheet) this
                        .getSurvey().getDataSource());

                this.spreadsheetView.setEditable(false);

                this.findLinkedMetadata();
            }
        }
    }
}
