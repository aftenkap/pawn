package raziel.pawn.ui.controls;


import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;

import org.jutility.javafx.control.TableDataView;

import raziel.pawn.core.domain.data.Spreadsheet;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * 
 */
public class TableGridPane
        extends GridPane {



    private ObjectProperty<Spreadsheet> spreadsheet;

    private TableDataView<String>       tableView;


    private Text                        uri;


    /**
     * Creates a new instance of the {@link TableGridPane} class.
     */
    public TableGridPane() {

        this(null);
    }

    /**
     * Creates a new instance of the {@link TableGridPane} class.
     * 
     * @param spreadsheet
     *            the spreadsheet to be displayed.
     */
    public TableGridPane(Spreadsheet spreadsheet) {

        this.setPadding(new Insets(10, 10, 10, 10));
        this.setHgap(20);
        this.setVgap(10);

        this.spreadsheet = new SimpleObjectProperty<>(spreadsheet);

        this.tableView = new TableDataView<>(spreadsheet);
        this.tableView.tableProperty().set(this.spreadsheet.get());
        this.tableView.tableProperty().bind(this.spreadsheet);

        this.tableView.selectionModelProperty().get()
                .setCellSelectionEnabled(true);


        this.uri = new Text();
        Label uriLabel = new Label("Data Source:");
        uriLabel.setLabelFor(uri);
        GridPane.setHgrow(uriLabel, Priority.ALWAYS);
        GridPane.setHgrow(uri, Priority.ALWAYS);

        HBox labelBox = new HBox(20);

        labelBox.getChildren().addAll(uriLabel, uri);

        if (spreadsheet != null) {

            this.uri.setText(spreadsheet.getUri().toASCIIString());
        }


        this.add(labelBox, 0, 0);

        GridPane.setHgrow(labelBox, Priority.ALWAYS);

        this.add(this.tableView, 0, 1);
        GridPane.setHgrow(tableView, Priority.ALWAYS);
        GridPane.setVgrow(tableView, Priority.ALWAYS);


        this.setupEventHandlers();
        this.setupContextMenus();
    }

    private void setupEventHandlers() {

        //
    }

    private void setupContextMenus() {

        //
    }

}
