package raziel.pawn.ui.controls;


import java.util.Arrays;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import org.jutility.javafx.control.labeled.LabeledComboBox;
import org.jutility.javafx.control.wrapper.TextAreaWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.IAnswer;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.ui.controls.dialog.EditLongTextDialog;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * The {@code AnswerDetailsGridPane} class provides a visual representation of
 * an {@link IAnswer Answer}.
 * 
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class AnswerDetailsGridPane
        extends MetadataDetailsGridPane<IAnswer> {

    private static Logger                       LOG = LoggerFactory
                                                            .getLogger(AnswerDetailsGridPane.class);


    private final LabeledComboBox<IQuestion>    questionCB;
    private final LabeledComboBox<IParticipant> participantCB;
    private final TextAreaWrapper               answerValue;

    private Action                              editAnswerValue;


    /**
     * Creates a new instance of the {@link AnswerDetailsGridPane} class.
     */
    public AnswerDetailsGridPane() {

        this(null, null);
    }


    /**
     * Creates a new instance of the {@link AnswerDetailsGridPane} class with
     * the provided {@link IAnswer Answer} and {@link Survey}.
     * 
     * @param answer
     *            the {@link IAnswer Answer}.
     * @param survey
     *            the {@link Survey} containing the {@link IAnswer Answer}.
     */
    public AnswerDetailsGridPane(final IAnswer answer, final Survey survey) {

        this(answer, survey, false);
    }

    /**
     * Creates a new instance of the {@link AnswerDetailsGridPane} class with
     * the provided {@link IAnswer Answer} and {@link Survey}. Also specifies
     * whether the update of the {@link IAnswer Answer} in the database should
     * be deferred.
     * 
     * @param answer
     *            the {@link IAnswer Answer}.
     * @param survey
     *            the {@link Survey} containing the {@link IAnswer Answer}.
     * @param deferUpdate
     *            whether or not the update of the {@link IAnswer Answer} in the
     *            database is deferred.
     */
    public AnswerDetailsGridPane(final IAnswer answer, final Survey survey,
            final boolean deferUpdate) {

        super(answer, survey, deferUpdate);


        this.questionCB = new LabeledComboBox<>("Answer to");
        this.questionCB.setConverter(new DomainObjectStringConverter<>());
        this.questionCB.setHgap(10);
        this.questionCB.setVisible(false);
        this.questionCB.setDisable(true);

        this.participantCB = new LabeledComboBox<>("by");
        this.participantCB.setConverter(new DomainObjectStringConverter<>());
        this.participantCB.setHgap(10);
        this.participantCB.setVisible(false);
        this.participantCB.setDisable(true);

        HBox labelHBox = new HBox(20, this.questionCB, this.participantCB);
        GridPane.setHgrow(labelHBox, Priority.SOMETIMES);

        this.answerValue = new TextAreaWrapper();
        this.answerValue.setWrapText(true);
        this.answerValue.setPromptText("Enter answer value (cannot be empty)");

        GridPane.setHgrow(this.answerValue, Priority.SOMETIMES);
        GridPane.setVgrow(this.answerValue, Priority.SOMETIMES);

        this.answerValue.setVisible(false);
        this.answerValue.setEditable(false);

        this.add(labelHBox, 0, 1);
        this.add(this.answerValue, 0, 2);


        this.setupValidation();
        this.setupContextMenus();
        this.setUpEventHandlers();

        this.refreshView();
    }

    private void setUpEventHandlers() {

        this.questionCB.visibleProperty().bind(
                this.metadata().isNotNull()
                        .and(this.surveyProperty().isNotNull()));
        this.participantCB.visibleProperty().bind(
                this.metadata().isNotNull()
                        .and(this.surveyProperty().isNotNull()));
        this.answerValue.visibleProperty().bind(
                this.metadata().isNotNull()
                        .and(this.surveyProperty().isNotNull()));


        this.questionCB.disableProperty().bind(
                this.updateDeferredProperty().not());
        this.participantCB.disableProperty().bind(
                this.updateDeferredProperty().not());
        this.answerValue.editableProperty().bind(this.updateDeferredProperty());


        this.updateDeferredProperty().addListener(
                (observable, oldValue, newValue) -> {

                    if (newValue) {

                        this.answerValue.getContextMenu().getItems().clear();
                    }
                    else {

                        this.answerValue
                                .getContextMenu()
                                .getItems()
                                .add(ActionUtils
                                        .createMenuItem(this.editAnswerValue));
                    }
                });
    }

    private void setupContextMenus() {

        this.editAnswerValue = new Action("Edit", (event) -> {

            this.editAnswerValue();
        });
        this.answerValue.setContextMenu(ActionUtils.createContextMenu(Arrays
                .asList(this.editAnswerValue)));
    }

    @Override
    protected void refreshView() {

        super.refreshView();

        this.questionCB.getItems().clear();
        this.participantCB.getItems().clear();
        this.answerValue.clear();

        if (this.getMetadata() != null && this.getSurvey() != null) {

            IAnswer answer = this.getMetadata();
            Survey survey = this.getSurvey();
            IQuestion question = answer.getQuestion();
            IParticipant participant = answer.getParticipant();

            this.questionCB.getItems().addAll(this.getSurvey().getQuestions());
            this.participantCB.getItems().addAll(
                    this.getSurvey().getParticipants());

            if (survey != null) {

                if (question != null) {

                    this.questionCB.getSelectionModel().select(question);
                }

                if (participant != null) {

                    this.participantCB.getSelectionModel().select(participant);
                }
            }
            if (this.getMetadata().getValue() != null) {

                this.answerValue.setText(this.getMetadata().getValue());
            }
        }
    }


    private void setupValidation() {

        this.answerValue.registerValidator(Validator
                .createEmptyValidator("Answer text cannot be empty!"));
        this.answerValue
                .setValidationDecorator(new GraphicValidationDecoration());
        this.answerValue.setErrorDecorationEnabled(true);
        this.validationGroup().registerSubValidation(this.answerValue,
                this.answerValue.validationSupport());

        this.questionCB.registerValidator(Validator.createPredicateValidator(
                value -> {
                    return this.questionCB.getValue() != null;
                }, "Question cannot be empty!"));
        this.questionCB
                .setValidationDecorator(new GraphicValidationDecoration());
        this.questionCB.setErrorDecorationEnabled(true);
        this.validationGroup().registerSubValidation(this.questionCB,
                this.questionCB.validationSupport());


        this.participantCB.registerValidator(Validator
                .createPredicateValidator(value -> {
                    return this.participantCB.getValue() != null;
                }, "Participant cannot be empty!"));
        this.participantCB
                .setValidationDecorator(new GraphicValidationDecoration());
        this.participantCB.setErrorDecorationEnabled(true);
        this.validationGroup().registerSubValidation(this.participantCB,
                this.participantCB.validationSupport());
    }

    @Override
    protected void update() {

        if (!this.isUpdateDeferred()) {

            IAnswer answer = this.getMetadata();

            if (answer != null) {

                LOG.debug("Updating answer " + answer);
                ApplicationContext.surveyController()
                        .updateAnswer(answer, this)
                        .ifPresent(updatedAnswer -> {

                            this.setMetadata(null);
                            this.setMetadata(updatedAnswer);
                        });
            }
        }
        else {

            LOG.debug("Update of answer deferred!");
        }
    }


    private void editAnswerValue() {

        EditLongTextDialog dlg = new EditLongTextDialog(this.getScene()
                .getWindow(), this.getMetadata().getValue());

        dlg.showAndWait().ifPresent(result -> {

            this.getMetadata().setValue(result);

            this.update();
        });
    }
}
