package raziel.pawn.ui.controls;


import java.util.LinkedList;

import org.jutility.javafx.control.labeled.LabeledComboBox;

import raziel.pawn.core.domain.metadata.IAnswer;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.IQuestion;
import raziel.pawn.core.domain.modeling.DataModel;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;


/**
 * @author Peter J. Radics
 * 
 */
public class DatasetSidebar
        extends VBox {

    private final ObservableList<DataModel>     datasets;
    private final ObservableList<IParticipant>  participants;
    private final ObservableList<IQuestion>     questions;
    // private final ObservableList<IAnswer> answers;
    private final ObjectProperty<IAnswer>       answer;

    private final LabeledComboBox<DataModel>    datasetSelectionCB;
    private final LabeledComboBox<IParticipant> participantComboBox;
    private final QuestionBox                   questionBox;
    private final AnswerDetailsGridPane         answerDetailsGridPane;

    /**
     * Returns the dataset property.
     * 
     * @return the dataset property.
     */
    public ObservableList<DataModel> datasets() {

        return this.datasets;
    }

    /**
     * Returns the participants property.
     * 
     * @return the participants property.
     */
    public ObservableList<IParticipant> participants() {

        return this.participants;
    }

    /**
     * Returns the questions property.
     * 
     * @return the questions property.
     */
    public ObservableList<IQuestion> questions() {

        return this.questions;
    }

    // /**
    // * Returns the answers property.
    // *
    // * @return the answers property.
    // */
    // public ObservableList<IAnswer> answers() {
    //
    // return this.answers;
    // }

    /**
     * Returns the selected answer property.
     * 
     * @return the selected answer property.
     */
    public ReadOnlyObjectProperty<IAnswer> selectedAnswer() {

        return this.answer;
    }

    /**
     * Returns the selected answer.
     * 
     * @return Returns the selected answer.
     */
    public IAnswer getSelectedAnswer() {

        return this.selectedAnswer().get();
    }

    /**
     * Returns the selected dataset property.
     * 
     * @return the selected dataset property.
     */
    public ReadOnlyObjectProperty<DataModel> selectedDataset() {

        return this.datasetSelectionCB.getSelectionModel()
                .selectedItemProperty();
    }

    /**
     * Returns the selected dataset.
     * 
     * @return the selected dataset.
     */
    public DataModel getSelectedDataset() {

        return this.selectedDataset().get();
    }

    /**
     * Returns the selected participant property.
     * 
     * @return the selected participant property.
     */
    public ReadOnlyObjectProperty<IParticipant> selectedParticipant() {

        return this.participantComboBox.getSelectionModel()
                .selectedItemProperty();
    }

    /**
     * Returns the selected participant.
     * 
     * @return the selected participant.
     */
    public IParticipant getSelectedParticipant() {

        return this.selectedParticipant().get();
    }

    /**
     * Returns the selected question property.
     * 
     * @return the selected question property.
     */
    public ReadOnlyObjectProperty<IQuestion> selectedQuestion() {

        return this.questionBox.selectedItemProperty();
    }

    /**
     * Returns the selected question.
     * 
     * @return the selected question.
     */
    public IQuestion getSelectedQuestion() {

        return this.selectedQuestion().get();
    }


    /**
     * Creates a new instance of the {@link DatasetSidebar} class.
     */
    public DatasetSidebar() {

        this(10);

    }

    /**
     * Creates a new instance of the {@link DatasetSidebar} class with the
     * provided spacing.
     * 
     * @param spacing
     *            the spacing between the children of the sidebar.
     */
    public DatasetSidebar(double spacing) {

        super(spacing);

        this.setPadding(new Insets(10, 10, 10, 10));

        this.datasets = FXCollections
                .observableList(new LinkedList<DataModel>());

        this.participants = FXCollections.observableList(new LinkedList<>());
        this.questions = FXCollections.observableList(new LinkedList<>());
        // this.answers = FXCollections.observableList(new
        // LinkedList<IAnswer>());
        this.answer = new SimpleObjectProperty<>();


        this.datasetSelectionCB = new LabeledComboBox<>("Select DataModel");
        this.datasetSelectionCB
                .setConverter(new DomainObjectStringConverter<DataModel>());

        GridPane.setHgrow(this.datasetSelectionCB, Priority.ALWAYS);

        Bindings.bindContent(this.datasetSelectionCB.getItems(), this.datasets);

        this.participantComboBox = new LabeledComboBox<>("Participants");
        this.participantComboBox
                .setConverter(new DomainObjectStringConverter<>());
        Bindings.bindContent(this.participantComboBox.getItems(),
                this.participants);


        this.questionBox = new QuestionBox();
        Bindings.bindContent(this.questionBox.questions(), this.questions);
        VBox.setVgrow(this.questionBox, Priority.ALWAYS);


        this.answerDetailsGridPane = new AnswerDetailsGridPane();
        this.answerDetailsGridPane.metadata().bind(answer);
        VBox.setVgrow(this.answerDetailsGridPane, Priority.ALWAYS);



        this.getChildren().addAll(datasetSelectionCB, participantComboBox,
                questionBox, answerDetailsGridPane);

        this.setupEventHandlers();
    }



    private void setupEventHandlers() {



        this.participantComboBox.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<IParticipant>() {

                    @Override
                    public void changed(
                            ObservableValue<? extends IParticipant> observable,
                            IParticipant oldValue, IParticipant newValue) {

                        if (oldValue != null) {
                            // DatasetSidebar.this.datasets.remove(oldValue
                            // .getDataset());
                        }
                        if (newValue != null) {

                            // DatasetSidebar.this.datasets.add(newValue
                            // .getDataset());
                        }
                        DatasetSidebar.this.setAnswer();
                    }
                });
        this.questionBox.selectedItemProperty().addListener(
                new ChangeListener<IQuestion>() {

                    @Override
                    public void changed(
                            ObservableValue<? extends IQuestion> observable,
                            IQuestion oldValue, IQuestion newValue) {

                        DatasetSidebar.this.setAnswer();
                        if (oldValue != null) {
                            // DatasetSidebar.this.datasets.remove(oldValue
                            // .getDataset());
                        }
                        if (newValue != null) {

                            // DatasetSidebar.this.datasets.add(newValue
                            // .getDataset());
                        }
                        DatasetSidebar.this.setAnswer();
                    }
                });

    }



    private void setAnswer() {

        // System.out.println("Setting answer");
        if (this.participantComboBox.getSelectionModel().getSelectedItem() != null
                && this.questionBox.getSelectedItem() != null) {

            // IParticipant participant = this.participantComboBox
            // .getSelectedItem();
            // IQuestion question = this.questionBox.getSelectedItem();

            // System.out.println("Both not null");
            // System.out.println("Participant: " + participant);
            // System.out.println("Question: " + question);


            this.answer.set(null);
            // for (IAnswer answer : question.getAnswers()) {
            //
            // // System.out.println("Answer: " + answer);
            // // System.out.println("\tParticipant: " +
            // // answer.getParticipant());
            // // System.out.println("\tQuestion: " + answer.getQuestion());
            // if (participant.equals(answer.getParticipant())) {
            //
            // // System.out.println("Setting answer to " + answer);
            // this.answer.set(answer);
            // break;
            // }
            // }
        }

    }
}
