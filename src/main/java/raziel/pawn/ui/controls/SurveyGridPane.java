package raziel.pawn.ui.controls;


import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import org.controlsfx.control.action.Action;
import org.jutility.javafx.control.labeled.LabeledTextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.ui.controls.dialog.DomainObjectSelectionDialog;



/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class SurveyGridPane
        extends SurveyContentGridPane {

    private static Logger             LOG = LoggerFactory
                                                  .getLogger(SurveyGridPane.class);

    private final LabeledTextField    dataSourceTextField;

    private final TabPane             content;

    private final QuestionGridPane    questionGridPane;
    private final ParticipantGridPane participantGridPane;
    private final AnswerGridPane      answerGridPane;
    private final DataLinkGridPane    dataLinkGridPane;

    private final Tab                 dataLinkTab;

    private Action                    setDataSource;
    private Action                    removeDataSource;

    /**
     * Creates a new instance of the {@link SurveyGridPane} class.
     * 
     * @param survey
     *            the survey.
     */
    public SurveyGridPane(Survey survey) {

        super(survey);
        LOG.debug("Creating SurveyGridPane.");

        this.dataSourceTextField = new LabeledTextField("Data Source:");

        this.dataSourceTextField.setEditable(false);
        this.dataSourceTextField.setHgap(20);

        GridPane.setHgrow(this.dataSourceTextField, Priority.ALWAYS);
        GridPane.setVgrow(this.dataSourceTextField, Priority.NEVER);


        this.content = new TabPane();
        this.questionGridPane = new QuestionGridPane(this.getSurvey());
        this.participantGridPane = new ParticipantGridPane(this.getSurvey());
        this.answerGridPane = new AnswerGridPane(this.getSurvey());
        this.dataLinkGridPane = new DataLinkGridPane(this.getSurvey());

        Tab questionTab = new Tab("Questions");
        questionTab.setClosable(false);
        questionTab.setContent(this.questionGridPane);

        Tab participantTab = new Tab("Participants");
        participantTab.setClosable(false);
        participantTab.setContent(this.participantGridPane);

        Tab answerTab = new Tab("Answers");
        answerTab.setClosable(false);
        answerTab.setContent(this.answerGridPane);


        this.dataLinkTab = new Tab("Linked Data");
        this.dataLinkTab.setClosable(false);
        this.dataLinkTab.setContent(this.dataLinkGridPane);
        this.dataLinkTab.setDisable(true);

        this.content.getTabs().add(questionTab);
        this.content.getTabs().add(participantTab);
        this.content.getTabs().add(answerTab);
        this.content.getTabs().add(this.dataLinkTab);
        this.content.getSelectionModel().select(questionTab);
        GridPane.setHgrow(this.content, Priority.ALWAYS);
        GridPane.setVgrow(this.content, Priority.ALWAYS);

        this.add(this.dataSourceTextField, 0, 0);
        this.add(this.content, 0, 1);


        this.surveyProperty().bindBidirectional(
                this.questionGridPane.surveyProperty());
        this.surveyProperty().bindBidirectional(
                this.participantGridPane.surveyProperty());
        this.surveyProperty().bindBidirectional(
                this.answerGridPane.surveyProperty());
        this.surveyProperty().bindBidirectional(
                this.dataLinkGridPane.surveyProperty());

        this.setupContextMenus();

        this.refreshView();
    }


    private void setupContextMenus() {

        this.setDataSource = new Action("Set Data Source", (actionEvent) -> {

            this.setDataSource();
        });

        this.removeDataSource = new Action("Remove Data Source",
                (actionEvent) -> {

                    this.removeDataSource();
                });


        this.dataSourceTextField.contextMenuActions().addAll(
                this.setDataSource, this.removeDataSource);
    }


    private void setDataSource() {

        ApplicationContext
                .projectController()
                .retrieveProjectContainingMetadataSource(this.getSurvey(), this)
                .ifPresent(
                        project -> {
                            new DomainObjectSelectionDialog<>(this,
                                    "Select Data Source", project
                                            .getDataSources()).showAndWait()
                                    .ifPresent(
                                            dataSource -> {

                                                this.getSurvey().setDataSource(
                                                        dataSource);

                                                this.updateSurvey();
                                            });
                        });


    }

    private void removeDataSource() {

        this.getSurvey().setDataSource(null);
        this.updateSurvey();
    }


    @Override
    protected void refreshView() {

        LOG.debug("Refreshing view.");
        this.dataSourceTextField.clear();
        if (this.getSurvey() != null
                && this.getSurvey().getDataSource() != null) {

            this.dataSourceTextField.setText(this.getSurvey().getDataSource()
                    .getName());
            this.dataLinkTab.setDisable(false);
        }
        else {

            if (this.content.getSelectionModel().getSelectedItem() != null
                    && this.content.getSelectionModel().getSelectedItem()
                            .equals(this.dataLinkTab)) {

                this.content.getSelectionModel().select(0);
            }
            this.dataLinkTab.setDisable(true);
        }
    }
}
