package raziel.pawn.ui.controls;



import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;

import org.controlsfx.control.action.Action;
import org.jutility.common.datatype.util.StringWithNumberComparator;
import org.jutility.javafx.control.ListViewWithSearchPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.metadata.IParticipant;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class ParticipantGridPane
        extends SurveyContentGridPane {


    private static Logger                               LOG = LoggerFactory
                                                                    .getLogger(ParticipantGridPane.class);

    private final ListViewWithSearchPanel<IParticipant> participantsListView;
    private final Label                                 participantLabel;
    private final ParticipantDetailsGridPane            participantDetails;


    private final Label                                 answersLabel;
    private final AnswerGridPane                        answerGridPane;

    private Action                                      addParticipant;
    private Action                                      removeParticipant;
    private Action                                      renameParticipant;



    /**
     * Creates a new instance of the {@link ParticipantGridPane} class.
     * 
     * @param survey
     *            the survey.
     */
    public ParticipantGridPane(final Survey survey) {

        super(survey);

        ColumnConstraints listColumnWidthConstraint = new ColumnConstraints();
        listColumnWidthConstraint.setPercentWidth(25);
        ColumnConstraints detailsColumnWidthConstraint = new ColumnConstraints();
        detailsColumnWidthConstraint.setPercentWidth(75);

        this.getColumnConstraints().addAll(listColumnWidthConstraint,
                detailsColumnWidthConstraint);


        this.participantsListView = new ListViewWithSearchPanel<>();
        this.participantsListView
                .setConverter(new DomainObjectStringConverter<>());
        this.participantsListView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE);


        this.add(this.participantsListView, 0, 0, 1, 4);
        GridPane.setHgrow(this.participantsListView, Priority.ALWAYS);
        GridPane.setVgrow(this.participantsListView, Priority.ALWAYS);


        this.participantDetails = new ParticipantDetailsGridPane();
        this.participantDetails.setVisible(false);

        this.participantLabel = new Label("Participant");
        this.participantLabel.setLabelFor(this.participantDetails);
        this.participantLabel.setFont(Font.font("verdana", 16));
        this.participantLabel.setVisible(false);

        this.add(this.participantLabel, 1, 0);
        this.add(this.participantDetails, 1, 1);

        GridPane.setHgrow(this.participantDetails, Priority.ALWAYS);
        GridPane.setHalignment(this.participantDetails, HPos.LEFT);

        this.answersLabel = new Label("Answered questions");
        this.answersLabel.setVisible(false);

        this.answerGridPane = new AnswerGridPane(survey);
        this.answerGridPane.setPadding(new Insets(0));
        this.answerGridPane.setVisible(false);
        GridPane.setHgrow(this.answerGridPane, Priority.ALWAYS);
        GridPane.setVgrow(this.answerGridPane, Priority.ALWAYS);
        GridPane.setHalignment(this.answerGridPane, HPos.LEFT);

        this.answersLabel.setLabelFor(this.answerGridPane);
        this.answersLabel.setFont(Font.font("verdana", 16));

        this.add(this.answersLabel, 1, 2);
        this.add(this.answerGridPane, 1, 3);

        this.participantDetails.surveyProperty().bindBidirectional(
                this.surveyProperty());
        this.answerGridPane.surveyProperty().bindBidirectional(
                this.surveyProperty());

        this.setupContextMenus();
        this.setupEventHandlers();

        this.refreshView();
    }


    private void setupContextMenus() {

        this.addParticipant = new Action("New", (actionEvent) -> {

            this.create();
        });


        this.removeParticipant = new Action("Delete", (actionEvent) -> {

            this.delete(this.participantsListView.getSelectedItem());
        });


        this.renameParticipant = new Action("Rename", (actionEvent) -> {

            this.rename(this.participantsListView.getSelectedItem());
        });


        this.participantsListView.contextMenuActions().addAll(addParticipant,
                renameParticipant, removeParticipant);
    }

    private void setupEventHandlers() {

        this.participantDetails.visibleProperty().bind(
                this.participantsListView.selectedItemProperty().isNotNull());
        this.participantLabel.visibleProperty().bind(
                this.participantsListView.selectedItemProperty().isNotNull());

        this.answerGridPane.visibleProperty().bind(
                this.participantsListView.selectedItemProperty().isNotNull());
        this.answersLabel.visibleProperty().bind(
                this.participantsListView.selectedItemProperty().isNotNull());

        this.removeParticipant.disabledProperty().bind(
                this.participantsListView.selectedItemProperty().isNull());
        this.renameParticipant.disabledProperty().bind(
                this.participantsListView.selectedItemProperty().isNull());


        this.participantsListView.selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    if (newValue != null) {
                        ApplicationContext
                                .surveyController()
                                .retrieveParticipant(newValue.getId(), this)
                                .ifPresent(
                                        participant -> {

                                            this.participantDetails
                                                    .setMetadata(participant);
                                            this.answerGridPane
                                                    .setFilter(participant);
                                        });
                    }
                });
    }

    private void create() {

        LOG.debug("Creating participant.");
        ApplicationContext
                .surveyController()
                .createParticipant(this)
                .ifPresent(
                        newParticipant -> {

                            this.getSurvey().addParticipant(newParticipant);

                            this.updateSurvey();

                            if (this.getSurvey().getParticipants()
                                    .contains(newParticipant)) {

                                this.participantsListView.getSelectionModel()
                                        .select(newParticipant);
                            }
                        });
    }

    private void rename(final IParticipant participant) {

        LOG.info("Renaming participant " + participant);
        Utils.renameDomainObject(participant, this).ifPresent(
                renamedParticipant -> {

                    ApplicationContext
                            .surveyController()
                            .updateParticipant(renamedParticipant, this)
                            .ifPresent(
                                    updatedParticipant -> {
                                        this.participantsListView.getItems()
                                                .set(this.participantsListView
                                                        .getItems().indexOf(
                                                                participant),
                                                        updatedParticipant);
                                    });
                });
    }

    private void delete(final IParticipant participant) {

        LOG.info("Deleting participant " + participant);
        ApplicationContext
                .surveyController()
                .deleteParticipant(participant, this)
                .ifPresent(
                        deletedParticipant -> {

                            LOG.debug("Clearing selection");
                            this.participantsListView.getSelectionModel()
                                    .clearSelection();
                            this.refreshSurvey();
                        });
    }


    @Override
    protected void refreshView() {

        LOG.debug("Refreshing view.");
        IParticipant selectedItem = this.participantsListView.getSelectedItem();

        this.participantsListView.clear();
        this.participantsListView.getSelectionModel().clearSelection();

        if (this.getSurvey() != null) {

            LOG.debug("Adding " + this.getSurvey().getParticipants().size()
                    + " Participants to view.");
            this.participantsListView.getItems().addAll(
                    this.getSurvey().getParticipants());
            this.participantsListView.getItems().sort(
                    (lhs, rhs) -> {
                        return StringWithNumberComparator.compareTo(
                                lhs.getName(), rhs.getName());
                    });

            if (selectedItem != null
                    && this.getSurvey().getParticipants()
                            .contains(selectedItem)) {

                this.participantsListView.getSelectionModel().select(
                        selectedItem);
            }
        }
    }

}
