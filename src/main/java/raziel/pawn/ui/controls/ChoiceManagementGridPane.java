package raziel.pawn.ui.controls;


import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.ExceptionDialog;
import org.jutility.javafx.control.ListViewWithSearchPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.core.domain.metadata.Choice;
import raziel.pawn.core.domain.metadata.ChoiceSet;
import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.ui.controllers.SurveyController;
import raziel.pawn.ui.controls.dialog.DomainObjectDialog;
import raziel.pawn.ui.stringConverters.DomainObjectStringConverter;


/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class ChoiceManagementGridPane
        extends GridPane {

    private static Logger                      LOG = LoggerFactory
                                                           .getLogger(ChoiceManagementGridPane.class);
    private SurveyController                   surveyController;

    private ChoiceSetDetailsGridPane           choiceSetDetails;

    private ListViewWithSearchPanel<Choice>    choices;
    private ListViewWithSearchPanel<ChoiceSet> choiceSets;


    private Action                             createChoice;
    private Action                             renameChoice;
    private Action                             deleteChoice;

    private Action                             createChoiceSet;
    private Action                             renameChoiceSet;
    private Action                             deleteChoiceSet;

    /**
     * Creates a new instance of the {@link ChoiceManagementGridPane} class.
     */
    public ChoiceManagementGridPane() {

        this.surveyController = ApplicationContext.surveyController();


        this.setPadding(new Insets(10, 10, 10, 10));
        this.setHgap(20);
        this.setVgap(10);

        this.choices = new ListViewWithSearchPanel<>("Choices",
                new DomainObjectStringConverter<>());
        this.choiceSets = new ListViewWithSearchPanel<>("Choice Sets",
                new DomainObjectStringConverter<>());

        GridPane.setVgrow(this.choices, Priority.SOMETIMES);
        GridPane.setHgrow(this.choices, Priority.SOMETIMES);
        GridPane.setVgrow(this.choiceSets, Priority.SOMETIMES);
        GridPane.setHgrow(this.choiceSets, Priority.SOMETIMES);

        this.add(this.choices, 0, 0, 1, 2);
        this.add(this.choiceSets, 1, 0, 1, 2);

        this.choiceSetDetails = new ChoiceSetDetailsGridPane("",
                new DomainObjectStringConverter<>());

        GridPane.setVgrow(this.choiceSetDetails, Priority.SOMETIMES);
        GridPane.setHgrow(this.choiceSetDetails, Priority.SOMETIMES);

        this.add(this.choiceSetDetails, 2, 0);


        this.setupActions();
        this.setupEventHandlers();
        this.setupContextMenus();

        this.refresh();
    }

    private void setupActions() {

        this.createChoice = new Action("New", (event) -> {

            this.create(Choice.class);
        });

        this.renameChoice = new Action("Rename", (event) -> {

            this.rename(this.choices.getSelectedItem());
        });

        this.deleteChoice = new Action("Remove", (event) -> {

            this.delete(this.choices.getSelectedItem());
        });


        this.createChoiceSet = new Action("New", (event) -> {

            this.create(ChoiceSet.class);
        });

        this.renameChoiceSet = new Action("Rename", (event) -> {

            this.rename(this.choiceSets.getSelectedItem());
        });

        this.deleteChoiceSet = new Action("Remove", (event) -> {

            this.delete(this.choiceSets.getSelectedItem());
        });
    }

    private void setupEventHandlers() {

        this.choices.selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    boolean selectionEmpty = newValue == null;

                    this.renameChoice.disabledProperty().set(selectionEmpty);
                    this.deleteChoice.disabledProperty().set(selectionEmpty);

                });
        this.choiceSets.selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {

                            boolean selectionEmpty = newValue == null;

                            this.renameChoiceSet.disabledProperty().set(
                                    selectionEmpty);
                            this.deleteChoiceSet.disabledProperty().set(
                                    selectionEmpty);
                        });

        this.choiceSetDetails.choiceSet().bind(
                this.choiceSets.selectedItemProperty());
    }


    private void setupContextMenus() {

        this.choices.contextMenuActions().addAll(this.createChoice,
                this.renameChoice, this.deleteChoice);
        this.choiceSets.contextMenuActions().addAll(this.createChoiceSet,
                this.renameChoiceSet, this.deleteChoiceSet);

    }

    private void create(Class<? extends IDomainObject> type) {


        if (type == Choice.class) {

            ApplicationContext
                    .surveyController()
                    .createChoice(this)
                    .ifPresent(
                            newChoice -> {

                                LOG.info("Created new choice "
                                        + newChoice.getName() + ".");
                                this.choices.getItems().add(newChoice);
                            });

        }
        else if (type == ChoiceSet.class) {

            ApplicationContext
                    .surveyController()
                    .createChoiceSet(this)
                    .ifPresent(
                            newChoiceSet -> {

                                LOG.info("Created Choice Set "
                                        + newChoiceSet.getName() + ".");
                                this.choiceSets.getItems().add(newChoiceSet);
                            });
        }
    }

    private void rename(IDomainObject domainObject) {

        if (domainObject != null) {
            DomainObjectDialog<IDomainObject> dlg = new DomainObjectDialog<>(
                    this.getScene().getWindow(), "Rename", domainObject);

            dlg.showAndWait()
                    .ifPresent(
                            changedObject -> {

                                LOG.info("Renaming " + domainObject + " to "
                                        + changedObject + ".");
                                if (changedObject instanceof Choice) {

                                    Choice choice = (Choice) changedObject;

                                    ApplicationContext
                                            .surveyController()
                                            .updateChoice(choice, this)
                                            .ifPresent(
                                                    updatedChoice -> {

                                                        this.choices
                                                                .getItems()
                                                                .set(this.choices
                                                                        .getItems()
                                                                        .indexOf(
                                                                                domainObject),
                                                                        updatedChoice);
                                                    });

                                }
                                else if (changedObject instanceof ChoiceSet) {
                                    ChoiceSet choiceSet = (ChoiceSet) changedObject;
                                    ApplicationContext
                                            .surveyController()
                                            .updateChoiceSet(choiceSet, this)
                                            .ifPresent(
                                                    updatedChoiceSet -> {

                                                        this.choiceSets
                                                                .getItems()
                                                                .set(this.choiceSets
                                                                        .getItems()
                                                                        .indexOf(
                                                                                domainObject),
                                                                        updatedChoiceSet);
                                                    });
                                }
                            });

        }
    }

    private void delete(IDomainObject domainObject) {

        if (domainObject != null) {

            if (domainObject instanceof Choice) {

                ApplicationContext.surveyController()
                        .deleteChoice((Choice) domainObject, this)
                        .ifPresent(removedChoice -> {

                            this.choices.remove((Choice) domainObject);
                        });
            }
            else if (domainObject instanceof ChoiceSet) {

                ApplicationContext.surveyController()
                        .deleteChoiceSet((ChoiceSet) domainObject, this)
                        .ifPresent(deletedChoiceSet -> {

                            this.choiceSets.remove((ChoiceSet) domainObject);
                        });
            }
        }
    }

    private void refresh() {

        this.choices.clear();
        this.choiceSets.clear();

        LOG.info("Retrieving Choices and Choice Sets.");
        ApplicationContext.surveyController().retrieveAllChoices(this)
                .ifPresent(choices -> {

                    this.choices.getItems().addAll(choices);
                });

        ApplicationContext.surveyController().retrieveAllChoiceSets(this)
                .ifPresent(choiceSets -> {

                    this.choiceSets.getItems().addAll(choiceSets);
                });
    }
}
