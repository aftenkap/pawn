package raziel.pawn.ui.controls;


import javafx.scene.control.TreeItem;
import raziel.pawn.core.domain.IDomainObject;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class DomainObjectTreeItem
        extends TreeItem<String> {

    private IDomainObject object;


    /**
     * Returns the object.
     * 
     * @return the object.
     */
    public IDomainObject getObject() {

        return object;
    }

    /**
     * Sets the object.
     * 
     * @param object
     *            the object.
     */
    public void setObject(IDomainObject object) {

        this.object = object;
        if (object != null) {

            this.setValue(object.getName());
        }
        else {

            this.setValue(null);
        }
    }


    /**
     * Creates a new instance of the {@link DomainObjectTreeItem} class.
     * 
     * @param object
     *            the domain object.
     */
    public DomainObjectTreeItem(IDomainObject object) {

        this.object = object;
        this.setValue(object.getName());
    }
}
