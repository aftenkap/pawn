package raziel.pawn.ui.controls;


import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import org.controlsfx.control.MasterDetailPane;
import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionGroup;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.dialog.ExceptionDialog;
import org.jutility.common.datatype.table.ITable;
import org.jutility.io.SerializationException;
import org.jutility.io.csv.CsvSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.ApplicationContext;
import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.core.domain.Project;
import raziel.pawn.core.domain.data.IDataSource;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.data.SpreadsheetCell;
import raziel.pawn.core.domain.metadata.IMetadataSource;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.core.domain.metadata.descriptors.TableDescriptor;
import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.ui.Utils;
import raziel.pawn.ui.controllers.ProjectController;
import raziel.pawn.ui.controllers.SurveyController;
import raziel.pawn.ui.controllers.SpreadsheetController;
import raziel.pawn.ui.controls.dialog.DomainObjectSelectionDialog;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public class WorkspaceGridPane
        extends BorderPane {


    private static Logger            LOG = LoggerFactory
                                                 .getLogger(WorkspaceGridPane.class);

    private static WorkspaceGridPane instance;

    /**
     * Returns the Singleton instance of the {@link WorkspaceGridPane} class.
     * 
     * @return the Singleton instance of the {@link WorkspaceGridPane} class.
     */
    public static WorkspaceGridPane Instance() {

        if (instance == null) {

            instance = new WorkspaceGridPane();
        }

        return instance;
    }


    private MenuBar                       mainMenu;

    private ActionGroup                   add;
    private Action                        addProject;
    private Action                        addSurvey;
    // private Action addSpreadsheet;

    private Action                        manageChoices;

    private Action                        open;
    private Action                        close;
    private Action                        rename;
    private Action                        delete;

    private Action                        importSpreadsheet;
    private Action                        linkMetadata;

    private Action                        refreshProjects;
    private Action                        exit;

    private final ProjectController       projectController;
    private final SpreadsheetController   spreadsheetController;
    private final SurveyController        surveyController;
    private MasterDetailPane              masterDetailPane;

    private ProjectTreeView               projectTreeView;

    private final TabPane                 tabPane;

    private final Map<IDomainObject, Tab> tabs;

    private final ObservableList<Project> projects;



    /**
     * The project list.
     * 
     * @return the project list.
     */
    public ObservableList<Project> projects() {

        return this.projects;
    }


    /**
     * Creates a new instance of the {@link WorkspaceGridPane} class.
     */
    private WorkspaceGridPane() {

        this.projects = FXCollections.observableList(new LinkedList<>());


        this.projectController = ApplicationContext.projectController();
        this.surveyController = ApplicationContext.surveyController();
        this.spreadsheetController = ApplicationContext.spreadsheetController();

        this.masterDetailPane = new MasterDetailPane(Side.LEFT);
        this.masterDetailPane.setAnimated(true);

        this.tabPane = new TabPane();
        this.tabPane.tabClosingPolicyProperty().set(TabClosingPolicy.ALL_TABS);
        this.tabs = new HashMap<>();

        this.masterDetailPane.setMasterNode(this.tabPane);


        this.projectTreeView = new ProjectTreeView();

        Bindings.bindContent(this.projectTreeView.projects(), this.projects());

        this.masterDetailPane.setDetailNode(this.projectTreeView);


        this.setupEventHandlers();
        this.setupActions();

        this.setupMenuBar();
        this.setupContextMenus();

        this.projectTreeView.getSelectionModel().clearSelection();

        this.setCenter(this.masterDetailPane);
        this.setTop(this.mainMenu);

        this.setActionState();
        // this.retrieveAllProjects();
    }

    private void setupEventHandlers() {

        this.projectTreeView.selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    this.setActionState();
                });


        this.projectTreeView.setOnMouseClickedTreeView((event) -> {

            if (event.getClickCount() == 2) {

                final IDomainObject domainObject = this.projectTreeView
                        .getSelectedDomainObject();

                if (domainObject != null && !this.isOpened(domainObject)) {

                    this.open();
                }
            }
        });
    }

    private void setupActions() {


        this.importSpreadsheet = new Action("Import Spreadsheet",
                (actionEvent) -> {

                    this.importSpreadsheet();
                });

        this.linkMetadata = new Action("Link", (actionEvent) -> {

            this.linkMetadata();
        });

        this.manageChoices = new Action("Manage Choices", (actionEvent) -> {

            this.manageChoices.disabledProperty().set(true);
            Tab choiceManagementTab = new Tab("Manage Choices");

            choiceManagementTab.setContent(new ChoiceManagementGridPane());

            choiceManagementTab.setOnClosed((event) -> {

                this.manageChoices.disabledProperty().set(false);

            });
            this.tabPane.getTabs().add(choiceManagementTab);
            this.tabPane.getSelectionModel().select(choiceManagementTab);
        });


        this.add = new ActionGroup("New");



        this.addProject = new Action("Project", (actionEvent) -> {

            this.createProject();
        });

        this.addSurvey = new Action("Survey", (actionEvent) -> {

            this.createSurvey();
        });

        this.add.getActions().addAll(addProject, addSurvey);



        this.open = new Action("Open", (actionEvent) -> {

            this.open();

        });

        this.close = new Action("Close", (actionEvent) -> {

            this.close();
        });
        this.close.disabledProperty().set(true);

        this.rename = new Action("Rename", (actionEvent) -> {

            this.rename();
        });

        this.delete = new Action("Delete", (actionEvent) -> {

            this.delete();
        });

        this.refreshProjects = new Action("Refresh Projects", (event) -> {

            this.refreshView();
        });

        this.exit = new Action("Exit", (actionEvent) -> {

            System.exit(0);
        });
    }

    private void setActionState() {

        IDomainObject domainObject = this.projectTreeView
                .getSelectedDomainObject();


        boolean isDomainObject = domainObject != null;

        boolean opened = this.isOpened(domainObject);


        this.linkMetadata.disabledProperty().set(
                !(domainObject instanceof IMetadataSource));
        this.addSurvey.disabledProperty().set(!isDomainObject);
        this.importSpreadsheet.disabledProperty().set(!isDomainObject);

        this.open.disabledProperty().set(!isDomainObject || opened);
        this.close.disabledProperty().set(!isDomainObject || !opened);

        this.rename.disabledProperty().set(!isDomainObject);
        this.delete.disabledProperty().set(!isDomainObject);
    }

    private void setupContextMenus() {

        List<Action> actions = new ArrayList<>();

        actions.add(this.add);
        actions.add(ActionUtils.ACTION_SEPARATOR);
        actions.add(this.open);
        actions.add(this.close);
        actions.add(ActionUtils.ACTION_SEPARATOR);
        actions.add(this.rename);
        actions.add(this.delete);

        this.projectTreeView.contextMenuProperty().set(
                ActionUtils.createContextMenu(actions));
    }

    private void setupMenuBar() {

        List<Action> menuBarActions = new ArrayList<>();

        ActionGroup file = new ActionGroup("File");

        file.getActions().addAll(this.refreshProjects,
                ActionUtils.ACTION_SEPARATOR, this.exit);


        ActionGroup data = new ActionGroup("Data");
        data.getActions().addAll(this.importSpreadsheet);

        ActionGroup metadata = new ActionGroup("Metadata");

        metadata.getActions().addAll(this.manageChoices, this.linkMetadata);


        menuBarActions.add(file);
        menuBarActions.add(data);
        menuBarActions.add(metadata);

        this.mainMenu = ActionUtils.createMenuBar(menuBarActions);
        VBox.setVgrow(this.mainMenu, Priority.ALWAYS);
    }


    private void refreshSelection() {

        int index = this.projectTreeView.getSelectedIndex();
        this.projectTreeView.getSelectionModel().clearSelection();
        this.projectTreeView.getSelectionModel().select(index);
    }



    private void open() {

        final IDomainObject domainObject = this.projectTreeView
                .getSelectedDomainObject();

        if (domainObject != null) {

            if (domainObject instanceof Project) {

                ApplicationContext.projectController()
                        .retrieveProject(domainObject.getId(), this)
                        .ifPresent(project -> {

                            this.projectTreeView.openProject(project);
                        });
            }
            else {

                Tab tab = new Tab(domainObject.getName());
                tab.setClosable(true);

                if (domainObject instanceof Survey) {

                    ApplicationContext
                            .surveyController()
                            .retrieveSurvey(domainObject.getId(), this)
                            .ifPresent(
                                    survey -> {

                                        SurveyGridPane surveyDetails = new SurveyGridPane(
                                                survey);
                                        tab.setContent(surveyDetails);
                                    });
                }
                else if (domainObject instanceof Spreadsheet) {

                    ApplicationContext
                            .spreadsheetController()
                            .retrieveSpreadsheet(domainObject.getId(), this)
                            .ifPresent(
                                    retrievedSpreadsheet -> {

                                        SpreadsheetView spreadsheetView = new SpreadsheetView(
                                                retrievedSpreadsheet);

                                        tab.setContent(spreadsheetView);
                                    });

                }

                if (tab.getContent() != null) {

                    this.tabs.put(domainObject, tab);
                    this.tabPane.getTabs().add(tab);

                    tab.setOnClosed((event) -> {

                        this.tabs.remove(domainObject);
                        this.refreshSelection();
                    });

                    this.tabPane.getSelectionModel().select(tab);
                    this.refreshSelection();
                }
            }
        }
    }

    private void close() {

        IDomainObject domainObject = this.projectTreeView
                .getSelectedDomainObject();

        if (domainObject != null) {

            if (domainObject instanceof Project) {

                Project project = (Project) domainObject;
                this.projectTreeView.closeProject(project);
                this.refreshSelection();
            }
            else {

                if (this.tabs.containsKey(domainObject)) {

                    Tab tab = this.tabs.get(domainObject);
                    this.tabPane.getTabs().remove(tab);
                }
            }
        }
    }


    private void rename() {

        IDomainObject domainObject = this.projectTreeView
                .getSelectedDomainObject();

        Map<IDomainObject, IDomainObject> updatedObjects = new LinkedHashMap<>();
        Utils.renameDomainObject(domainObject, this)
                .ifPresent(
                        changedObject -> {

                            if (changedObject instanceof Project) {

                                this.projectController.updateProject(
                                        (Project) changedObject, this)
                                        .ifPresent(
                                                project -> {

                                                    updatedObjects.put(
                                                            domainObject,
                                                            project);
                                                });
                            }
                            else {

                                Project project = this.projectTreeView
                                        .getParentProject();

                                if (changedObject instanceof Survey) {
                                    this.surveyController
                                            .updateSurvey(
                                                    (Survey) changedObject,
                                                    this)
                                            .ifPresent(
                                                    survey -> {
                                                        project.getMetadataSources()
                                                                .set(project
                                                                        .getMetadataSources()
                                                                        .indexOf(
                                                                                domainObject),
                                                                        survey);
                                                        updatedObjects.put(
                                                                domainObject,
                                                                survey);
                                                    });


                                }
                                else if (domainObject instanceof Spreadsheet) {

                                    this.spreadsheetController
                                            .updateSpreadsheet(
                                                    (Spreadsheet) changedObject,
                                                    this)
                                            .ifPresent(
                                                    spreadsheet -> {

                                                        project.getDataSources()
                                                                .set(project
                                                                        .getDataSources()
                                                                        .indexOf(
                                                                                domainObject),
                                                                        spreadsheet);
                                                        updatedObjects.put(
                                                                domainObject,
                                                                spreadsheet);
                                                    });

                                }

                                this.projectController.updateProject(project,
                                        this).ifPresent(
                                        updatedProject -> {

                                            updatedObjects.put(project,
                                                    updatedProject);
                                        });
                            }

                            for (IDomainObject originalObject : updatedObjects
                                    .keySet()) {

                                IDomainObject updatedObject = updatedObjects
                                        .get(originalObject);
                                this.projectTreeView.updateTreeItem(
                                        originalObject, updatedObject);

                                if (this.tabs.containsKey(originalObject)) {

                                    Tab tab = this.tabs.get(originalObject);
                                    this.tabs.remove(originalObject);

                                    tab.setText(updatedObject.getName());

                                    this.tabs.put(updatedObject, tab);
                                }
                            }
                        });
    }

    private void delete() {

        IDomainObject domainObject = this.projectTreeView
                .getSelectedDomainObject();



        if (domainObject instanceof Project) {

            this.projectController.deleteProject((Project) domainObject, this);
            // TODO: take care of removing all UI elements associated with
            // the project
        }
        else {

            Project project = this.projectTreeView.getParentProject();

            if (domainObject instanceof Survey) {

                this.surveyController.deleteSurvey((Survey) domainObject, this)
                        .ifPresent(deletedSurvey -> {

                            this.projectTreeView.remove(domainObject);
                            this.refreshProject(project);
                        });


            }
            else if (domainObject instanceof Spreadsheet) {

                Spreadsheet spreadsheet = (Spreadsheet) domainObject;

                this.spreadsheetController
                        .deleteSpreadsheet(spreadsheet, this)
                        .ifPresent(
                                deletedSpreadsheet -> {

                                    for (SpreadsheetCell spreadsheetCell : deletedSpreadsheet
                                            .getCells()) {

                                        this.spreadsheetController
                                                .deleteSpreadsheetCell(
                                                        spreadsheetCell, this);
                                    }
                                });



                this.projectTreeView.remove(domainObject);
                this.refreshProject(project);
            }

        }

    }

    private void createProject() {

        this.projectController.createProject(this).ifPresent(newProject -> {

            this.projects.add(newProject);
        });
    }

    private void createSurvey() {

        Project project = this.projectTreeView.getParentProject();

        if (project != null) {

            this.surveyController.createSurvey(this).ifPresent(survey -> {

                project.getMetadataSources().add(survey);
                this.updateProject(project);
            });
        }
    }


    private void refreshProject(Project project) {

        if (project != null) {

            this.projectController.retrieveProject(project.getId(), this)
                    .ifPresent(
                            refreshedProject -> {

                                this.projectTreeView.updateTreeItem(project,
                                        refreshedProject);
                            });
        }
    }

    private void updateProject(final Project project) {

        if (project != null) {

            this.projectController.updateProject(project, this).ifPresent(
                    updatedProject -> {

                        this.projectTreeView.updateTreeItem(project,
                                updatedProject);
                    });
        }
    }


    private void refreshView() {

        IDomainObject selectedItem = this.projectTreeView
                .getSelectedDomainObject();

        this.projectTreeView.getSelectionModel().clearSelection();
        this.projects.clear();

        this.projectController.retrieveAllProjects(this).ifPresent(
                retrievedProjects -> {

                    this.projects.addAll(retrievedProjects);
                });


        if (selectedItem != null) {

            this.projectTreeView.selectDomainObject(selectedItem);
        }
    }


    private void importSpreadsheet() {

        FileChooser fc = new FileChooser();

        LOG.debug("Attempting to import spreadsheet.");
        // TODO: IOManager should handle this!
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter(
                "CSV files (*.csv)", "*.csv");
        fc.getExtensionFilters().add(filter);
        File file = fc.showOpenDialog(this.getWindow());

        if (file != null) {

            URI uri = file.toURI();
            ITable<String> stringSpreadsheet = null;
            try {

                stringSpreadsheet = CsvSerializer.Instance().deserialize(file,
                        org.jutility.common.datatype.table.Table.class);
            }
            catch (SerializationException e) {

                LOG.error(
                        "Importing spreadsheet failed during deserialization!",
                        e);
                ExceptionDialog edlg = new ExceptionDialog(e);

                edlg.initOwner(this.getScene().getWindow());
                edlg.setTitle("Importing spreadsheet failed during deserialization!");
                edlg.setContentText("An exception occurred when "
                        + "attempting to import spreadsheet!");

                edlg.showAndWait();
            }

            if (stringSpreadsheet != null) {

                Spreadsheet spreadsheet = Spreadsheet.convert("Spreadsheet: "
                        + uri.toString(), stringSpreadsheet);
                spreadsheet.setURI(uri);



                this.spreadsheetController
                        .persistSpreadsheet(spreadsheet, this).ifPresent(
                                newSpreadsheet -> {

                                    Project project = this.projectTreeView
                                            .getParentProject();

                                    project.getDataSources()
                                            .add(newSpreadsheet);
                                    this.updateProject(project);
                                });
            }
            else {

                LOG.error("Importing spreadsheet failed during conversion: content null!");

                ExceptionDialog edlg = new ExceptionDialog(
                        new ObjectNotFoundException("Content Null"));

                edlg.initOwner(this.getScene().getWindow());
                edlg.setTitle("Importing spreadsheet failed!");
                edlg.setContentText("Content of imported spreadsheet is null!");

                edlg.showAndWait();
            }
        }
    }

    private void linkMetadata() {

        IDomainObject domainObject = this.projectTreeView
                .getSelectedDomainObject();

        if (domainObject instanceof IMetadataSource) {

            IMetadataSource metadataSource = (IMetadataSource) domainObject;

            ApplicationContext
                    .projectController()
                    .retrieveProjectContainingMetadataSource(metadataSource,
                            this)
                    .ifPresent(
                            parentProject -> {

                                DomainObjectSelectionDialog<IDataSource> dlg = new DomainObjectSelectionDialog<>(
                                        this.getWindow(), "Select Data Source",
                                        parentProject.getDataSources());

                                dlg.showAndWait()
                                        .ifPresent(
                                                dataSource -> {

                                                    if (dataSource instanceof Spreadsheet) {

                                                        Spreadsheet spreadsheet = (Spreadsheet) dataSource;

                                                        TableDescriptor descriptor = new TableDescriptor(
                                                                spreadsheet);


                                                        TableDescriptorGridPane descriptorGridPane = new TableDescriptorGridPane();

                                                        descriptorGridPane
                                                                .setTableDescriptor(descriptor);

                                                        Tab tab = new Tab(
                                                                descriptor
                                                                        .getName());
                                                        tab.setClosable(true);

                                                        tab.setContent(descriptorGridPane);
                                                        this.tabs
                                                                .put(descriptor,
                                                                        tab);
                                                        this.tabPane.getTabs()
                                                                .add(tab);

                                                        tab.setOnClosed((event) -> {

                                                            this.tabs
                                                                    .remove(descriptor);
                                                            this.refreshSelection();
                                                        });

                                                        this.refreshSelection();
                                                    }
                                                });
                            });



        }
    }

    private boolean isOpened(IDomainObject domainObject) {

        if (domainObject != null) {

            if (domainObject instanceof Project) {

                return this.projectTreeView.isOpen((Project) domainObject);
            }
            else {

                return this.tabs.containsKey(domainObject);
            }

        }

        return false;
    }

    private Window getWindow() {

        return this.getScene().getWindow();
    }
}
