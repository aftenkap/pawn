/**
 * 
 */
package raziel.pawn.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Configuration
@ComponentScan(basePackages = "raziel.pawn.ui.controllers")
public class GuiConfig {

    // Populated through component scan.
}
