package raziel.pawn.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import raziel.pawn.core.services.DataModelService;
import raziel.pawn.core.services.ProjectService;
import raziel.pawn.core.services.SurveyService;
import raziel.pawn.core.services.SpreadsheetService;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "raziel.pawn.core.services",
        includeFilters = @ComponentScan.Filter(
                value = { ProjectService.class, DataModelService.class,
                        SurveyService.class, SpreadsheetService.class },
                type = FilterType.ASSIGNABLE_TYPE))
public class CoreConfig {


    //
}
