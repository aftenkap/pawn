package raziel.pawn.persistence.repository;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.persistence.domain.metadata.Participant;
import raziel.pawn.persistence.domain.metadata.Question;
import raziel.pawn.persistence.domain.metadata.Survey;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Transactional
public interface SurveyRepository
        extends DomainObjectRepository<Survey> {


    /**
     * Attempts to find the {@link Survey} of a {@link Question}.
     * 
     * @param id
     *            the id of the question.
     * @return the {@link Survey}.
     */
    @Query(value = "SELECT s FROM Survey s JOIN s.questions q WHERE q.id = :id")
    public abstract Survey findSurveyForQuestion(@Param(value = "id") Integer id);

    /**
     * Attempts to find the Survey of an Answer.
     * 
     * @param id
     *            the id of the answer.
     * @return the {@link Survey}.
     */
    @Query(value = "SELECT s FROM Survey s JOIN s.answers a WHERE a.id = :id")
    public abstract Survey findSurveyForAnswer(@Param(value = "id") Integer id);

    /**
     * Attempts to find the {@link Survey} of a {@link Participant}.
     * 
     * @param id
     *            the id of the {@link Participant}.
     * @return the {@link Survey}.
     */
    @Query(
            value = "SELECT s FROM Survey s JOIN s.participants p WHERE p.id = :id")
    public abstract Survey findSurveyForParticipant(
            @Param(value = "id") Integer id);


    /**
     * Returns all {@link Question Questions} of the provided type for the
     * {@link Survey} with the provided id.
     * 
     * @param type
     *            the {@link Question Questions} type.
     * @param id
     *            the {@link Survey} id.
     * @return a list of all {@link Question Questions} with the desired type
     *         for the {@link Survey}.
     */
    @Query(
            value = "SELECT q FROM Survey s JOIN s.questions q WHERE s.id = :id "
                    + "AND TYPE(q) = :type ORDER BY q.order ASC")
    public <T extends Question> List<T> findQuestionsWithTypeForSurvey(@Param(
            value = "type") Class<T> type, @Param(value = "id") Integer id);
}
