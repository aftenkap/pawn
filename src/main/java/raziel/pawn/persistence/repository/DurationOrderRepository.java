package raziel.pawn.persistence.repository;



import raziel.pawn.persistence.domain.modeling.time.DurationOrder;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface DurationOrderRepository
        extends DomainObjectRepository<DurationOrder> {

    // tagging interface.
}
