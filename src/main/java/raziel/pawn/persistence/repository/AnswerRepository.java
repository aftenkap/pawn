package raziel.pawn.persistence.repository;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.metadata.Answer;
import raziel.pawn.persistence.domain.metadata.Participant;
import raziel.pawn.persistence.domain.metadata.Question;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 *
 */
public interface AnswerRepository
        extends DomainObjectRepository<Answer> {


    /**
     * Retrieves all {@link Answer Answers} of a {@link Question}.
     * 
     * @param id
     *            the id of the question.
     * @return a {@link List} of all {@link Answer Answers} to a
     *         {@link Question}.
     */
    @Query(value = "SELECT a FROM Answer a  WHERE a.question.id = :id")
    public abstract List<Answer> findAnswersForQuestion(
            @Param(value = "id") Integer id);

    /**
     * Retrieves all {@link Answer Answers} of a {@link Participant}.
     * 
     * @param id
     *            the id of the participant.
     * @return a {@link List} of all {@link Answer Answers} made by a
     *         {@link Participant}.
     */
    @Query(value = "SELECT a FROM Answer a  WHERE a.participant.id = :id")
    public abstract List<Answer> findAnswersForParticipant(
            @Param(value = "id") Integer id);


    /**
     * Retrieves the {@link Answer} of a provided participant to a provided
     * question.
     * 
     * @param questionId
     *            the question id.
     * @param participantId
     *            the participant id
     * @return the Answer of a provided participant to a provided question.
     */
    @Query(
            value = "SELECT a FROM Answer a  WHERE a.participant.id = :participantId AND a.question.id = :questionId")
    public abstract Answer findAnswerToQuestionByParticipant(@Param(
            value = "questionId") Integer questionId, @Param(
            value = "participantId") Integer participantId);
}
