package raziel.pawn.persistence.repository;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.modeling.Assertion;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface AssertionRepository
        extends DomainObjectRepository<Assertion> {


    /**
     * Retrieves a list of {@link Assertion Assertions} with the given concrete
     * type.
     * 
     * @param type
     *            the type.
     * @return a list of {@link Assertion Assertions} with the given concrete
     *         type.
     */
    @Query(value = "SELECT a FROM Assertion a WHERE TYPE(a) = :type ")
    public abstract <T extends Assertion> List<T> findAllWithType(@Param(
            value = "type") Class<? extends T> type);

    /**
     * Retrieves an {@link Assertion} with the given concrete type and the
     * provided id.
     * 
     * @param id
     * 
     * @param type
     *            the type.
     * @return a list of {@link Assertion Assertions} with the given concrete
     *         type.
     */
    @Query(value = "SELECT a FROM Assertion a "
            + "WHERE TYPE(a) = :type AND a.id = :id")
    public abstract <T extends Assertion> T findOneWithType(
            @Param(value = "id") Integer id,
            @Param(value = "type") Class<? extends T> type);
}
