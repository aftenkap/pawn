package raziel.pawn.persistence.repository;



import raziel.pawn.persistence.domain.modeling.time.SemiIntervalOrder;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface SemiIntervalOrderRepository
        extends DomainObjectRepository<SemiIntervalOrder> {

    // tagging interface.
}
