package raziel.pawn.persistence.repository;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.metadata.Question;
import raziel.pawn.persistence.domain.metadata.Survey;
import raziel.pawn.persistence.domain.modeling.Assertion;
import raziel.pawn.persistence.domain.modeling.DataModel;
import raziel.pawn.persistence.domain.modeling.Term;
import raziel.pawn.persistence.domain.modeling.time.DurationOrder;
import raziel.pawn.persistence.domain.modeling.time.SemiIntervalOrder;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public interface DataModelRepository
        extends DomainObjectRepository<DataModel> {

    /**
     * Attempts to find the {@link DataModel} of a {@link Term}.
     * 
     * @param id
     *            the id of the term.
     * @return the {@link DataModel}.
     */
    @Query(value = "SELECT d FROM DataModel d JOIN d.terms t WHERE t.id = :id")
    public abstract DataModel findDataModelForTerm(
            @Param(value = "id") Integer id);

    /**
     * Attempts to find the {@link DataModel} of an {@link Assertion}.
     * 
     * @param id
     *            the id of the assertion.
     * @return the {@link DataModel}.
     */
    @Query(
            value = "SELECT d FROM DataModel d JOIN d.assertions a WHERE a.id = :id")
    public abstract DataModel findDataModelForAssertion(
            @Param(value = "id") Integer id);

    /**
     * Attempts to find the {@link DataModel} of a {@link DurationOrder}.
     * 
     * @param id
     *            the id of the durationOrder.
     * @return the {@link DataModel}.
     */
    @Query(
            value = "SELECT d FROM DataModel d JOIN d.durationOrders a WHERE a.id = :id")
    public abstract DataModel findDataModelForDurationOrder(
            @Param(value = "id") Integer id);

    /**
     * Attempts to find the {@link DataModel} of a {@link SemiIntervalOrder}.
     * 
     * @param id
     *            the id of the semiIntervalOrder.
     * @return the {@link DataModel}.
     */
    @Query(
            value = "SELECT d FROM DataModel d JOIN d.semiIntervalOrders s WHERE s.id = :id")
    public abstract DataModel findDataModelForSemiIntervalOrder(@Param(
            value = "id") Integer id);



    /**
     * Returns all {@link Question Questions} of the provided type for the
     * {@link Survey} with the provided id.
     * 
     * @param type
     *            the {@link Question Questions} type.
     * @param id
     *            the {@link Survey} id.
     * @return a list of all {@link Question Questions} with the desired type
     *         for the {@link Survey}.
     */
    @Query(value = "SELECT d FROM DataModel d JOIN d.terms t WHERE d.id = :id "
            + "AND TYPE(t) = :type")
    public <T extends Term<?>> List<T> findTermsWithTypeForDataModel(@Param(
            value = "type") Class<T> type, @Param(value = "id") Integer id);


    /**
     * Returns all {@link Question Questions} of the provided type for the
     * {@link Survey} with the provided id.
     * 
     * @param type
     *            the {@link Question Questions} type.
     * @param id
     *            the {@link Survey} id.
     * @return a list of all {@link Question Questions} with the desired type
     *         for the {@link Survey}.
     */
    @Query(
            value = "SELECT d FROM DataModel d JOIN d.assertions a WHERE d.id = :id "
                    + "AND TYPE(a) = :type")
    public <T extends Assertion> List<T> findAssertionsWithTypeForDataModel(
            @Param(value = "type") Class<T> type,
            @Param(value = "id") Integer id);
}
