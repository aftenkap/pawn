package raziel.pawn.persistence.repository;



import raziel.pawn.persistence.domain.data.SpreadsheetCell;



/**
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.3
 *
 */
public interface SpreadsheetCellRepository
        extends DomainObjectRepository<SpreadsheetCell> {

    // 
}
