package raziel.pawn.persistence.repository;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.metadata.Question;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface QuestionRepository
        extends DomainObjectRepository<Question> {


    /**
     * Retrieves a list of {@link Question Questions} with the given concrete
     * type.
     * 
     * @param type
     *            the type
     * @return a list of {@link Question Questions} with the given concrete
     *         type.
     */
    @Query(
            value = "SELECT q FROM Question q WHERE TYPE(q) = :type ORDER BY q.order ASC")
    public abstract <T extends Question> List<T> findAllWithType(@Param(
            value = "type") Class<? extends T> type);

    /**
     * Retrieves the {@link Question} with provided id and the given concrete
     * type.
     * 
     * @param id
     *            the id.
     * @param type
     *            the type.
     * @return the {@link Question} with provided id and the given concrete
     *         type.
     */
    @Query(
            value = "SELECT q FROM Question q WHERE TYPE(q) = :type AND q.id = :id")
    public abstract <T extends Question> T findOneWithType(
            @Param(value = "id") Integer id,
            @Param(value = "type") Class<? extends T> type);
}
