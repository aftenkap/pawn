/**
 * 
 */
package raziel.pawn.persistence.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import raziel.pawn.persistence.domain.DomainObject;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 * @param <T>
 *            the object type.
 */
@NoRepositoryBean
public interface DomainObjectRepository<T extends DomainObject>
        extends JpaRepository<T, Integer> {

    /**
     * Updates the lightweight domain object.
     * 
     * @param domainObject
     *            the domain object to update.
     * @return the updated domain object.
     */
//    public abstract <S extends T> S update(S domainObject);

    /**
     * Updates the lightweight domain object and immediately flushes the
     * changes.
     * 
     * @param domainObject
     *            the domain object to update.
     * @return the updated domain object.
     */
//    public abstract <S extends T> S updateAndFlush(S domainObject);
}
