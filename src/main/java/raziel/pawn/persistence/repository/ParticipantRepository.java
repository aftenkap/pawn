package raziel.pawn.persistence.repository;



import raziel.pawn.persistence.domain.metadata.Participant;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface ParticipantRepository
        extends DomainObjectRepository<Participant> {

    // tagging interface
}
