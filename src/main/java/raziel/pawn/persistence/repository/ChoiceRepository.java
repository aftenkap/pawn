package raziel.pawn.persistence.repository;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.metadata.Choice;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface ChoiceRepository
        extends DomainObjectRepository<Choice> {

    /**
     * Returns a list of Choices with names similar to the name provided.
     * 
     * @param name
     *            the name to search for.
     * @return a list of choices similar to the provided name.
     */
    @Query(value = "SELECT c FROM Choice c WHERE c.name LIKE '%:name%'")
    public abstract List<Choice> findAllByName(
            @Param(value = "name") String name);

    /**
     * Returns the {@link Choice} with the provided name.
     * 
     * @param name
     *            the name.
     * @return the {@link Choice} with the provided name or {@code null}.
     */
    @Query(value = "SELECT c FROM Choice c WHERE c.name = ':name'")
    public abstract Choice findOneByName(@Param(value = "name") String name);

}
