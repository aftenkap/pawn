package raziel.pawn.persistence.repository;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.data.Spreadsheet;



/**
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.0
 */
public interface SpreadsheetRepository
        extends DomainObjectRepository<Spreadsheet> {


    /**
     * Attempts to find the Spreadsheet of a Spreadsheet Cell.
     * 
     * @param id
     *            the id of the spreadsheet cell.
     * @return the spreadsheet.
     */
    @Query(
            value = "SELECT t FROM Spreadsheet t JOIN t.cells c WHERE c.id = :id")
    public Spreadsheet findSpreadsheetForSpreadsheetCell(
            @Param(value = "id") Integer id);
}
