package raziel.pawn.persistence.repository;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.modeling.IInstance;
import raziel.pawn.persistence.domain.modeling.terminology.Concept;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface ConceptRepository
        extends DomainObjectRepository<Concept<?>> {


    /**
     * Retrieves a list of {@link Concept Concepts} with the given concrete
     * type.
     * 
     * @param type
     *            the type.
     * @return a list of {@link Concept Concepts} with the given concrete type.
     */
    @Query(value = "SELECT c FROM Concept c WHERE TYPE(c) = :type ")
    public abstract <T extends Concept<?>> List<T> findAllWithType(@Param(
            value = "type") Class<? extends T> type);

    /**
     * Retrieves a{@link Concept} with the given concrete type and the provided
     * id.
     * 
     * @param id
     * 
     * @param type
     *            the type.
     * @return a list of {@link Concept Concepts} with the given concrete type.
     */
    @Query(
            value = "SELECT c FROM Concept c WHERE TYPE(c) = :type AND c.id = :id")
    public abstract <T extends Concept<?>> T findOneWithType(@Param(
            value = "id") Integer id,
            @Param(value = "type") Class<? extends T> type);

    /**
     * Retrieves a list of the ancestors of the concept with the provided id.
     * 
     * @param id
     *            the child id.
     * @return A list of ancestors of the concept with the provided id.
     */
    @Query(
            value = "SELECT c FROM Concept c JOIN c.descendants d WHERE d.id = :id")
    public abstract List<Concept<?>> findAllAncestors(
            @Param(value = "id") Integer id);

    /**
     * Retrieves a list of the ancestors of the concept with the provided id
     * that have the provided concrete type.
     * 
     * @param id
     *            the child id.
     * @param type
     *            the type.
     * @return A list of ancestors of the concept with the provided id that have
     *         the provided concrete type.
     */
    @Query(
            value = "SELECT c FROM Concept c JOIN c.descendants d WHERE d.id = :id AND TYPE(c) = :type")
    public abstract <T extends Concept<?>> List<T> findAllAncestorsWithType(
            @Param(value = "id") Integer id,
            @Param(value = "type") Class<? extends T> type);


    /**
     * Attempts to find the {@link Concept Concepts} an {@link IInstance} is
     * derived from.
     * 
     * @param id
     *            the id of the question.
     * @return the {@link Concept}.
     */
    @Query(
            value = "SELECT c FROM Concept c JOIN c.instances i WHERE i.id = :id")
    public abstract List<Concept<?>> findConceptsForInstance(@Param(
            value = "id") Integer id);
}
