package raziel.pawn.persistence.repository;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.modeling.assertions.Relationship;
import raziel.pawn.persistence.domain.modeling.terminology.Role;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public interface RoleRepository
        extends DomainObjectRepository<Role<?, ?, ?>> {


    /**
     * Retrieves a list of {@link Role Relations} with the given concrete
     * type.
     * 
     * @param type
     *            the type.
     * @return a list of {@link Role Relations} with the given concrete
     *         type.
     */
    @Query(value = "SELECT r FROM Role r WHERE TYPE(r) = :type ")
    public abstract <T extends Role<?, ?, ?>> List<T> findAllWithType(
            @Param(value = "type") Class<? extends T> type);

    /**
     * Retrieves a {@link Role} with the given concrete type and the
     * provided id.
     * 
     * @param id
     *            the id.
     * @param type
     *            the type.
     * @return a {@link Role} with the given concrete type and the provided
     *         id.
     */
    @Query(value = "SELECT r FROM Role r "
            + "WHERE TYPE(r) = :type AND r.id = :id")
    public abstract <T extends Role<?, ?, ?>> T findOneWithType(@Param(
            value = "id") Integer id,
            @Param(value = "type") Class<? extends T> type);

    /**
     * Retrieves a list of the ancestors of the concept with the provided id.
     * 
     * @param id
     *            the child id.
     * @return A list of ancestors of the concept with the provided id.
     */
    @Query(value = "SELECT r FROM Role r JOIN r.descendants d "
            + "WHERE d.id = :id")
    public abstract List<Role<?, ?, ?>> findAllAncestors(@Param(
            value = "id") Integer id);

    /**
     * Retrieves a list of the ancestors of the concept with the provided id
     * that have the provided concrete type.
     * 
     * @param id
     *            the child id.
     * @param type
     *            the type.
     * @return A list of ancestors of the concept with the provided id that have
     *         the provided concrete type.
     */
    @Query(value = "SELECT r FROM Role r JOIN r.descendants d "
            + "WHERE d.id = :id AND TYPE(r) = :type")
    public abstract <T extends Role<?, ?, ?>> List<T> findAllAncestorsWithType(
            @Param(value = "id") Integer id,
            @Param(value = "type") Class<? extends T> type);

    /**
     * Attempts to find the {@link Role Relations} a {@link Relationship} is
     * derived from.
     * 
     * @param id
     *            the id of the question.
     * @return the {@link Role}.
     */
    @Query(
            value = "SELECT r FROM Role r JOIN r.instances i WHERE i.id = :id")
    public abstract List<Role<?, ?, ?>> findRelationForRelationship(@Param(
            value = "id") Integer id);
}
