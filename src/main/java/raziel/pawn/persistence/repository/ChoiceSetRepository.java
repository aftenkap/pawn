package raziel.pawn.persistence.repository;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import raziel.pawn.persistence.domain.metadata.ChoiceSet;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface ChoiceSetRepository
        extends DomainObjectRepository<ChoiceSet> {

    /**
     * Attempts to retrieve the {@link ChoiceSet} with the provided name.
     * 
     * @param name
     *            the name.
     * @return the {@link ChoiceSet} with the provided name or {@code null}.
     */
    @Query(value = "SELECT c FROM ChoiceSet c WHERE c.name = :name")
    public ChoiceSet findOneByName(@Param(value = "name") String name);
}
