package raziel.pawn.persistence.repository;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.persistence.domain.Project;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Transactional
public interface ProjectRepository
        extends DomainObjectRepository<Project> {

    /**
     * Attempts to find the Project of a DataSource.
     * 
     * @param id
     *            the id of the dataSource.
     * @return the project.
     */
    @Query(
            value = "SELECT p FROM Project p JOIN p.dataSources d WHERE d.id = :id")
    public Project findProjectForDataSource(@Param(value = "id") Integer id);

    /**
     * Attempts to find the Project of a MetadataSource.
     * 
     * @param id
     *            the id of the metadataSource.
     * @return the project.
     */
    @Query(
            value = "SELECT p FROM Project p JOIN p.metadataSources m WHERE m.id = :id")
    public Project findProjectForMetadataSource(@Param(value = "id") Integer id);

    /**
     * Attempts to find the Project of a DataModel.
     * 
     * @param id
     *            the id of the data model.
     * @return the project.
     */
    @Query(
            value = "SELECT p FROM Project p JOIN p.dataModel m WHERE m.id = :id")
    public Project findProjectForDataModel(@Param(value = "id") Integer id);
}
