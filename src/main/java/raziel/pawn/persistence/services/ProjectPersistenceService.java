package raziel.pawn.persistence.services;


import java.util.List;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.ProjectDetails;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public interface ProjectPersistenceService {


    /**
     * Creates a project.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public abstract ProjectDetails createProject(ProjectDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all projects.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public abstract List<ProjectDetails> retrieveAllProjects()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single project.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public abstract ProjectDetails retrieveProject(int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Retrieves the project containing the provided data source.
     * 
     * @param id
     *            the id of the data source.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public abstract ProjectDetails retrieveProjectForDataSource(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves the project containing the provided metadata source.
     * 
     * @param id
     *            the id of the metadata source.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    ProjectDetails retrieveProjectForMetadataSource(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves the project containing the provided data model.
     * 
     * @param id
     *            the id of the data model.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    ProjectDetails retrieveProjectForDataModel(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Updates a project.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public abstract ProjectDetails updateProject(ProjectDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Deletes a project.
     * 
     * @param id
     *            the id of the object to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public abstract ProjectDetails deleteProject(int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

}
