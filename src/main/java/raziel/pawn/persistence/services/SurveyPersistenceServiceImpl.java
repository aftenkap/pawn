package raziel.pawn.persistence.services;



import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.metadata.AnswerDetails;
import raziel.pawn.dto.metadata.ChoiceDetails;
import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.ChoiceSetDetails;
import raziel.pawn.dto.metadata.ParticipantDTO;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;
import raziel.pawn.dto.metadata.SurveyDetails;
import raziel.pawn.dto.metadata.TextQuestionDetails;
import raziel.pawn.persistence.domain.metadata.Answer;
import raziel.pawn.persistence.domain.metadata.Choice;
import raziel.pawn.persistence.domain.metadata.ChoiceQuestion;
import raziel.pawn.persistence.domain.metadata.ChoiceSet;
import raziel.pawn.persistence.domain.metadata.IAnswer;
import raziel.pawn.persistence.domain.metadata.IQuestion;
import raziel.pawn.persistence.domain.metadata.Participant;
import raziel.pawn.persistence.domain.metadata.RankingQuestion;
import raziel.pawn.persistence.domain.metadata.Survey;
import raziel.pawn.persistence.domain.metadata.TextQuestion;
import raziel.pawn.persistence.repository.AnswerRepository;
import raziel.pawn.persistence.repository.ChoiceRepository;
import raziel.pawn.persistence.repository.ChoiceSetRepository;
import raziel.pawn.persistence.repository.ParticipantRepository;
import raziel.pawn.persistence.repository.QuestionRepository;
import raziel.pawn.persistence.repository.SurveyRepository;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Service
@Transactional
public class SurveyPersistenceServiceImpl
        implements SurveyPersistenceService {

    private static Logger               LOG = LoggerFactory
                                                    .getLogger(SurveyPersistenceServiceImpl.class);

    private final SurveyRepository      surveyRepository;
    private final QuestionRepository    questionRepository;
    private final ParticipantRepository participantRepository;
    private final AnswerRepository      answerRepository;
    private final ChoiceRepository      choiceRepository;
    private final ChoiceSetRepository   choiceSetRepository;

    /**
     * Creates a new instance of the {@link SurveyPersistenceServiceImpl} class.
     * 
     * @param surveyRepository
     *            the {@link SurveyRepository}.
     * @param questionRepository
     *            the {@link QuestionRepository}.
     * @param participantRepository
     *            the {@link ParticipantRepository}.
     * @param answerRepository
     *            the {@link AnswerRepository}.
     * @param choiceRepository
     *            the {@link ChoiceRepository}.
     * @param choiceSetRepository
     *            the {@link ChoiceSetRepository}.
     */
    @Autowired
    public SurveyPersistenceServiceImpl(
            final SurveyRepository surveyRepository,
            final QuestionRepository questionRepository,
            final ParticipantRepository participantRepository,
            final AnswerRepository answerRepository,
            final ChoiceRepository choiceRepository,
            final ChoiceSetRepository choiceSetRepository) {

        this.surveyRepository = surveyRepository;
        this.questionRepository = questionRepository;
        this.participantRepository = participantRepository;
        this.answerRepository = answerRepository;
        this.choiceRepository = choiceRepository;
        this.choiceSetRepository = choiceSetRepository;
    }


    @Override
    public SurveyDetails createSurvey(SurveyDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Survey.");
        try {
            Integer id = event.getId();

            if (id != null && this.surveyRepository.exists(id)) {

                LOG.warn("Cannot create Survey: Survey with id " + id
                        + " already exists!");
                throw new RequestFailedException("Cannot create Survey: "
                        + "Survey with id " + id + " already exists!");
            }
            Survey survey = Survey.fromDetails(event);


            Survey createdSurvey = this.surveyRepository.saveAndFlush(survey);

            LOG.debug("Survey created: " + createdSurvey);
            return createdSurvey.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create Survey: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Survey: Exception thrown!", e);
        }
    }

    @Override
    public List<SurveyDetails> retrieveAllSurveys()
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to retrieve all Surveys.");
        try {

            List<SurveyDetails> generatedDetails = new ArrayList<>();
            for (Survey survey : this.surveyRepository.findAll()) {

                generatedDetails.add(survey.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Surveys failed: No Surveys found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Surveys failed: No Surveys found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " Surveys.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Surveys failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Surveys failed: Exception thrown!", e);
        }
    }

    @Override
    public SurveyDetails retrieveSurvey(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Survey with id " + id + ".");
        try {

            Survey survey = null;
            survey = this.surveyRepository.findOne(id);

            if (survey == null) {

                LOG.info("Could not retrieve Survey: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Survey: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning Survey " + survey);
            return survey.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Survey: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Survey: Exception thrown!", e);
        }
    }

    @Override
    public SurveyDetails updateSurvey(SurveyDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Survey with id " + event.getId() + ".");
        try {

            if (!this.surveyRepository.exists(event.getId())) {

                LOG.info("Could not update Survey: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Survey: No object with id "
                                + event.getId() + " found!");
            }

            Survey survey = Survey.fromDetails(event);


            Survey updatedSurvey = this.surveyRepository.saveAndFlush(survey);

            return updatedSurvey.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update Survey: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update Survey: Exception thrown!", e);
        }
    }

    @Override
    public SurveyDetails deleteSurvey(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Survey with id " + id + ".");
        try {

            Survey survey = this.surveyRepository.findOne(id);

            if (survey == null) {

                LOG.info("Could not delete Survey: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Survey: No object with id " + id
                                + " found!");
            }

            this.surveyRepository.delete(id);

            return survey.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Survey: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Survey: Exception thrown!", e);
        }
    }

    @Override
    public SurveyDetails retrieveSurveyForQuestion(final int id)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to retrieve Survey for Question with id " + id
                + ".");
        try {

            Survey survey = this.surveyRepository.findSurveyForQuestion(id);


            if (survey == null) {


                LOG.info("Could not retrieve Survey for Question: No object"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Survey for Question: No object"
                                + " found!");
            }
            return survey.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Survey: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Survey: Exception thrown!", e);
        }
    }

    @Override
    public SurveyDetails retrieveSurveyForAnswer(final int id)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to retrieve Survey for Answer with id " + id
                + ".");
        try {

            Survey survey = this.surveyRepository.findSurveyForAnswer(id);


            if (survey == null) {


                LOG.info("Could not retrieve Survey for Answer: No object"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Survey for Answer: No object"
                                + " found!");
            }
            return survey.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Survey: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Survey: Exception thrown!", e);
        }
    }

    @Override
    public SurveyDetails retrieveSurveyForParticipant(final int id)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to retrieve Survey for Participant with id " + id
                + ".");
        try {

            Survey survey = this.surveyRepository.findSurveyForParticipant(id);


            if (survey == null) {


                LOG.info("Could not retrieve Survey for Participant: No object"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Survey for Participant: No object"
                                + " found!");
            }
            return survey.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Survey: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Survey: Exception thrown!", e);
        }
    }

    @Override
    public TextQuestionDetails createTextQuestion(TextQuestionDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create TextQuestion.");
        try {
            Integer id = event.getId();

            if (id != null && this.questionRepository.exists(id)) {

                LOG.warn("Cannot create TextQuestion: TextQuestion with id "
                        + id + " already exists!");
                throw new RequestFailedException("Cannot create TextQuestion: "
                        + "TextQuestion with id " + id + " already exists!");
            }
            TextQuestion question = TextQuestion.fromDetails(event);


            TextQuestion createdQuestion = this.questionRepository
                    .saveAndFlush(question);

            LOG.debug("TextQuestion created: " + createdQuestion);
            return createdQuestion.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create TextQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create TextQuestion: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceQuestionDetails createChoiceQuestion(
            ChoiceQuestionDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create ChoiceQuestion.");
        try {
            Integer id = event.getId();

            if (id != null && this.questionRepository.exists(id)) {

                LOG.warn("Cannot create ChoiceQuestion: ChoiceQuestion with id "
                        + id + " already exists!");
                throw new RequestFailedException(
                        "Cannot create ChoiceQuestion: "
                                + "ChoiceQuestion with id " + id
                                + " already exists!");
            }
            ChoiceQuestion question = ChoiceQuestion.fromDetails(event);


            ChoiceQuestion createdQuestion = this.questionRepository
                    .saveAndFlush(question);

            LOG.debug("ChoiceQuestion created: " + createdQuestion);
            return createdQuestion.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create ChoiceQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create ChoiceQuestion: Exception thrown!", e);
        }
    }

    @Override
    public RankingQuestionDetails createRankingQuestion(
            RankingQuestionDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create RankingQuestion.");
        try {
            Integer id = event.getId();

            if (id != null && this.questionRepository.exists(id)) {

                LOG.warn("Cannot create RankingQuestion: RankingQuestion with id "
                        + id + " already exists!");
                throw new RequestFailedException(
                        "Cannot create RankingQuestion: "
                                + "RankingQuestion with id " + id
                                + " already exists!");
            }
            RankingQuestion question = RankingQuestion.fromDetails(event);


            RankingQuestion createdQuestion = this.questionRepository
                    .saveAndFlush(question);

            LOG.debug("RankingQuestion created: " + createdQuestion);
            return createdQuestion.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create RankingQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create RankingQuestion: Exception thrown!", e);
        }
    }


    @Override
    public List<QuestionDetails> retrieveAllQuestions()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Questions.");
        try {

            List<QuestionDetails> generatedDetails = new ArrayList<>();
            for (IQuestion question : this.questionRepository.findAll()) {

                generatedDetails.add(question.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Questions failed: No Questions found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Questions failed: No Questions found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " Questions.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Questions failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Questions failed: Exception thrown!", e);
        }
    }


    @Override
    public QuestionDetails retrieveQuestion(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Question with id " + id + ".");
        try {

            IQuestion question = null;
            question = this.questionRepository.findOne(id);

            if (question == null) {

                LOG.info("Could not retrieve Question: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Question: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning Question " + question);
            return question.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Question: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Question: Exception thrown!", e);
        }
    }

    @Override
    public TextQuestionDetails retrieveTextQuestion(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve TextQuestion with id " + id + ".");
        try {

            TextQuestion question = null;
            question = this.questionRepository.findOneWithType(id,
                    TextQuestion.class);

            if (question == null) {

                LOG.info("Could not retrieve TextQuestion: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve TextQuestion: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning TextQuestion " + question);
            return question.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve TextQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve TextQuestion: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceQuestionDetails retrieveChoiceQuestion(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve ChoiceQuestion with id " + id + ".");
        try {

            ChoiceQuestion question = null;
            question = this.questionRepository.findOneWithType(id,
                    ChoiceQuestion.class);

            if (question == null) {

                LOG.info("Could not retrieve ChoiceQuestion: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve ChoiceQuestion: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning ChoiceQuestion " + question);
            return question.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve ChoiceQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve ChoiceQuestion: Exception thrown!", e);
        }
    }

    @Override
    public RankingQuestionDetails retrieveRankingQuestion(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve RankingQuestion with id " + id + ".");
        try {

            RankingQuestion question = null;
            question = this.questionRepository.findOneWithType(id,
                    RankingQuestion.class);

            if (question == null) {

                LOG.info("Could not retrieve RankingQuestion: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve RankingQuestion: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning RankingQuestion " + question);
            return question.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve RankingQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve RankingQuestion: Exception thrown!", e);
        }
    }

    @Override
    public TextQuestionDetails updateTextQuestion(TextQuestionDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update TextQuestion with id " + event.getId()
                + ".");
        try {

            if (!this.questionRepository.exists(event.getId())) {

                LOG.info("Could not update TextQuestion: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update TextQuestion: No object with id "
                                + event.getId() + " found!");
            }

            TextQuestion question = TextQuestion.fromDetails(event);


            TextQuestion updatedTextQuestion = this.questionRepository
                    .saveAndFlush(question);

            return updatedTextQuestion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update TextQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update TextQuestion: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceQuestionDetails updateChoiceQuestion(
            ChoiceQuestionDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update ChoiceQuestion with id "
                + event.getId() + ".");
        try {

            if (!this.questionRepository.exists(event.getId())) {

                LOG.info("Could not update ChoiceQuestion: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update ChoiceQuestion: No object with id "
                                + event.getId() + " found!");
            }

            ChoiceQuestion question = ChoiceQuestion.fromDetails(event);


            ChoiceQuestion updatedChoiceQuestion = this.questionRepository
                    .saveAndFlush(question);

            return updatedChoiceQuestion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update ChoiceQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update ChoiceQuestion: Exception thrown!", e);
        }
    }

    @Override
    public RankingQuestionDetails updateRankingQuestion(
            RankingQuestionDetails event)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to update RankingQuestion with id "
                + event.getId() + ".");
        try {

            if (!this.questionRepository.exists(event.getId())) {

                LOG.info("Could not update RankingQuestion: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update RankingQuestion: No object with id "
                                + event.getId() + " found!");
            }

            RankingQuestion question = RankingQuestion.fromDetails(event);


            RankingQuestion updatedRankingQuestion = this.questionRepository
                    .saveAndFlush(question);

            return updatedRankingQuestion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update RankingQuestion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update RankingQuestion: Exception thrown!", e);
        }
    }

    @Override
    public QuestionDetails deleteQuestion(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Question with id " + id + ".");
        try {

            IQuestion question = this.questionRepository.findOne(id);

            if (question == null) {

                LOG.info("Could not delete Question: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Question: No object with id " + id
                                + " found!");
            }

            this.questionRepository.delete(id);

            return question.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Question: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Question: Exception thrown!", e);
        }
    }

    @Override
    public AnswerDetails createAnswer(AnswerDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Answer.");
        try {
            Integer id = event.getId();

            if (id != null && this.answerRepository.exists(id)) {

                LOG.warn("Cannot create Answer: Answer with id " + id
                        + " already exists!");
                throw new RequestFailedException("Cannot create Answer: "
                        + "Answer with id " + id + " already exists!");
            }
            Answer answer = Answer.fromDetails(event);

            Answer createdAnswer = this.answerRepository.saveAndFlush(answer);

            LOG.debug("Answer created: " + createdAnswer);
            return createdAnswer.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create Answer: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Answer: Exception thrown!", e);
        }
    }


    @Override
    public List<AnswerDetails> retrieveAllAnswers()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Answers.");
        try {

            List<AnswerDetails> generatedDetails = new ArrayList<>();
            for (IAnswer answer : this.answerRepository.findAll()) {

                generatedDetails.add(answer.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Answers failed: No Answers found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Answers failed: No Answers found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " Answers.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Answers failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Answers failed: Exception thrown!", e);
        }
    }

    @Override
    public AnswerDetails retrieveAnswer(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Answer with id " + id + ".");
        try {

            IAnswer answer = null;
            answer = this.answerRepository.findOne(id);

            if (answer == null) {

                LOG.info("Could not retrieve Answer: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Answer: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning Answer " + answer);
            return answer.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Answer: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Answer: Exception thrown!", e);
        }
    }

    @Override
    public AnswerDetails updateAnswer(AnswerDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Answer with id " + event.getId() + ".");
        try {

            if (!this.answerRepository.exists(event.getId())) {

                LOG.info("Could not update Answer: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Answer: No object with id "
                                + event.getId() + " found!");
            }

            Answer answer = Answer.fromDetails(event);


            Answer updatedAnswer = this.answerRepository.saveAndFlush(answer);

            return updatedAnswer.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update Answer: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update Answer: Exception thrown!", e);
        }

    }

    @Override
    public AnswerDetails deleteAnswer(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Answer with id " + id + ".");
        try {

            Answer answer = this.answerRepository.findOne(id);

            if (answer == null) {

                LOG.info("Could not delete Answer: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Answer: No object with id " + id
                                + " found!");
            }

            this.answerRepository.delete(id);

            return answer.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Answer: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Answer: Exception thrown!", e);
        }
    }


    @Override
    public List<AnswerDetails> retrieveAllAnswersForQuestion(final int id)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to retrieve Answers for Question with id " + id
                + ".");
        try {

            List<Answer> answers = this.answerRepository
                    .findAnswersForQuestion(id);

            if (answers == null || answers.isEmpty()) {


                LOG.info("Could not retrieve Answers for Question: No objects"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Answers for Question: No object"
                                + " found!");
            }

            List<AnswerDetails> result = new ArrayList<>(answers.size());

            for (Answer answer : answers) {

                result.add(answer.toDTO());
            }

            return result;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn(
                    "Could not retrieve Answers for Question: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Could not retrieve Answers for Question: Exception thrown!",
                    e);
        }
    }


    @Override
    public List<AnswerDetails> retrieveAllAnswersForParticipant(final int id)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to retrieve Answers for Participant with id "
                + id + ".");
        try {

            List<Answer> answers = this.answerRepository
                    .findAnswersForParticipant(id);

            if (answers == null || answers.isEmpty()) {


                LOG.info("Could not retrieve Answers for Participant: No objects"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Answers for Participant: No object"
                                + " found!");
            }

            List<AnswerDetails> result = new ArrayList<>(answers.size());

            for (Answer answer : answers) {

                result.add(answer.toDTO());
            }

            return result;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn(
                    "Could not retrieve Answers for Participant: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Could not retrieve Answers for Participant: Exception thrown!",
                    e);
        }
    }

    @Override
    public AnswerDetails retrieveAnswerToQuestionByParticipant(int questionId,
            int participantId)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        LOG.debug("Attempting to retrieve Answer to Question with id "
                + questionId + " by Participant with id " + participantId + ".");
        try {

            Answer answer = this.answerRepository
                    .findAnswerToQuestionByParticipant(questionId,
                            participantId);

            if (answer == null) {


                LOG.info("Could not retrieve Answer to Question with id "
                        + questionId + " by Participant with id "
                        + participantId + ": No objects found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Answer to Question with id "
                                + questionId + " by Participant with id "
                                + participantId + ": No objects found!");
            }

            return answer.toDTO();
        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Answer to Question with id "
                    + questionId + " by Participant with id " + participantId
                    + ": Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Answer to Question with id "
                            + questionId + " by Participant with id "
                            + participantId + ": Exception thrown!", e);
        }
    }


    @Override
    public ParticipantDTO createParticipant(ParticipantDTO event)
            throws RequestFailedException {


        LOG.debug("Attempting to create Participant.");
        try {
            Integer id = event.getId();

            if (id != null && this.participantRepository.exists(id)) {

                LOG.warn("Cannot create Participant: Participant with id " + id
                        + " already exists!");
                throw new RequestFailedException("Cannot create Participant: "
                        + "Participant with id " + id + " already exists!");
            }
            Participant participant = Participant.fromDetails(event);


            Participant createdParticipant = this.participantRepository
                    .saveAndFlush(participant);

            LOG.debug("Participant created: " + createdParticipant);
            return createdParticipant.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create Participant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Participant: Exception thrown!", e);
        }
    }

    @Override
    public List<ParticipantDTO> retrieveAllParticipants()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Participants.");
        try {

            List<ParticipantDTO> generatedDetails = new ArrayList<>();
            for (Participant participant : this.participantRepository.findAll()) {

                generatedDetails.add(participant.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Participants failed: No Participants found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Participants failed: No Participants found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " Participants.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Participants failed: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Retrieval of all Participants failed: Exception thrown!",
                    e);
        }
    }

    @Override
    public ParticipantDTO retrieveParticipant(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Participant with id " + id + ".");
        try {

            Participant participant = null;
            participant = this.participantRepository.findOne(id);

            if (participant == null) {

                LOG.info("Could not retrieve Participant: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Participant: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning Participant " + participant);
            return participant.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Participant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Participant: Exception thrown!", e);
        }
    }

    @Override
    @Transactional(readOnly = false)
    public ParticipantDTO updateParticipant(ParticipantDTO event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Participant with id " + event.getId()
                + ".");
        try {

            if (!this.participantRepository.exists(event.getId())) {

                LOG.info("Could not update Participant: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Participant: No object with id "
                                + event.getId() + " found!");
            }

            Participant participant = Participant.fromDetails(event);


            Participant updatedParticipant = this.participantRepository
                    .saveAndFlush(participant);

            return updatedParticipant.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update Participant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update Participant: Exception thrown!", e);
        }

    }

    @Override
    public ParticipantDTO deleteParticipant(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Participant with id " + id + ".");
        try {

            Participant participant = this.participantRepository.findOne(id);

            if (participant == null) {

                LOG.info("Could not delete Participant: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Participant: No object with id " + id
                                + " found!");
            }

            this.participantRepository.delete(id);

            return participant.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Participant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Participant: Exception thrown!", e);
        }
    }


    @Override
    public ChoiceDetails createChoice(ChoiceDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Choice.");
        try {
            Integer id = event.getId();

            if (id != null && this.choiceRepository.exists(id)) {

                LOG.warn("Cannot create Choice: Choice with id " + id
                        + " already exists!");
                throw new RequestFailedException("Cannot create Choice: "
                        + "Choice with id " + id + " already exists!");
            }
            Choice choice = Choice.fromDetails(event);


            Choice createdChoice = this.choiceRepository.saveAndFlush(choice);

            LOG.debug("Choice created: " + createdChoice);
            return createdChoice.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create Choice: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Choice: Exception thrown!", e);
        }
    }

    @Override
    public List<ChoiceDetails> retrieveAllChoices()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Choices.");
        try {

            List<ChoiceDetails> generatedDetails = new ArrayList<>();
            for (Choice choice : this.choiceRepository.findAll()) {

                generatedDetails.add(choice.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Choices failed: No Choices found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Choices failed: No Choices found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " Choices.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Choices failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Choices failed: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceDetails retrieveChoice(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Choice with id " + id + ".");
        try {

            Choice choice = null;
            choice = this.choiceRepository.findOne(id);

            if (choice == null) {

                LOG.info("Could not retrieve Choice: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Choice: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning Choice " + choice);
            return choice.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Choice: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Choice: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceDetails retrieveChoiceByName(String name)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Choice with name " + name + ".");
        try {

            Choice choice = null;
            choice = this.choiceRepository.findOneByName(name);

            if (choice == null) {

                LOG.info("Could not retrieve Choice: No object with name "
                        + name + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Choice: No object with name "
                                + name + " found!");
            }
            LOG.debug("Returning Choice " + choice);
            return choice.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Choice: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Choice: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceDetails updateChoice(ChoiceDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Choice with id " + event.getId() + ".");
        try {

            if (!this.choiceRepository.exists(event.getId())) {

                LOG.info("Could not update Choice: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Choice: No object with id "
                                + event.getId() + " found!");
            }

            Choice choice = Choice.fromDetails(event);


            Choice updatedChoice = this.choiceRepository.saveAndFlush(choice);

            return updatedChoice.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update Choice: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update Choice: Exception thrown!", e);
        }

    }

    @Override
    public ChoiceDetails deleteChoice(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Choice with id " + id + ".");
        try {

            Choice choice = this.choiceRepository.findOne(id);

            if (choice == null) {

                LOG.info("Could not delete Choice: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Choice: No object with id " + id
                                + " found!");
            }

            this.choiceRepository.delete(id);

            return choice.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Choice: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Choice: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceSetDetails createChoiceSet(ChoiceSetDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create ChoiceSet.");
        try {
            Integer id = event.getId();

            if (id != null && this.choiceSetRepository.exists(id)) {

                LOG.warn("Cannot create ChoiceSet: ChoiceSet with id " + id
                        + " already exists!");
                throw new RequestFailedException("Cannot create ChoiceSet: "
                        + "ChoiceSet with id " + id + " already exists!");
            }
            ChoiceSet choiceSet = ChoiceSet.fromDetails(event);


            ChoiceSet createdChoiceSet = this.choiceSetRepository
                    .saveAndFlush(choiceSet);

            LOG.debug("ChoiceSet created: " + createdChoiceSet);
            return createdChoiceSet.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create ChoiceSet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create ChoiceSet: Exception thrown!", e);
        }
    }

    @Override
    public List<ChoiceSetDetails> retrieveAllChoiceSets()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all ChoiceSets.");
        try {

            List<ChoiceSetDetails> generatedDetails = new ArrayList<>();
            for (ChoiceSet choiceSet : this.choiceSetRepository.findAll()) {

                generatedDetails.add(choiceSet.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all ChoiceSets failed: No ChoiceSets found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all ChoiceSets failed: No ChoiceSets found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " ChoiceSets.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all ChoiceSets failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all ChoiceSets failed: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceSetDetails retrieveChoiceSet(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve ChoiceSet with id " + id + ".");
        try {

            ChoiceSet choiceSet = null;
            choiceSet = this.choiceSetRepository.findOne(id);

            if (choiceSet == null) {

                LOG.info("Could not retrieve ChoiceSet: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve ChoiceSet: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning ChoiceSet " + choiceSet);
            return choiceSet.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve ChoiceSet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve ChoiceSet: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceSetDetails retrieveChoiceSetByName(String name)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve ChoiceSet with name " + name + ".");
        try {

            ChoiceSet choiceSet = null;
            choiceSet = this.choiceSetRepository.findOneByName(name);

            if (choiceSet == null) {

                LOG.info("Could not retrieve ChoiceSet: No object with name "
                        + name + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve ChoiceSet: No object with name "
                                + name + " found!");
            }
            LOG.debug("Returning ChoiceSet " + choiceSet);
            return choiceSet.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve ChoiceSet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve ChoiceSet: Exception thrown!", e);
        }
    }

    @Override
    public ChoiceSetDetails updateChoiceSet(ChoiceSetDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update ChoiceSet with id " + event.getId()
                + ".");
        try {

            if (!this.choiceSetRepository.exists(event.getId())) {

                LOG.info("Could not update ChoiceSet: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update ChoiceSet: No object with id "
                                + event.getId() + " found!");
            }

            ChoiceSet choiceSet = ChoiceSet.fromDetails(event);


            ChoiceSet updatedChoiceSet = this.choiceSetRepository
                    .saveAndFlush(choiceSet);

            return updatedChoiceSet.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update ChoiceSet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update ChoiceSet: Exception thrown!", e);
        }

    }

    @Override
    public ChoiceSetDetails deleteChoiceSet(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete ChoiceSet with id " + id + ".");
        try {

            ChoiceSet choiceSet = this.choiceSetRepository.findOne(id);

            if (choiceSet == null) {

                LOG.info("Could not delete ChoiceSet: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete ChoiceSet: No object with id " + id
                                + " found!");
            }

            this.choiceSetRepository.delete(id);

            return choiceSet.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete ChoiceSet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete ChoiceSet: Exception thrown!", e);
        }
    }
}
