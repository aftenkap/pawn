package raziel.pawn.persistence.services;


import java.util.List;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.data.SpreadsheetDTO;


/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public interface SpreadsheetPersistenceService {


    /**
     * Creates a {@link SpreadsheetDTO Spreadsheet}.
     * 
     * @param event
     *            the details of the {@link SpreadsheetDTO Spreadsheet}.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public <T> SpreadsheetDTO createSpreadsheet(SpreadsheetDTO event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all {@link SpreadsheetDTO Spreadsheets}.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<SpreadsheetDTO> retrieveAllSpreadsheets()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single {@link SpreadsheetDTO Spreadsheet}.
     * 
     * @param id
     *            the id of the {@link SpreadsheetDTO Spreadsheet}.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SpreadsheetDTO retrieveSpreadsheet(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Updates a {@link SpreadsheetDTO Spreadsheet}.
     * 
     * @param event
     *            the details of the {@link SpreadsheetDTO Spreadsheet}.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public <T> SpreadsheetDTO updateSpreadsheet(SpreadsheetDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Deletes a {@link SpreadsheetDTO Spreadsheet}.
     * 
     * @param event
     *            the details of the {@link SpreadsheetDTO Spreadsheet}.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public <T> SpreadsheetDTO deleteSpreadsheet(final SpreadsheetDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a {@link SpreadsheetDTO Spreadsheet}.
     * 
     * @param id
     *            the id of the {@link SpreadsheetDTO Spreadsheet} to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SpreadsheetDTO deleteSpreadsheet(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Retrieves the {@link SpreadsheetDTO Spreadsheet} of a
     * {@link SpreadsheetCellDTO SpreadsheetCell}.
     * 
     * @param id
     *            the id of the {@link SpreadsheetCellDTO SpreadsheetCell}
     *            associated with the {@link SpreadsheetDTO Spreadsheet}.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SpreadsheetDTO retrieveSpreadsheetForSpreadsheetCell(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Creates a {@link SpreadsheetCellDTO SpreadsheetCell}.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public <T> SpreadsheetCellDTO createSpreadsheetCell(SpreadsheetCellDTO event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all {@link SpreadsheetCellDTO SpreadsheetCells}.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<SpreadsheetCellDTO> retrieveAllSpreadsheetCells()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single {@link SpreadsheetCellDTO SpreadsheetCell}.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SpreadsheetCellDTO retrieveSpreadsheetCell(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Updates a {@link SpreadsheetCellDTO SpreadsheetCell}.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public <T> SpreadsheetCellDTO updateSpreadsheetCell(SpreadsheetCellDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a {@link SpreadsheetCellDTO SpreadsheetCell}.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public <T> SpreadsheetCellDTO deleteSpreadsheetCell(final SpreadsheetCellDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a {@link SpreadsheetCellDTO SpreadsheetCell}.
     * 
     * @param id
     *            the id of the object to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SpreadsheetCellDTO deleteSpreadsheetCell(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

}
