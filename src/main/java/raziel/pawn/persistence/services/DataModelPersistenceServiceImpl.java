package raziel.pawn.persistence.services;



import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AssociationDetails;
import raziel.pawn.dto.modeling.AssociativeRoleDetails;
import raziel.pawn.dto.modeling.AttributionDetails;
import raziel.pawn.dto.modeling.AttributiveRoleDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.ConstantDetails;
import raziel.pawn.dto.modeling.DataModelDetails;
import raziel.pawn.dto.modeling.DurationOrderDetails;
import raziel.pawn.dto.modeling.EndurantDetails;
import raziel.pawn.dto.modeling.EntityDetails;
import raziel.pawn.dto.modeling.InteractionDetails;
import raziel.pawn.dto.modeling.InteractiveRoleDetails;
import raziel.pawn.dto.modeling.PerdurantDetails;
import raziel.pawn.dto.modeling.RoleDetails;
import raziel.pawn.dto.modeling.SemiIntervalOrderDetails;
import raziel.pawn.persistence.domain.modeling.DataModel;
import raziel.pawn.persistence.domain.modeling.IAssertion;
import raziel.pawn.persistence.domain.modeling.IConcept;
import raziel.pawn.persistence.domain.modeling.IRole;
import raziel.pawn.persistence.domain.modeling.assertions.Association;
import raziel.pawn.persistence.domain.modeling.assertions.Attribution;
import raziel.pawn.persistence.domain.modeling.assertions.Constant;
import raziel.pawn.persistence.domain.modeling.assertions.Entity;
import raziel.pawn.persistence.domain.modeling.assertions.Interaction;
import raziel.pawn.persistence.domain.modeling.terminology.AssociativeRole;
import raziel.pawn.persistence.domain.modeling.terminology.AttributiveRole;
import raziel.pawn.persistence.domain.modeling.terminology.Endurant;
import raziel.pawn.persistence.domain.modeling.terminology.InteractiveRole;
import raziel.pawn.persistence.domain.modeling.terminology.Perdurant;
import raziel.pawn.persistence.domain.modeling.time.DurationOrder;
import raziel.pawn.persistence.domain.modeling.time.SemiIntervalOrder;
import raziel.pawn.persistence.repository.AssertionRepository;
import raziel.pawn.persistence.repository.ConceptRepository;
import raziel.pawn.persistence.repository.DataModelRepository;
import raziel.pawn.persistence.repository.DurationOrderRepository;
import raziel.pawn.persistence.repository.RoleRepository;
import raziel.pawn.persistence.repository.SemiIntervalOrderRepository;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Service
@Transactional
public class DataModelPersistenceServiceImpl
        implements DataModelPersistenceService {

    private static Logger                     LOG = LoggerFactory
                                                          .getLogger(DataModelPersistenceServiceImpl.class);

    private final DataModelRepository         dataModelRepository;
    private final ConceptRepository           conceptRepository;
    private final RoleRepository              roleRepository;
    private final AssertionRepository         assertionRepository;
    private final DurationOrderRepository     durationOrderRepository;
    private final SemiIntervalOrderRepository semiIntervalOrderRepository;

    /**
     * Creates a new instance of the {@link DataModelPersistenceServiceImpl}
     * class.
     * 
     * @param dataModelRepository
     *            the {@link DataModelRepository}.
     * @param conceptRepository
     *            the {@link ConceptRepository}.
     * @param roleRepository
     *            the {@link RoleRepository}.
     * @param assertionRepository
     *            the {@link AssertionRepository}.
     * @param durationOrderRepository
     *            the {@link DurationOrderRepository}.
     * @param semiIntervalOrderRepository
     *            the {@link SemiIntervalOrderRepository}.
     */
    @Autowired
    public DataModelPersistenceServiceImpl(
            final DataModelRepository dataModelRepository,
            final ConceptRepository conceptRepository,
            final RoleRepository roleRepository,
            final AssertionRepository assertionRepository,
            final DurationOrderRepository durationOrderRepository,
            final SemiIntervalOrderRepository semiIntervalOrderRepository) {

        this.dataModelRepository = dataModelRepository;
        this.conceptRepository = conceptRepository;
        this.roleRepository = roleRepository;
        this.assertionRepository = assertionRepository;
        this.durationOrderRepository = durationOrderRepository;
        this.semiIntervalOrderRepository = semiIntervalOrderRepository;
    }


    @Override
    public DataModelDetails createDataModel(DataModelDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create DataModel.");
        try {
            Integer id = event.getId();

            if (id != null && this.dataModelRepository.exists(id)) {

                LOG.warn("Cannot create DataModel: DataModel with id " + id
                        + " already exists!");
                throw new RequestFailedException("Cannot create DataModel: "
                        + "DataModel with id " + id + " already exists!");
            }
            DataModel dataModel = DataModel.fromDetails(event);


            DataModel createdDataModel = this.dataModelRepository
                    .saveAndFlush(dataModel);

            LOG.debug("Created DataModel: " + dataModel);
            return createdDataModel.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create DataModel: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create DataModel: Exception thrown!", e);
        }
    }

    @Override
    public List<DataModelDetails> retrieveAllDataModels()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Data Models.");
        try {

            List<DataModelDetails> generatedDetails = new ArrayList<>();
            for (DataModel dataModel : this.dataModelRepository.findAll()) {

                generatedDetails.add(dataModel.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Data Models failed: No Data Models found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Data Models failed: No Data Models found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " Data Models.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Data Models failed: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Retrieval of all Data Models failed: Exception thrown!", e);
        }
    }



    @Override
    public DataModelDetails retrieveDataModel(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Data Model with id " + id + ".");
        try {

            DataModel dataModel = null;
            dataModel = this.dataModelRepository.findOne(id);

            if (dataModel == null) {

                LOG.info("Could not retrieve Data Model: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Data Model: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning Data Model " + dataModel);
            return dataModel.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Data Model: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Data Model: Exception thrown!", e);
        }
    }

    @Override
    public DataModelDetails updateDataModel(DataModelDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Data Model with id " + event.getId()
                + ".");
        try {

            if (!this.dataModelRepository.exists(event.getId())) {

                LOG.info("Could not update Data Model: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Data Model: No object with id "
                                + event.getId() + " found!");
            }

            DataModel dataModel = DataModel.fromDetails(event);


            DataModel updatedDataModel = this.dataModelRepository
                    .saveAndFlush(dataModel);

            return updatedDataModel.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update Data Model: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update Data Model: Exception thrown!", e);
        }
    }

    @Override
    public DataModelDetails deleteDataModel(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Data Model with id " + id + ".");
        try {

            DataModel dataModel = this.dataModelRepository.findOne(id);

            if (dataModel == null) {

                LOG.info("Could not delete Data Model: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Data Model: No object with id " + id
                                + " found!");
            }

            this.dataModelRepository.delete(id);

            return dataModel.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Data Model: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Data Model: Exception thrown!", e);
        }
    }

    @Override
    public EndurantDetails createEndurant(EndurantDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create endurant.");
        try {
            Integer id = event.getId();

            if (id != null && this.conceptRepository.exists(id)) {

                throw new RequestFailedException("Cannot create Endurant: "
                        + "Concept with id " + id + " already exists!");
            }
            Endurant endurant = Endurant.fromDetails(event);

            Endurant createdEndurant = this.conceptRepository
                    .saveAndFlush(endurant);

            return createdEndurant.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create Endurant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Endurant: Exception thrown!", e);
        }
    }

    @Override
    public PerdurantDetails createPerdurant(PerdurantDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create perdurant.");
        try {

            Integer id = event.getId();

            if (id != null && this.conceptRepository.exists(id)) {

                throw new RequestFailedException("Cannot create Endurant: "
                        + "Concept with id " + id + " already exists!");
            }
            Perdurant perdurant = Perdurant.fromDetails(event);

            Perdurant createdPerdurant = this.conceptRepository
                    .saveAndFlush(perdurant);

            return createdPerdurant.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create Perdurant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Perdurant: Exception thrown!", e);
        }
    }

    @Override
    public List<ConceptDetails> retrieveAllConcepts()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Concepts.");
        try {

            List<ConceptDetails> generatedDetails = new ArrayList<>();

            for (IConcept<?> concept : this.conceptRepository.findAll()) {

                generatedDetails.add(concept.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Concepts failed: No Data Models found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Concepts failed: No Data Models found!");
            }

            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Concepts failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Concepts failed: Exception thrown!", e);
        }
    }

    @Override
    public ConceptDetails retrieveConcept(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Concept with id " + id + ".");
        try {
            IConcept<?> concept = this.conceptRepository.findOne(id);

            if (concept == null) {

                LOG.info("Could not retrieve Concept: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Concept: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning Concept " + concept);

            return concept.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Concept: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Concept: Exception thrown!", e);
        }
    }

    @Override
    public EndurantDetails updateEndurant(EndurantDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Endurant.");
        try {

            Integer id = event.getId();

            if (!this.conceptRepository.exists(id)) {

                LOG.info("Could not udpate Endurant: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Endurant: No object with id " + id
                                + " found!");
            }

            Endurant concept = Endurant.fromDetails(event);

            Endurant updatedConcept = this.conceptRepository
                    .saveAndFlush(concept);

            return updatedConcept.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate Endurant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate Endurant: Exception thrown!", e);
        }

    }

    @Override
    public PerdurantDetails updatePerdurant(PerdurantDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Perdurant.");
        try {

            Integer id = event.getId();

            if (!this.conceptRepository.exists(id)) {

                LOG.info("Could not udpate Perdurant: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Perdurant: No object with id " + id
                                + " found!");
            }

            Perdurant concept = Perdurant.fromDetails(event);

            Perdurant updatedConcept = this.conceptRepository
                    .saveAndFlush(concept);

            return updatedConcept.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate Perdurant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate Perdurant: Exception thrown!", e);
        }
    }

    @Override
    public ConceptDetails deleteConcept(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Concept with id " + id + ".");
        try {

            IConcept<?> concept = this.conceptRepository.findOne(id);

            if (concept == null) {

                LOG.info("Could not delete Concept: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Concept: No object with id " + id
                                + " found!");
            }

            this.conceptRepository.delete(id);

            return concept.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Concept: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Concept: Exception thrown!", e);
        }
    }


    @Override
    public AssociativeRoleDetails createAssociativeRole(
            AssociativeRoleDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create AssociativeRole.");
        try {

            Integer id = event.getId();

            if (id != null && this.roleRepository.exists(id)) {

                throw new RequestFailedException(
                        "Cannot create AssociativeRole: " + "Role with id "
                                + id + " already exists!");
            }
            AssociativeRole role = AssociativeRole.fromDetails(event);

            AssociativeRole createdRole = this.roleRepository
                    .saveAndFlush(role);

            return createdRole.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create AssociativeRole: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create AssociativeRole: Exception thrown!", e);
        }
    }

    @Override
    public AttributiveRoleDetails createAttributiveRole(
            AttributiveRoleDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create AttributiveRole.");
        try {

            Integer id = event.getId();

            if (id != null && this.roleRepository.exists(id)) {

                throw new RequestFailedException(
                        "Cannot create AttributiveRole: " + "Role with id "
                                + id + " already exists!");
            }
            AttributiveRole role = AttributiveRole.fromDetails(event);

            AttributiveRole createdRole = this.roleRepository
                    .saveAndFlush(role);

            return createdRole.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create AttributiveRole: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create AttributiveRole: Exception thrown!", e);
        }
    }

    @Override
    public InteractiveRoleDetails createInteractiveRole(
            InteractiveRoleDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create InteractiveRole.");
        try {

            Integer id = event.getId();

            if (id != null && this.roleRepository.exists(id)) {

                throw new RequestFailedException(
                        "Cannot create InteractiveRole: " + "Role with id "
                                + id + " already exists!");
            }
            InteractiveRole role = InteractiveRole.fromDetails(event);

            InteractiveRole createdRole = this.roleRepository
                    .saveAndFlush(role);

            return createdRole.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create InteractiveRole: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create InteractiveRole: Exception thrown!", e);
        }
    }

    @Override
    public List<RoleDetails> retrieveAllRoles()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Roles.");
        try {

            List<RoleDetails> generatedDetails = new ArrayList<>();

            for (IRole<?, ?, ?> role : this.roleRepository.findAll()) {

                generatedDetails.add(role.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Roles failed: No Data Models found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Roles failed: No Data Models found!");
            }

            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Roles failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Roles failed: Exception thrown!", e);
        }
    }



    @Override
    public RoleDetails retrieveRole(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Role with id " + id + ".");
        try {
            IRole<?, ?, ?> role = this.roleRepository.findOne(id);

            if (role == null) {

                LOG.info("Could not retrieve Role: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Role: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning Role " + role);

            return role.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Role: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Role: Exception thrown!", e);
        }
    }


    @Override
    public AssociativeRoleDetails updateAssociativeRole(
            AssociativeRoleDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update AssociativeRole.");
        try {

            Integer id = event.getId();

            if (!this.roleRepository.exists(id)) {

                LOG.info("Could not udpate AssociativeRole: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not update AssociativeRole: No object with id "
                                + id + " found!");
            }

            AssociativeRole role = AssociativeRole.fromDetails(event);

            AssociativeRole updatedRole = this.roleRepository
                    .saveAndFlush(role);

            return updatedRole.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate AssociativeRole: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate AssociativeRole: Exception thrown!", e);
        }

    }

    @Override
    public AttributiveRoleDetails updateAttributiveRole(
            AttributiveRoleDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update AttributiveRole.");
        try {

            Integer id = event.getId();

            if (!this.roleRepository.exists(id)) {

                LOG.info("Could not udpate AttributiveRole: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not update AttributiveRole: No object with id "
                                + id + " found!");
            }

            AttributiveRole role = AttributiveRole.fromDetails(event);

            AttributiveRole updatedRole = this.roleRepository
                    .saveAndFlush(role);

            return updatedRole.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate AttributiveRole: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate AttributiveRole: Exception thrown!", e);
        }
    }

    @Override
    public InteractiveRoleDetails updateInteractiveRole(
            InteractiveRoleDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update InteractiveRole.");
        try {

            Integer id = event.getId();

            if (!this.roleRepository.exists(id)) {

                LOG.info("Could not udpate InteractiveRole: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not update InteractiveRole: No object with id "
                                + id + " found!");
            }

            InteractiveRole role = InteractiveRole.fromDetails(event);

            InteractiveRole updatedRole = this.roleRepository
                    .saveAndFlush(role);

            return updatedRole.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate InteractiveRole: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate InteractiveRole: Exception thrown!", e);
        }
    }

    @Override
    public RoleDetails deleteRole(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Role with id " + id + ".");
        try {

            IRole<?, ?, ?> role = this.roleRepository.findOne(id);

            if (role == null) {

                LOG.info("Could not delete Role: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Role: No object with id " + id
                                + " found!");
            }

            this.roleRepository.delete(id);

            return role.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Role: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Role: Exception thrown!", e);
        }
    }


    @Override
    public ConstantDetails createConstant(ConstantDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Constant.");
        try {
            Integer id = event.getId();

            if (id != null && this.assertionRepository.exists(id)) {

                throw new RequestFailedException("Cannot create Constant: "
                        + "assertion with id " + id + " already exists!");
            }
            Constant assertion = Constant.fromDetails(event);

            Constant createdConstant = this.assertionRepository
                    .saveAndFlush(assertion);

            return createdConstant.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create Constant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Constant: Exception thrown!", e);
        }
    }


    @Override
    public EntityDetails createEntity(EntityDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Entity.");
        try {
            Integer id = event.getId();

            if (id != null && this.assertionRepository.exists(id)) {

                throw new RequestFailedException("Cannot create Entity: "
                        + "Assertion with id " + id + " already exists!");
            }
            Entity assertion = Entity.fromDetails(event);

            Entity createdEntity = this.assertionRepository
                    .saveAndFlush(assertion);

            return createdEntity.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create Entity: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Entity: Exception thrown!", e);
        }
    }


    @Override
    public AssociationDetails createAssociation(AssociationDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Association.");
        try {
            Integer id = event.getId();

            if (id != null && this.assertionRepository.exists(id)) {

                throw new RequestFailedException("Cannot create Association: "
                        + "Assertion with id " + id + " already exists!");
            }
            Association assertion = Association.fromDetails(event);

            Association createdAssociation = this.assertionRepository
                    .saveAndFlush(assertion);

            return createdAssociation.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create Association: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Association: Exception thrown!", e);
        }
    }


    @Override
    public AttributionDetails createAttribution(AttributionDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Attribution.");
        try {
            Integer id = event.getId();

            if (id != null && this.assertionRepository.exists(id)) {

                throw new RequestFailedException("Cannot create Attribution: "
                        + "Assertion with id " + id + " already exists!");
            }
            Attribution assertion = Attribution.fromDetails(event);

            Attribution createdAttribution = this.assertionRepository
                    .saveAndFlush(assertion);

            return createdAttribution.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create Attribution: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Attribution: Exception thrown!", e);
        }
    }


    @Override
    public InteractionDetails createInteraction(InteractionDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Interaction.");
        try {

            Integer id = event.getId();

            if (id != null && this.assertionRepository.exists(id)) {

                throw new RequestFailedException("Cannot create Interaction: "
                        + "Assertion with id " + id + " already exists!");
            }
            Interaction assertion = Interaction.fromDetails(event);

            Interaction createdInteraction = this.assertionRepository
                    .saveAndFlush(assertion);

            return createdInteraction.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create Interaction: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Interaction: Exception thrown!", e);
        }
    }


    @Override
    public List<AssertionDetails> retrieveAllAssertions()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Assertions.");
        try {

            List<AssertionDetails> generatedDetails = new ArrayList<>();

            for (IAssertion assertion : this.assertionRepository.findAll()) {

                generatedDetails.add(assertion.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Assertions failed: No Data Models found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Assertions failed: No Data Models found!");
            }

            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Assertions failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Assertions failed: Exception thrown!", e);
        }
    }



    @Override
    public AssertionDetails retrieveAssertion(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Assertion with id " + id + ".");
        try {

            IAssertion assertion = this.assertionRepository.findOne(id);

            if (assertion == null) {

                LOG.info("Could not retrieve Assertion: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Assertion: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning Assertion " + assertion);

            return assertion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Assertion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Assertion: Exception thrown!", e);
        }
    }

    @Override
    public ConstantDetails updateConstant(ConstantDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Constant.");
        try {

            Integer id = event.getId();

            if (!this.assertionRepository.exists(id)) {

                LOG.info("Could not udpate Constant: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Constant: No object with id " + id
                                + " found!");
            }

            Constant assertion = Constant.fromDetails(event);

            Constant updatedAssertion = this.assertionRepository
                    .saveAndFlush(assertion);

            return updatedAssertion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate Constant: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate Constant: Exception thrown!", e);
        }
    }

    @Override
    public EntityDetails updateEntity(EntityDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Entity.");
        try {

            Integer id = event.getId();

            if (!this.assertionRepository.exists(id)) {

                LOG.info("Could not udpate Entity: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Entity: No object with id " + id
                                + " found!");
            }

            Entity assertion = Entity.fromDetails(event);

            Entity updatedAssertion = this.assertionRepository
                    .saveAndFlush(assertion);

            return updatedAssertion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate Entity: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate Entity: Exception thrown!", e);
        }
    }

    @Override
    public AssociationDetails updateAssociation(AssociationDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Association.");
        try {

            Integer id = event.getId();

            if (!this.assertionRepository.exists(id)) {

                LOG.info("Could not udpate Association: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Association: No object with id " + id
                                + " found!");
            }

            Association assertion = Association.fromDetails(event);

            Association updatedAssertion = this.assertionRepository
                    .saveAndFlush(assertion);

            return updatedAssertion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate Association: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate Association: Exception thrown!", e);
        }
    }

    @Override
    public AttributionDetails updateAttribution(AttributionDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Attribution.");
        try {

            Integer id = event.getId();

            if (!this.assertionRepository.exists(id)) {

                LOG.info("Could not udpate Attribution: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Attribution: No object with id " + id
                                + " found!");
            }

            Attribution assertion = Attribution.fromDetails(event);

            Attribution updatedAssertion = this.assertionRepository
                    .saveAndFlush(assertion);

            return updatedAssertion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate Attribution: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate Attribution: Exception thrown!", e);
        }
    }

    @Override
    public InteractionDetails updateInteraction(InteractionDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Interaction.");
        try {

            Integer id = event.getId();

            if (!this.assertionRepository.exists(id)) {

                LOG.info("Could not udpate Interaction: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Interaction: No object with id " + id
                                + " found!");
            }

            Interaction assertion = Interaction.fromDetails(event);

            Interaction updatedAssertion = this.assertionRepository
                    .saveAndFlush(assertion);

            return updatedAssertion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate Interaction: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate Interaction: Exception thrown!", e);
        }
    }

    @Override
    public AssertionDetails deleteAssertion(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Assertion with id " + id + ".");
        try {

            IAssertion assertion = this.assertionRepository.findOne(id);

            if (assertion == null) {

                LOG.info("Could not delete Assertion: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Assertion: No object with id " + id
                                + " found!");
            }

            this.assertionRepository.delete(id);

            return assertion.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Assertion: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Assertion: Exception thrown!", e);
        }
    }

    @Override
    public DurationOrderDetails createDurationOrder(DurationOrderDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create durationOrder.");
        try {
            Integer id = event.getId();

            if (id != null && this.durationOrderRepository.exists(id)) {

                throw new RequestFailedException(
                        "Cannot create DurationOrder: "
                                + "DurationOrder with id " + id
                                + " already exists!");
            }
            DurationOrder durationOrder = DurationOrder.fromDetails(event);

            DurationOrder createdDurationOrder = this.durationOrderRepository
                    .saveAndFlush(durationOrder);

            return createdDurationOrder.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create DurationOrder: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create DurationOrder: Exception thrown!", e);
        }
    }

    @Override
    public List<DurationOrderDetails> retrieveAllDurationOrders()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all DurationOrders.");
        try {

            List<DurationOrderDetails> generatedDetails = new ArrayList<>();

            for (DurationOrder durationOrder : this.durationOrderRepository
                    .findAll()) {

                generatedDetails.add(durationOrder.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all DurationOrders failed: No Data Models found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all DurationOrders failed: No Data Models found!");
            }

            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn(
                    "Retrieval of all DurationOrders failed: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Retrieval of all DurationOrders failed: Exception thrown!",
                    e);
        }
    }



    @Override
    public DurationOrderDetails retrieveDurationOrder(final int id)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to retrieve DurationOrder with id " + id + ".");
        try {
            DurationOrder durationOrder = this.durationOrderRepository
                    .findOne(id);

            if (durationOrder == null) {

                LOG.info("Could not retrieve DurationOrder: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve DurationOrder: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning DurationOrder " + durationOrder);

            return durationOrder.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve DurationOrder: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve DurationOrder: Exception thrown!", e);
        }
    }

    @Override
    public DurationOrderDetails updateDurationOrder(DurationOrderDetails event)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to update DurationOrder.");
        try {

            Integer id = event.getId();

            if (!this.durationOrderRepository.exists(id)) {

                LOG.info("Could not udpate DurationOrder: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not update DurationOrder: No object with id "
                                + id + " found!");
            }

            DurationOrder durationOrder = DurationOrder.fromDetails(event);

            DurationOrder updatedDurationOrder = this.durationOrderRepository
                    .saveAndFlush(durationOrder);

            return updatedDurationOrder.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate DurationOrder: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate DurationOrder: Exception thrown!", e);
        }
    }

    @Override
    public DurationOrderDetails deleteDurationOrder(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete DurationOrder with id " + id + ".");
        try {

            DurationOrder durationOrder = this.durationOrderRepository
                    .findOne(id);

            if (durationOrder == null) {

                LOG.info("Could not delete DurationOrder: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete DurationOrder: No object with id "
                                + id + " found!");
            }

            this.durationOrderRepository.delete(id);

            return durationOrder.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete DurationOrder: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete DurationOrder: Exception thrown!", e);
        }
    }

    @Override
    public SemiIntervalOrderDetails createSemiIntervalOrder(
            SemiIntervalOrderDetails event)
            throws RequestFailedException {


        LOG.debug("Attempting to create semiIntervalOrder.");
        try {
            Integer id = event.getId();

            if (id != null && this.semiIntervalOrderRepository.exists(id)) {

                throw new RequestFailedException(
                        "Cannot create SemiIntervalOrder: "
                                + "SemiIntervalOrder with id " + id
                                + " already exists!");
            }
            SemiIntervalOrder semiIntervalOrder = SemiIntervalOrder
                    .fromDetails(event);

            SemiIntervalOrder createdSemiIntervalOrder = this.semiIntervalOrderRepository
                    .saveAndFlush(semiIntervalOrder);

            return createdSemiIntervalOrder.toDTO();
        }
        catch (Exception e) {

            LOG.warn("Could not create SemiIntervalOrder: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create SemiIntervalOrder: Exception thrown!", e);
        }
    }

    @Override
    public List<SemiIntervalOrderDetails> retrieveAllSemiIntervalOrders()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all SemiIntervalOrders.");
        try {

            List<SemiIntervalOrderDetails> generatedDetails = new ArrayList<>();

            for (SemiIntervalOrder semiIntervalOrder : this.semiIntervalOrderRepository
                    .findAll()) {

                generatedDetails.add(semiIntervalOrder.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all SemiIntervalOrders failed: No Data Models found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all SemiIntervalOrders failed: No Data Models found!");
            }

            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn(
                    "Retrieval of all SemiIntervalOrders failed: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Retrieval of all SemiIntervalOrders failed: Exception thrown!",
                    e);
        }
    }



    @Override
    public SemiIntervalOrderDetails retrieveSemiIntervalOrder(final int id)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to retrieve SemiIntervalOrder with id " + id
                + ".");
        try {

            SemiIntervalOrder semiIntervalOrder = this.semiIntervalOrderRepository
                    .findOne(id);

            if (semiIntervalOrder == null) {

                LOG.info("Could not retrieve SemiIntervalOrder: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve SemiIntervalOrder: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning SemiIntervalOrder " + semiIntervalOrder);

            return semiIntervalOrder.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve SemiIntervalOrder: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Could not retrieve SemiIntervalOrder: Exception thrown!",
                    e);
        }
    }

    @Override
    public SemiIntervalOrderDetails updateSemiIntervalOrder(
            SemiIntervalOrderDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update SemiIntervalOrder.");
        try {

            Integer id = event.getId();

            if (!this.semiIntervalOrderRepository.exists(id)) {

                LOG.info("Could not udpate SemiIntervalOrder: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not update SemiIntervalOrder: No object with id "
                                + id + " found!");
            }

            SemiIntervalOrder semiIntervalOrder = SemiIntervalOrder
                    .fromDetails(event);

            SemiIntervalOrder updatedSemiIntervalOrder = this.semiIntervalOrderRepository
                    .saveAndFlush(semiIntervalOrder);

            return updatedSemiIntervalOrder.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not udpate SemiIntervalOrder: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not udpate SemiIntervalOrder: Exception thrown!", e);
        }
    }

    @Override
    public SemiIntervalOrderDetails deleteSemiIntervalOrder(final int id)
            throws ObjectNotFoundException, RequestFailedException {


        LOG.debug("Attempting to delete SemiIntervalOrder with id " + id + ".");
        try {

            SemiIntervalOrder semiIntervalOrder = this.semiIntervalOrderRepository
                    .findOne(id);

            if (semiIntervalOrder == null) {

                LOG.info("Could not delete SemiIntervalOrder: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete SemiIntervalOrder: No object with id "
                                + id + " found!");
            }

            this.semiIntervalOrderRepository.delete(id);

            return semiIntervalOrder.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete SemiIntervalOrder: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete SemiIntervalOrder: Exception thrown!", e);
        }
    }
}
