package raziel.pawn.persistence.services;


import java.util.List;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.metadata.AnswerDetails;
import raziel.pawn.dto.metadata.ChoiceDetails;
import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.ChoiceSetDetails;
import raziel.pawn.dto.metadata.ParticipantDTO;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;
import raziel.pawn.dto.metadata.SurveyDetails;
import raziel.pawn.dto.metadata.TextQuestionDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public interface SurveyPersistenceService {


    /**
     * Creates a survey.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public SurveyDetails createSurvey(SurveyDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all surveys.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<SurveyDetails> retrieveAllSurveys()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single survey.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SurveyDetails retrieveSurvey(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a survey.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SurveyDetails updateSurvey(SurveyDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a survey.
     * 
     * @param id
     *            the id of the object to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SurveyDetails deleteSurvey(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Retrieves the survey of the provided question.
     * 
     * @param id
     *            the id of the object associated with the survey.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SurveyDetails retrieveSurveyForQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves the survey of the provided participant.
     * 
     * @param id
     *            the id of the object associated with the survey.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SurveyDetails retrieveSurveyForParticipant(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves the survey of the provided answer.
     * 
     * @param id
     *            the id of the object associated with the survey.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SurveyDetails retrieveSurveyForAnswer(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves the answers of the provided question.
     * 
     * @param id
     *            the id of the object associated with the question.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<AnswerDetails> retrieveAllAnswersForQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves the answers of the provided participant.
     * 
     * @param id
     *            the id of the object associated with the participant.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<AnswerDetails> retrieveAllAnswersForParticipant(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Retrieves the answer to the provided question by the provided
     * participant.
     * 
     * @param questionId
     *            the question id.
     * @param participantId
     *            the participant id.
     * @return the answer to the provided question by the provided participant.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AnswerDetails retrieveAnswerToQuestionByParticipant(
            final int questionId, final int participantId)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Creates a text question.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public TextQuestionDetails createTextQuestion(TextQuestionDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates a choice question.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public ChoiceQuestionDetails createChoiceQuestion(
            ChoiceQuestionDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates a ranking question.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public RankingQuestionDetails createRankingQuestion(
            RankingQuestionDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Updates a text question.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public TextQuestionDetails updateTextQuestion(TextQuestionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a choice question.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceQuestionDetails updateChoiceQuestion(
            ChoiceQuestionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a ranking question.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public RankingQuestionDetails updateRankingQuestion(
            RankingQuestionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves all questions.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<QuestionDetails> retrieveAllQuestions()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Retrieves a single question.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public QuestionDetails retrieveQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single text question.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public TextQuestionDetails retrieveTextQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single choice question.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceQuestionDetails retrieveChoiceQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single ranking question.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public RankingQuestionDetails retrieveRankingQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a question.
     * 
     * @param id
     *            the id of the object to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public QuestionDetails deleteQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Creates an answer.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public AnswerDetails createAnswer(AnswerDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all answers.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<AnswerDetails> retrieveAllAnswers()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single answer.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AnswerDetails retrieveAnswer(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an answer.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AnswerDetails updateAnswer(AnswerDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes an Answer.
     * 
     * @param id
     *            the id of the object to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AnswerDetails deleteAnswer(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Creates an participant.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public ParticipantDTO createParticipant(ParticipantDTO event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all participants.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<ParticipantDTO> retrieveAllParticipants()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single participant.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ParticipantDTO retrieveParticipant(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an participant.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ParticipantDTO updateParticipant(ParticipantDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a Participant.
     * 
     * @param id
     *            the id of the object to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ParticipantDTO deleteParticipant(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Creates an choice.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public ChoiceDetails createChoice(ChoiceDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all choices.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<ChoiceDetails> retrieveAllChoices()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single choice.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceDetails retrieveChoice(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Retrieves a choice by name.
     * 
     * @param name
     *            the name.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceDetails retrieveChoiceByName(final String name)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a choice.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceDetails updateChoice(ChoiceDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a question.
     * 
     * @param id
     *            the id of the object to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceDetails deleteChoice(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Creates an choiceSet.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public ChoiceSetDetails createChoiceSet(ChoiceSetDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all choiceSets.
     * 
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<ChoiceSetDetails> retrieveAllChoiceSets()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single choiceSet.
     * 
     * @param id
     *            the id of the requested object.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceSetDetails retrieveChoiceSet(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Retrieves a choice set by name.
     * 
     * @param name
     *            the name.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceSetDetails retrieveChoiceSetByName(final String name)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a choiceSet.
     * 
     * @param event
     *            the details of the request.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceSetDetails updateChoiceSet(ChoiceSetDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a choiceSet.
     * 
     * @param id
     *            the id of the object to delete.
     * @return an event containing the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ChoiceSetDetails deleteChoiceSet(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;
}
