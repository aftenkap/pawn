package raziel.pawn.persistence.services;



import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.data.SpreadsheetDTO;
import raziel.pawn.persistence.domain.data.Spreadsheet;
import raziel.pawn.persistence.domain.data.SpreadsheetCell;
import raziel.pawn.persistence.repository.SpreadsheetCellRepository;
import raziel.pawn.persistence.repository.SpreadsheetRepository;


/**
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 *
 */
@Service
@Transactional
public class SpreadsheetPersistenceServiceImpl
        implements SpreadsheetPersistenceService {

    private static Logger             LOG = LoggerFactory
                                                  .getLogger(SpreadsheetPersistenceServiceImpl.class);

    private final SpreadsheetRepository     spreadsheetRepository;
    private final SpreadsheetCellRepository spreadsheetCellRepository;

    /**
     * Creates a new instance of the {@link SpreadsheetPersistenceServiceImpl}
     * class.
     * 
     * @param spreadsheetRepository
     *            the {@link SpreadsheetRepository}.
     * @param spreadsheetCellRepository
     *            the {@link SpreadsheetCellRepository}.
     */
    @Autowired
    public SpreadsheetPersistenceServiceImpl(
            final SpreadsheetRepository spreadsheetRepository,
            final SpreadsheetCellRepository spreadsheetCellRepository) {

        this.spreadsheetCellRepository = spreadsheetCellRepository;
        this.spreadsheetRepository = spreadsheetRepository;
    }



    @Override
    public  SpreadsheetDTO createSpreadsheet(SpreadsheetDTO event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Spreadsheet.");
        try {
            Integer id = event.getId();

            if (id != null && this.spreadsheetRepository.exists(id)) {

                LOG.warn("Cannot create Spreadsheet: Spreadsheet with id " + id
                        + " already exists!");
                throw new RequestFailedException("Cannot create Spreadsheet: "
                        + "Spreadsheet with id " + id + " already exists!");
            }
            Spreadsheet spreadsheet = Spreadsheet.fromDTO(event);


            Spreadsheet createdSpreadsheet = this.spreadsheetRepository.saveAndFlush(spreadsheet);

            LOG.debug("Spreadsheet created: " + createdSpreadsheet);
            return createdSpreadsheet.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create Spreadsheet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Spreadsheet: Exception thrown!", e);
        }
    }


    @Override
    public List<SpreadsheetDTO> retrieveAllSpreadsheets()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Spreadsheets.");
        try {

            List<SpreadsheetDTO> generatedDetails = new ArrayList<>();
            for (Spreadsheet spreadsheet : this.spreadsheetRepository.findAll()) {

                generatedDetails.add(spreadsheet.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Spreadsheets failed: No Spreadsheets found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Spreadsheets failed: No Spreadsheets found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " Spreadsheets.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Spreadsheets failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Spreadsheets failed: Exception thrown!", e);
        }
    }



    @Override
    public SpreadsheetDTO retrieveSpreadsheet(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Spreadsheet with id " + id + ".");
        try {

            Spreadsheet spreadsheet = null;
            spreadsheet = this.spreadsheetRepository.findOne(id);

            if (spreadsheet == null) {

                LOG.info("Could not retrieve Spreadsheet: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Spreadsheet: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning Spreadsheet " + spreadsheet);
            return spreadsheet.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Spreadsheet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Spreadsheet: Exception thrown!", e);
        }
    }

    @Override
    public  SpreadsheetDTO updateSpreadsheet(SpreadsheetDTO event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Spreadsheet with id " + event.getId()
                + ".");
        try {

            if (!this.spreadsheetRepository.exists(event.getId())) {

                LOG.info("Could not update Spreadsheet: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Spreadsheet: No object with id "
                                + event.getId() + " found!");
            }

            Spreadsheet spreadsheet = Spreadsheet.fromDTO(event);


            Spreadsheet updatedSpreadsheet = this.spreadsheetRepository.saveAndFlush(spreadsheet);

            return updatedSpreadsheet.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update Spreadsheet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update Spreadsheet: Exception thrown!", e);
        }
    }


    @Override
    public  SpreadsheetDTO deleteSpreadsheet(final SpreadsheetDTO event)
            throws ObjectNotFoundException, RequestFailedException {

        if (event == null) {

            LOG.info("Could not delete Spreadsheet: Object is null!");
            throw new ObjectNotFoundException(
                    "Could not delete Spreadsheet: Object is null!");
        }

        LOG.debug("Attempting to delete Spreadsheet with id " + event.getId()
                + ".");
        try {

            this.spreadsheetRepository.delete(Spreadsheet.fromDTO(event));

            return event;
        }
        catch (Exception e) {

            LOG.warn("Could not delete Spreadsheet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Spreadsheet: Exception thrown!", e);
        }
    }


    @Override
    public SpreadsheetDTO deleteSpreadsheet(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Spreadsheet with id " + id + ".");
        try {

            Spreadsheet spreadsheet = this.spreadsheetRepository.findOne(id);

            if (spreadsheet == null) {

                LOG.info("Could not delete Spreadsheet: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Spreadsheet: No object with id " + id
                                + " found!");
            }

            this.spreadsheetRepository.delete(id);

            return spreadsheet.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Spreadsheet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Spreadsheet: Exception thrown!", e);
        }
    }

    @Override
    public SpreadsheetDTO retrieveSpreadsheetForSpreadsheetCell(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Spreadsheet for SpreadsheetCell with id "
                + id + ".");
        try {

            Spreadsheet spreadsheet = this.spreadsheetRepository.findSpreadsheetForSpreadsheetCell(id);


            if (spreadsheet == null) {


                LOG.info("Could not retrieve Spreadsheet for SpreadsheetCell: No object"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Spreadsheet for SpreadsheetCell: No object"
                                + " found!");
            }
            return spreadsheet.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Spreadsheet: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Spreadsheet: Exception thrown!", e);
        }
    }


    @Override
    public  SpreadsheetCellDTO createSpreadsheetCell(SpreadsheetCellDTO event)
            throws RequestFailedException {

        LOG.debug("Attempting to create SpreadsheetCell.");
        try {
            Integer id = event.getId();

            if (id != null && this.spreadsheetCellRepository.exists(id)) {

                LOG.warn("Cannot create SpreadsheetCell: SpreadsheetCell with id "
                        + id + " already exists!");
                throw new RequestFailedException(
                        "Cannot create SpreadsheetCell: "
                                + "SpreadsheetCell with id " + id
                                + " already exists!");
            }
            SpreadsheetCell spreadsheetCell = SpreadsheetCell.fromDTO(event);


            SpreadsheetCell createdSpreadsheetCell = this.spreadsheetCellRepository
                    .saveAndFlush(spreadsheetCell);

            LOG.debug("SpreadsheetCell created: " + createdSpreadsheetCell);
            return createdSpreadsheetCell.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create SpreadsheetCell: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create SpreadsheetCell: Exception thrown!", e);
        }
    }



    @Override
    public List<SpreadsheetCellDTO> retrieveAllSpreadsheetCells()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all SpreadsheetCells.");
        try {

            List<SpreadsheetCellDTO> generatedDetails = new ArrayList<>();
            for (SpreadsheetCell spreadsheetCell : this.spreadsheetCellRepository.findAll()) {

                generatedDetails.add(spreadsheetCell.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all SpreadsheetCells failed: No SpreadsheetCells found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all SpreadsheetCells failed: No SpreadsheetCells found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " SpreadsheetCells.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all SpreadsheetCells failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all SpreadsheetCells failed: Exception thrown!", e);
        }
    }

    @Override
    public SpreadsheetCellDTO retrieveSpreadsheetCell(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve SpreadsheetCell with id " + id + ".");
        try {

            SpreadsheetCell spreadsheetCell = null;
            spreadsheetCell = this.spreadsheetCellRepository.findOne(id);

            if (spreadsheetCell == null) {

                LOG.info("Could not retrieve SpreadsheetCell: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve SpreadsheetCell: No object with id "
                                + id + " found!");
            }
            LOG.debug("Returning SpreadsheetCell " + spreadsheetCell);
            return spreadsheetCell.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve SpreadsheetCell: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve SpreadsheetCell: Exception thrown!", e);
        }
    }

    @Override
    public  SpreadsheetCellDTO updateSpreadsheetCell(SpreadsheetCellDTO event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update SpreadsheetCell with id "
                + event.getId() + ".");
        try {

            if (!this.spreadsheetCellRepository.exists(event.getId())) {

                LOG.info("Could not update SpreadsheetCell: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update SpreadsheetCell: No object with id "
                                + event.getId() + " found!");
            }

            SpreadsheetCell spreadsheetCell = SpreadsheetCell.fromDTO(event);


            SpreadsheetCell updatedSpreadsheetCell = this.spreadsheetCellRepository
                    .saveAndFlush(spreadsheetCell);

            return updatedSpreadsheetCell.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update SpreadsheetCell: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update SpreadsheetCell: Exception thrown!", e);
        }
    }


    @Override
    public  SpreadsheetCellDTO deleteSpreadsheetCell(final SpreadsheetCellDTO event)
            throws ObjectNotFoundException, RequestFailedException {

        if (event == null) {

            LOG.info("Could not delete Spreadsheet Cell: Object is null!");
            throw new ObjectNotFoundException(
                    "Could not delete Spreadsheet Cell: Object is null!");
        }

        LOG.debug("Attempting to delete Spreadsheet Cell with id "
                + event.getId() + ".");
        try {

            this.spreadsheetCellRepository.delete(SpreadsheetCell.fromDTO(event));

            return event;
        }
        catch (Exception e) {

            LOG.warn("Could not delete Spreadsheet Cell: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Spreadsheet Cell: Exception thrown!", e);
        }
    }

    @Override
    public SpreadsheetCellDTO deleteSpreadsheetCell(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete SpreadsheetCell with id " + id + ".");
        try {

            SpreadsheetCell spreadsheetCell = this.spreadsheetCellRepository.findOne(id);

            if (spreadsheetCell == null) {

                LOG.info("Could not delete SpreadsheetCell: No object with id "
                        + id + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete SpreadsheetCell: No object with id "
                                + id + " found!");
            }

            this.spreadsheetCellRepository.delete(id);

            return spreadsheetCell.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete SpreadsheetCell: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete SpreadsheetCell: Exception thrown!", e);
        }
    }

}
