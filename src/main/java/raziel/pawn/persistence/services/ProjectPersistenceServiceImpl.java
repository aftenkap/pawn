package raziel.pawn.persistence.services;



import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.ProjectDetails;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.persistence.domain.Project;
import raziel.pawn.persistence.repository.ProjectRepository;


/**
 * @author Peter J. Radics
 * @version 0.0.1
 * @since 0.0.1
 *
 */
@Service
@Transactional
public class ProjectPersistenceServiceImpl
        implements ProjectPersistenceService {


    private static Logger           LOG = LoggerFactory
                                                .getLogger(ProjectPersistenceServiceImpl.class);

    private final ProjectRepository projectRepository;

    /**
     * Creates a new instance of the {@link ProjectPersistenceServiceImpl}
     * class.
     * 
     * @param projectRepository
     *            the project repository.
     */
    @Autowired
    public ProjectPersistenceServiceImpl(
            final ProjectRepository projectRepository) {

        this.projectRepository = projectRepository;
    }



    @Override
    public ProjectDetails createProject(ProjectDetails event)
            throws RequestFailedException {

        LOG.debug("Attempting to create Project.");
        try {
            Integer id = event.getId();

            if (id != null && this.projectRepository.exists(id)) {

                LOG.warn("Cannot create Project: Project with id " + id
                        + " already exists!");
                throw new RequestFailedException("Cannot create Project: "
                        + "Project with id " + id + " already exists!");
            }
            Project project = Project.fromDetails(event);


            Project createdProject = this.projectRepository
                    .saveAndFlush(project);

            LOG.debug("Project created: " + createdProject);
            return createdProject.toDTO();
        }
        catch (Exception e) {

            if (e instanceof RequestFailedException) {

                throw e;
            }
            LOG.warn("Could not create Project: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not create Project: Exception thrown!", e);
        }
    }



    @Override
    public List<ProjectDetails> retrieveAllProjects()
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve all Projects.");
        try {

            List<ProjectDetails> generatedDetails = new ArrayList<>();
            for (Project project : this.projectRepository.findAll()) {

                generatedDetails.add(project.toDTO());
            }

            if (generatedDetails.isEmpty()) {

                LOG.info("Retrieval of all Projects failed: No Projects found!");
                throw new ObjectNotFoundException(
                        "Retrieval of all Projects failed: No Projects found!");
            }

            LOG.debug("Returning " + generatedDetails.size() + " Projects.");
            return generatedDetails;

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Retrieval of all Projects failed: Exception thrown!", e);
            throw new RequestFailedException(
                    "Retrieval of all Projects failed: Exception thrown!", e);
        }
    }


    @Override
    public ProjectDetails retrieveProject(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Project with id " + id + ".");
        try {

            Project project = null;
            project = this.projectRepository.findOne(id);

            if (project == null) {

                LOG.info("Could not retrieve Project: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Project: No object with id " + id
                                + " found!");
            }
            LOG.debug("Returning Project " + project);
            return project.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Project: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Project: Exception thrown!", e);
        }
    }

    @Override
    public ProjectDetails retrieveProjectForDataSource(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Project for DataSource with id " + id
                + ".");
        try {

            Project project = this.projectRepository
                    .findProjectForDataSource(id);


            if (project == null) {


                LOG.info("Could not retrieve Project for DataSource: No object"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Project for DataSource: No object"
                                + " found!");
            }
            return project.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not retrieve Project: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not retrieve Project: Exception thrown!", e);
        }
    }

    @Override
    public ProjectDetails retrieveProjectForMetadataSource(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Project for MetadataSource with id "
                + id + ".");
        try {

            Project project = this.projectRepository
                    .findProjectForMetadataSource(id);


            if (project == null) {


                LOG.info("Could not retrieve Project for MetadataSource: No object"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Project for MetadataSource: No object"
                                + " found!");
            }
            return project.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn(
                    "Could not retrieve Project for MetadataSource: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Could not retrieve Project for MetadataSource: Exception thrown!",
                    e);
        }
    }

    @Override
    public ProjectDetails retrieveProjectForDataModel(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to retrieve Project for DataModel with id " + id
                + ".");
        try {

            Project project = this.projectRepository
                    .findProjectForDataModel(id);


            if (project == null) {


                LOG.info("Could not retrieve Project for DataModel: No object"
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not retrieve Project for DataModel: No object"
                                + " found!");
            }
            return project.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn(
                    "Could not retrieve Project for DataModel: Exception thrown!",
                    e);
            throw new RequestFailedException(
                    "Could not retrieve Project for DataModel: Exception thrown!",
                    e);
        }
    }

    @Override
    public ProjectDetails updateProject(ProjectDetails event)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to update Project with id " + event.getId() + ".");
        try {

            if (!this.projectRepository.exists(event.getId())) {

                LOG.info("Could not update Project: No object with id "
                        + event.getId() + " found!");
                throw new ObjectNotFoundException(
                        "Could not update Project: No object with id "
                                + event.getId() + " found!");
            }

            Project project = Project.fromDetails(event);


            Project updatedProject = this.projectRepository
                    .saveAndFlush(project);

            return updatedProject.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not update Project: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not update Project: Exception thrown!", e);
        }
    }



    @Override
    public ProjectDetails deleteProject(final int id)
            throws ObjectNotFoundException, RequestFailedException {

        LOG.debug("Attempting to delete Project with id " + id + ".");
        try {

            Project project = this.projectRepository.findOne(id);

            if (project == null) {

                LOG.info("Could not delete Project: No object with id " + id
                        + " found!");
                throw new ObjectNotFoundException(
                        "Could not delete Project: No object with id " + id
                                + " found!");
            }

            this.projectRepository.delete(id);

            return project.toDTO();

        }
        catch (Exception e) {

            if (e instanceof ObjectNotFoundException) {

                throw e;
            }
            LOG.warn("Could not delete Project: Exception thrown!", e);
            throw new RequestFailedException(
                    "Could not delete Project: Exception thrown!", e);
        }
    }
}
