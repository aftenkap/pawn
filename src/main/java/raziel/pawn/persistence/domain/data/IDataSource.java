package raziel.pawn.persistence.domain.data;


import java.util.List;

import raziel.pawn.dto.data.DataSourceDetails;
import raziel.pawn.persistence.domain.IDomainObject;


/**
 *
 *
 * @author Peter J. Radics
 * @version 0.1
 */
public interface IDataSource
        extends IDomainObject {

    /**
     * Returns the location of the data source.
     *
     * @return the location of the data source.
     */
    public abstract String getURI();

    /**
     * Sets the URI.
     *
     * @param uri
     *            the URI.
     */
    public abstract void setURI(final String uri);


    /**
     * Returns the {@link IDataPoint DataPoints} of this {@link IDataSource}.
     *
     * @return the {@link IDataPoint DataPoints}.
     */
    public abstract List<? extends IDataPoint<?>> getDataPoints();

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public DataSourceDetails toDTO();
}
