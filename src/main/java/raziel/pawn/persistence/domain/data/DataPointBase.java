package raziel.pawn.persistence.domain.data;


import javax.persistence.DiscriminatorColumn;

import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.persistence.domain.DomainObject;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @param <T>
 *            the content type of the data point.
 */
@Entity(name = "DataPoint")
@Table(name = "datapoints")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "_type")
public abstract class DataPointBase<T>
        extends DomainObject
        implements IDataPoint<T> {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link DataPointBase} class.
     */
    protected DataPointBase() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DataPointBase} class with the
     * provided name.
     * 
     * @param name
     *            the name.
     */
    public DataPointBase(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link DataPointBase} class from the
     * provided details.
     * 
     * @param details
     *            the details of the data point.
     */
    public DataPointBase(DataPointDTO details) {

        super(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails(boolean)
     */
    @Override
    public DataPointDTO toDTO() {

        return new DataPointDTO(super.toDTO());
    }
}
