package raziel.pawn.persistence.domain.data;


import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
@Entity
@DiscriminatorValue("Transcript")
@Table(name = "transcripts")
@AttributeOverrides({

        @AttributeOverride(name = "id", column = @Column(name = "_id",
                table = "datasources")),
        @AttributeOverride(name = "version", column = @Column(
                name = "_version", table = "datasources")),
        @AttributeOverride(name = "name", column = @Column(name = "name",
                table = "datasources")) })
public class Transcript
        extends DataSourceBase {

    /**
     * The Serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public List<IDataPoint<?>> getDataPoints() {

        return null;
    }

}
