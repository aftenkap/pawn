package raziel.pawn.persistence.domain.data;


import static javax.persistence.CascadeType.ALL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.data.SpreadsheetDTO;



/**
 * The generic {@link Spreadsheet} class models a two-dimensional table of
 * arbitrary data stored in {@link SpreadsheetCell Cells}.
 * <p/>
 * This class serves as a database wrapper of the {@link Spreadsheet} class and
 * provides integration of tables as {@link IDataSource DataSources}.
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
@Entity(name = "Spreadsheet")
@javax.persistence.Table(name = "spreadsheets")
@DiscriminatorValue("Spreadsheet")
@AttributeOverrides({

        @AttributeOverride(name = "id", column = @Column(name = "_id",
                table = "datasources")),
        @AttributeOverride(name = "version", column = @Column(
                name = "_version", table = "datasources")),
        @AttributeOverride(name = "name", column = @Column(name = "name",
                table = "datasources")) })
public class Spreadsheet
        extends DataSourceBase {


    private static final long     serialVersionUID = 1L;

    @SuppressWarnings("unused")
    private static Logger         LOG              = LoggerFactory
                                                           .getLogger(Spreadsheet.class);

    @OneToMany(cascade = ALL, orphanRemoval = true)
    @JoinTable(name = "datasource_datapoints", joinColumns = @JoinColumn(
            name = "_dataSourceID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(
                    name = "FK_datasource_datapoints_dataSourceID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_dataPointID",
                    referencedColumnName = "_id", foreignKey = @ForeignKey(
                            name = "FK_datasource_datapoints_dataPointID",
                            value = ConstraintMode.CONSTRAINT)))
    private List<SpreadsheetCell> cells;

    /**
     * Returns the cells.
     * 
     * @return the cells.
     */
    public List<SpreadsheetCell> getCells() {


        return this.cells;
    }

    /**
     * Sets the cells
     * 
     * @param cells
     *            the cells.
     */
    protected void setCells(Collection<SpreadsheetCell> cells) {

        this.cells.clear();
        for (SpreadsheetCell cell : cells) {

            this.cells.add(cell);
        }
    }

    @Override
    public List<SpreadsheetCell> getDataPoints() {

        return this.getCells();
    }



    /**
     * Creates a new instance of the {@link Spreadsheet} class.
     */
    protected Spreadsheet() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Spreadsheet} class.
     * 
     * @param name
     *            the name of the data table.
     */
    public Spreadsheet(String name) {

        super(name);

        this.cells = new LinkedList<>();
    }


    /**
     * Creates a new instance of the {@link Spreadsheet} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Spreadsheet(SpreadsheetDTO details) {

        super(details);
        this.cells = new LinkedList<>();

        if (details.getCells() != null) {

            for (SpreadsheetCellDTO cell : details.getCells()) {

                this.cells.add(SpreadsheetCell.fromDTO(cell));
            }
        }
    }

    @Override
    public SpreadsheetDTO toDTO() {

        SpreadsheetDTO details = new SpreadsheetDTO(super.toDTO());

        details.setUri(this.getURI());

        List<SpreadsheetCellDTO> cellDetails = new ArrayList<>(
                this.cells.size());

        for (SpreadsheetCell cell : this.cells) {

            cellDetails.add(cell.toDTO());
        }
        details.setCells(cellDetails);

        return details;
    }

    /**
     * Creates a {@link Spreadsheet} corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return the {@link Spreadsheet} corresponding to the details.
     */
    public static Spreadsheet fromDTO(SpreadsheetDTO details) {

        return new Spreadsheet(details);
    }
}
