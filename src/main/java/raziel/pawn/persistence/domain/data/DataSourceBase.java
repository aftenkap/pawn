package raziel.pawn.persistence.domain.data;


import static javax.persistence.InheritanceType.JOINED;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.Table;

import raziel.pawn.dto.data.DataSourceDetails;
import raziel.pawn.persistence.domain.DomainObject;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
@Inheritance(strategy = JOINED)
@Entity(name = "DataSource")
@Table(name = "datasources")
@DiscriminatorColumn(name = "_type")
public abstract class DataSourceBase
        extends DomainObject
        implements IDataSource {

    /**
     * The serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @Basic
    @Column(name = "uri")
    private String            uri;


    @Override
    public String getURI() {

        return this.uri;
    }

    @Override
    public void setURI(String uri) {

        this.uri = uri;
    }

    /**
     * Creates a new instance of the {@link DataSourceBase} class.
     */
    public DataSourceBase() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DataSourceBase} class.
     * 
     * @param name
     *            the name of the data source.
     */
    public DataSourceBase(final String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link DataSourceBase} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected DataSourceBase(DataSourceDetails details) {

        super(details);

        this.uri = details.getUri();
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public DataSourceDetails toDTO() {

        DataSourceDetails details = new DataSourceDetails(super.toDTO());

        details.setUri(this.uri);

        return details;
    }
}
