package raziel.pawn.persistence.domain.data;



import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.dto.data.SpreadsheetCellDTO;



/**
 * The generic {@link SpreadsheetCell} class is a container class for elements
 * that are referenced by two indices.
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
@Entity
@Table(name = "spreadsheetcells")
@DiscriminatorValue("SpreadsheetCell")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "_id",
                table = "datapoints")),
        @AttributeOverride(name = "version", column = @Column(
                name = "_version", table = "datapoints")),
        @AttributeOverride(name = "name", column = @Column(name = "name",
                table = "datapoints")) })
public class SpreadsheetCell
        extends DataPointBase<String>
        implements Comparable<SpreadsheetCell> {


    @SuppressWarnings("unused")
    private static Logger     LOG              = LoggerFactory
                                                       .getLogger(SpreadsheetCell.class);
    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @Basic
    @Column(name = "_row")
    private int               row;

    @Basic
    @Column(name = "_column")
    private int               column;

    @Basic
    @Column(name = "value")
    private String            value;


    /**
     * Returns the row.
     * 
     * @return the row.
     */
    public int getRow() {

        return this.row;
    }

    /**
     * Sets the row.
     * 
     * @param row
     *            the row.
     */
    public void setRow(int row) {

        this.row = row;
    }

    /**
     * Returns the column.
     * 
     * @return the column.
     */
    public int getColumn() {

        return this.column;
    }

    /**
     * Sets the column.
     * 
     * @param column
     *            the column.
     */
    public void setColumn(int column) {

        this.column = column;
    }

    /**
     * Returns the value.
     * 
     * @return the value.
     */
    @Override
    public String getValue() {

        return this.value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     *            the value.
     */
    @Override
    public void setValue(String value) {

        this.value = value;
    }

    /**
     * Creates a new instance of the {@link SpreadsheetCell} class.
     */
    public SpreadsheetCell() {

        this(0, 0);
    }

    /**
     * 
     * Creates a new instance of the {@link SpreadsheetCell} class.
     * 
     * @param row
     *            the row.
     * @param column
     *            the column.
     */
    public SpreadsheetCell(int row, int column) {

        this(row, column, null);
    }

    /**
     * Creates a new instance of the {@link SpreadsheetCell} class with the
     * provided value.
     * 
     * @param row
     *            the row.
     * @param column
     *            the column.
     * @param value
     *            the value.
     */
    public SpreadsheetCell(int row, int column, String value) {

        super("SpreadsheetCell: (" + row + ", " + column + ")");

        this.row = row;
        this.column = column;
        this.value = value;
    }


    /**
     * Creates a new instance of the {@link SpreadsheetCell} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected SpreadsheetCell(SpreadsheetCellDTO details) {

        super(details);

        this.row = details.getRow();
        this.column = details.getColumn();
        this.value = details.getValue();
    }

    @Override
    public int compareTo(SpreadsheetCell other) {

        int compareRows = Integer.compare(this.getRow(), other.getRow());

        // If rows are equal, determine order by column index order.
        if (compareRows == 0) {

            return Integer.compare(this.getColumn(), other.getColumn());
        }

        // If rows are not equal, determine order by row index order.
        return compareRows;
    }

    @Override
    public SpreadsheetCellDTO toDTO() {

        SpreadsheetCellDTO dto = new SpreadsheetCellDTO(super.toDTO());

        dto.setRow(this.getRow());
        dto.setColumn(this.getColumn());
        dto.setValue(this.getValue());

        return dto;
    }

    /**
     * Returns a SpreadsheetCell corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a SpreadsheetCell corresponding to the provided details.
     */
    public static SpreadsheetCell fromDTO(SpreadsheetCellDTO details) {

        if (details == null) {

            return null;
        }

        return new SpreadsheetCell(details);
    }
}
