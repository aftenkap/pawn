package raziel.pawn.persistence.domain.modeling.terminology;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AssociationDetails;
import raziel.pawn.dto.modeling.AssociativeRoleDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.EndurantDetails;
import raziel.pawn.dto.modeling.RoleDetails;
import raziel.pawn.persistence.domain.modeling.IInstance;
import raziel.pawn.persistence.domain.modeling.assertions.Association;


/**
 * The {@code AssociativeRole} class models a connection between two or more
 * {@link Endurant Endurants}.
 * <p>
 * The {@link IInstance Instances} of {@code AssociativeRoles} are
 * {@link Association Associations}.
 * <p>
 * Since {@code AssociativeRoles} do not relate {@link Perdurant Perdurants},
 * they are considered {@link StaticRole StaticRoles}.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
@DiscriminatorValue(value = "AssociativeRole")
public class AssociativeRole
        extends StaticRole<Endurant, Endurant, Association> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@code AssociativeRole} class
     * (Serialization Constructor).
     */
    protected AssociativeRole() {

        this("");
    }

    /**
     * Creates a new instance of the {@code AssociativeRole} class with the
     * provided name.
     * 
     * @param name
     *            the name of this {@code AssociativeRole}.
     */
    public AssociativeRole(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@code AssociativeRole} class. (Copy
     * Constructor)
     * 
     * @param roleToCopy
     *            the {@code AssociativeRole} to copy.
     */
    public AssociativeRole(AssociativeRole roleToCopy) {

        super(roleToCopy);
    }

    /**
     * Creates a new instance of the {@link AssociativeRole} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public AssociativeRole(AssociativeRoleDetails details) {

        super(details);
        if (details.getDomainConcepts() != null) {

            for (ConceptDetails domainConcept : details.getDomainConcepts()) {

                if (domainConcept instanceof EndurantDetails) {

                    this.addDomain(Endurant
                            .fromDetails((EndurantDetails) domainConcept));
                }
            }
        }
        if (details.getRangeConcepts() != null) {

            for (ConceptDetails rangeConcept : details.getRangeConcepts()) {

                if (rangeConcept instanceof EndurantDetails) {

                    this.addRange(Endurant
                            .fromDetails((EndurantDetails) rangeConcept));
                }
            }
        }
        if (details.getDescendants() != null) {

            for (RoleDetails descendant : details.getDescendants()) {
                if (descendant instanceof AssociativeRoleDetails) {

                    this.addDescendant(AssociativeRole
                            .fromDetails((AssociativeRoleDetails) descendant));
                }
            }
        }
        if (details.getInstances() != null) {

            for (AssertionDetails instance : details.getInstances()) {

                if (instance instanceof AssociationDetails) {

                    this.addInstance(Association
                            .fromDetails((AssociationDetails) instance));
                }
            }
        }
    }



    @Override
    public AssociativeRoleDetails toDTO() {

        AssociativeRoleDetails details = new AssociativeRoleDetails(
                super.toDTO());
        return details;
    }

    /**
     * Returns an InteractiveRole corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an InteractiveRole corresponding to the provided details.
     */
    public static AssociativeRole fromDetails(
            final AssociativeRoleDetails details) {

        return new AssociativeRole(details);
    }
}
