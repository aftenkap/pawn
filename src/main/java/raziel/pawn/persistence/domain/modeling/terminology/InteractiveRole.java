package raziel.pawn.persistence.domain.modeling.terminology;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.InteractionDetails;
import raziel.pawn.dto.modeling.InteractiveRoleDetails;
import raziel.pawn.dto.modeling.PerdurantDetails;
import raziel.pawn.dto.modeling.RoleDetails;
import raziel.pawn.persistence.domain.modeling.IInstance;
import raziel.pawn.persistence.domain.modeling.assertions.Interaction;


/**
 * The {@code InteractiveRole} class models a connection between two or more
 * {@link Perdurant Perdurants}. The {@link IInstance Instances} of
 * {@code InteractiveRoles} are {@link Interaction Interactions}.
 * <p>
 * Since {@code InteractiveRoles} relate {@link Perdurant Perdurants}, they are
 * considered {@link DynamicRole DynamicRoles}.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
@DiscriminatorValue(value = "InteractiveRole")
public class InteractiveRole
        extends DynamicRole<Perdurant, Perdurant, Interaction> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link InteractiveRole} class
     * (Serialization Constructor).
     */
    protected InteractiveRole() {

        this("");
    }

    /**
     * Creates a new instance of the {@link InteractiveRole} class with the
     * provided name.
     * 
     * @param name
     *            the name of this {@link InteractiveRole}.
     */
    public InteractiveRole(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link InteractiveRole} class. (Copy
     * Constructor)
     * 
     * @param relationToCopy
     *            the relation to copy.
     */
    public InteractiveRole(InteractiveRole relationToCopy) {

        super(relationToCopy);
    }

    /**
     * Creates a new instance of the {@link DynamicRole} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public InteractiveRole(final InteractiveRoleDetails details) {

        super(details);
        if (details.getDomainConcepts() != null) {

            for (ConceptDetails domainConcept : details.getDomainConcepts()) {

                if (domainConcept instanceof PerdurantDetails) {

                    this.addDomain(Perdurant
                            .fromDetails((PerdurantDetails) domainConcept));
                }
            }
        }
        if (details.getRangeConcepts() != null) {

            for (ConceptDetails rangeConcept : details.getRangeConcepts()) {

                if (rangeConcept instanceof PerdurantDetails) {

                    this.addRange(Perdurant
                            .fromDetails((PerdurantDetails) rangeConcept));
                }
            }
        }
        if (details.getDescendants() != null) {

            for (RoleDetails descendant : details.getDescendants()) {
                if (descendant instanceof InteractiveRoleDetails) {

                    this.addDescendant(InteractiveRole
                            .fromDetails((InteractiveRoleDetails) descendant));
                }
            }
        }

        if (details.getInstances() != null) {

            for (AssertionDetails instance : details.getInstances()) {

                if (instance instanceof InteractionDetails) {

                    this.addInstance(Interaction
                            .fromDetails((InteractionDetails) instance));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.terminology.Role#toDetails()
     */
    @Override
    public InteractiveRoleDetails toDTO() {

        InteractiveRoleDetails details = new InteractiveRoleDetails(
                super.toDTO());
        return details;
    }

    /**
     * Returns an InteractiveRole corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an InteractiveRole corresponding to the provided details.
     */
    public static InteractiveRole fromDetails(
            final InteractiveRoleDetails details) {

        return new InteractiveRole(details);
    }
}
