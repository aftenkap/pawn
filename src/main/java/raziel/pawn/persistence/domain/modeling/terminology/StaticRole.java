package raziel.pawn.persistence.domain.modeling.terminology;


import javax.persistence.Entity;

import raziel.pawn.dto.modeling.StaticRoleDetails;
import raziel.pawn.persistence.domain.modeling.IConcept;
import raziel.pawn.persistence.domain.modeling.IConstant;
import raziel.pawn.persistence.domain.modeling.IRole;
import raziel.pawn.persistence.domain.modeling.assertions.PermanentRelationship;


/**
 * The {@code StaticRole} class defines a connection between {@link IConcept
 * Concepts} that holds for an unlimited duration.
 * <p>
 * Instances of {@code StaticRoles} are {@link PermanentRelationship
 * PermanentRelationships}.
 * 
 * @param <DOMAIN>
 *            the domain type.
 * @param <RANGE>
 *            the range type.
 * @param <INSTANCE>
 *            the instance type.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
public abstract class StaticRole<DOMAIN extends IConcept<?>, RANGE extends IConcept<?>, INSTANCE extends PermanentRelationship<?, ?>>
        extends Role<DOMAIN, RANGE, INSTANCE>
        implements IConstant {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link StaticRole} class (Serialization
     * Constructor).
     */
    protected StaticRole() {

        this("");
    }

    /**
     * Creates a new instance of the {@link StaticRole} class with the provided
     * name.
     * 
     * @param name
     *            the name of this {@link StaticRole}.
     */
    public StaticRole(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link StaticRole} class. (Copy
     * Constructor)
     * 
     * @param relationToCopy
     *            the relation to copy.
     */
    public StaticRole(IRole<DOMAIN, RANGE, INSTANCE> relationToCopy) {

        super(relationToCopy);
    }

    /**
     * Creates a new instance of the {@link StaticRole} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected StaticRole(final StaticRoleDetails details) {

        super(details);

    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public StaticRoleDetails toDTO() {

        StaticRoleDetails details = new StaticRoleDetails(super.toDTO());

        return details;
    }

}