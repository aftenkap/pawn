package raziel.pawn.persistence.domain.modeling.terminology;



import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.persistence.domain.modeling.IConcept;
import raziel.pawn.persistence.domain.modeling.IInstance;
import raziel.pawn.persistence.domain.modeling.Term;


/**
 * The {@code Concept} class models a 
 * 
 * @author Peter J. Radics
 * @param <T>
 *            the instance type.
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
public abstract class Concept<T extends IInstance>
        extends Term<T>
        implements IConcept<T> {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;



    @ManyToMany(targetEntity = Concept.class)
    @JoinTable(name = "concept_hierarchy", joinColumns = @JoinColumn(
            name = "_parentConceptID", nullable = false,
            referencedColumnName = "_id", foreignKey = @ForeignKey(
                    name = "FK_concept-hierarchy_parentConceptID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_childConceptID",
                    referencedColumnName = "_id", nullable = false,
                    foreignKey = @ForeignKey(
                            name = "FK_concept-hierarchy_childConceptID",
                            value = ConstraintMode.CONSTRAINT)))
    private List<IConcept<T>> descendants;

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IConcept#getDescendants()
     */
    @Override
    public List<IConcept<T>> descendants() {

        return this.descendants;
    }


    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.IConcept#addDescendant(raziel
     * .pawn.persistence.domain.modeling.IConcept)
     */
    @Override
    public boolean addDescendant(final IConcept<T> descendant) {

        return this.descendants.add(descendant);
    }


    /*
     * (non-Javadoc)
     * 
     * @see
     * 
     * raziel.pawn.persistence.domain.modeling.IConcept#removeDescendant(raziel
     * .pawn.persistence.domain.modeling.IConcept)
     */
    @Override
    public boolean removeDescendant(final IConcept<T> descendant) {

        return this.descendants.remove(descendant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IConcept#clearDescendants()
     */
    @Override
    public void clearDescendants() {

        this.descendants.clear();
    }

    /**
     * Creates a new instance of the {@link Concept} class.
     */
    protected Concept() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Concept} class with the provided
     * name.
     * 
     * @param name
     *            the name.
     */
    public Concept(String name) {

        super(name);

        this.descendants = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Concept} class. (Copy Constructor)
     * 
     * @param conceptToCopy
     *            the concept to copy.
     */
    public Concept(IConcept<T> conceptToCopy) {

        super(conceptToCopy);

        this.descendants = new LinkedList<>(conceptToCopy.descendants());
    }


    /**
     * Creates a new instance of the {@link Concept} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Concept(ConceptDetails details) {

        super(details);
        this.descendants = new LinkedList<>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.Term#toDetails()
     */
    @Override
    public ConceptDetails toDTO() {

        ConceptDetails details = new ConceptDetails(super.toDTO());


        List<ConceptDetails> descendants = new ArrayList<>(
                this.descendants.size());
        for (IConcept<T> descendant : this.descendants) {

            descendants.add(descendant.toDTO());
        }
        details.setDescendants(descendants);

        return details;
    }
}
