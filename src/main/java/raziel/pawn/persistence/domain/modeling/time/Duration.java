package raziel.pawn.persistence.domain.modeling.time;



import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import raziel.pawn.dto.modeling.DurationDetails;
import raziel.pawn.persistence.domain.DomainObject;
import raziel.pawn.persistence.domain.modeling.IAssertion;
import raziel.pawn.persistence.domain.modeling.IFluent;


/**
 * The {@link Duration} class provides the capability to specify the time
 * interval of validity of an {@link IAssertion Assertion}. Temporally
 * restricted Assertions are called {@link IFluent FLuents}.
 * <p/>
 * This also allows us to specify an {@link DurationOrder Order} on the
 * durations.
 * <p/>
 * {@link Duration Durations} need not be defined, explicitly defined by
 * specific {@link SemiInterval Start} and {@link SemiInterval End} points,
 * implicitly defined through a {@link Date concrete duration}, or any
 * combination thereof.
 *
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Entity
@Table(name = "durations")
public class Duration
        extends DomainObject {

    /**
     * The serial version id.
     */
    private static final long serialVersionUID = 1L;


    @OneToOne(orphanRemoval = false,
            targetEntity = SemiInterval.class)
    @JoinColumn(name = "start", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_durations_start",
                    value = ConstraintMode.CONSTRAINT))
    private SemiInterval      start;


    @OneToOne(orphanRemoval = false,
            targetEntity = SemiInterval.class)
    @JoinColumn(name = "end", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_durations_end",
                    value = ConstraintMode.CONSTRAINT))
    private SemiInterval      end;


    @Basic
    @Column(name = "duration")
    private Date              duration;


    /**
     * Returns the {@SemiInterval Start} of the interval.
     *
     * @return the start.
     */
    public SemiInterval getStart() {

        if (this.start == null) {

            this.start = new SemiInterval(this.getName() + " (Start)");
        }
        return start;
    }


    /**
     * Sets the {@SemiInterval Start} of the interval.
     *
     * @param start
     *            the start.
     */
    public void setStart(SemiInterval start) {

        this.start = start;
    }


    /**
     * Returns the {@SemiInterval End} of the interval.
     *
     * @return the end.
     */
    public SemiInterval getEnd() {

        if (this.end == null) {

            this.end = new SemiInterval(this.getName() + " (End)");
        }
        return end;
    }


    /**
     * Sets the {@SemiInterval End} of the interval.
     *
     * @param end
     *            the end.
     */
    public void setEnd(SemiInterval end) {

        this.end = end;
    }


    /**
     * Returns the {@Date Duration} of the interval.
     *
     * @return the duration.
     */
    public Date getDuration() {

        return duration;
    }


    /**
     * Returns the {@Date Duration} of the interval.
     *
     * @param duration
     *            the duration.
     */
    public void setDuration(Date duration) {

        this.duration = duration;
    }


    /**
     * Creates a new instance of the {@link Duration} class.
     */
    protected Duration() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Duration} class with the provided
     * name.
     *
     * @param name
     *            the name.
     */
    public Duration(String name) {

        super(name);
        this.start = null;
        this.end = null;
        this.duration = null;
    }

    /**
     * Creates a new instance of the {@link Duration} class. (Copy Constructor)
     * 
     * @param durationToCopy
     *            the duration to copy.
     */
    public Duration(final Duration durationToCopy) {

        super(durationToCopy);
        this.start = durationToCopy.getStart();
        this.end = durationToCopy.getEnd();
        this.duration = durationToCopy.getDuration();
    }

    /**
     * Creates a new instance of the {@link Duration} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Duration(final DurationDetails details) {

        super(details);

        if (details.getStart() != null) {

            this.start = SemiInterval.fromDetails(details.getStart());
        }

        if (details.getEnd() != null) {

            this.end = SemiInterval.fromDetails(details.getEnd());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public DurationDetails toDTO() {

        DurationDetails details = new DurationDetails(super.toDTO());


        if (this.start != null) {

            details.setStart(this.start.toDTO());
        }
        if (this.end != null) {

            details.setEnd(this.end.toDTO());
        }
        details.setDuration(this.duration);

        return details;
    }

    /**
     * Returns a Duration corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a Duration corresponding to the provided details.
     */
    public static Duration fromDetails(final DurationDetails details) {

        return new Duration(details);
    }
}
