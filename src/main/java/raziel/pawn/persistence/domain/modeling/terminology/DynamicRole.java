package raziel.pawn.persistence.domain.modeling.terminology;


import javax.persistence.Entity;

import raziel.pawn.dto.modeling.DynamicRoleDetails;
import raziel.pawn.persistence.domain.modeling.IConcept;
import raziel.pawn.persistence.domain.modeling.IRole;
import raziel.pawn.persistence.domain.modeling.assertions.TemporaryRelationship;


/**
 * The {@code DynamicRole} class defines a connection between {@link IConcept
 * Concepts} that holds for a limited duration.
 * <p>
 * Instances of {@code DynamicRoles} are {@link TemporaryRelationship
 * TemporaryRelationships}.
 * 
 * @param <DOMAIN>
 *            the domain type.
 * @param <RANGE>
 *            the range type.
 * @param <INSTANCE>
 *            the instance type.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
public abstract class DynamicRole<DOMAIN extends IConcept<?>, RANGE extends IConcept<?>, INSTANCE extends TemporaryRelationship<?, ?>>
        extends Role<DOMAIN, RANGE, INSTANCE> {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link DynamicRole} class (Serialization
     * Constructor).
     */
    protected DynamicRole() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DynamicRole} class with the provided
     * name.
     * 
     * @param name
     *            the name of this {@link DynamicRole}.
     */
    public DynamicRole(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link DynamicRole} class. (Copy
     * Constructor)
     * 
     * @param relationToCopy
     *            the relation to copy.
     */
    public DynamicRole(IRole<DOMAIN, RANGE, INSTANCE> relationToCopy) {

        super(relationToCopy);
    }

    /**
     * Creates a new instance of the {@link DynamicRole} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public DynamicRole(DynamicRoleDetails details) {

        super(details);
    }


    @Override
    public DynamicRoleDetails toDTO() {

        DynamicRoleDetails details = new DynamicRoleDetails(super.toDTO());
        return details;
    }
}
