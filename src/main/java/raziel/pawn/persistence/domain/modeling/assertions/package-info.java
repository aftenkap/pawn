/**
 * This package contains all forms of assertions.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
package raziel.pawn.persistence.domain.modeling.assertions;