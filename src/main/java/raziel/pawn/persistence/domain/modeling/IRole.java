package raziel.pawn.persistence.domain.modeling;


import java.util.List;

import raziel.pawn.dto.modeling.RoleDetails;



/**
 * @param <DOMAIN>
 *            the domain type.
 * @param <RANGE>
 *            the range type.
 * @param <INSTANCE>
 *            the instance type.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public interface IRole<DOMAIN extends ITerm<?>, RANGE extends ITerm<?>, INSTANCE extends IRelationship<?, ?>>
        extends ITerm<INSTANCE> {

    /**
     * Returns the domains of this {@link IRole}.
     * 
     * @return the domains of this {@link IRole}.
     */
    public abstract List<DOMAIN> getDomains();


    /**
     * Adds the provided {@link IConcept Concept} to the domains of this
     * {@link IRole}.
     * 
     * @param domain
     *            the {@link IConcept Concept} to add.
     * @return {@code true}, if the domains changed as a result of the call.
     */
    public abstract boolean addDomain(DOMAIN domain);

    /**
     * Removes the provided {@link IConcept Concept} from the domains of this
     * {@link IRole}.
     * 
     * @param domain
     *            the {@link IConcept Concept} to remove.
     * @return {@code true}, if a domain was removed as a result of this call.
     */
    public abstract boolean removeDomain(DOMAIN domain);

    /**
     * Removes all domains.
     */
    public abstract void clearDomains();

    /**
     * Returns the ranges of this {@link IRole}.
     * 
     * @return the ranges of this {@link IRole}.
     */
    public abstract List<RANGE> getRanges();

    /**
     * Adds the provided {@link IConcept Concept} to the ranges of this
     * {@link IRole}.
     * 
     * @param range
     *            the {@link IConcept Concept} to add.
     * @return {@code true}, if the ranges changed as a result of the call.
     */
    public abstract boolean addRange(RANGE range);

    /**
     * Removes the provided {@link IConcept Concept} from the ranges of this
     * {@link IRole}.
     * 
     * @param range
     *            the {@link IConcept Concept} to remove.
     * @return {@code true}, if a range was removed as a result of this call.
     */
    public abstract boolean removeRange(RANGE range);

    /**
     * Removes all ranges.
     */
    public abstract void clearRanges();

    /**
     * Returns the list of Descendants of this {@link IRole Role}.
     * 
     * @return the list of Descendants of this {@link IRole Role}.
     */
    public abstract List<IRole<DOMAIN, RANGE, INSTANCE>> descendants();

    /**
     * Adds the provided{@link IRole Role} to the descendants of this
     * {@link IRole Role}.
     * 
     * @param descendant
     *            the {@link IRole Role} to be added.
     * @return {@code true}, if the collection was changed as a result of this
     *         operation; {@code false} otherwise.
     */
    public abstract boolean addDescendant(
            final IRole<DOMAIN, RANGE, INSTANCE> descendant);

    /**
     * Removes the provided{@link IRole Role} from the descendants of this
     * {@link IRole Role}.
     * 
     * @param descendant
     *            the {@link IRole Role} to be removed.
     * @return {@code true}, if the collection was changed as a result of this
     *         operation; {@code false} otherwise.
     */
    public abstract boolean removeDescendant(
            final IRole<DOMAIN, RANGE, INSTANCE> descendant);

    /**
     * Clears the descendants of this {@link IRole Role}.
     */
    public abstract void clearDescendants();

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public abstract RoleDetails toDTO();
}
