package raziel.pawn.persistence.domain.modeling.time;


import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import raziel.pawn.dto.modeling.SemiIntervalOrderDetails;
import raziel.pawn.persistence.domain.DomainObject;


/**
 * The {@link SemiIntervalOrder} class provides the capability of specifying an
 * order on {@link SemiInterval SemiIntervals}.
 *
 * @author Peter J. Radics
 * @version 0.1
 *
 */
@Entity
@Table(name = "semiintervalorders")
public class SemiIntervalOrder
        extends DomainObject {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    @OneToOne
    @JoinColumn(name = "_operand1", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_semiintervalorder_operand1",
                    value = ConstraintMode.CONSTRAINT))
    private SemiInterval      firstOperand;


    @OneToOne
    @JoinColumn(name = "_operand2", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_semiintervalorder_operand2",
                    value = ConstraintMode.CONSTRAINT))
    private SemiInterval      secondOperand;


    @Enumerated(EnumType.STRING)
    @Column(name = "operator")
    private Operator          operator;


    /**
     * Returns the {@link SemiInterval first operand}.
     *
     * @return the first operand.
     */
    public SemiInterval getFirstOperand() {

        return firstOperand;
    }


    /**
     * Returns the {@link SemiInterval second operand}.
     *
     * @return the second operand.
     */
    public SemiInterval getSecondOperand() {

        return secondOperand;
    }

    /**
     * Returns the {@link Operator operator}.
     *
     * @return the operator.
     */
    public Operator getOperator() {

        return operator;
    }

    @SuppressWarnings("unused")
    private void setOperator(String operator) {

        this.operator = Operator.fromString(operator);
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class.
     */
    protected SemiIntervalOrder() {

        this("");
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class with the
     * provided name (Serialization Constructor).
     *
     * @param name
     *            the name.
     */
    private SemiIntervalOrder(String name) {

        this(name, null, null, null);
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class with the
     * provided name, operands, and operator.
     *
     * @param name
     *            the name.
     * @param firstOperand
     *            the first operand.
     * @param secondOperand
     *            the second operand.
     * @param operator
     *            the operator.
     */
    public SemiIntervalOrder(String name, SemiInterval firstOperand,
            SemiInterval secondOperand, Operator operator) {

        super(name);
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
        this.operator = operator;
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class. (Copy
     * Constructor)
     * 
     * @param orderToCopy
     *            the order to copy.
     */
    public SemiIntervalOrder(final SemiIntervalOrder orderToCopy) {

        super(orderToCopy);

        if (orderToCopy.getFirstOperand() != null) {

            this.firstOperand = new SemiInterval(orderToCopy.getFirstOperand());
        }

        if (orderToCopy.getSecondOperand() != null) {

            this.secondOperand = new SemiInterval(
                    orderToCopy.getSecondOperand());
        }

        this.operator = orderToCopy.getOperator();
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected SemiIntervalOrder(final SemiIntervalOrderDetails details) {

        super(details);

        if (details.getFirstOperand() != null) {

            this.firstOperand = SemiInterval.fromDetails(details
                    .getFirstOperand());
        }

        if (details.getSecondOperand() != null) {

            this.secondOperand = SemiInterval.fromDetails(details
                    .getSecondOperand());
        }

        if (details.getOperator() != null) {

            this.operator = Operator.fromString(details.getOperator());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public SemiIntervalOrderDetails toDTO() {

        SemiIntervalOrderDetails details = new SemiIntervalOrderDetails(
                super.toDTO());

        if (this.firstOperand != null) {

            details.setFirstOperand(this.firstOperand.toDTO());
        }
        if (this.secondOperand != null) {

            details.setSecondOperand(this.secondOperand.toDTO());
        }

        details.setOperator(this.operator.toString());

        return details;
    }

    /**
     * Returns a SemiIntervalOrder corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a SemiIntervalOrder corresponding to the provided details.
     */
    public static SemiIntervalOrder fromDetails(
            final SemiIntervalOrderDetails details) {

        return new SemiIntervalOrder(details);
    }

}
