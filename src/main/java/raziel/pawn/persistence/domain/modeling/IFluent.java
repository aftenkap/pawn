package raziel.pawn.persistence.domain.modeling;


import raziel.pawn.persistence.domain.modeling.time.Duration;



/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public interface IFluent {

    /**
     * Returns the {@link Duration duration} of this {@link IFluent}.
     * 
     * @return the {@link Duration duration}.
     */
    public abstract Duration getDuration();

    /**
     * Sets the {@link Duration duration} of this {@link IFluent}.
     * 
     * @param duration
     *            the {@link Duration duration}.
     */
    public abstract void setDuration(Duration duration);

}