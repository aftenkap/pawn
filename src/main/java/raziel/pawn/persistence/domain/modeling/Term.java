package raziel.pawn.persistence.domain.modeling;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.ConstraintMode;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.TermDTO;
import raziel.pawn.persistence.domain.DomainObject;
import raziel.pawn.persistence.domain.data.DataPointBase;
import raziel.pawn.persistence.domain.data.IDataPoint;
import raziel.pawn.persistence.domain.data.SpreadsheetCell;


/**
 * The {@code Term} class provides a reference implementation of the
 * {@link ITerm} interface.
 * 
 * @param <INSTANCE>
 *            the {@link IInstance} type of this {@link ITerm}.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@javax.persistence.Entity
@Table(name = "terms")
@DiscriminatorColumn(name = "_type")
public abstract class Term<INSTANCE extends IAssertion>
        extends DomainObject
        implements ITerm<INSTANCE> {

    /**
     * The serial version UID.
     */
    private static final long   serialVersionUID = 1L;

    @ManyToMany(targetEntity = DataPointBase.class)
    @JoinTable(name = "concept_support", joinColumns = @JoinColumn(
            name = "_conceptID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_concept-support_conceptID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_dataPointID",
                    referencedColumnName = "_id", foreignKey = @ForeignKey(
                            name = "FK_concept-support_dataPointID",
                            value = ConstraintMode.CONSTRAINT)))
    private List<IDataPoint<?>> support;

    @ManyToMany(targetEntity = Assertion.class)
    @JoinTable(name = "concept_instances", joinColumns = @JoinColumn(
            name = "_conceptID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_concept-instances_conceptID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_instanceID",
                    referencedColumnName = "_id", foreignKey = @ForeignKey(
                            name = "FK_concept-instances_instanceID",
                            value = ConstraintMode.CONSTRAINT)))
    private List<INSTANCE>      instances;



    @Override
    public List<IDataPoint<?>> getSupport() {

        return this.support;
    }


    @Override
    public boolean addSupport(final IDataPoint<?> dataPoint) {

        return this.support.add(dataPoint);
    }


    @Override
    public boolean removeSupport(final IDataPoint<?> dataPoint) {

        return this.support.remove(dataPoint);
    }


    @Override
    public void clearSupport() {

        this.support.clear();
    }



    @Override
    public List<INSTANCE> getInstances() {

        return this.instances;
    }


    @Override
    public boolean addInstance(final INSTANCE instance) {

        return this.instances.add(instance);
    }


    @Override
    public boolean removeInstance(final INSTANCE instance) {

        return this.support.remove(instance);
    }


    @Override
    public void clearInstances() {

        this.instances.clear();
    }



    /**
     * Creates a new instance of the {@link Term} class.
     */
    protected Term() {

        this("");
    }

    /**
     * Creates a new instance of the {@code Term} class.
     * 
     * @param name
     *            the name of the {@code Term}.
     */
    public Term(String name) {

        super(name);

        this.instances = new LinkedList<>();
        this.support = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@code Term} class.
     * 
     * @param termToCopy
     *            the {@code Term} to copy.
     */
    public Term(ITerm<INSTANCE> termToCopy) {

        super(termToCopy);

        this.instances = new LinkedList<>(termToCopy.getInstances());
        this.support = new LinkedList<>(termToCopy.getSupport());
    }

    /**
     * Creates a new instance of the {@code Term} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Term(TermDTO details) {

        super(details);
        this.instances = new LinkedList<>();
        this.support = new LinkedList<>();

        if (details.getSupport() != null) {

            for (DataPointDTO dataPoint : details.getSupport()) {

                if (dataPoint instanceof SpreadsheetCellDTO) {

                    this.support.add(SpreadsheetCell
                            .fromDTO((SpreadsheetCellDTO) dataPoint));
                }
            }
        }

    }


    @Override
    public TermDTO toDTO() {

        TermDTO details = new TermDTO(super.toDTO());


        List<DataPointDTO> support = new ArrayList<>(this.support.size());
        for (IDataPoint<?> dataPoint : this.support) {

            support.add(dataPoint.toDTO());
        }
        details.setSupport(support);

        List<AssertionDetails> instances = new ArrayList<>(
                this.instances.size());
        for (IAssertion assertion : this.instances) {

            instances.add(assertion.toDTO());
        }
        details.setInstances(instances);

        return details;
    }
}
