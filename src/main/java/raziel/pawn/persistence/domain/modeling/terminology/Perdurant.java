package raziel.pawn.persistence.domain.modeling.terminology;


import javax.persistence.DiscriminatorValue;

import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.EntityDetails;
import raziel.pawn.dto.modeling.PerdurantDetails;
import raziel.pawn.persistence.domain.modeling.IConcept;
import raziel.pawn.persistence.domain.modeling.assertions.Entity;



/**
 * The {@code Perdurant} class represents a {@link Concept} that can change over
 * time.
 * <p>
 * Instances of {@code Perdurants} are {@link Entity Entities}.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@javax.persistence.Entity
@DiscriminatorValue(value = "Perdurant")
public class Perdurant
        extends Concept<Entity> {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link Perdurant} class.
     */
    public Perdurant() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Perdurant} class with the provided
     * name.
     * 
     * @param name
     *            the name.
     */
    public Perdurant(String name) {

        super(name);
    }


    /**
     * Creates a new instance of the {@link Perdurant} class. (Copy Constructor)
     * 
     * @param perdurantToCopy
     *            the perdurant to copy.
     */
    protected Perdurant(final IConcept<Entity> perdurantToCopy) {

        super(perdurantToCopy);
    }

    /**
     * Creates a new instance of the {@link Perdurant} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public Perdurant(final PerdurantDetails details) {

        super(details);

        if (details.getInstances() != null) {

            for (AssertionDetails instance : details.getInstances()) {

                if (instance instanceof EntityDetails) {

                    this.addInstance(Entity
                            .fromDetails((EntityDetails) instance));
                }
            }
        }
        if (details.getDescendants() != null) {

            for (ConceptDetails descendant : details.getDescendants()) {

                if (descendant instanceof PerdurantDetails) {

                    this.addDescendant(Perdurant
                            .fromDetails((PerdurantDetails) descendant));
                }

            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.terminology.Concept#toDetails()
     */
    @Override
    public PerdurantDetails toDTO() {

        PerdurantDetails details = new PerdurantDetails(super.toDTO());

        return details;
    }

    /**
     * Returns a Perdurant corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a Perdurant corresponding to the provided details.
     */
    public static Perdurant fromDetails(final PerdurantDetails details) {

        return new Perdurant(details);
    }
}
