package raziel.pawn.persistence.domain.modeling.time;


import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import raziel.pawn.dto.modeling.DurationOrderDetails;
import raziel.pawn.persistence.domain.DomainObject;


/**
 * The {@link DurationOrder} class provides the capability of specifying an
 * order on {@link Duration Durations}.
 *
 * @author Peter J. Radics
 * @version 0.1
 *
 */
@Entity
@Table(name = "durationorders")
public class DurationOrder
        extends DomainObject {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    @OneToOne()
    @JoinColumn(name = "_operand1", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_durationorders_operand1",
                    value = ConstraintMode.CONSTRAINT))
    private Duration          firstOperand;


    @OneToOne()
    @JoinColumn(name = "_operand2", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_durationorders_operand2",
                    value = ConstraintMode.CONSTRAINT))
    private Duration          secondOperand;


    @Enumerated(EnumType.STRING)
    @Column(name = "operator")
    private Operator          operator;


    /**
     * Returns the {@link Duration first operand}.
     *
     * @return the first operand.
     */
    public Duration getFirstOperand() {

        return firstOperand;
    }


    /**
     * Returns the {@link Duration second operand}.
     *
     * @return the second operand.
     */
    public Duration getSecondOperand() {

        return secondOperand;
    }

    /**
     * Returns the {@link Operator operator}.
     *
     * @return the operator.
     */
    public Operator getOperator() {

        return operator;
    }


    @SuppressWarnings("unused")
    private void setOperator(String operator) {

        this.operator = Operator.fromString(operator);
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class.
     */
    protected DurationOrder() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class with the
     * provided name (Serialization Constructor).
     *
     * @param name
     *            the name.
     */
    private DurationOrder(String name) {

        this(name, null, null, null);
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class with the
     * provided name, operands, and operator.
     *
     * @param name
     *            the name.
     * @param firstOperand
     *            the first operand.
     * @param secondOperand
     *            the second operand.
     * @param operator
     *            the operator.
     */
    public DurationOrder(String name, Duration firstOperand,
            Duration secondOperand, Operator operator) {

        super(name);
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
        this.operator = operator;
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class. (Copy
     * Constructor)
     * 
     * @param orderToCopy
     *            the order to copy.
     */
    public DurationOrder(final DurationOrder orderToCopy) {

        super(orderToCopy);
        if (orderToCopy.getFirstOperand() != null) {

            this.firstOperand = new Duration(orderToCopy.getFirstOperand());
        }
        if (orderToCopy.getSecondOperand() != null) {

            this.secondOperand = new Duration(orderToCopy.getSecondOperand());
        }
        this.operator = orderToCopy.getOperator();
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected DurationOrder(final DurationOrderDetails details) {

        super(details);
        if (details.getFirstOperand() != null) {

            this.firstOperand = Duration.fromDetails(details.getFirstOperand());
        }
        if (details.getSecondOperand() != null) {

            this.secondOperand = Duration.fromDetails(details
                    .getSecondOperand());
        }
        if (details.getOperator() != null) {

            this.operator = Operator.fromString(details.getOperator());
        }
    }


    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public DurationOrderDetails toDTO() {

        DurationOrderDetails details = new DurationOrderDetails(
                super.toDTO());

        if (this.firstOperand != null) {

            details.setFirstOperand(this.firstOperand.toDTO());
        }
        if (this.secondOperand != null) {

            details.setSecondOperand(this.secondOperand.toDTO());
        }

        details.setOperator(this.operator.toString());

        return details;
    }

    /**
     * Returns a DurationOrder corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a DurationOrder corresponding to the provided details.
     */
    public static DurationOrder fromDetails(final DurationOrderDetails details) {

        return new DurationOrder(details);
    }
}
