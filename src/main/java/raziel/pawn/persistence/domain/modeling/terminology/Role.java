package raziel.pawn.persistence.domain.modeling.terminology;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.RoleDetails;
import raziel.pawn.persistence.domain.modeling.IConcept;
import raziel.pawn.persistence.domain.modeling.IRole;
import raziel.pawn.persistence.domain.modeling.IRelationship;
import raziel.pawn.persistence.domain.modeling.Term;


/**
 * The {@link Role} class defines a connection between one or multiple
 * {@link IConcept Concepts}. Instances of Roles are {@link IRelationship
 * Relationships}.
 * 
 * @param <DOMAIN>
 *            the domain type.
 * @param <RANGE>
 *            the range type.
 * @param <INSTANCE>
 *            the instance type.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
public abstract class Role<DOMAIN extends IConcept<?>, RANGE extends IConcept<?>, INSTANCE extends IRelationship<?, ?>>
        extends Term<INSTANCE>
        implements IRole<DOMAIN, RANGE, INSTANCE> {

    /**
     * The serial version UID.
     */
    private static final long                    serialVersionUID = 1L;



    @ManyToMany(targetEntity = Concept.class)
    @JoinTable(name = "relation_domains", joinColumns = @JoinColumn(
            name = "_relationID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_relation-domains_relationID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_conceptID",
                    referencedColumnName = "_id", foreignKey = @ForeignKey(
                            name = "FK_relation-domains_conceptID",
                            value = ConstraintMode.CONSTRAINT)))
    private List<DOMAIN>                         domainConcepts;

    @ManyToMany(targetEntity = Concept.class)
    @JoinTable(name = "relation_ranges", joinColumns = @JoinColumn(
            name = "_relationID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_relation-ranges_relationID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_conceptID",
                    referencedColumnName = "_id", foreignKey = @ForeignKey(
                            name = "FK_relation-ranges_conceptID",
                            value = ConstraintMode.CONSTRAINT)))
    private List<RANGE>                          rangeConcepts;



    @ManyToMany(targetEntity = Concept.class)
    @JoinTable(name = "concept_hierarchy", joinColumns = @JoinColumn(
            name = "_parentConceptID", nullable = false,
            referencedColumnName = "_id", foreignKey = @ForeignKey(
                    name = "FK_concept-hierarchy_parentConceptID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_childConceptID",
                    referencedColumnName = "_id", nullable = false,
                    foreignKey = @ForeignKey(
                            name = "FK_concept-hierarchy_childConceptID",
                            value = ConstraintMode.CONSTRAINT)))
    private List<IRole<DOMAIN, RANGE, INSTANCE>> descendants;


    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#getDomains()
     */
    @Override
    public List<DOMAIN> getDomains() {

        return this.domainConcepts;
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#addDomain(raziel.pawn
     * .persistence.domain.modeling.IConcept)
     */
    @Override
    public boolean addDomain(DOMAIN domain) {

        return this.domainConcepts.add(domain);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#removeDomain(raziel
     * .pawn.persistence.domain.modeling.IConcept)
     */
    @Override
    public boolean removeDomain(DOMAIN domain) {

        return this.domainConcepts.remove(domain);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#clearDomains()
     */
    @Override
    public void clearDomains() {

        this.domainConcepts.clear();
    }



    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#getRanges()
     */
    @Override
    public List<RANGE> getRanges() {

        return this.rangeConcepts;
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#addRange(raziel.pawn
     * .persistence.domain.modeling.IConcept)
     */
    @Override
    public boolean addRange(RANGE range) {

        return this.rangeConcepts.add(range);
    }



    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#removeRange(raziel.
     * pawn.persistence.domain.modeling.IConcept)
     */
    @Override
    public boolean removeRange(RANGE range) {

        return this.rangeConcepts.remove(range);
    }



    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#clearRanges()
     */
    @Override
    public void clearRanges() {

        this.rangeConcepts.clear();
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#descendants()
     */
    @Override
    public List<IRole<DOMAIN, RANGE, INSTANCE>> descendants() {

        return this.descendants;
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#addDescendant(raziel
     * .pawn.persistence.domain.modeling.IRelation)
     */
    @Override
    public boolean addDescendant(IRole<DOMAIN, RANGE, INSTANCE> descendant) {

        return this.descendants.add(descendant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.IRole#removeDescendant(raziel
     * .pawn.persistence.domain.modeling.IRelation)
     */
    @Override
    public boolean removeDescendant(IRole<DOMAIN, RANGE, INSTANCE> descendant) {

        return this.descendants.remove(descendant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IRole#clearDescendants()
     */
    @Override
    public void clearDescendants() {

        this.descendants.clear();
    }

    /**
     * Creates a new instance of the {@link Role} class (Serialization
     * Constructor).
     */
    protected Role() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Role} class with the provided name.
     * 
     * @param name
     *            the name of this {@link Role}.
     */
    public Role(String name) {

        super(name);

        this.domainConcepts = new LinkedList<>();
        this.rangeConcepts = new LinkedList<>();
        this.descendants = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Role} class. (Copy Constructor)
     * 
     * @param relationToCopy
     *            the relation to copy.
     */
    public Role(IRole<DOMAIN, RANGE, INSTANCE> relationToCopy) {

        super(relationToCopy);

        this.domainConcepts = new LinkedList<>(relationToCopy.getDomains());
        this.rangeConcepts = new LinkedList<>(relationToCopy.getRanges());
        this.descendants = new LinkedList<>(relationToCopy.descendants());
    }


    /**
     * Creates a new instance of the {@link Role} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Role(RoleDetails details) {

        super(details);

        this.domainConcepts = new LinkedList<>();
        this.rangeConcepts = new LinkedList<>();
        this.descendants = new LinkedList<>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public RoleDetails toDTO() {

        RoleDetails details = new RoleDetails(super.toDTO());


        List<ConceptDetails> domainConcepts = new ArrayList<>(
                this.domainConcepts.size());
        for (IConcept<?> domain : this.domainConcepts) {

            domainConcepts.add(domain.toDTO());
        }
        details.setDomainConcepts(domainConcepts);

        List<ConceptDetails> rangeConcepts = new ArrayList<>(
                this.rangeConcepts.size());
        for (IConcept<?> range : this.rangeConcepts) {

            domainConcepts.add(range.toDTO());
        }
        details.setRangeConcepts(rangeConcepts);

        List<RoleDetails> descendants = new ArrayList<>(this.descendants.size());
        for (IRole<DOMAIN, RANGE, INSTANCE> descendant : this.descendants) {

            descendants.add(descendant.toDTO());
        }
        details.setDescendants(descendants);

        return details;
    }
}
