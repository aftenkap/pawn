package raziel.pawn.persistence.domain.modeling.assertions;


import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import raziel.pawn.dto.modeling.TemporaryRelationshipDetails;
import raziel.pawn.persistence.domain.modeling.IAssertion;
import raziel.pawn.persistence.domain.modeling.IFluent;
import raziel.pawn.persistence.domain.modeling.IRelationship;
import raziel.pawn.persistence.domain.modeling.time.Duration;


/**
 * @param <DOMAIN>
 *            the domain instance type.
 * @param <RANGE>
 *            the range instance type.
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
public abstract class TemporaryRelationship<DOMAIN extends IAssertion, RANGE extends IAssertion>
        extends Relationship<DOMAIN, RANGE>
        implements IFluent {


    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;


    @OneToOne(orphanRemoval = false, optional = true)
    @JoinColumn(name = "_duration", referencedColumnName = "_id")
    private Duration          duration;



    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.assertions.IFluent#getDuration()
     */
    @Override
    public Duration getDuration() {

        if (this.duration == null) {

            this.duration = new Duration(this.getName() + " - Duration");
        }
        return this.duration;
    }


    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.assertions.IFluent#setDuration
     * (raziel.pawn.persistence.domain.modeling.time.Duration)
     */
    @Override
    public void setDuration(Duration duration) {

        this.duration = duration;
    }

    /**
     * Creates a new instance of the {@link TemporaryRelationship} class
     * (Serialization Constructor).
     */
    protected TemporaryRelationship() {

        this("");
    }

    /**
     * Creates a new instance of the {@link TemporaryRelationship} class with
     * the provided name.
     * 
     * @param name
     *            the name.
     */
    public TemporaryRelationship(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link PermanentRelationship} class. (Copy
     * Constructor)
     * 
     * @param relationshipToCopy
     *            the relationship to copy.
     */
    public TemporaryRelationship(
            final IRelationship<DOMAIN, RANGE> relationshipToCopy) {

        super(relationshipToCopy);

        if (relationshipToCopy instanceof IFluent) {

            this.duration = ((IFluent) relationshipToCopy).getDuration();
        }
    }

    /**
     * Creates a new instance of the {@link TemporaryRelationship} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    public TemporaryRelationship(final TemporaryRelationshipDetails details) {

        super(details);

        if (details.getDuration() != null) {

            this.duration = Duration.fromDetails(details.getDuration());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.Assertion#toDetails()
     */
    @Override
    public TemporaryRelationshipDetails toDTO() {

        TemporaryRelationshipDetails details = new TemporaryRelationshipDetails(
                super.toDTO());


        if (this.duration != null) {

            details.setDuration(this.duration.toDTO());
        }

        return details;
    }
}
