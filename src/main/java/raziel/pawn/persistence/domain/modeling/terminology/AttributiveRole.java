package raziel.pawn.persistence.domain.modeling.terminology;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AttributionDetails;
import raziel.pawn.dto.modeling.AttributiveRoleDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.EndurantDetails;
import raziel.pawn.dto.modeling.PerdurantDetails;
import raziel.pawn.dto.modeling.RoleDetails;
import raziel.pawn.persistence.domain.modeling.IInstance;
import raziel.pawn.persistence.domain.modeling.assertions.Attribution;


/**
 * The {@code AttributiveRole} class models a connection between
 * {@link Perdurant Perdurants} and {@link Endurant Endurants}.
 * <p>
 * The {@link IInstance Instances} of {@code AttributiveRoles} are
 * {@link Attribution Attributions}.
 * <p>
 * Since {@code AttributiveRole} relate {@link Perdurant Perdurants}, they are
 * considered {@link DynamicRole DynamicRoles}.
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
@DiscriminatorValue(value = "AttributiveRole")
public class AttributiveRole
        extends DynamicRole<Perdurant, Endurant, Attribution> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link AttributiveRole} class
     * (Serialization Constructor).
     */
    protected AttributiveRole() {

        this("");
    }

    /**
     * Creates a new instance of the {@link AttributiveRole} class with the
     * provided name.
     * 
     * @param name
     *            the name of this {@link AttributiveRole}.
     */
    public AttributiveRole(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link AttributiveRole} class. (Copy
     * Constructor)
     * 
     * @param relationToCopy
     *            the relation to copy.
     */
    public AttributiveRole(AttributiveRole relationToCopy) {

        super(relationToCopy);
    }

    /**
     * Creates a new instance of the {@link AttributiveRole} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public AttributiveRole(AttributiveRoleDetails details) {

        super(details);
        if (details.getDomainConcepts() != null) {

            for (ConceptDetails domainConcept : details.getDomainConcepts()) {

                if (domainConcept instanceof PerdurantDetails) {

                    this.addDomain(Perdurant
                            .fromDetails((PerdurantDetails) domainConcept));
                }
            }
        }
        if (details.getRangeConcepts() != null) {

            for (ConceptDetails rangeConcept : details.getRangeConcepts()) {

                if (rangeConcept instanceof EndurantDetails) {

                    this.addRange(Endurant
                            .fromDetails((EndurantDetails) rangeConcept));
                }
            }
        }
        if (details.getDescendants() != null) {

            for (RoleDetails descendant : details.getDescendants()) {
                if (descendant instanceof AttributiveRoleDetails) {

                    this.addDescendant(AttributiveRole
                            .fromDetails((AttributiveRoleDetails) descendant));
                }
            }
        }
        if (details.getInstances() != null) {

            for (AssertionDetails instance : details.getInstances()) {

                if (instance instanceof AttributionDetails) {

                    this.addInstance(Attribution
                            .fromDetails((AttributionDetails) instance));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.terminology.Role#toDetails()
     */
    @Override
    public AttributiveRoleDetails toDTO() {

        AttributiveRoleDetails details = new AttributiveRoleDetails(
                super.toDTO());
        return details;
    }

    /**
     * Returns an InteractiveRole corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an InteractiveRole corresponding to the provided details.
     */
    public static AttributiveRole fromDetails(
            final AttributiveRoleDetails details) {

        return new AttributiveRole(details);
    }

}
