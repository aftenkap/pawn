package raziel.pawn.persistence.domain.modeling;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.ConstraintMode;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.persistence.domain.DomainObject;
import raziel.pawn.persistence.domain.data.DataPointBase;
import raziel.pawn.persistence.domain.data.IDataPoint;
import raziel.pawn.persistence.domain.data.SpreadsheetCell;


/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
@Table(name = "assertions")
@DiscriminatorColumn(name = "_type")
public abstract class Assertion
        extends DomainObject
        implements IAssertion {

    /**
     * The serial version UID.
     */
    private static final long   serialVersionUID = 1L;

    @ManyToMany(targetEntity = DataPointBase.class)
    @JoinTable(name = "concept_support", joinColumns = @JoinColumn(
            name = "_conceptID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_concept-support_conceptID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_dataPointID",
                    referencedColumnName = "_id", foreignKey = @ForeignKey(
                            name = "FK_concept-support_dataPointID",
                            value = ConstraintMode.CONSTRAINT)))
    private List<IDataPoint<?>> support;



    @Override
    public List<IDataPoint<?>> getSupport() {

        return this.support;
    }

    @Override
    public boolean addSupport(IDataPoint<?> dataPoint) {

        return this.support.add(dataPoint);
    }

    @Override
    public boolean removeSupport(IDataPoint<?> dataPoint) {

        return this.support.remove(dataPoint);
    }

    @Override
    public void clearSupport() {

        this.support.clear();
    }



    /**
     * Creates a new instance of the {@code Assertion} class.
     */
    protected Assertion() {

        this("");
    }

    /**
     * Creates a new instance of the {@code Assertion} class.
     * 
     * @param name
     *            the name of the {@code Assertion}.
     */
    public Assertion(String name) {

        super(name);
        this.support = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@code Assertion} class.
     * 
     * @param assertionToCopy
     *            the {@code Assertion} to copy.
     */
    public Assertion(IAssertion assertionToCopy) {

        super(assertionToCopy);
        this.support = new LinkedList<>(assertionToCopy.getSupport());
    }

    /**
     * Creates a new instance of the {@code Assertion} class.
     * 
     * @param details
     *            the DTO.
     */
    protected Assertion(AssertionDetails details) {

        super(details);

        this.support = new LinkedList<>();
        if (details.getSupport() != null) {

            for (DataPointDTO dataPoint : details.getSupport()) {

                if (dataPoint instanceof SpreadsheetCellDTO) {

                    this.support.add(SpreadsheetCell
                            .fromDTO((SpreadsheetCellDTO) dataPoint));
                }
            }
        }
    }



    @Override
    public AssertionDetails toDTO() {

        AssertionDetails details = new AssertionDetails(super.toDTO());

        List<DataPointDTO> support = new ArrayList<>(this.support.size());
        for (IDataPoint<?> dataPoint : this.support) {

            support.add(dataPoint.toDTO());
        }
        details.setSupport(support);

        return details;
    }
}
