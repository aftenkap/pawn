package raziel.pawn.persistence.domain.modeling.assertions;


import javax.persistence.DiscriminatorValue;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import raziel.pawn.dto.modeling.EntityDetails;
import raziel.pawn.persistence.domain.modeling.Assertion;
import raziel.pawn.persistence.domain.modeling.IFluent;
import raziel.pawn.persistence.domain.modeling.IInstance;
import raziel.pawn.persistence.domain.modeling.time.Duration;



/**
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
@javax.persistence.Entity
@DiscriminatorValue("Entity")
public class Entity
        extends Assertion
        implements IInstance, IFluent {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;



    @OneToOne(orphanRemoval = false, optional = true)
    @JoinColumn(name = "_duration", referencedColumnName = "_id")
    private Duration          duration;



    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.assertions.IFluent#getDuration()
     */
    @Override
    public Duration getDuration() {

        if (this.duration == null) {

            this.duration = new Duration(this.getName() + " - Duration");
        }
        return this.duration;
    }


    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.assertions.IFluent#setDuration
     * (raziel.pawn.persistence.domain.modeling.time.Duration)
     */
    @Override
    public void setDuration(Duration duration) {

        this.duration = duration;

    }

    /**
     * Creates a new instance of the {@link Entity} class.
     */
    protected Entity() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Entity} class with the provided
     * name.
     * 
     * @param name
     *            the name of the {@link Entity}.
     */
    public Entity(String name) {

        super(name);
        this.duration = null;
    }


    /**
     * Creates a new instance of the {@link Constant} class. (Copy Constructor)
     * 
     * @param constantToCopy
     *            the constant to copy.
     */
    public <T extends IInstance & IFluent> Entity(final T constantToCopy) {

        super(constantToCopy);
    }

    /**
     * Creates a new instance of the {@link Constant} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Entity(final EntityDetails details) {

        super(details);
        if (details.getDuration() != null) {

            this.duration = Duration.fromDetails(details.getDuration());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.Assertion#toDetails(boolean)
     */
    @Override
    public EntityDetails toDTO() {

        EntityDetails details = new EntityDetails(super.toDTO());


        if (this.duration != null) {

            details.setDuration(this.duration.toDTO());
        }

        return details;
    }

    /**
     * Returns an entity corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an entity corresponding to the provided details.
     */
    public static Entity fromDetails(final EntityDetails details) {

        return new Entity(details);
    }
}
