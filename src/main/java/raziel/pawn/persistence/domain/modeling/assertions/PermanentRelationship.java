package raziel.pawn.persistence.domain.modeling.assertions;


import javax.persistence.Entity;

import raziel.pawn.dto.modeling.PermanentRelationshipDetails;
import raziel.pawn.persistence.domain.modeling.IAssertion;
import raziel.pawn.persistence.domain.modeling.IConstant;
import raziel.pawn.persistence.domain.modeling.IRelationship;


/**
 * @param <DOMAIN>
 *            the domain instance type.
 * @param <RANGE>
 *            the range instance type.
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
public abstract class PermanentRelationship<DOMAIN extends IAssertion, RANGE extends IAssertion>
        extends Relationship<DOMAIN, RANGE>
        implements IConstant {


    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link PermanentRelationship} class
     * (Serialization Constructor).
     */
    protected PermanentRelationship() {

        this("");
    }

    /**
     * Creates a new instance of the {@link PermanentRelationship} class with
     * the provided name.
     * 
     * @param name
     *            the name.
     */
    public PermanentRelationship(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link PermanentRelationship} class. (Copy
     * Constructor)
     * 
     * @param relationshipToCopy
     *            the relationship to copy.
     */
    public PermanentRelationship(
            final IRelationship<DOMAIN, RANGE> relationshipToCopy) {

        super(relationshipToCopy);
    }

    /**
     * Creates a new instance of the {@link PermanentRelationship} class from
     * the provided details.
     * 
     * @param details
     *            the details.
     */
    protected PermanentRelationship(final PermanentRelationshipDetails details) {

        super(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.assertions.Relationship#toDetails
     * ()
     */
    @Override
    public PermanentRelationshipDetails toDTO() {

        PermanentRelationshipDetails details = new PermanentRelationshipDetails(
                super.toDTO());

        return details;
    }
}
