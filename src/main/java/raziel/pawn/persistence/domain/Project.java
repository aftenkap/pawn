package raziel.pawn.persistence.domain;


import java.util.LinkedList;
import java.util.List;

import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import raziel.pawn.dto.ProjectDetails;
import raziel.pawn.dto.data.DataSourceDetails;
import raziel.pawn.dto.data.SpreadsheetDTO;
import raziel.pawn.dto.metadata.MetadataSourceDetails;
import raziel.pawn.dto.metadata.SurveyDetails;
import raziel.pawn.persistence.domain.data.DataSourceBase;
import raziel.pawn.persistence.domain.data.IDataSource;
import raziel.pawn.persistence.domain.data.Spreadsheet;
import raziel.pawn.persistence.domain.metadata.IMetadataSource;
import raziel.pawn.persistence.domain.metadata.MetadataSource;
import raziel.pawn.persistence.domain.metadata.Survey;
import raziel.pawn.persistence.domain.modeling.DataModel;


/**
 *
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
@javax.persistence.Table(name = "projects")
public class Project
        extends DomainObject {

    /**
     * Serial version uid.
     */
    private static final long     serialVersionUID = 1L;

    @OneToOne(orphanRemoval = false, optional = false)
    @JoinColumn(name = "_datamodelID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_projects_datamodelID",
                    value = ConstraintMode.CONSTRAINT))
    private DataModel             dataModel;

    @OneToMany(orphanRemoval = false, targetEntity = MetadataSource.class)
    @JoinTable(name = "project_metadatasources", joinColumns = @JoinColumn(
            name = "_projectID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(
                    name = "FK_project-metadatasources_projectID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_metaDataSourceID",
                    referencedColumnName = "_id"), foreignKey = @ForeignKey(
                    name = "FK_project-metadatasources_metaDataSourceID",
                    value = ConstraintMode.CONSTRAINT))
    private List<IMetadataSource> metadataSources;

    @OneToMany(orphanRemoval = false, targetEntity = DataSourceBase.class)
    @JoinTable(name = "project_datasources", joinColumns = @JoinColumn(
            name = "_projectID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_project-datasources_projectID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_dataSourceID",
                    referencedColumnName = "_id"), foreignKey = @ForeignKey(
                    name = "FK_project-datasources_dataSourceID",
                    value = ConstraintMode.CONSTRAINT))
    private List<IDataSource>     dataSources;

    /**
     * Returns the {@link DataModel}.
     * 
     * @return the {@link DataModel}.
     */
    public DataModel getDataModel() {

        return this.dataModel;
    }



    /**
     * Returns a list of {@link IMetadataSource MetadataSources}.
     *
     * @return a list of {@link IMetadataSource MetadataSources}.
     */
    public List<IMetadataSource> getMetadataSources() {

        return this.metadataSources;
    }


    /**
     * Returns a list of {@link IDataSource DataSources}.
     *
     * @return a list of {@link IDataSource DataSources}.
     */
    public List<IDataSource> getDataSources() {

        return this.dataSources;
    }

    /**
     * Creates a new instance of the {@link Project} class.
     */
    protected Project() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Project} class.
     *
     * @param name
     *            the name of the project.
     */
    public Project(String name) {

        super(name);
        this.dataModel = new DataModel(name + " (master)");

        this.metadataSources = new LinkedList<>();
        this.dataSources = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Project} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Project(ProjectDetails details) {

        super(details);

        this.metadataSources = new LinkedList<>();
        this.dataSources = new LinkedList<>();
        if (details.getDataModel() != null) {

            this.dataModel = DataModel.fromDetails(details.getDataModel());
        }
        if (details.getDataSources() != null) {

            for (DataSourceDetails dataSource : details.getDataSources()) {

                if (dataSource instanceof SpreadsheetDTO) {

                    this.dataSources.add(Spreadsheet
                            .fromDTO((SpreadsheetDTO) dataSource));
                }
            }
        }
        if (details.getMetadataSources() != null) {

            for (MetadataSourceDetails metadataSource : details
                    .getMetadataSources()) {

                if (metadataSource instanceof SurveyDetails) {

                    this.metadataSources.add(Survey
                            .fromDetails((SurveyDetails) metadataSource));

                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public ProjectDetails toDTO() {

        ProjectDetails details = new ProjectDetails(super.toDTO());


        details.setDataModel(this.dataModel.toDTO());

        List<DataSourceDetails> dataSourceDetails = new LinkedList<>();
        for (IDataSource dataSource : this.dataSources) {

            dataSourceDetails.add(dataSource.toDTO());
        }
        details.setDataSources(dataSourceDetails);

        List<MetadataSourceDetails> metadataSourceDetails = new LinkedList<>();
        for (IMetadataSource metadataSource : this.metadataSources) {

            metadataSourceDetails.add(metadataSource.toDTO());
        }
        details.setMetadataSources(metadataSourceDetails);


        return details;
    }


    /**
     * Returns a corresponding to the provided detail.
     * 
     * @param details
     *            the {@link ProjectDetails}.
     * @return a {@link Project} corresponding to the provided details.
     */
    public static Project fromDetails(ProjectDetails details) {

        return new Project(details);
    }
}
