package raziel.pawn.persistence.domain.metadata;


import java.util.LinkedList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;


/**
 * The {@link RankingQuestion} class represents a question that presents the
 * participant with a set of choices.
 * <p/>
 * This question type optionally allows the addition of choices provided by the
 * participants while answering the question.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
@Entity
@DiscriminatorValue("Ranking")
@Table(name = "rankingquestions")
@AttributeOverrides({

        @AttributeOverride(name = "id", column = @Column(name = "_id",
                table = "questions")),
        @AttributeOverride(name = "version", column = @Column(
                name = "_version", table = "questions")),
        @AttributeOverride(name = "name", column = @Column(name = "name",
                table = "questions")) })
public class RankingQuestion
        extends Question {

    /**
     * Serial version uid.
     */
    private static final long    serialVersionUID = 1L;


    @ManyToOne
    @JoinColumn(name = "_choiceSet", referencedColumnName = "_id")
    private ChoiceSet            choices;

    @OneToMany(orphanRemoval = true)
    @JoinTable(name = "rankingquestion_ranks", joinColumns = @JoinColumn(
            name = "_rankingQuestionId", referencedColumnName = "_id"),
            inverseJoinColumns = @JoinColumn(name = "_choiceQuestionId",
                    referencedColumnName = "_id"))
    @OrderBy("order ASC")
    private List<ChoiceQuestion> ranks;


    /**
     * Returns the ranks.
     * 
     * @return the ranks.
     */
    public List<ChoiceQuestion> getRanks() {

        return this.ranks;
    }

    /**
     * Adds a rank.
     * 
     * @param rank
     *            the rank to add.
     */
    public void addRank(ChoiceQuestion rank) {

        this.ranks.add(rank);
    }


    /**
     * Removes a rank.
     * 
     * @param rank
     *            the ranking to rank.
     */
    public void removeRank(ChoiceQuestion rank) {

        this.ranks.remove(rank);
    }


    /**
     * Clears all ranks.
     */
    public void clearRanks() {

        this.ranks.clear();
    }



    /**
     * Returns the choiceSet.
     * 
     * @return the choiceSet.
     */
    public ChoiceSet getChoices() {

        return choices;
    }


    /**
     * Sets the choices.
     * 
     * @param choices
     *            the choices.
     */
    public void setChoices(ChoiceSet choices) {

        this.choices = choices;
    }


    /**
     * Creates a new instance of the {@link RankingQuestion} class.
     */
    protected RankingQuestion() {

        this("");
    }

    /**
     * Creates a new instance of the {@link RankingQuestion} class.
     * 
     * @param name
     *            the name of the question.
     */
    public RankingQuestion(final String name) {

        this(name, "");
    }

    /**
     * Creates a new instance of the {@link RankingQuestion} class.
     * 
     * @param name
     *            the name of the question.
     * @param questionText
     *            the question text.
     */
    public RankingQuestion(final String name, final String questionText) {

        super(name, questionText);

        this.choices = null;
        this.ranks = new LinkedList<>();
    }


    /**
     * Creates a new instance of the {@link RankingQuestion} class.
     * 
     * @param details
     *            the details.
     */
    protected RankingQuestion(RankingQuestionDetails details) {

        super(details);

        this.ranks = new LinkedList<>();

        if (details.getRanks() != null) {

            for (ChoiceQuestionDetails cqDetails : details.getRanks()) {

                this.ranks.add(ChoiceQuestion.fromDetails(cqDetails));
            }

        }
        if (details.getChoices() != null) {

            this.choices = ChoiceSet.fromDetails(details.getChoices());
        }
    }



    @Override
    public RankingQuestionDetails toDTO() {


        RankingQuestionDetails details = new RankingQuestionDetails(
                super.toDTO());


        List<ChoiceQuestionDetails> choiceQuestionDetails = new LinkedList<>();
        for (ChoiceQuestion choice : this.getRanks()) {

            choiceQuestionDetails.add(choice.toDTO());
        }
        details.setRankings(choiceQuestionDetails);

        if (this.choices != null) {

            details.setChoices(this.choices.toDTO());
        }

        return details;
    }

    /**
     * Returns a {@link RankingQuestion} corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a {@link RankingQuestion} corresponding to the provided details.
     */
    public static RankingQuestion fromDetails(RankingQuestionDetails details) {

        return new RankingQuestion(details);
    }
}
