package raziel.pawn.persistence.domain.metadata;



import raziel.pawn.dto.metadata.AnswerDetails;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public interface IAnswer
        extends IMetadata {

    /**
     * Returns the {@link Participant participant}.
     * 
     * @return the {@link Participant participant}.
     */
    public abstract Participant getParticipant();

    /**
     * Returns the {@link IQuestion question}.
     * 
     * @return the {@link IQuestion question}.
     */
    public abstract IQuestion getQuestion();

    /**
     * Sets the {@link IQuestion question}.
     * 
     * @param question
     *            the {@link IQuestion question}.
     */
    public void setQuestion(IQuestion question);


    /**
     * Returns the value.
     * 
     * @return the value.
     */
    public abstract String getValue();

    /**
     * Sets the value.
     * 
     * @param value
     *            the value.
     */
    public abstract void setValue(String value);


    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public AnswerDetails toDTO();

}