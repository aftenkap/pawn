/**
 * 
 */
package raziel.pawn.persistence.domain.metadata;


import raziel.pawn.dto.metadata.ParticipantDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface IParticipant
        extends IMetadata {

    @Override
    public abstract ParticipantDTO toDTO();
}
