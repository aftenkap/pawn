package raziel.pawn.persistence.domain.metadata;



import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.ConstraintMode;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

import raziel.pawn.dto.data.DataSourceDetails;
import raziel.pawn.dto.data.SpreadsheetDTO;
import raziel.pawn.dto.metadata.MetadataSourceDetails;
import raziel.pawn.dto.metadata.ParticipantDTO;
import raziel.pawn.persistence.domain.DomainObject;
import raziel.pawn.persistence.domain.data.DataSourceBase;
import raziel.pawn.persistence.domain.data.IDataSource;
import raziel.pawn.persistence.domain.data.Spreadsheet;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
@javax.persistence.Table(name = "metadatasources")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "_type")
public abstract class MetadataSource
        extends DomainObject
        implements IMetadataSource {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @OneToMany(orphanRemoval = false)
    @JoinTable(name = "metadatasource_participants", joinColumns = @JoinColumn(
            name = "_metaDataSourceID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(
                    name = "FK_metadatasource-participants_metaDataSourceID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_participantID",
                    referencedColumnName = "_id"), foreignKey = @ForeignKey(
                    name = "FK_metadatasource-participants_participantID",
                    value = ConstraintMode.CONSTRAINT))
    @OrderBy("name ASC")
    private List<Participant> participants;

    @OneToOne(targetEntity = DataSourceBase.class)
    @JoinColumn(name = "_dataSourceID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_metadatasources_dataSourceID",
                    value = ConstraintMode.CONSTRAINT))
    private IDataSource       dataSource;


    /**
     * Creates a new instance of the {@link MetadataSource} class.
     */
    public MetadataSource() {

        this("");
    }


    /**
     * Creates a new instance of the {@link MetadataSource} class.
     * 
     * @param name
     *            the name of the data collection tool.
     */
    public MetadataSource(String name) {

        super(name);

        this.participants = new LinkedList<>();
    }


    /**
     * Creates a new instance of the {@link MetadataSource} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public MetadataSource(MetadataSourceDetails details) {

        super(details);

        this.participants = new LinkedList<>();

        if (details.getDataSource() != null) {

            DataSourceDetails dataSource = details.getDataSource();
            if (dataSource instanceof SpreadsheetDTO) {

                this.dataSource = Spreadsheet
                        .fromDTO((SpreadsheetDTO) dataSource);
            }
        }

        if (details.getParticipants() != null) {

            for (ParticipantDTO participant : details.getParticipants()) {

                this.participants.add(Participant.fromDetails(participant));
            }
        }
    }

    @Override
    public List<Participant> getParticipants() {

        return this.participants;
    }

    @Override
    public boolean addParticipant(Participant participant) {

        return this.participants.add(participant);
    }

    @Override
    public boolean removeParticipant(Participant participant) {

        return this.participants.remove(participant);
    }

    @Override
    public void clearParticipants() {

        this.participants.clear();
    }

    @Override
    public IDataSource getDataSource() {

        return this.dataSource;
    }

    @Override
    public void setDataSource(IDataSource dataSource) {

        this.dataSource = dataSource;
    }

    @Override
    public MetadataSourceDetails toDTO() {

        MetadataSourceDetails details = new MetadataSourceDetails(super.toDTO());


        if (this.dataSource != null) {
            details.setDataSource(this.dataSource.toDTO());
        }

        List<ParticipantDTO> participantDTO = new ArrayList<>(
                this.participants.size());
        for (Participant participant : this.participants) {

            participantDTO.add(participant.toDTO());
        }
        details.setParticipants(participantDTO);

        return details;
    }
}
