/**
 * 
 */
package raziel.pawn.persistence.domain.metadata;


import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import raziel.pawn.dto.metadata.ChoiceDetails;
import raziel.pawn.dto.metadata.ChoiceSetDetails;
import raziel.pawn.persistence.domain.DomainObject;

import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.OrderBy;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
@Table(name = "choicesets")
@AttributeOverrides({

        @AttributeOverride(name = "id", column = @Column(name = "_id",
                table = "choicesets")),
        @AttributeOverride(name = "version", column = @Column(
                name = "_version", table = "choicesets")),
        @AttributeOverride(name = "name", column = @Column(name = "name",
                table = "choicesets")) })
public class ChoiceSet
        extends DomainObject {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    @OneToMany
    @JoinTable(name = "choiceset_choices", joinColumns = @JoinColumn(
            name = "_choicesetID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_choiceset-choices_choicesetID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_choiceID",
                    referencedColumnName = "_id", foreignKey = @ForeignKey(
                            name = "FK_choiceset-choices_choiceID",
                            value = ConstraintMode.CONSTRAINT)))
    @OrderBy
    private Set<Choice>       choices;


    /**
     * Returns the {@link Choice Choices} of this {@link ChoiceSet}.
     * 
     * @return the {@link Choice Choices} of this {@link ChoiceSet}.
     */
    public Set<Choice> getChoices() {

        return this.choices;
    }


    /**
     * Creates a new instance of the {@link ChoiceSet} class.
     */
    public ChoiceSet() {

        this("");
    }

    /**
     * Creates a new instance of the {@link ChoiceSet} class.
     * 
     * @param name
     *            the name of the {@link ChoiceSet}.
     */
    public ChoiceSet(String name) {

        super(name);

        this.choices = new LinkedHashSet<>();
    }

    /**
     * Creates a new instance of the {@link ChoiceSet} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public ChoiceSet(ChoiceSetDetails details) {

        super(details);

        this.choices = new LinkedHashSet<>();
        if (details.getChoices() != null) {

            for (ChoiceDetails choiceDetails : details.getChoices()) {

                this.choices.add(Choice.fromDetails(choiceDetails));
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public ChoiceSetDetails toDTO() {

        ChoiceSetDetails details = new ChoiceSetDetails(super.toDTO());


        if (!this.choices.isEmpty()) {

            List<ChoiceDetails> choiceDetails = new ArrayList<>(
                    this.choices.size());

            for (Choice choice : this.choices) {

                choiceDetails.add(choice.toDTO());
            }
            details.setChoices(choiceDetails);
        }

        return details;
    }

    /**
     * Returns a {@link ChoiceSet} corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a {@link ChoiceSet} corresponding to the provided details.
     */
    public static ChoiceSet fromDetails(ChoiceSetDetails details) {

        return new ChoiceSet(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toString()
     */
    @Override
    public String toString() {

        return super.toString() + ": " + this.choices;
    }
}
