package raziel.pawn.persistence.domain.metadata;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import raziel.pawn.dto.metadata.AnswerDetails;
import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;
import raziel.pawn.dto.metadata.SurveyDetails;
import raziel.pawn.dto.metadata.TextQuestionDetails;



/**
 * The {@link Survey} class models a survey.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
@Entity
@Table(name = "surveys")
@DiscriminatorValue("Survey")
@AttributeOverrides({

        @AttributeOverride(name = "id", column = @Column(name = "_id",
                table = "metadatasources")),
        @AttributeOverride(name = "version", column = @Column(
                name = "_version", table = "metadatasources")),
        @AttributeOverride(name = "name", column = @Column(name = "name",
                table = "metadatasources")) })
public class Survey
        extends MetadataSource {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Question.class)
    @OrderBy("order ASC")
    @JoinTable(name = "survey_questions", joinColumns = @JoinColumn(
            name = "_surveyID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_survey-questions_surveyID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_questionID",
                    referencedColumnName = "_id"), foreignKey = @ForeignKey(
                    name = "FK_survey-questions_questionID",
                    value = ConstraintMode.CONSTRAINT))
    private List<IQuestion>   questions;

    @OneToMany(orphanRemoval = false)
    @JoinTable(name = "survey_answers", joinColumns = @JoinColumn(
            name = "_surveyID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_survey_answers_surveyID",
                    value = ConstraintMode.CONSTRAINT)),
            inverseJoinColumns = @JoinColumn(name = "_answerID",
                    referencedColumnName = "_id"), foreignKey = @ForeignKey(
                    name = "FK_survey_answers_answerID",
                    value = ConstraintMode.CONSTRAINT))
    private List<Answer>      answers;



    /**
     * Returns the questions.
     * 
     * @return the questions.
     */
    public List<IQuestion> getQuestions() {

        return this.questions;
    }



    /**
     * Returns the answers.
     * 
     * @return the answers.
     */
    public List<Answer> getAnswers() {

        return this.answers;
    }

    /**
     * Creates a new instance of the {@link Survey} class.
     */
    protected Survey() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Survey} class.
     * 
     * @param name
     *            the name of the survey.
     */
    public Survey(String name) {

        super(name);

        this.questions = new LinkedList<>();
        this.answers = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Survey} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public Survey(SurveyDetails details) {

        super(details);
        this.questions = new LinkedList<>();
        this.answers = new LinkedList<>();

        if (details.getQuestions() != null) {

            for (QuestionDetails question : details.getQuestions()) {

                if (question instanceof TextQuestionDetails) {

                    this.questions.add(TextQuestion
                            .fromDetails((TextQuestionDetails) question));
                }
                else if (question instanceof ChoiceQuestionDetails) {

                    this.questions.add(ChoiceQuestion
                            .fromDetails((ChoiceQuestionDetails) question));
                }
                else if (question instanceof RankingQuestionDetails) {

                    this.questions.add(RankingQuestion
                            .fromDetails((RankingQuestionDetails) question));
                }
            }
        }

        if (details.getAnswers() != null) {

            for (AnswerDetails answer : details.getAnswers()) {

                this.answers.add(Answer.fromDetails(answer));
            }
        }
    }

    /**
     * Adds a question.
     * 
     * @param question
     *            the question to add.
     */
    public void addQuestion(IQuestion question) {

        this.questions.add(question);
    }

    /**
     * Removes a question.
     * 
     * @param question
     *            the question to remove.
     */
    public void removeQuestion(IQuestion question) {

        this.questions.remove(question);
    }



    /**
     * Clears the questions.
     */
    public void clearQuestions() {

        this.questions.clear();
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public SurveyDetails toDTO() {

        SurveyDetails details = new SurveyDetails(super.toDTO());



        List<QuestionDetails> questionDetails = new ArrayList<>(
                this.questions.size());
        for (IQuestion question : this.questions) {

            questionDetails.add(question.toDTO());
        }
        details.setQuestions(questionDetails);


        List<AnswerDetails> answerDetails = new ArrayList<>(this.answers.size());
        for (IAnswer answer : this.answers) {

            answerDetails.add(answer.toDTO());
        }
        details.setAnswers(answerDetails);

        return details;
    }

    /**
     * Returns a {@link Survey} corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a {@link Survey} corresponding to the provided details.
     */
    public static Survey fromDetails(SurveyDetails details) {

        return new Survey(details);
    }
}
