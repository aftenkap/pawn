package raziel.pawn.persistence.domain.metadata;



import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import raziel.pawn.dto.metadata.TextQuestionDetails;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity(name = "TextQuestion2")
@Table(name = "textquestions")
@DiscriminatorValue("Text")
@AttributeOverrides({

        @AttributeOverride(name = "id", column = @Column(name = "_id",
                table = "questions")),
        @AttributeOverride(name = "version", column = @Column(
                name = "_version", table = "questions")),
        @AttributeOverride(name = "name", column = @Column(name = "name",
                table = "questions")) })
public class TextQuestion
        extends Question {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;



    /**
     * Creates a new instance of the {@link TextQuestion} class.
     */
    protected TextQuestion() {

        this("");
    }

    /**
     * Creates a new instance of the {@link TextQuestion} class.
     * 
     * @param name
     *            the name of the question.
     */
    public TextQuestion(String name) {

        this(name, "");
    }

    /**
     * Creates a new instance of the {@link TextQuestion} class.
     * 
     * @param name
     *            the name of the question.
     * @param questionText
     *            the question text.
     */
    public TextQuestion(String name, String questionText) {

        super(name, questionText);
    }


    /**
     * Creates a new instance of the {@link TextQuestion} class from the
     * provided details.
     * 
     * @param details
     *            the question.
     */
    protected TextQuestion(TextQuestionDetails details) {

        super(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.metadata.IQuestion#toDetails()
     */
    @Override
    public TextQuestionDetails toDTO() {

        TextQuestionDetails details = new TextQuestionDetails(super.toDTO());

        return details;
    }

    /**
     * Returns a {@link TextQuestion} corresponding to the details provided.
     * 
     * @param details
     *            the details.
     * @return a {@link TextQuestion} corresponding to the details provided.
     */
    public static TextQuestion fromDetails(TextQuestionDetails details) {

        return new TextQuestion(details);
    }
}
