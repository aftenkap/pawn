/**
 * 
 */
package raziel.pawn.persistence.domain.metadata;


import raziel.pawn.persistence.domain.IDomainObject;
import raziel.pawn.persistence.domain.data.IDataPoint;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface IMetadata
        extends IDomainObject {

    /**
     * Returns the {@link IDataPoint DataPoint} associated with this
     * {@link IMetadata Metadata} (if applicable).
     * 
     * @return the {@link IDataPoint DataPoint}.
     */
    public abstract IDataPoint<?> getDataPoint();


    /**
     * Associates a {@link IDataPoint DataPoint} with this {@link IMetadata
     * Metadata}.
     * 
     * @param dataPoint
     *            the {@link IDataPoint DataPoint}.
     */
    public abstract void setDataPoint(IDataPoint<?> dataPoint);
}
