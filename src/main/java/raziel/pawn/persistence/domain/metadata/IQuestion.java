package raziel.pawn.persistence.domain.metadata;


import raziel.pawn.dto.metadata.QuestionDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * 
 */
public interface IQuestion
        extends IMetadata {


    /**
     * Returns the text of the question.
     * 
     * @return the text of the question.
     */
    public abstract String getQuestionText();


    /**
     * Sets the text of the question.
     * 
     * @param questionText
     *            the text of the question.
     */
    public abstract void setQuestionText(final String questionText);


    /**
     * Returns whether or not an answer to the question is required.
     * 
     * @return {@code true}, if an answer is required; {@code false} otherwise.
     */
    public abstract boolean isAnswerRequired();


    /**
     * Sets whether or not an answer to the question is required.
     * 
     * @param isAnswerRequired
     *            set to {@code true}, if an answer is required; set to
     *            {@code false} otherwise.
     */
    public abstract void setAnswerRequired(final boolean isAnswerRequired);


    /**
     * Returns the order of the question.
     * 
     * @return the order of the question.
     */
    public abstract int getOrder();


    /**
     * Sets the order of the question.
     * 
     * @param order
     *            the order of the question.
     */
    public abstract void setOrder(final int order);



    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public QuestionDetails toDTO();
}
