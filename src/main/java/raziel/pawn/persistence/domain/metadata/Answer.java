package raziel.pawn.persistence.domain.metadata;



import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.metadata.AnswerDetails;
import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.ParticipantDTO;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;
import raziel.pawn.dto.metadata.TextQuestionDetails;
import raziel.pawn.persistence.domain.DomainObject;
import raziel.pawn.persistence.domain.data.DataPointBase;
import raziel.pawn.persistence.domain.data.IDataPoint;
import raziel.pawn.persistence.domain.data.SpreadsheetCell;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
@Table(name = "answers")
public class Answer
        extends DomainObject
        implements IAnswer {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    @OneToOne(optional = false)
    @JoinColumn(name = "_participantID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_answers_participantID",
                    value = ConstraintMode.CONSTRAINT))
    private Participant       participant;

    @ManyToOne(optional = false, targetEntity = Question.class)
    @JoinColumn(name = "_questionID", referencedColumnName = "_id",
            nullable = false, foreignKey = @ForeignKey(
                    name = "FK_answers_questionID",
                    value = ConstraintMode.CONSTRAINT))
    private IQuestion         question;


    @OneToOne(cascade = { PERSIST, MERGE, REFRESH, DETACH },
            targetEntity = DataPointBase.class)
    @JoinColumn(name = "_dataPoint", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_answers_dataPoint",
                    value = ConstraintMode.CONSTRAINT))
    private IDataPoint<?>     dataPoint;


    @Basic
    @Column(length = 5000)
    private String            value;

    @Override
    public Participant getParticipant() {

        return this.participant;
    }



    @Override
    public IQuestion getQuestion() {

        return this.question;
    }


    @Override
    public void setQuestion(IQuestion question) {

        this.question = question;
    }



    @Override
    public IDataPoint<?> getDataPoint() {

        return this.dataPoint;
    }


    @Override
    public void setDataPoint(IDataPoint<?> dataPoint) {

        this.dataPoint = dataPoint;
    }

    @Override
    public String getValue() {

        return this.value;
    }

    @Override
    public void setValue(String value) {

        this.value = value;
    }


    /**
     * Creates a new instance of the {@link Answer} class.
     */
    protected Answer() {

        this("", null, null);
    }

    /**
     * Creates a new instance of the {@link Answer} class with the provided
     * question and participant.
     * 
     * @param name
     *            the name of the answer.
     * @param question
     *            the {@IQuestion question} to which this object is
     *            the answer.
     * @param participant
     *            the (@Participant} who answered the question.
     */
    public Answer(final String name, final IQuestion question,
            final Participant participant) {

        super(name);

        this.question = question;
        this.participant = participant;
    }

    /**
     * Creates a new instance of the {@link Answer} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Answer(AnswerDetails details) {

        super(details);

        DataPointDTO dataPointDTO = details.getDataPoint();
        if (dataPointDTO != null && dataPointDTO instanceof SpreadsheetCellDTO) {

            this.dataPoint = SpreadsheetCell
                    .fromDTO((SpreadsheetCellDTO) dataPointDTO);
        }

        ParticipantDTO participantDTO = details.getParticipant();
        if (participantDTO != null) {

            this.participant = Participant.fromDetails(participantDTO);
        }

        QuestionDetails questionDetails = details.getQuestion();
        if (questionDetails != null) {

            if (questionDetails instanceof TextQuestionDetails) {

                this.question = TextQuestion
                        .fromDetails((TextQuestionDetails) questionDetails);
            }
            else if (questionDetails instanceof ChoiceQuestionDetails) {

                this.question = ChoiceQuestion
                        .fromDetails((ChoiceQuestionDetails) questionDetails);
            }
            else if (questionDetails instanceof RankingQuestionDetails) {

                this.question = RankingQuestion
                        .fromDetails((RankingQuestionDetails) questionDetails);
            }
        }

        this.setValue(details.getValue());
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public AnswerDetails toDTO() {

        AnswerDetails details = new AnswerDetails(super.toDTO());


        if (this.getDataPoint() != null) {

            details.setDataPoint(this.dataPoint.toDTO());
        }

        if (this.question != null) {

            details.setQuestion(this.question.toDTO());
        }

        if (this.participant != null) {

            details.setParticipant(participant.toDTO());
        }

        details.setValue(this.getValue());

        return details;
    }

    /**
     * Creates a Answer corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a Answer corresponding to the provided details.
     */
    public static Answer fromDetails(AnswerDetails details) {

        return new Answer(details);
    }
}
