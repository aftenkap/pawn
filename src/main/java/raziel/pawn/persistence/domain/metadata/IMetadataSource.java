package raziel.pawn.persistence.domain.metadata;


import java.util.List;

import raziel.pawn.dto.metadata.MetadataSourceDetails;
import raziel.pawn.persistence.domain.IDomainObject;
import raziel.pawn.persistence.domain.data.IDataSource;


/**
 *
 *
 * @author Peter J. Radics
 * @version 0.1
 */
public interface IMetadataSource
        extends IDomainObject {

    /**
     * Returns a list of participants.
     *
     * @return the list of participants.
     */
    public List<Participant> getParticipants();

    /**
     * Adds a participant.
     *
     * @param participant
     *            the participant to add.
     * @return {@code true} if this collection changed as a result of the call.
     */
    public boolean addParticipant(final Participant participant);

    /**
     * Removes a participant.
     *
     * @param participant
     *            the participant to remove.
     * @return {@code true} if this collection changed as a result of the call.
     */
    public boolean removeParticipant(final Participant participant);

    /**
     * Clears the list of participants.
     */
    public void clearParticipants();

    /**
     * Returns the {@link IDataSource data source}.
     *
     * @return the {@link IDataSource data source}.
     */
    public IDataSource getDataSource();

    /**
     * Sets the {@link IDataSource data source}.
     *
     * @param dataSource
     *            the {@link IDataSource data source}.
     */
    public void setDataSource(IDataSource dataSource);

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public MetadataSourceDetails toDTO();

}
