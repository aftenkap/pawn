package raziel.pawn.persistence.domain.metadata;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.persistence.domain.DomainObject;
import raziel.pawn.persistence.domain.data.DataPointBase;
import raziel.pawn.persistence.domain.data.IDataPoint;
import raziel.pawn.persistence.domain.data.SpreadsheetCell;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
@Table(name = "questions")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "_type")
public abstract class Question
        extends DomainObject
        implements IQuestion {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(length = 5000, name = "questionText")
    private String            questionText;

    @Basic
    @Column(name = "answerRequired")
    private boolean           isAnswerRequired;

    @Basic
    @Column(name = "_order")
    private int               order;



    @OneToOne(targetEntity = DataPointBase.class)
    @JoinColumn(name = "_dataPoint", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_questions_dataPoint",
                    value = ConstraintMode.CONSTRAINT))
    private IDataPoint<?>     dataPoint;



    @Override
    public String getQuestionText() {

        return this.questionText;
    }


    @Override
    public void setQuestionText(String questionText) {

        this.questionText = questionText;

    }

    @Override
    public boolean isAnswerRequired() {

        return this.isAnswerRequired;
    }


    @Override
    public int getOrder() {

        return this.order;
    }


    @Override
    public void setOrder(int order) {

        this.order = order;
    }


    @Override
    public void setAnswerRequired(final boolean isAnswerRequired) {

        this.isAnswerRequired = isAnswerRequired;
    }

    @Override
    public IDataPoint<?> getDataPoint() {

        return this.dataPoint;
    }

    @Override
    public void setDataPoint(IDataPoint<?> dataPoint) {

        this.dataPoint = dataPoint;
    }



    /**
     * Creates a new instance of the {@link Question} class.
     */
    public Question() {

        this("", "");
    }

    /**
     * Creates a new instance of the {@link Question} class.
     * <p/>
     * Sets the question text to the string provided.
     * 
     * @param name
     *            the name of the question.
     * 
     * @param questionText
     *            the question text.
     */
    public Question(final String name, final String questionText) {

        super(name);

        this.questionText = questionText;

        this.isAnswerRequired = false;
        this.dataPoint = null;
        this.order = Integer.MAX_VALUE;
    }


    /**
     * Creates a new instance of the {@link Question} class.
     * 
     * @param details
     *            the Question details.
     */
    protected Question(QuestionDetails details) {

        super(details);

        this.questionText = details.getQuestionText();
        this.isAnswerRequired = details.isAnswerRequired();
        this.order = details.getOrder();

        DataPointDTO dataPoint = details.getDataPoint();
        if (dataPoint != null) {

            if (dataPoint instanceof SpreadsheetCellDTO) {

                this.dataPoint = SpreadsheetCell
                        .fromDTO((SpreadsheetCellDTO) dataPoint);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails(boolean)
     */
    @Override
    public QuestionDetails toDTO() {

        QuestionDetails details = new QuestionDetails(super.toDTO());

        details.setAnswerRequired(this.isAnswerRequired());
        details.setQuestionText(this.getQuestionText());
        details.setOrder(this.getOrder());

        if (this.getDataPoint() != null) {

            details.setDataPoint(this.getDataPoint().toDTO());
        }

        return details;
    }
}
