/**
 * 
 */
package raziel.pawn.persistence.domain.metadata;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import raziel.pawn.dto.metadata.ChoiceDetails;
import raziel.pawn.persistence.domain.DomainObject;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
@Table(name = "choices", uniqueConstraints = { @UniqueConstraint(
        columnNames = { "name" }) })
@AttributeOverride(name = "name", column = @Column(name = "name",
        table = "choices", unique = true, nullable = true))
public class Choice
        extends DomainObject {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link Choice} class.
     */
    public Choice() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Choice} class.
     * 
     * @param name
     *            the name of the choice.
     */
    public Choice(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link Choice} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public Choice(ChoiceDetails details) {

        super(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public ChoiceDetails toDTO() {

        ChoiceDetails details = new ChoiceDetails(super.toDTO());

        return details;
    }

    /**
     * Creates a Choice from the provided details.
     * 
     * @param details
     *            the details.
     * @return the Choice.
     */
    public static Choice fromDetails(ChoiceDetails details) {

        return new Choice(details);
    }
}
