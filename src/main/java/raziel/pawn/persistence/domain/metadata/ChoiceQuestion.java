package raziel.pawn.persistence.domain.metadata;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import raziel.pawn.dto.metadata.ChoiceQuestionDetails;


/**
 * The {@link ChoiceQuestion} class represents a question that presents the
 * participant with a set of choices.
 * <p/>
 * This question type optionally allows the addition of choices provided by the
 * participants while answering the question.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
@Entity
@DiscriminatorValue("Choice")
@Table(name = "choicequestions")
@AttributeOverrides({

        @AttributeOverride(name = "id", column = @Column(name = "_id",
                table = "questions")),
        @AttributeOverride(name = "version", column = @Column(
                name = "_version", table = "questions")),
        @AttributeOverride(name = "name", column = @Column(name = "name",
                table = "questions")) })
public class ChoiceQuestion
        extends Question {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @OneToOne
    @JoinColumn(name = "_alternativeChoice", referencedColumnName = "_id",
            foreignKey = @ForeignKey(
                    name = "FK_choicequestions_alternativeChoice",
                    value = ConstraintMode.CONSTRAINT))
    private TextQuestion      alternativeChoice;


    @ManyToOne
    @JoinColumn(name = "_choiceSet", referencedColumnName = "_id")
    private ChoiceSet         choices;



    /**
     * Returns the question for write-in (or "other") choices.
     * 
     * @return the question for write-in (or "other") choices.
     */
    public TextQuestion getAlternativeChoice() {

        return this.alternativeChoice;
    }


    /**
     * Sets the question for write-in (or "other") choices.
     * 
     * @param alternativeChoice
     *            the question text for write-in (or "other") choices.
     */
    public void setOtherChoiceQuestion(TextQuestion alternativeChoice) {

        this.alternativeChoice = alternativeChoice;
    }



    /**
     * Returns the choices.
     * 
     * @return the choices.
     */
    public ChoiceSet getChoices() {

        return choices;
    }



    /**
     * Sets the choices.
     * 
     * @param choices
     *            the choices.
     */
    public void setChoiceSs(ChoiceSet choices) {

        this.choices = choices;
    }


    /**
     * Creates a new instance of the {@link ChoiceQuestion} class.
     */
    protected ChoiceQuestion() {

        this("");
    }

    /**
     * Creates a new instance of the {@link ChoiceQuestion} class.
     * 
     * @param name
     *            the name of the question.
     */
    public ChoiceQuestion(final String name) {

        this(name, "");
    }

    /**
     * Creates a new instance of the {@link ChoiceQuestion} class.
     * 
     * @param name
     *            the name of the question.
     * @param questionText
     *            the question text.
     */
    public ChoiceQuestion(final String name, final String questionText) {

        this(name, questionText, null);
    }


    /**
     * Creates a new instance of the {@link ChoiceQuestion} class.
     * 
     * @param name
     *            the name of the question.
     * @param questionText
     *            the question text.
     * @param alternativeChoice
     *            the question for write-in ("other") choices.
     */
    public ChoiceQuestion(final String name, final String questionText,
            final TextQuestion alternativeChoice) {

        super(name, questionText);

        this.alternativeChoice = alternativeChoice;
        this.choices = null;
    }


    /**
     * Creates a new instance of the {@link ChoiceQuestion} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected ChoiceQuestion(ChoiceQuestionDetails details) {

        super(details);


        if (details.getChoices() != null) {

            this.choices = ChoiceSet.fromDetails(details.getChoices());
        }
        if (details.getAlternativeChoice() != null) {

            this.alternativeChoice = TextQuestion.fromDetails(details
                    .getAlternativeChoice());
        }

    }



    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public ChoiceQuestionDetails toDTO() {

        ChoiceQuestionDetails details = new ChoiceQuestionDetails(
                super.toDTO());


        if (this.getAlternativeChoice() != null) {

            details.setAlternativeChoice(alternativeChoice.toDTO());
        }

        if (choices != null) {

            details.setChoices(this.choices.toDTO());
        }

        return details;
    }


    /**
     * Returns the ChoiceQuestion equivalent to the provided details.
     * 
     * @param details
     *            the details.
     * @return the ChoiceQuestion.
     */
    public static ChoiceQuestion fromDetails(ChoiceQuestionDetails details) {

        return new ChoiceQuestion(details);
    }
}
