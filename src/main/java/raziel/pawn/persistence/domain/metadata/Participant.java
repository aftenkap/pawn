package raziel.pawn.persistence.domain.metadata;


import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.metadata.ParticipantDTO;
import raziel.pawn.persistence.domain.DomainObject;
import raziel.pawn.persistence.domain.data.DataPointBase;
import raziel.pawn.persistence.domain.data.IDataPoint;
import raziel.pawn.persistence.domain.data.SpreadsheetCell;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
@Entity
@Table(name = "participants")
public class Participant
        extends DomainObject
        implements IParticipant {

    @OneToOne(targetEntity = DataPointBase.class)
    @JoinColumn(name = "_dataPointID", referencedColumnName = "_id",
            foreignKey = @ForeignKey(name = "FK_participants_dataPointID",
                    value = ConstraintMode.CONSTRAINT))
    private IDataPoint<?>     dataPoint;


    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Returns the {@link IDataPoint} used to derive this Participant's
     * identifier (name).
     * 
     * @return the {@link IDataPoint}.
     */
    @Override
    public IDataPoint<?> getDataPoint() {

        return this.dataPoint;
    }


    /**
     * Sets the {@link IDataPoint} used to derive this Participant's identifier
     * (name).
     * 
     * @param dataPoint
     *            the {@link IDataPoint}.
     */
    @Override
    public void setDataPoint(IDataPoint<?> dataPoint) {

        this.dataPoint = dataPoint;
    }

    /**
     * Creates a new instance of the {@link Participant} class.
     */
    protected Participant() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Participant} class with the provided
     * name.
     * 
     * @param name
     *            the participant's name.
     */
    public Participant(String name) {

        super(name);
        this.dataPoint = null;
    }

    /**
     * Creates a new instance of the {@link Participant} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Participant(ParticipantDTO details) {

        super(details);

        DataPointDTO dataPointDTO = details.getDataPoint();
        if (dataPointDTO != null) {

            if (dataPointDTO instanceof SpreadsheetCellDTO) {

                this.dataPoint = SpreadsheetCell
                        .fromDTO((SpreadsheetCellDTO) dataPointDTO);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.metadata.IParticipant#toDetails()
     */
    @Override
    public ParticipantDTO toDTO() {

        ParticipantDTO details = new ParticipantDTO(super.toDTO());

        if (this.getDataPoint() != null) {

            details.setDataPoint(this.dataPoint.toDTO());
        }
        return details;
    }

    /**
     * Creates a Participant corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a Participant corresponding to the provided details.
     */
    public static Participant fromDetails(ParticipantDTO details) {

        return new Participant(details);
    }
}
