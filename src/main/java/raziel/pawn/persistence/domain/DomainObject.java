package raziel.pawn.persistence.domain;


import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import raziel.pawn.dto.DomainObjectDTO;


/**
 * The abstract {@link DomainObject} class serves as basis for any object to be
 * stored in a database.
 *
 * @author Peter J. Radics
 * @version 0.1
 *
 */
@MappedSuperclass
public abstract class DomainObject
        implements Serializable, IDomainObject {

    /**
     * The serial version id.
     */
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "_id")
    private Integer           id;
    @Version
    @Column(name = "_version")
    private Long              version;
    @Basic
    @Column(name = "name")
    private String            name;


    // ----------------------------------------------------------
    @Override
    public Integer getId() {

        return this.id;
    }


    @Override
    public void setId(final int id) {

        this.id = id;
    }

    @Override
    public long getVersion() {

        return this.version;
    }

    @Override
    public void setVersion(final long version) {

        this.version = version;
    }


    @Override
    public String getName() {

        return this.name;
    }

    @Override
    public void setName(String name) {

        this.name = name;
    }


    /**
     * Creates a new instance of the {@link DomainObject} class.
     */
    protected DomainObject() {

        this("");
    }

    /**
     * Creates a new Instance of the {@link DomainObject} with the provided
     * name.
     *
     * @param name
     *            the name of the object.
     */
    protected DomainObject(String name) {

        this.id = null;
        this.version = 0l;
        this.name = name;
    }

    /**
     * Creates a new instance of the {@link DomainObject} class. (Copy
     * Constructor)
     * 
     * @param domainObjectToCopy
     *            the domain object to copy.
     */
    protected DomainObject(final IDomainObject domainObjectToCopy) {

        this.id = domainObjectToCopy.getId();
        this.name = domainObjectToCopy.getName();
        this.version = domainObjectToCopy.getVersion();
    }

    /**
     * Creates a new instance of the {@link DomainObject} class.
     * 
     * @param details
     *            the details of the {@link DomainObject}.
     */
    protected DomainObject(final DomainObjectDTO details) {

        this.id = details.getId();
        this.version = details.getVersion();
        this.name = details.getName();
    }


    @Override
    public String toString() {

        return this.id + "@" + this.version + ": " + this.name;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj != null && obj.getClass() == this.getClass()) {

            IDomainObject otherDBObject = (IDomainObject) obj;

            boolean bothNull = this.id == null && otherDBObject.getId() == null;

            if (!bothNull) {

                if (this.id != null) {

                    return this.id.equals(otherDBObject.getId());
                }
            }
        }

        return false;
    }

    @Override
    public int hashCode() {

        int hashcode = 7 * (this.id == null ? 0 : this.id);

        return hashcode;
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public DomainObjectDTO toDTO() {

        DomainObjectDTO details = new DomainObjectDTO(this.getId());

        details.setVersion(this.getVersion());
        details.setName(this.getName());

        return details;
    }
}
