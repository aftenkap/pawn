package raziel.pawn.core.domain;


import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.core.domain.data.IDataSource;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.IMetadataSource;
import raziel.pawn.core.domain.metadata.Survey;
import raziel.pawn.core.domain.modeling.DataModel;
import raziel.pawn.dto.ProjectDetails;
import raziel.pawn.dto.data.DataSourceDetails;
import raziel.pawn.dto.data.SpreadsheetDTO;
import raziel.pawn.dto.metadata.MetadataSourceDetails;
import raziel.pawn.dto.metadata.SurveyDetails;


/**
 *
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class Project
        extends DomainObject {

    /**
     * Serial version uid.
     */
    private static final long           serialVersionUID = 1L;

    private final DataModel             dataModel;

    private final List<IMetadataSource> metadataSources;
    private final List<IDataSource>     dataSources;



    /**
     * Returns the {@link DataModel}.
     * 
     * @return the {@link DataModel}.
     */
    public DataModel getDataModel() {

        return this.dataModel;
    }



    /**
     * Returns a list of {@link IMetadataSource MetadataSources}.
     *
     * @return a list of {@link IMetadataSource MetadataSources}.
     */
    public List<IMetadataSource> getMetadataSources() {

        return this.metadataSources;
    }


    /**
     * Returns a list of {@link IDataSource DataSources}.
     *
     * @return a list of {@link IDataSource DataSources}.
     */
    public List<IDataSource> getDataSources() {

        return this.dataSources;
    }

    /**
     * Creates a new instance of the {@link Project} class.
     */
    protected Project() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Project} class.
     *
     * @param name
     *            the name of the project.
     */
    public Project(String name) {

        super(name);
        this.dataModel = new DataModel(name + " (master)");

        this.metadataSources = new LinkedList<>();
        this.dataSources = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Project} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Project(ProjectDetails details) {

        super(details);

        this.metadataSources = new LinkedList<>();
        this.dataSources = new LinkedList<>();

        if (details.getDataModel() != null) {

            this.dataModel = DataModel.fromDetails(details.getDataModel());
        }
        else {

            throw new IllegalArgumentException(
                    "Cannot initialize project without valid data model!");
        }


        if (details.getDataSources() != null) {

            for (DataSourceDetails dataSource : details.getDataSources()) {

                if (dataSource instanceof SpreadsheetDTO) {

                    this.dataSources.add(Spreadsheet
                            .fromDTO((SpreadsheetDTO) dataSource));
                }
            }
        }

        if (details.getMetadataSources() != null) {

            for (MetadataSourceDetails metadataSource : details
                    .getMetadataSources()) {

                if (metadataSource instanceof SurveyDetails) {

                    this.metadataSources.add(Survey
                            .fromDetails((SurveyDetails) metadataSource));

                }
                else {
                    System.out.println("Metadata source of unknown type! "
                            + this.metadataSources.getClass());
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails(boolean)
     */
    @Override
    public ProjectDetails toDTO() {

        ProjectDetails details = new ProjectDetails(super.toDTO());


        details.setDataModel(this.dataModel.toDTO());

        List<DataSourceDetails> dataSourceDetails = new LinkedList<>();
        for (IDataSource dataSource : this.dataSources) {

            dataSourceDetails.add(dataSource.toDTO());
        }
        details.setDataSources(dataSourceDetails);

        List<MetadataSourceDetails> metadataSourceDetails = new LinkedList<>();
        for (IMetadataSource metadataSource : this.metadataSources) {

            metadataSourceDetails.add(metadataSource.toDTO());
        }
        details.setMetadataSources(metadataSourceDetails);

        return details;
    }


    /**
     * Returns a {@link Project} corresponding to the provided details.
     * 
     * @param details
     *            the {@link ProjectDetails}.
     * @return a {@link Project} corresponding to the provided details.
     */
    public static Project fromDetails(ProjectDetails details) {

        return new Project(details);
    }

    /**
     * Returns a List of {@link Project Projects} corresponding to the provided
     * details.
     * 
     * @param details
     *            the {@link ProjectDetails}.
     * @return a List of {@link Project Projects} corresponding to the provided
     *         details.
     */
    public static List<Project> fromDetails(Collection<ProjectDetails> details) {

        List<Project> projects = new ArrayList<>(details.size());

        for (ProjectDetails project : details) {

            projects.add(Project.fromDetails(project));
        }

        return projects;
    }
}
