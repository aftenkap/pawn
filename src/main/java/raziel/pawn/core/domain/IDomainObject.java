package raziel.pawn.core.domain;


import java.io.Serializable;

import raziel.pawn.dto.DomainObjectDTO;



/**
 *
 *
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public interface IDomainObject
        extends Serializable {

    /**
     * Returns the id of this object.
     *
     * @return the id.
     */
    public abstract Integer getId();


    /**
     * Sets the id of this object.
     *
     * @param id
     *            the id.
     */
    public abstract void setId(final int id);

    /**
     * Returns the version of this object.
     *
     * @return the version.
     */
    public abstract long getVersion();

    /**
     * Sets the version of this object.
     *
     * @param version
     *            the version.
     */
    public abstract void setVersion(final long version);

    /**
     * Returns the name of the object.
     *
     * @return the name of the object.
     */
    public abstract String getName();

    /**
     * Sets the name of the object.
     *
     * @param name
     *            the name of the object.
     */
    public abstract void setName(String name);

    /**
     * Returns the details of the domain object.
     * 
     * @return the details of the domain object.
     */
    public abstract DomainObjectDTO toDTO();

}