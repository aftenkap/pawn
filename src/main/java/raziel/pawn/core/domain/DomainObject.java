package raziel.pawn.core.domain;



import raziel.pawn.dto.DomainObjectDTO;


/**
 * The abstract {@link DomainObject} class serves as basis for any object to be
 * stored in a database.
 *
 * @author Peter J. Radics
 * @version 0.1
 *
 */
public abstract class DomainObject
        implements IDomainObject {

    /**
     * The serial version id.
     */
    private static final long serialVersionUID = 1L;


    private Integer           id;
    private Long              version;
    private String            name;


    @Override
    public Integer getId() {

        return this.id;
    }


    @Override
    public void setId(final int id) {

        this.id = id;
    }

    @Override
    public long getVersion() {

        return this.version;
    }

    @Override
    public void setVersion(final long version) {

        this.version = version;
    }


    @Override
    public String getName() {

        return this.name;
    }

    @Override
    public void setName(String name) {

        this.name = name;
    }


    /**
     * Creates a new instance of the {@link DomainObject} class.
     */
    protected DomainObject() {

        this("");
    }

    /**
     * Creates a new Instance of the {@link DomainObject} with the provided
     * name.
     *
     * @param name
     *            the name of the object.
     */
    protected DomainObject(String name) {

        this.id = null;
        this.version = -1l;
        this.name = name;
    }

    /**
     * Creates a new instance of the {@link DomainObject} class. (Copy
     * Constructor)
     * 
     * @param domainObjectToCopy
     *            the domain object to copy.
     */
    protected DomainObject(final IDomainObject domainObjectToCopy) {

        this.id = domainObjectToCopy.getId();
        this.name = domainObjectToCopy.getName();
        this.version = domainObjectToCopy.getVersion();
    }

    /**
     * Creates a new instance of the {@link DomainObject} class.
     * 
     * @param details
     *            the details of the {@link DomainObject}.
     */
    protected DomainObject(DomainObjectDTO details) {

        this.id = details.getId();
        this.version = details.getVersion();
        this.name = details.getName();
    }


    @Override
    public String toString() {

        return this.id + "@" + this.version + ": " + this.name;
    }

    @Override
    public boolean equals(Object obj) {

        if (super.equals(obj)) {

            return true;
        }

        if (obj != null && obj.getClass() == this.getClass()) {

            IDomainObject otherDBObject = (IDomainObject) obj;


            if (this.id != null) {

                return this.id.equals(otherDBObject.getId());
            }
        }

        return false;
    }

    @Override
    public int hashCode() {

        int hashcode = 7 * (this.id == null ? 0 : this.id);

        return hashcode;
    }


    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails(boolean)
     */
    @Override
    public DomainObjectDTO toDTO() {

        DomainObjectDTO details = new DomainObjectDTO(this.getId());

        details.setVersion(this.getVersion());
        details.setName(this.getName());

        return details;
    }
}
