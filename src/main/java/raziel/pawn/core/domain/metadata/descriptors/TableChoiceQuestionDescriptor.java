package raziel.pawn.core.domain.metadata.descriptors;


import java.util.Objects;

import org.jutility.common.datatype.table.CellLocation;
import org.jutility.common.datatype.table.CellRange;

import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.ChoiceQuestion;
import raziel.pawn.core.domain.metadata.ChoiceSet;
import raziel.pawn.core.domain.metadata.TextQuestion;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public class TableChoiceQuestionDescriptor
        extends TableQuestionDescriptor<ChoiceQuestion> {

    private final ChoiceSet choiceSet;

    /**
     * Returns the choiceSet.
     * 
     * @return the choiceSet.
     */
    public ChoiceSet getChoiceSet() {

        return choiceSet;
    }


    /**
     * Creates a new instance of the {@link TableChoiceQuestionDescriptor}
     * class.
     * 
     * @param question
     *            the {@link ChoiceQuestion}.
     */
    public TableChoiceQuestionDescriptor(final ChoiceQuestion question) {

        super(question);

        this.choiceSet = question.getChoices();
    }

    /**
     * Creates a new instance of the {@link TableChoiceQuestionDescriptor}
     * class.
     * 
     * 
     * @param cellRange
     *            the {@link CellRange Range} of the question.
     * @param choiceSet
     *            the {@link ChoiceSet Choices}.
     */
    public TableChoiceQuestionDescriptor(CellRange cellRange,
            ChoiceSet choiceSet) {

        super(cellRange);
        Objects.requireNonNull(choiceSet, this.getClass().getSimpleName()
                + " requires valid Choice Set!");

        if (cellRange.isEmpty() || cellRange.size() > 2) {

            throw new IllegalArgumentException(
                    "Cell Range of ChoiceQuestionDescriptor cannot be empty or "
                            + "contain more than two cells!");
        }

        this.choiceSet = choiceSet;
    }

    @Override
    public ChoiceQuestion extractMetadata(Spreadsheet source) {

        ChoiceQuestion question = null;

        for (CellLocation location : this.getCellRange()) {

            IDataPoint<?> cell = source.getCell(location);
            if (cell != null) {

                Object cellValue = source.get(location);

                String questionText = null;
                if (cellValue != null) {

                    questionText = cellValue.toString();

                    if (question == null) {

                        question = new ChoiceQuestion(this.getPrefix() + " "
                                + (this.getOrder() + 1));

                        question.setQuestionText(questionText);
                        question.setDataPoint(cell);
                        question.setOrder(this.getOrder());
                        question.setChoices(this.getChoiceSet());

                    }
                    else {

                        TextQuestion alternativeChoice = new TextQuestion(
                                question.getName() + " [Other]");

                        alternativeChoice.setQuestionText(questionText);
                        alternativeChoice.setDataPoint(cell);

                        alternativeChoice.setOrder(Integer.MAX_VALUE);
                        question.setAlternativeChoice(alternativeChoice);

                    }
                }
                else {

                    throw new IllegalArgumentException(
                            "Question text cannot be empty! (Cell value is null)");
                }
            }
            else {

                throw new IllegalArgumentException(
                        "Question text cannot be empty! (Cell is null)");
            }
        }

        return question;
    }

    @Override
    public String toString() {

        return "ChoiceQuestion in " + super.toString() + " with choices from "
                + this.choiceSet;
    }
}
