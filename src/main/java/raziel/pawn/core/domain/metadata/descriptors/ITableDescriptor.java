package raziel.pawn.core.domain.metadata.descriptors;


import raziel.pawn.core.domain.data.Spreadsheet;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface ITableDescriptor
        extends IDataSourceDescriptor<Spreadsheet> {

    // Tagging interface
}
