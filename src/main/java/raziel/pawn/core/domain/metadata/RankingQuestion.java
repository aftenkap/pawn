package raziel.pawn.core.domain.metadata;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;


/**
 * The {@link RankingQuestion} class represents a question that presents the
 * participant with a set of choices.
 * <p/>
 * This question type optionally allows the addition of choices provided by the
 * participants while answering the question.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */

public class RankingQuestion
        extends Question {

    /**
     * Serial version uid.
     */
    private static final long          serialVersionUID = 1L;



    private ChoiceSet                  choices;
    private final List<ChoiceQuestion> ranks;



    /**
     * Returns the ranks.
     * 
     * @return the ranks.
     */
    public List<ChoiceQuestion> getRanks() {

        return Collections.unmodifiableList(this.ranks);
    }


    /**
     * Returns the choices of this {@link RankingQuestion question}.
     * 
     * @return the choices of this {@link RankingQuestion question}.
     */
    public ChoiceSet getChoices() {

        return this.choices;
    }

    /**
     * Sets the choices of this {@link RankingQuestion question}.
     * 
     * @param choices
     *            the choices of this {@link RankingQuestion question}.
     */
    public void setChoices(ChoiceSet choices) {

        this.setChoicesWithoutGeneration(choices);

        this.ranks.clear();
        if (this.choices != null) {

            for (int i = 0; i < this.choices.getChoices().size(); i++) {

                String suffix = RankingQuestion
                        .generateRankSuffix("Rank", i + 1);
                String name = this.getName() + suffix;
                String questionText = this.getQuestionText() + suffix;

                ChoiceQuestion question = new ChoiceQuestion(name,
                        questionText, null);
                question.setChoices(this.getChoices());
                question.setOrder(i);

                this.ranks.add(question);
            }
        }
    }

    private void setChoicesWithoutGeneration(ChoiceSet choices) {

        this.choices = choices;
    }


    /**
     * Creates a new instance of the {@link RankingQuestion} class.
     */
    protected RankingQuestion() {

        this("");
    }

    /**
     * Creates a new instance of the {@link RankingQuestion} class.
     * 
     * @param name
     *            the name of the question.
     */
    public RankingQuestion(final String name) {

        this(name, "");
    }

    /**
     * Creates a new instance of the {@link RankingQuestion} class.
     * 
     * @param name
     *            the name of the question.
     * @param questionText
     *            the question text.
     */
    public RankingQuestion(final String name, final String questionText) {

        super(name, questionText);

        this.choices = null;
        this.ranks = new LinkedList<>();
    }


    /**
     * Creates a new instance of the {@link RankingQuestion} class.
     * 
     * @param details
     *            the details.
     */
    protected RankingQuestion(RankingQuestionDetails details) {

        super(details);

        this.choices = null;
        this.ranks = new LinkedList<>();

        if (details.getRanks() != null) {

            for (ChoiceQuestionDetails cqDetails : details.getRanks()) {

                this.ranks.add(ChoiceQuestion.fromDetails(cqDetails));
            }
        }
        if (details.getChoices() != null) {

            this.setChoicesWithoutGeneration(ChoiceSet.fromDetails(details
                    .getChoices()));
        }
    }


    @Override
    public RankingQuestionDetails toDTO() {


        RankingQuestionDetails details = new RankingQuestionDetails(
                super.toDTO());


        List<ChoiceQuestionDetails> choiceQuestionDetails = new LinkedList<>();
        for (ChoiceQuestion choice : this.getRanks()) {

            choiceQuestionDetails.add(choice.toDTO());
        }
        details.setRankings(choiceQuestionDetails);

        if (this.choices != null) {

            details.setChoices(this.choices.toDTO());
        }

        return details;
    }

    /**
     * Returns a {@link RankingQuestion} corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a {@link RankingQuestion} corresponding to the provided details.
     */
    public static RankingQuestion fromDetails(RankingQuestionDetails details) {

        return new RankingQuestion(details);
    }



    /**
     * Returns a List of {@link RankingQuestion RankingQuestions} corresponding
     * to the provided details.
     * 
     * @param details
     *            the {@link RankingQuestionDetails}.
     * @return a List of {@link RankingQuestion RankingQuestions} corresponding
     *         to the provided details.
     */
    public static List<RankingQuestion> fromDetails(
            Collection<RankingQuestionDetails> details) {

        List<RankingQuestion> objects = new ArrayList<>(details.size());

        for (RankingQuestionDetails detail : details) {

            objects.add(RankingQuestion.fromDetails(detail));
        }

        return objects;
    }

    /**
     * Generates the suffix for the provided rank.
     * 
     * @param prefix
     *            the prefix placed before the rank.
     * @param rank
     *            the rank.
     * @return the rank suffix (with the format " [prefix rank]").
     */
    private static String generateRankSuffix(final String prefix, final int rank) {

        String suffix = " [" + prefix + " " + rank + "]";

        return suffix;
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.metadata.Question#toString()
     */
    @Override
    public String toString() {

        if (this.getChoices() != null) {

            return super.toString() + ". Ranks: "
                    + this.getChoices().getChoices();
        }
        else {

            return super.toString();
        }
    }
}
