package raziel.pawn.core.domain.metadata;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.dto.metadata.ChoiceQuestionDetails;


/**
 * The {@link ChoiceQuestion} class represents a question that presents the
 * participant with a set of choices.
 * <p/>
 * This question type optionally allows the addition of choices provided by the
 * participants while answering the question.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */

public class ChoiceQuestion
        extends Question {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    private TextQuestion      alternativeChoice;
    private ChoiceSet         choices;



    /**
     * Returns the question for write-in (or "other") choices.
     * 
     * @return the question for write-in (or "other") choices.
     */
    public TextQuestion getAlternativeChoice() {

        return this.alternativeChoice;
    }


    /**
     * Sets the question for write-in (or "other") choices.
     * 
     * @param alternativeChoice
     *            the question text for write-in (or "other") choices.
     */
    public void setAlternativeChoice(TextQuestion alternativeChoice) {

        this.alternativeChoice = alternativeChoice;
    }


    /**
     * Returns the ChoiceSet of this {@link IQuestion question}.
     * 
     * @return the ChoiceSet of this {@link IQuestion question}.
     */
    public ChoiceSet getChoices() {

        return this.choices;
    }

    /**
     * Sets the ChoiceSet of this {@link IQuestion question}.
     * 
     * @param choices
     *            the ChoiceSet.
     */
    public void setChoices(ChoiceSet choices) {

        this.choices = choices;
    }



    /**
     * Creates a new instance of the {@link ChoiceQuestion} class.
     */
    protected ChoiceQuestion() {

        this("");
    }

    /**
     * Creates a new instance of the {@link ChoiceQuestion} class.
     * 
     * @param name
     *            the name of the question.
     */
    public ChoiceQuestion(final String name) {

        this(name, "");
    }

    /**
     * Creates a new instance of the {@link ChoiceQuestion} class.
     * 
     * @param name
     *            the name of the question.
     * @param questionText
     *            the question text.
     */
    public ChoiceQuestion(final String name, final String questionText) {

        this(name, questionText, null);
    }


    /**
     * Creates a new instance of the {@link ChoiceQuestion} class.
     * 
     * @param name
     *            the name of the question.
     * @param questionText
     *            the question text.
     * @param alternativeChoice
     *            the question for write-in ("other") choices.
     */
    public ChoiceQuestion(final String name, final String questionText,
            final TextQuestion alternativeChoice) {

        super(name, questionText);

        this.alternativeChoice = alternativeChoice;
        this.choices = null;
    }

    /**
     * Creates a new instance of the {@link ChoiceQuestion} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected ChoiceQuestion(ChoiceQuestionDetails details) {

        super(details);

        if (details.getChoices() != null) {

            this.choices = ChoiceSet.fromDetails(details.getChoices());
        }
        if (details.getAlternativeChoice() != null) {

            this.alternativeChoice = TextQuestion.fromDetails(details
                    .getAlternativeChoice());
        }
    }


    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public ChoiceQuestionDetails toDTO() {

        ChoiceQuestionDetails details = new ChoiceQuestionDetails(
                super.toDTO());

        if (this.choices != null) {

            details.setChoices(this.choices.toDTO());
        }

        if (this.getAlternativeChoice() != null) {

            details.setAlternativeChoice(alternativeChoice.toDTO());
        }

        return details;
    }


    /**
     * Returns the ChoiceQuestion equivalent to the provided details.
     * 
     * @param details
     *            the details.
     * @return the ChoiceQuestion.
     */
    public static ChoiceQuestion fromDetails(ChoiceQuestionDetails details) {

        return new ChoiceQuestion(details);
    }


    /**
     * Returns a List of {@link ChoiceQuestion ChoiceQuestions} corresponding to
     * the provided details.
     * 
     * @param details
     *            the {@link ChoiceQuestionDetails}.
     * @return a List of {@link ChoiceQuestion ChoiceQuestions} corresponding to
     *         the provided details.
     */
    public static List<ChoiceQuestion> fromDetails(
            Collection<ChoiceQuestionDetails> details) {

        List<ChoiceQuestion> objects = new ArrayList<>(details.size());

        for (ChoiceQuestionDetails detail : details) {

            objects.add(ChoiceQuestion.fromDetails(detail));
        }

        return objects;
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.metadata.Question#toString()
     */
    @Override
    public String toString() {

        if (this.getChoices() != null) {

            return super.toString() + ". Choices: "
                    + this.getChoices().getChoices();
        }
        else {

            return super.toString();
        }
    }
}
