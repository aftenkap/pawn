package raziel.pawn.core.domain.metadata.descriptors;


import org.jutility.common.datatype.table.CellRange;

import raziel.pawn.core.domain.metadata.IQuestion;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 * 
 * @param <S>
 *            the {@link IQuestion Question} type.
 *
 */
public abstract class TableQuestionDescriptor<S extends IQuestion>
        extends TableMetadataDescriptor<S> {

    /**
     * The default prefix.
     */
    public static final String DEFAULT_PREFIX = "Question";
    /**
     * The default suffix.
     */
    public static final String DEFAULT_SUFFIX = "";

    /**
     * Creates a new instance of the {@link TableQuestionDescriptor} class.
     * 
     * @param question
     *            the {@link IQuestion Question}.
     */
    public TableQuestionDescriptor(final S question) {

        this(question, DEFAULT_PREFIX, DEFAULT_SUFFIX);
    }

    /**
     * Creates a new instance of the {@link TableQuestionDescriptor} class.
     * 
     * @param question
     *            the {@link IQuestion Question}.
     * @param prefix
     *            the prefix.
     * @param suffix
     *            the suffix.
     */
    public TableQuestionDescriptor(final S question, String prefix,
            String suffix) {

        super(question, prefix, suffix);
    }

    /**
     * Creates a new instance of the {@link TableQuestionDescriptor} class.
     * 
     * @param cellRange
     *            the {@link CellRange}.
     */
    public TableQuestionDescriptor(CellRange cellRange) {

        this(cellRange, DEFAULT_PREFIX, DEFAULT_SUFFIX);
    }

    /**
     * Creates a new instance of the {@link TableQuestionDescriptor} class.
     * 
     * @param cellRange
     *            the {@link CellRange}.
     * @param prefix
     *            the prefix.
     * @param suffix
     *            the suffix.
     */
    public TableQuestionDescriptor(CellRange cellRange, String prefix,
            String suffix) {

        super(cellRange, prefix, suffix);
    }
}
