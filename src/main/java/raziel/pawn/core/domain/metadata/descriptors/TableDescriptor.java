/**
 * 
 */
package raziel.pawn.core.domain.metadata.descriptors;


import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.IMetadata;



/**
 * @author Peter J. Radics
 * @version 0.1
 */
public class TableDescriptor
        extends DomainObject
        implements ITableDescriptor {

    /**
     * Serial Version UID.
     */
    private static final long                                                              serialVersionUID = -2214965653590657683L;

    private final Spreadsheet                                                                 spreadsheet;

    private final Map<Class<? extends IMetadata>, List<IDataPointDescriptor<Spreadsheet, ? extends IMetadata>>> dataPointDescriptors;



    @Override
    public Spreadsheet getDataSource() {

        return this.spreadsheet;
    }

    /**
     * Returns the spreadsheet.
     * 
     * @return the spreadsheet.
     */
    public Spreadsheet getTable() {

        return this.spreadsheet;
    }



    /**
     * Creates a new instance of the {@link TableDescriptor} class.
     * 
     * @param spreadsheet
     *            the spreadsheet.
     */
    public TableDescriptor(final Spreadsheet table) {

        super("Descriptor" + " - " + table.getName());

        Objects.requireNonNull(table,
                "Spreadsheet Descriptor requires a valid Spreadsheet!");

        this.dataPointDescriptors = new LinkedHashMap<>();

        this.spreadsheet = table;
    }


    @Override
    public List<IDataPointDescriptor<Spreadsheet, ?>> getDataPointDescriptors(
            Class<? extends IMetadata> type) {

        return this.dataPointDescriptors.get(type);
    }

    @Override
    public <M extends IMetadata> boolean addDataPointDescriptor(Class<M> type,
            IDataPointDescriptor<Spreadsheet, ? extends M> descriptor) {

        List<IDataPointDescriptor<Spreadsheet, ?>> list = this.dataPointDescriptors
                .get(type);
        if (list == null) {

            list = new LinkedList<>();
            this.dataPointDescriptors.put(type, list);
        }

        return list.add(descriptor);
    }

    @Override
    public <M extends IMetadata> boolean removeDataPointDescriptor(
            final Class<M> type,
            IDataPointDescriptor<Spreadsheet, ? extends M> descriptor) {


        List<IDataPointDescriptor<Spreadsheet, ?>> list = this.dataPointDescriptors
                .get(type);

        if (list == null) {

            return false;
        }
        else {

            boolean removed = list.remove(descriptor);

            if (list.isEmpty()) {

                this.dataPointDescriptors.remove(type);
            }

            return removed;
        }
    }
}
