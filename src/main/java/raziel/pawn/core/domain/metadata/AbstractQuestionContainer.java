///**
// * 
// */
//package raziel.pawn.core.domain.metadata;
//
//
//import java.util.Collection;
//import java.util.Iterator;
//import java.util.List;
//
//
///**
// * @author Peter J. Radics
// *
// */
//public abstract class AbstractQuestionContainer {
//
//
//
//    /**
//     * Adds the {@link IQuestion Question} to the {@link List}.
//     * 
//     * @param questions
//     *            the {@link List}.
//     * 
//     * @param question
//     *            the {@link IQuestion Question} to add.
//     * @return {@code true}, if the {@link List} was changed by the operation.
//     */
//    public static <T extends IQuestion, S extends T> boolean addQuestion(
//            final List<T> questions, final S question) {
//
//        int index = 0;
//        if (!questions.isEmpty()) {
//
//            index = questions.get(questions.size() - 1).getOrder() + 1;
//        }
//
//        question.setOrder(index);
//        return questions.add(question);
//    }
//
//    /**
//     * Adds the {@link IQuestion Questions} to the {@link List}.
//     * 
//     * @param questions
//     *            the {@link List}.
//     * 
//     * @param questionsToAdd
//     *            the {@link IQuestion Questions} to add.
//     */
//    public static <T extends IQuestion, S extends T> void addQuestions(
//            final List<T> questions, final Collection<S> questionsToAdd) {
//
//        for (S question : questionsToAdd) {
//
//            AbstractQuestionContainer.addQuestion(questions, question);
//        }
//
//    }
//
//    /**
//     * Removes the {@link IQuestion Question} from the {@link List}.
//     * 
//     * @param questions
//     *            the {@link List}.
//     * 
//     * @param question
//     *            the {@link IQuestion Question} to remove.
//     * @return {@code true}, if the {@link List} was changed by the operation.
//     */
//    public static boolean removeQuestion(
//            final List<? extends IQuestion> questions, final IQuestion question) {
//
//        boolean removed = questions.remove(question);
//        if (removed) {
//
//            question.setOrder(Integer.MAX_VALUE);
//        }
//        return removed;
//    }
//
//    /**
//     * Removes the {@link IQuestion Question} from the {@link List}.
//     * 
//     * @param questions
//     *            the {@link List}.
//     * 
//     * @param questionsToRemove
//     *            the {@link IQuestion Questions} to remove.
//     */
//    public static void removeQuestions(
//            final List<? extends IQuestion> questions,
//            final Collection<? extends IQuestion> questionsToRemove) {
//
//        for (IQuestion question : questionsToRemove) {
//
//            AbstractQuestionContainer.removeQuestion(questions, question);
//        }
//    }
//
//    /**
//     * Removes all {@link IQuestion Questions} from the {@link List}.
//     * 
//     * @param questions
//     *            the {@link List}.
//     */
//    public static void clearQuestions(final List<? extends IQuestion> questions) {
//
//        Iterator<? extends IQuestion> it = questions.iterator();
//        while (it.hasNext()) {
//
//            IQuestion question = it.next();
//
//            it.remove();
//            question.setOrder(Integer.MAX_VALUE);
//        }
//    }
//
//
//    /**
//     * Replaces a {@link IQuestion Question} with another {@link IQuestion
//     * Question}.
//     * 
//     * @param questions
//     *            the {@link List}.
//     * @param questionToReplace
//     *            the {@link IQuestion Question} to replace.
//     * @param replacement
//     *            the replacement {@link IQuestion Question}.
//     * @return {@code true}, if the {@link List} was changed by the operation.
//     */
//    public static <T extends IQuestion, S extends T, U extends T> boolean replace(
//            final List<T> questions, final S questionToReplace,
//            final U replacement) {
//
//        int key = questions.indexOf(questionToReplace);
//
//        if (key >= 0) {
//
//            questions.set(key, replacement);
//
//            replacement.setOrder(questionToReplace.getOrder());
//            questionToReplace.setOrder(Integer.MAX_VALUE);
//
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * Swaps the two {@link IQuestion Questions}.
//     * 
//     * @param questions
//     *            the {@link List}.
//     * @param lhs
//     *            the left-hand side {@link IQuestion Question}.
//     * @param rhs
//     *            the right-hand side {@link IQuestion Question}.
//     * @return {@code true}, if the {@link List} was changed by the operation.
//     */
//    public static <T extends IQuestion, S extends T, U extends T> boolean swap(
//            final List<T> questions, final S lhs, final U rhs) {
//
//        int lhsIndex = questions.indexOf(lhs);
//        int rhsIndex = questions.indexOf(rhs);
//
//        if (lhsIndex >= 0 && rhsIndex >= 0) {
//
//            questions.set(lhsIndex, rhs);
//            questions.set(rhsIndex, lhs);
//
//            int backupOrder = lhs.getOrder();
//
//            lhs.setOrder(rhs.getOrder());
//            rhs.setOrder(backupOrder);
//
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * Re-numbers the {@link IQuestion Questions} in the {@link List} to span
//     * from {@code 0} to {@link List#size()} while maintaining the order of
//     * elements.
//     * 
//     * @param questions
//     *            the {@link List}.
//     */
//    public static void defragmentOrder(List<? extends IQuestion> questions) {
//
//        int i = 0;
//        for (IQuestion question : questions) {
//
//            question.setOrder(i);
//            i++;
//        }
//    }
//
//}
