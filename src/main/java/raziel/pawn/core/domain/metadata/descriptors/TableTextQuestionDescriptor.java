package raziel.pawn.core.domain.metadata.descriptors;


import org.jutility.common.datatype.table.CellLocation;
import org.jutility.common.datatype.table.CellRange;
import org.jutility.common.datatype.table.CellRange.Characteristics;

import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.TextQuestion;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class TableTextQuestionDescriptor
        extends TableQuestionDescriptor<TextQuestion> {


    /**
     * Creates a new instance of the {@link TableTextQuestionDescriptor} class.
     * 
     * @param question
     *            the {@link TextQuestion}.
     */
    public TableTextQuestionDescriptor(final TextQuestion question) {

        super(question);
    }

    /**
     * Creates a new instance of the {@link TableTextQuestionDescriptor} class.
     * 
     * @param cellRange
     *            the {@link CellRange}.
     */
    public TableTextQuestionDescriptor(final CellRange cellRange) {

        super(cellRange);

        if (cellRange.characteristic() != Characteristics.CELL) {

            throw new IllegalArgumentException(
                    "TextQuestionDescriptor cannot contain more than one cell!");
        }
    }

    @Override
    public TextQuestion extractMetadata(Spreadsheet source) {


        CellLocation location = this.getCellRange().getBeginning();

        IDataPoint<?> cell = source.getCell(location);
        if (cell != null) {

            Object cellValue = source.get(location);

            String questionText = null;
            if (cellValue != null) {

                questionText = cellValue.toString();

                TextQuestion question = new TextQuestion("Question "
                        + (this.getOrder() + 1), questionText);

                question.setOrder(this.getOrder());
                question.setDataPoint(cell);

                return question;
            }
            else {

                throw new IllegalArgumentException(
                        "Question text cannot be empty! (Cell value is null)");
            }
        }
        else {

            throw new IllegalArgumentException(
                    "Question text cannot be empty! (Cell is null)");
        }
    }

    @Override
    public String toString() {

        return "TextQuestion in " + super.toString();
    }
}
