package raziel.pawn.core.domain.metadata.descriptors;


import org.jutility.common.datatype.table.CellRange;

import raziel.pawn.core.domain.metadata.IMetadata;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 * @param <S>
 *            the {@link IMetadata Description} type
 */
public abstract class TableMetadataDescriptor<S extends IMetadata>
        implements ITableMetadataDescriptor<S> {


    private S         metadata;
    private CellRange cellRange;

    private String    prefix;
    private String    suffix;

    private int       order;

    @Override
    public S getMetadata() {

        return this.metadata;
    }

    @Override
    public void setMetadata(S metadata) {

        this.metadata = metadata;
    }


    @Override
    public CellRange getCellRange() {

        return cellRange;
    }

    @Override
    public void setCellRange(final CellRange cellRange) {

        this.cellRange = cellRange;
    }


    @Override
    public String getPrefix() {

        if (this.prefix == null) {

            return "";
        }
        return " " + this.prefix.trim();
    }

    @Override
    public void setPrefix(String prefix) {

        this.prefix = prefix;
    }

    @Override
    public String getSuffix() {

        if (this.suffix == null) {

            return "";
        }
        return " " + this.suffix.trim();
    }


    @Override
    public void setSuffix(String suffix) {

        this.suffix = suffix;
    }

    @Override
    public int getOrder() {

        return order;
    }

    @Override
    public void setOrder(int offset) {

        this.order = offset;
    }

    /**
     * Creates a new instance of the {@link TableMetadataDescriptor} class.
     * 
     * @param metadata
     *            the {@link IMetadata Metadata}.
     * @param prefix
     *            the prefix of the {@link IMetadata Metadata} name.
     * @param suffix
     *            the suffix of the {@link IMetadata Metadata} name.
     */
    public TableMetadataDescriptor(final S metadata, String prefix,
            String suffix) {

        this(metadata, null, prefix, suffix);
    }

    /**
     * Creates a new instance of the {@link TableMetadataDescriptor} class.
     * 
     * @param cellRange
     *            the {@link CellRange}.
     * @param prefix
     *            the name prefix for created {@link IMetadata}.
     * @param suffix
     *            the name suffix for created {@link IMetadata}.
     */
    public TableMetadataDescriptor(final CellRange cellRange, String prefix,
            String suffix) {

        this(null, cellRange, prefix, suffix);
    }

    /**
     * Creates a new instance of the {@link TableMetadataDescriptor} class.
     * 
     * @param metadata
     *            the {@link IMetadata Metadata}.
     * @param cellRange
     *            the {@link CellRange}.
     * @param prefix
     *            the prefix of the {@link IMetadata Metadata} name.
     * @param suffix
     *            the suffix of the {@link IMetadata Metadata} name.
     */
    private TableMetadataDescriptor(final S metadata,
            final CellRange cellRange, String prefix, String suffix) {

        if (metadata == null && cellRange == null) {

            throw new IllegalArgumentException(
                    "Metadata and cell range cannot both be null!");
        }

        this.metadata = metadata;
        this.cellRange = cellRange;
        this.prefix = prefix;
        this.suffix = suffix;
        this.order = 0;
    }

    @Override
    public String toString() {

        if (this.cellRange != null) {

            return this.cellRange.characteristic() + " " + this.cellRange;
        }
        return "Empty Descriptor.";
    }
}
