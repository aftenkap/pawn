package raziel.pawn.core.domain.metadata.descriptors;


import org.jutility.common.datatype.table.CellRange;

import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.IMetadata;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 * @param <S>
 */
public interface ITableMetadataDescriptor<S extends IMetadata>
        extends IDataPointDescriptor<Spreadsheet, S> {


    /**
     * Returns the {@link CellRange}.
     * 
     * @return the {@link CellRange}.
     */
    public abstract CellRange getCellRange();

    /**
     * Sets the {@link CellRange}.
     * 
     * @param cellRange
     *            the {@link CellRange}.
     */
    public abstract void setCellRange(final CellRange cellRange);

    /**
     * Returns the prefix.
     * 
     * @return the prefix.
     */
    public String getPrefix();


    /**
     * Sets the prefix.
     * 
     * @param prefix
     *            the prefix.
     */
    public void setPrefix(String prefix);


    /**
     * Returns the suffix.
     * 
     * @return the suffix.
     */
    public String getSuffix();


    /**
     * Sets the suffix.
     * 
     * @param suffix
     *            the suffix.
     */
    public void setSuffix(String suffix);
}
