package raziel.pawn.core.domain.metadata;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.core.domain.data.IDataSource;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.dto.data.DataSourceDetails;
import raziel.pawn.dto.data.SpreadsheetDTO;
import raziel.pawn.dto.metadata.MetadataSourceDetails;
import raziel.pawn.dto.metadata.ParticipantDTO;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.0
 */
public abstract class MetadataSource
        extends DomainObject
        implements IMetadataSource {

    /**
     * Serial version uid.
     */
    private static final long  serialVersionUID = 1L;

    private List<IParticipant> participants;


    private IDataSource        dataSource;

    @Override
    public List<IParticipant> getParticipants() {

        return this.participants;
    }

    @Override
    public boolean addParticipant(IParticipant participant) {

        return this.participants.add(participant);
    }

    @Override
    public boolean removeParticipant(IParticipant participant) {

        return this.participants.remove(participant);
    }

    @Override
    public void clearParticipants() {

        this.participants.clear();
    }

    @Override
    public IDataSource getDataSource() {

        return this.dataSource;
    }

    @Override
    public void setDataSource(IDataSource dataSource) {

        this.dataSource = dataSource;
    }



    /**
     * Creates a new instance of the {@link MetadataSource} class.
     */
    public MetadataSource() {

        this("");
    }

    /**
     * Creates a new instance of the {@link MetadataSource} class.
     * 
     * @param name
     *            the name of the data collection tool.
     */
    public MetadataSource(String name) {

        super(name);

        this.participants = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link MetadataSource} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public MetadataSource(MetadataSourceDetails details) {

        super(details);

        this.participants = new LinkedList<>();

        if (details.getDataSource() != null) {

            DataSourceDetails dataSource = details.getDataSource();
            if (dataSource instanceof SpreadsheetDTO) {

                this.dataSource = Spreadsheet
                        .fromDTO((SpreadsheetDTO) dataSource);
            }
        }

        if (details.getParticipants() != null) {

            for (ParticipantDTO participant : details.getParticipants()) {

                this.participants.add(Participant.fromDetails(participant));
            }
        }
    }



    @Override
    public MetadataSourceDetails toDTO() {

        MetadataSourceDetails details = new MetadataSourceDetails(super.toDTO());


        if (this.dataSource != null) {
            details.setDataSource(this.dataSource.toDTO());
        }

        List<ParticipantDTO> participantDTO = new ArrayList<>(
                this.participants.size());
        for (IParticipant participant : this.participants) {

            participantDTO.add(participant.toDTO());
        }
        details.setParticipants(participantDTO);

        return details;
    }
}
