package raziel.pawn.core.domain.metadata;


/**
 * @author Peter J. Radics
 * @version 0.1
 * 
 */
public enum QuestionType {

    /**
     * A text question.
     */
    TEXT("Text Question"),

    /**
     * A choice question.
     */
    CHOICE("Choice Question"),

    /**
     * A ranking question.
     */
    RANKING("Ranking Question"),
    // TODO: implement support
    // /**
    // *
    // */
    // TEXT_ARRAY("Choice Question"),
    //
    // /**
    // *
    // */
    // CHOICE_ARRAY("Choice Question")
    ;

    private final String name;

    /**
     * Creates a new instance of the {@link QuestionType} class.
     * 
     * @param name
     *            the name.
     */
    private QuestionType(String name) {

        this.name = name;
    }

    @Override
    public String toString() {

        return this.name;
    }
}
