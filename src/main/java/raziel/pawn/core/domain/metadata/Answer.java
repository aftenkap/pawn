package raziel.pawn.core.domain.metadata;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.SpreadsheetCell;
import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.metadata.AnswerDetails;
import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.ParticipantDTO;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;
import raziel.pawn.dto.metadata.TextQuestionDetails;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class Answer
        extends DomainObject
        implements IAnswer {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;



    private IParticipant      participant;
    private IQuestion         question;

    private IDataPoint<?>     dataPoint;

    private String            value;

    @Override
    public IParticipant getParticipant() {

        return this.participant;
    }



    @Override
    public IQuestion getQuestion() {

        return this.question;
    }


    @Override
    public void setQuestion(IQuestion question) {

        this.question = question;
    }



    @Override
    public IDataPoint<?> getDataPoint() {

        return this.dataPoint;
    }


    @Override
    public void setDataPoint(IDataPoint<?> dataPoint) {

        this.dataPoint = dataPoint;
    }



    @Override
    public String getValue() {

        return this.value;
    }

    @Override
    public void setValue(String value) {

        this.value = value;
    }


    /**
     * Creates a new instance of the {@link Answer} class.
     */
    protected Answer() {

        this("", null, null);
    }

    /**
     * Creates a new instance of the {@link Answer} class with the provided
     * question and participant.
     * 
     * @param name
     *            the name of the answer.
     * @param question
     *            the {@IQuestion question} to which this object is
     *            the answer.
     * @param participant
     *            the (@Participant} who answered the question.
     */
    public Answer(final String name, final IQuestion question,
            final IParticipant participant) {

        super(name);

        this.question = question;
        this.participant = participant;
    }


    /**
     * Creates a new instance of the {@link Answer} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Answer(AnswerDetails details) {

        super(details);

        DataPointDTO dataPointDTO = details.getDataPoint();
        if (dataPointDTO != null
                && dataPointDTO instanceof SpreadsheetCellDTO) {

            this.dataPoint = SpreadsheetCell
                    .fromDTO((SpreadsheetCellDTO) dataPointDTO);
        }

        ParticipantDTO participantDTO = details.getParticipant();
        if (participantDTO != null) {

            this.participant = Participant.fromDetails(participantDTO);
        }

        QuestionDetails questionDetails = details.getQuestion();
        if (questionDetails != null) {

            if (questionDetails instanceof TextQuestionDetails) {

                this.question = TextQuestion
                        .fromDetails((TextQuestionDetails) questionDetails);
            }
            else if (questionDetails instanceof ChoiceQuestionDetails) {

                this.question = ChoiceQuestion
                        .fromDetails((ChoiceQuestionDetails) questionDetails);
            }
            else if (questionDetails instanceof RankingQuestionDetails) {

                this.question = RankingQuestion
                        .fromDetails((RankingQuestionDetails) questionDetails);
            }
        }

        this.setValue(details.getValue());
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.DomainObject#toDetails()
     */
    @Override
    public AnswerDetails toDTO() {

        AnswerDetails details = new AnswerDetails(super.toDTO());


        if (this.getDataPoint() != null) {

            details.setDataPoint(this.dataPoint.toDTO());
        }

        if (this.question != null) {

            details.setQuestion(this.question.toDTO());
        }

        if (this.participant != null) {

            details.setParticipant(participant.toDTO());
        }

        details.setValue(this.getValue());

        return details;
    }

    /**
     * Creates a Answer corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a Answer corresponding to the provided details.
     */
    public static Answer fromDetails(AnswerDetails details) {

        return new Answer(details);
    }


    /**
     * Returns a List of {@link Answer Answers} corresponding to the provided
     * details.
     * 
     * @param details
     *            the {@link AnswerDetails}.
     * @return a List of {@link Answer Answers} corresponding to the provided
     *         details.
     */
    public static List<Answer> fromDetails(Collection<AnswerDetails> details) {

        List<Answer> objects = new ArrayList<>(details.size());

        for (AnswerDetails detail : details) {

            objects.add(Answer.fromDetails(detail));
        }

        return objects;
    }
}
