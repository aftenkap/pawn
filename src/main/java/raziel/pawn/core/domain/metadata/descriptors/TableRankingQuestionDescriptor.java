package raziel.pawn.core.domain.metadata.descriptors;


import java.util.Objects;

import org.jutility.common.datatype.table.CellLocation;
import org.jutility.common.datatype.table.CellRange;

import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.ChoiceQuestion;
import raziel.pawn.core.domain.metadata.ChoiceSet;
import raziel.pawn.core.domain.metadata.RankingQuestion;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class TableRankingQuestionDescriptor
        extends TableQuestionDescriptor<RankingQuestion> {


    private final ChoiceSet choiceSet;

    private String          rankPrefix;
    private String          rankSuffix;

    /**
     * Returns the choiceSet.
     * 
     * @return the choiceSet.
     */
    public ChoiceSet getChoiceSet() {

        return choiceSet;
    }



    /**
     * Returns the rankPrefix.
     * 
     * @return the rankPrefix.
     */
    public String getRankPrefix() {

        if (this.rankPrefix == null) {

            return "";
        }
        return this.rankPrefix.trim();
    }



    /**
     * Sets the rankPrefix.
     * 
     * @param rankPrefix
     *            the rankPrefix.
     */
    public void setRankPrefix(String rankPrefix) {

        this.rankPrefix = rankPrefix;
    }



    /**
     * Returns the rankSuffix.
     * 
     * @return the rankSuffix.
     */
    public String getRankSuffix() {

        if (this.rankSuffix == null) {

            return "";
        }
        return this.rankSuffix.trim();
    }



    /**
     * Sets the rankSuffix.
     * 
     * @param rankSuffix
     *            the rankSuffix.
     */
    public void setRankSuffix(String rankSuffix) {

        this.rankSuffix = rankSuffix;
    }



    /**
     * Creates a new instance of the {@link TableRankingQuestionDescriptor}
     * class.
     * 
     * @param question
     *            the {@link RankingQuestion}.
     */
    public TableRankingQuestionDescriptor(final RankingQuestion question) {

        super(question);

        this.choiceSet = question.getChoices();
    }

    /**
     * Creates a new instance of the {@link TableRankingQuestionDescriptor}
     * class.
     * 
     * @param cellRange
     *            the range of the question.
     * @param choiceSet
     *            the {@link ChoiceSet Choices}.
     */
    public TableRankingQuestionDescriptor(CellRange cellRange,
            ChoiceSet choiceSet) {

        super(cellRange);

        Objects.requireNonNull(choiceSet, this.getClass().getSimpleName()
                + " requires valid Choice Set!");

        this.choiceSet = choiceSet;

        this.rankPrefix = "[Ranking";
        this.rankSuffix = "]";
    }

    @Override
    public RankingQuestion extractMetadata(Spreadsheet source) {

        RankingQuestion question = new RankingQuestion(this.getPrefix() + " "
                + (this.getOrder() + 1));
        question.setOrder(this.getOrder());

        question.setChoices(this.getChoiceSet());

        int rank = 0;

        for (CellLocation location : this.getCellRange()) {

            IDataPoint<?> cell = source.getCell(location);
            if (cell != null) {

                Object cellValue = source.get(location);

                String questionText = null;
                if (cellValue != null) {

                    questionText = cellValue.toString();

                    if (question.getQuestionText().isEmpty()) {

                        question.setQuestionText(questionText.substring(0,
                                questionText.indexOf(this.getRankPrefix())));
                    }

                    ChoiceQuestion ranking = question.getRanks().get(rank);

                    ranking.setQuestionText(questionText);
                    ranking.setChoices(this.getChoiceSet());
                    ranking.setDataPoint(cell);
                    ranking.setOrder(rank);
                }
                else {

                    throw new IllegalArgumentException(
                            "Question text cannot be empty! (Cell value is null)");
                }
            }
            else {

                throw new IllegalArgumentException(
                        "Question text cannot be empty! (Cell is null)");
            }

            rank++;
        }

        return question;
    }

    @Override
    public String toString() {

        return "RankingQuestion in " + super.toString()
                + " ranking choices from " + this.choiceSet;
    }
}
