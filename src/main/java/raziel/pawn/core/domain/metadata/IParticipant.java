/**
 * 
 */
package raziel.pawn.core.domain.metadata;


import raziel.pawn.dto.metadata.ParticipantDTO;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
public interface IParticipant
        extends IMetadata {

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.metadata.IParticipant#toDetails()
     */
    @Override
    public abstract ParticipantDTO toDTO();

}