package raziel.pawn.core.domain.metadata.descriptors;


import java.util.LinkedList;
import java.util.List;

import raziel.pawn.core.domain.data.IDataSource;
import raziel.pawn.core.domain.metadata.IMetadata;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 * @param <S>
 *            the {@link IDataSource DataSource} type.
 */
public interface IDataSourceDescriptor<S extends IDataSource> {

    /**
     * Returns the {@link IDataSource Target} of the description.
     * 
     * @return the {@link IDataSource Target} of the description.
     */
    public abstract S getDataSource();

    /**
     * Returns the {@link IDataPointDescriptor DataPointDescriptors}.
     * 
     * @param type
     *            the {@link IMetadata} type.
     * @return the {@link IDataPointDescriptor DataPointDescriptors}.
     */
    public abstract List<IDataPointDescriptor<S, ?>> getDataPointDescriptors(
            final Class<? extends IMetadata> type);

    /**
     * Adds the {@link IDataPointDescriptor DataPointDescriptor}.
     * 
     * @param type
     *            the {@link IMetadata} type.
     * @param descriptor
     *            the descriptor to add.
     * @return if the collection was changed by the operation.
     */
    public abstract <M extends IMetadata> boolean addDataPointDescriptor(
            final Class<M> type,
            final IDataPointDescriptor<S, ? extends M> descriptor);

    /**
     * Adds the {@link IDataPointDescriptor DataPointDescriptor}.
     * 
     * @param type
     *            the {@link IMetadata} type.
     * @param descriptor
     *            the descriptor to remove.
     * @return if the collection was changed by the operation.
     */
    public abstract <M extends IMetadata> boolean removeDataPointDescriptor(
            final Class<M> type,
            final IDataPointDescriptor<S, ? extends M> descriptor);


    /**
     * Creates the {@link IMetadata Descriptions} of the provided type.
     * 
     * @param type
     *            the type.
     * @return the {@link IMetadata Descriptions} of the provided type.
     */
    public default <M extends IMetadata> List<M> createDescriptions(
            Class<M> type) {

        List<M> descriptions = new LinkedList<>();

        int offset = 0;

        for (IDataPointDescriptor<S, ?> descriptor : this
                .getDataPointDescriptors(type)) {

            descriptor.setOrder(offset);
            IMetadata metadata = descriptor.extractMetadata(this
                    .getDataSource());


            if (type.isAssignableFrom(metadata.getClass())) {

                descriptions.add(type.cast(metadata));
            }
            else {

                throw new IllegalStateException("Descriptor " + descriptor
                        + " created Metadata of type " + metadata.getClass()
                        + " instead of type compatible with designated"
                        + " type " + type);
            }
            offset = descriptor.getOrder();
        }

        return descriptions;
    }
}
