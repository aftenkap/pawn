package raziel.pawn.core.domain.metadata;


import java.util.List;

import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.descriptors.TableDescriptor;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.data.SpreadsheetCellDTO;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class SurveyTableLinker {



    /**
     * Automatically creates {@link IQuestion Questions} from the data in the
     * {@link Spreadsheet}.
     * 
     * @param link
     *            the {@link TableDescriptor Link} containing the information
     *            needed to link the table to the survey.
     * @return the updated survey.
     * @throws RequestFailedException
     *             if the creation of a question or the update of the survey
     *             failed.
     */
    public static List<IQuestion> parseQuestions(TableDescriptor link)
            throws RequestFailedException {

        return link.createDescriptions(IQuestion.class);
    }


    /**
     * Automatically creates {@link Participant Participants} from the data in
     * the {@link Spreadsheet} and adds them to the {@link Survey}.
     * 
     * @param link
     *            the {@link TableDescriptor Link} containing the information
     *            needed to link the table to the survey.
     * @return the updated survey.
     * @throws RequestFailedException
     *             if creating a participant or updating the survey failed.
     */
    public static List<IParticipant> parseParticipantIDs(TableDescriptor link)
            throws RequestFailedException {

        return link.createDescriptions(IParticipant.class);
    }



    /**
     * Automatically creates {@link Answer Answers} from the data in the
     * {@link Spreadsheet} and adds them to the {@link Survey}.
     * 
     * @param link
     * 
     * @precondition Requires a {@link Survey} containing {@link IQuestion
     *               Questions} and {@link Participant Participants} linked to
     *               {@link SpreadsheetCellDTO DataCells} within the {@link Spreadsheet}
     *               .
     * @return returns a {@link List} of {@link IAnswer Answers}.
     */
    public static List<IAnswer> parseAnswers(TableDescriptor link) {



        return link.createDescriptions(IAnswer.class);

        // for (Participant participant : survey.getParticipants()) {
        //
        // IDataPoint<?> participantDataPoint = participant.getDataPoint();
        //
        // if (participantDataPoint instanceof SpreadsheetCell) {
        //
        // SpreadsheetCell<?> participantDataCell = (SpreadsheetCell<?>)
        // participantDataPoint;
        //
        // for (IQuestion question : survey.getQuestions()) {
        //
        // IDataPoint<?> questionDataPoint = question.getDataPoint();
        //
        // if (questionDataPoint instanceof SpreadsheetCell) {
        //
        // SpreadsheetCell<?> questionDataCell = (SpreadsheetCell<?>) questionDataPoint;
        //
        // int maxRow = Math.max(participantDataCell.getRow(),
        // questionDataCell.getRow());
        // int maxColumn = Math.max(
        // participantDataCell.getColumn(),
        // questionDataCell.getColumn());
        //
        // CellLocation answerLocation = null;
        //
        // if (table.cellRange().contains(maxRow, maxColumn)) {
        //
        // answerLocation = new CellLocation(maxRow, maxColumn);
        // }
        // else {
        // int minRow = Math.min(participantDataCell.getRow(),
        // questionDataCell.getRow());
        // int minColumn = Math.min(
        // participantDataCell.getColumn(),
        // questionDataCell.getColumn());
        //
        //
        // if (table.cellRange().contains(minRow, minColumn)) {
        //
        // answerLocation = new CellLocation(minRow,
        // minColumn);
        // }
        // }
        //
        //
        // if (answerLocation != null) {
        //
        // System.out.println("AnswerLocation: "
        // + answerLocation);
        // IDataPoint<?> answerDataCell = table
        // .getCell(answerLocation);
        //
        // Answer answer = new Answer(participant.getName()
        // + " - " + question.getName(), question,
        // participant);
        //
        //
        // if (answerDataCell != null
        // && answerDataCell.getValue() != null) {
        //
        // answer.setValue(answerDataCell.getValue()
        // .toString());
        // }
        // else {
        //
        // answer.setValue(null);
        // }
        //
        // answer.setDataPoint(answerDataCell);
        //
        // survey.getAnswers().add(answer);
        // }
        //
        // }
        // }
        // }
        // }
    }



//    private static Survey updateSurvey(Survey survey)
//            throws RequestFailedException {
//
//
//        PerformedUpdateEvent<SurveyDetails> result = SurveyTableLinker
//                .surveyService().updateSurvey(
//                        new RequestUpdateEvent<>(survey.getId(), survey
//                                .toDetails()));
//
//        if (result.isEntityFound()) {
//
//            return Survey.fromDetails(result.getObjectDetails());
//        }
//
//        throw new RequestFailedException("Could not update Survey " + survey
//                + "!");
//    }



    // /**
    // * @param args
    // * unused
    // * @throws SerializationException
    // * @throws RequestFailedException
    // */
    // public static void main(String[] args)
    // throws SerializationException, RequestFailedException {
    //
    // SpreadsheetController tableController = SpringApplicationContext
    // .tableController();
    // SurveyController controller = SpringApplicationContext
    // .surveyController();
    //
    // Survey survey = controller.retrieveSurvey(1);
    //
    //
    // Spreadsheet<?> dataTable = tableController.retrieve(8);
    //
    // TableDescriptor map = new TableDescriptor(dataTable);
    //
    // ChoiceSet gender = null;
    // ChoiceSet relationship = null;
    // ChoiceSet livingEnvironment = null;
    // ChoiceSet yesNo = null;
    // ChoiceSet occupation = null;
    // ChoiceSet rankingRoles = null;
    // try {
    //
    // gender = controller.retrieveChoiceSetWithName("Gender");
    // relationship = controller.retrieveChoiceSetWithName("Relationship");
    // livingEnvironment = controller
    // .retrieveChoiceSetWithName("Living Environment");
    // yesNo = controller.retrieveChoiceSetWithName("Yes/No");
    // occupation = controller.retrieveChoiceSetWithName("Occupation");
    // rankingRoles = controller
    // .retrieveChoiceSetWithName("Ranking Roles");
    // }
    // catch (RequestFailedException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    //
    //
    //
    // for (int i = 0; i < 38; i++) {
    //
    //
    // switch (i) {
    //
    // case 0:
    // map.addQuestion(new CellRange(0, 1, 1, 2),
    // QuestionType.TEXT);
    // break;
    // case 1:
    // map.addQuestion(new CellRange(0, 2, 1, 3),
    // QuestionType.CHOICE, gender);
    // break;
    // case 2:
    // map.addQuestion(new CellRange(0, 3, 1, 5),
    // QuestionType.CHOICE, occupation);
    // break;
    // case 3:
    // map.addQuestion(new CellRange(0, 5, 1, 7),
    // QuestionType.CHOICE, livingEnvironment);
    // break;
    // case 4:
    // map.addQuestion(new CellRange(0, 7, 1, 8),
    // QuestionType.TEXT);
    // break;
    // case 5:
    // map.addQuestion(new CellRange(0, 8, 1, 9),
    // QuestionType.CHOICE, gender);
    // break;
    // case 6:
    // map.addQuestion(new CellRange(0, 9, 1, 11),
    // QuestionType.CHOICE, relationship);
    // break;
    // case 7:
    // map.addQuestion(new CellRange(0, 11, 1, 12),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 8:
    // map.addQuestion(new CellRange(0, 12, 1, 13),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 9:
    // map.addQuestion(new CellRange(0, 13, 1, 14),
    // QuestionType.CHOICE, gender);
    // break;
    // case 10:
    // map.addQuestion(new CellRange(0, 14, 1, 16),
    // QuestionType.CHOICE, relationship);
    // break;
    // case 11:
    // map.addQuestion(new CellRange(0, 16, 1, 17),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 12:
    // map.addQuestion(new CellRange(0, 17, 1, 18),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 13:
    // map.addQuestion(new CellRange(0, 18, 1, 19),
    // QuestionType.CHOICE, gender);
    // break;
    // case 14:
    // map.addQuestion(new CellRange(0, 19, 1, 21),
    // QuestionType.CHOICE, relationship);
    // break;
    // case 15:
    // map.addQuestion(new CellRange(0, 21, 1, 22),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 16:
    // map.addQuestion(new CellRange(0, 22, 1, 23),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 17:
    // map.addQuestion(new CellRange(0, 23, 1, 24),
    // QuestionType.CHOICE, gender);
    // break;
    // case 18:
    // map.addQuestion(new CellRange(0, 24, 1, 26),
    // QuestionType.CHOICE, relationship);
    // break;
    // case 19:
    // map.addQuestion(new CellRange(0, 26, 1, 27),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 20:
    // map.addQuestion(new CellRange(0, 27, 1, 28),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 21:
    // map.addQuestion(new CellRange(0, 28, 1, 29),
    // QuestionType.CHOICE, gender);
    // break;
    // case 22:
    // map.addQuestion(new CellRange(0, 29, 1, 31),
    // QuestionType.CHOICE, relationship);
    // break;
    // case 23:
    // map.addQuestion(new CellRange(0, 31, 1, 32),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 24:
    // map.addQuestion(new CellRange(0, 32, 1, 33),
    // QuestionType.CHOICE, yesNo);
    // break;
    // case 25:
    // map.addQuestion(new CellRange(0, 33, 1, 35),
    // QuestionType.TEXT);
    // break;
    // case 26:
    // map.addQuestion(new CellRange(0, 35, 1, 47),
    // QuestionType.RANKING, rankingRoles);
    // break;
    // case 27:
    // map.addQuestion(new CellRange(0, 47, 1, 49),
    // QuestionType.TEXT);
    // break;
    // case 28:
    // map.addQuestion(new CellRange(0, 49, 1, 61),
    // QuestionType.RANKING, rankingRoles);
    // break;
    // case 29:
    // map.addQuestion(new CellRange(0, 61, 1, 63),
    // QuestionType.TEXT);
    // break;
    // case 30:
    // map.addQuestion(new CellRange(0, 63, 1, 75),
    // QuestionType.RANKING, rankingRoles);
    // break;
    // case 31:
    // map.addQuestion(new CellRange(0, 75, 1, 76),
    // QuestionType.TEXT);
    // break;
    // case 32:
    // map.addQuestion(new CellRange(0, 76, 1, 88),
    // QuestionType.RANKING, rankingRoles);
    // break;
    // case 33:
    // map.addQuestion(new CellRange(0, 88, 1, 90),
    // QuestionType.TEXT);
    // break;
    // case 34:
    // break;
    // case 35:
    // break;
    // case 36:
    // break;
    // case 37:
    // break;
    // }
    //
    //
    // }
    //
    // SurveyTableLinker.parseQuestions(dataTable, survey, map);
    //
    //
    // System.out.println("Survey: " + survey);
    // for (IQuestion question : survey.getQuestions()) {
    //
    // System.out.println("  " + question);
    // }
    // }
}
