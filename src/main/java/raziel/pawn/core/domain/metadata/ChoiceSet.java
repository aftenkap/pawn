/**
 * 
 */
package raziel.pawn.core.domain.metadata;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.dto.metadata.ChoiceDetails;
import raziel.pawn.dto.metadata.ChoiceSetDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class ChoiceSet
        extends DomainObject
        implements Set<Choice> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    private Set<Choice>       choices;


    /**
     * Returns the {@link Choice Choices} of this {@link ChoiceSet}.
     * 
     * @return the {@link Choice Choices} of this {@link ChoiceSet}.
     */
    public Set<Choice> getChoices() {

        return this.choices;
    }


    /**
     * Creates a new instance of the {@link ChoiceSet} class.
     */
    public ChoiceSet() {

        this("");
    }

    /**
     * Creates a new instance of the {@link ChoiceSet} class.
     * 
     * @param name
     *            the name of the {@link ChoiceSet}.
     */
    public ChoiceSet(String name) {

        super(name);

        this.choices = new LinkedHashSet<>();
    }

    /**
     * Creates a new instance of the {@link ChoiceSet} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public ChoiceSet(ChoiceSetDetails details) {

        super(details);

        this.choices = new LinkedHashSet<>();
        if (details.getChoices() != null) {

            for (ChoiceDetails choiceDetails : details.getChoices()) {

                this.choices.add(Choice.fromDetails(choiceDetails));
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.DomainObject#toDetails()
     */
    @Override
    public ChoiceSetDetails toDTO() {

        ChoiceSetDetails details = new ChoiceSetDetails(super.toDTO());


        if (!this.choices.isEmpty()) {

            List<ChoiceDetails> choiceDetails = new ArrayList<>(
                    this.choices.size());

            for (Choice choice : this.choices) {

                choiceDetails.add(choice.toDTO());
            }
            details.setChoices(choiceDetails);
        }

        return details;
    }

    /**
     * Returns a {@link ChoiceSet} corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a {@link ChoiceSet} corresponding to the provided details.
     */
    public static ChoiceSet fromDetails(ChoiceSetDetails details) {

        return new ChoiceSet(details);
    }



    /**
     * Returns a List of {@link ChoiceSet ChoiceSets} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link ChoiceSetDetails}.
     * @return a List of {@link ChoiceSet ChoiceSets} corresponding to the
     *         provided details.
     */
    public static List<ChoiceSet> fromDetails(
            Collection<ChoiceSetDetails> details) {

        List<ChoiceSet> objects = new ArrayList<>(details.size());

        for (ChoiceSetDetails detail : details) {

            objects.add(ChoiceSet.fromDetails(detail));
        }

        return objects;
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#size()
     */
    @Override
    public int size() {

        return this.choices.size();
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#isEmpty()
     */
    @Override
    public boolean isEmpty() {

        return this.choices.isEmpty();
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#contains(java.lang.Object)
     */
    @Override
    public boolean contains(Object o) {

        return this.choices.contains(o);
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#iterator()
     */
    @Override
    public Iterator<Choice> iterator() {

        return this.choices.iterator();
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#toArray()
     */
    @Override
    public Object[] toArray() {

        return this.choices.toArray();
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#toArray(java.lang.Object[])
     */
    @Override
    public <T> T[] toArray(T[] a) {

        return this.choices.toArray(a);
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#add(java.lang.Object)
     */
    @Override
    public boolean add(Choice e) {

        return this.choices.add(e);
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#remove(java.lang.Object)
     */
    @Override
    public boolean remove(Object o) {

        return this.choices.remove(o);
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#containsAll(java.util.Collection)
     */
    @Override
    public boolean containsAll(Collection<?> c) {

        return this.choices.containsAll(c);
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#addAll(java.util.Collection)
     */
    @Override
    public boolean addAll(Collection<? extends Choice> c) {

        return this.choices.addAll(c);
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#retainAll(java.util.Collection)
     */
    @Override
    public boolean retainAll(Collection<?> c) {

        return this.choices.retainAll(c);
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#removeAll(java.util.Collection)
     */
    @Override
    public boolean removeAll(Collection<?> c) {

        return this.choices.removeAll(c);
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.util.Set#clear()
     */
    @Override
    public void clear() {

        this.choices.clear();
    }
}
