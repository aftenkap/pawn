package raziel.pawn.core.domain.metadata;


import java.util.List;

import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.core.domain.data.IDataSource;
import raziel.pawn.dto.metadata.MetadataSourceDetails;


/**
 *
 *
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public interface IMetadataSource
        extends IDomainObject {

    /**
     * Returns a list of participants.
     *
     * @return the list of participants.
     */
    public List<IParticipant> getParticipants();

    /**
     * Adds a participant.
     *
     * @param participant
     *            the participant to add.
     * @return {@code true} if this collection changed as a result of the call.
     */
    public boolean addParticipant(final IParticipant participant);

    /**
     * Removes a participant.
     *
     * @param participant
     *            the participant to remove.
     * @return {@code true} if this collection changed as a result of the call.
     */
    public boolean removeParticipant(final IParticipant participant);

    /**
     * Clears the list of participants.
     */
    public void clearParticipants();

    /**
     * Returns the {@link IDataSource data source}.
     *
     * @return the {@link IDataSource data source}.
     */
    public IDataSource getDataSource();

    /**
     * Sets the {@link IDataSource data source}.
     *
     * @param dataSource
     *            the {@link IDataSource data source}.
     */
    public void setDataSource(IDataSource dataSource);

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.IDomainObject#toDetails()
     */
    @Override
    public MetadataSourceDetails toDTO();
}
