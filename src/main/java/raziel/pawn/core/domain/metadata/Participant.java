package raziel.pawn.core.domain.metadata;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.SpreadsheetCell;
import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.metadata.ParticipantDTO;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.0
 */
public class Participant
        extends DomainObject
        implements IParticipant {


    private IDataPoint<?>     dataPoint;



    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link Participant} class.
     */
    protected Participant() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Participant} class.
     * 
     * @param name
     *            the participant's name.
     */
    public Participant(String name) {

        super(name);
        this.dataPoint = null;
    }

    /**
     * Returns the {@link IDataPoint} used to derive this Participant's
     * identifier (name).
     * 
     * @return the ID {@link IDataPoint}.
     */
    @Override
    public IDataPoint<?> getDataPoint() {

        return this.dataPoint;
    }


    /**
     * Sets the {@link IDataPoint} used to derive this Participant's identifier
     * (name).
     * 
     * @param dataPoint
     *            the ID {@link IDataPoint}.
     */
    @Override
    public void setDataPoint(IDataPoint<?> dataPoint) {

        this.dataPoint = dataPoint;
    }

    /**
     * Creates a new instance of the {@link Participant} class from the provided
     * DTO.
     * 
     * @param dto
     *            the DTO.
     */
    protected Participant(ParticipantDTO dto) {

        super(dto);

        DataPointDTO dataPointDTO = dto.getDataPoint();
        if (dataPointDTO != null) {

            if (dataPointDTO instanceof SpreadsheetCellDTO) {

                this.dataPoint = SpreadsheetCell
                        .fromDTO((SpreadsheetCellDTO) dataPointDTO);
            }
        }
    }

    @Override
    public ParticipantDTO toDTO() {

        ParticipantDTO details = new ParticipantDTO(super.toDTO());

        if (this.getDataPoint() != null) {

            details.setDataPoint(this.dataPoint.toDTO());
        }
        return details;
    }

    /**
     * Creates a Participant corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a Participant corresponding to the provided details.
     */
    public static Participant fromDetails(ParticipantDTO details) {

        return new Participant(details);
    }

    /**
     * Returns a List of {@link Participant Participants} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link ParticipantDTO}.
     * @return a List of {@link Participant Participants} corresponding to the
     *         provided details.
     */
    public static List<Participant> fromDetails(
            Collection<ParticipantDTO> details) {

        List<Participant> objects = new ArrayList<>(details.size());

        for (ParticipantDTO detail : details) {

            objects.add(Participant.fromDetails(detail));
        }

        return objects;
    }
}
