package raziel.pawn.core.domain.metadata.descriptors;


import org.jutility.common.datatype.table.CellLocation;
import org.jutility.common.datatype.table.CellRange;
import org.jutility.common.datatype.table.CellRange.Characteristics;

import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.Spreadsheet;
import raziel.pawn.core.domain.metadata.Participant;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class TableParticipantDescriptor
        extends TableParticipantDescriptorBase<Participant> {



    /**
     * Creates a new instance of the {@link TableParticipantDescriptor} class.
     * 
     * @param cellRange
     *            the cell range.
     */
    public TableParticipantDescriptor(final CellRange cellRange) {

        super(cellRange);

        if (!cellRange.characteristic().equals(Characteristics.CELL)) {

            throw new IllegalArgumentException(
                    "Cannot create Participant Descriptor from more than one cell!");
        }
    }



    @Override
    public Participant extractMetadata(Spreadsheet source) {


        CellLocation location = this.getCellRange().getBeginning();


        IDataPoint<?> cell = source.getCell(location);
        Participant participant = null;
        if (cell != null) {

            Object cellValue = source.get(location);

            String participantID = null;
            if (cellValue != null) {

                participantID = cellValue.toString();

                participant = new Participant(this.getPrefix() + participantID
                        + this.getSuffix());

                participant.setDataPoint(cell);
            }
        }

        if (participant == null) {

            throw new IllegalStateException(
                    "Could not create Participant from Descriptor!");
        }

        return participant;
    }
}
