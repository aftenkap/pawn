package raziel.pawn.core.domain.metadata;


import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.SpreadsheetCell;
import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.metadata.QuestionDetails;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.0
 */

public abstract class Question
        extends DomainObject
        implements IQuestion {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    private String            questionText;
    private boolean           isAnswerRequired;

    private int               order;

    private IDataPoint<?>     dataPoint;

    @Override
    public String getQuestionText() {

        return this.questionText;
    }


    @Override
    public void setQuestionText(String questionText) {

        this.questionText = questionText;

    }

    @Override
    public boolean isAnswerRequired() {

        return this.isAnswerRequired;
    }


    @Override
    public int getOrder() {

        return this.order;
    }


    @Override
    public void setOrder(int order) {

        this.order = order;
    }

    @Override
    public void setAnswerRequired(final boolean isAnswerRequired) {

        this.isAnswerRequired = isAnswerRequired;
    }

    @Override
    public IDataPoint<?> getDataPoint() {

        return this.dataPoint;
    }

    @Override
    public void setDataPoint(IDataPoint<?> dataPoint) {

        this.dataPoint = dataPoint;
    }



    /**
     * Creates a new instance of the {@link Question} class.
     */
    public Question() {

        this("", "");
    }

    /**
     * Creates a new instance of the {@link Question} class.
     * <p/>
     * Sets the question text to the string provided.
     * 
     * @param name
     *            the name of the question.
     * 
     * @param questionText
     *            the question text.
     */
    public Question(final String name, final String questionText) {

        super(name);

        this.questionText = questionText;

        this.isAnswerRequired = false;
        this.dataPoint = null;
        this.order = Integer.MAX_VALUE;
    }



    /**
     * Creates a new instance of the {@link Question} class.
     * 
     * @param details
     *            the Question details.
     */
    protected Question(QuestionDetails details) {

        super(details);

        if (details.getQuestionText() != null) {

            this.questionText = details.getQuestionText();
        }
        else {

            this.questionText = "";
        }
        this.isAnswerRequired = details.isAnswerRequired();
        this.order = details.getOrder();

        DataPointDTO dataPoint = details.getDataPoint();
        if (dataPoint != null) {

            if (dataPoint instanceof SpreadsheetCellDTO) {

                this.dataPoint = SpreadsheetCell
                        .fromDTO((SpreadsheetCellDTO) dataPoint);
            }
        }
    }

    @Override
    public QuestionDetails toDTO() {

        QuestionDetails details = new QuestionDetails(super.toDTO());

        details.setAnswerRequired(this.isAnswerRequired());
        details.setQuestionText(this.getQuestionText());
        details.setOrder(this.getOrder());
        if (this.getDataPoint() != null) {

            details.setDataPoint(this.getDataPoint().toDTO());
        }
        return details;
    }

    @Override
    public String toString() {

        return super.toString() + ": " + this.getQuestionText();
    }
}
