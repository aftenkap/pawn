/**
 * 
 */
package raziel.pawn.core.domain.metadata;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.dto.metadata.ChoiceDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class Choice
        extends DomainObject {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link Choice} class.
     */
    public Choice() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Choice} class.
     * 
     * @param name
     *            the name of the choice.
     */
    public Choice(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link Choice} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public Choice(ChoiceDetails details) {

        super(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.IDomainObject#toDetails()
     */
    @Override
    public ChoiceDetails toDTO() {

        ChoiceDetails details = new ChoiceDetails(super.toDTO());

        return details;
    }

    /**
     * Creates a Choice from the provided details.
     * 
     * @param details
     *            the details.
     * @return the Choice.
     */
    public static Choice fromDetails(ChoiceDetails details) {

        return new Choice(details);
    }

    /**
     * Returns a List of {@link Choice Choices} corresponding to the provided
     * details.
     * 
     * @param details
     *            the {@link ChoiceDetails}.
     * @return a List of {@link Choice Choices} corresponding to the provided
     *         details.
     */
    public static List<Choice> fromDetails(Collection<ChoiceDetails> details) {

        List<Choice> objects = new ArrayList<>(details.size());

        for (ChoiceDetails detail : details) {

            objects.add(Choice.fromDetails(detail));
        }

        return objects;
    }
}
