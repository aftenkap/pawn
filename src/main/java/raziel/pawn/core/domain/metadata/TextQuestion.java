package raziel.pawn.core.domain.metadata;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.dto.metadata.TextQuestionDetails;


/**
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */

public class TextQuestion
        extends Question {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link TextQuestion} class.
     */
    protected TextQuestion() {

        this("");
    }

    /**
     * Creates a new instance of the {@link TextQuestion} class.
     * 
     * @param name
     *            the name of the question.
     */
    public TextQuestion(String name) {

        this(name, "");
    }

    /**
     * Creates a new instance of the {@link TextQuestion} class.
     * 
     * @param name
     *            the name of the question.
     * @param questionText
     *            the question text.
     */
    public TextQuestion(String name, String questionText) {

        super(name, questionText);
    }


    /**
     * Creates a new instance of the {@link TextQuestion} class from the
     * provided details.
     * 
     * @param details
     *            the question.
     */
    protected TextQuestion(TextQuestionDetails details) {

        super(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.metadata.IQuestion#toDetails()
     */
    @Override
    public TextQuestionDetails toDTO() {

        TextQuestionDetails details = new TextQuestionDetails(super.toDTO());

        return details;
    }

    /**
     * Returns a {@link TextQuestion} corresponding to the details provided.
     * 
     * @param details
     *            the details.
     * @return a {@link TextQuestion} corresponding to the details provided.
     */
    public static TextQuestion fromDetails(TextQuestionDetails details) {

        return new TextQuestion(details);
    }



    /**
     * Returns a List of {@link TextQuestion TextQuestions} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link TextQuestionDetails}.
     * @return a List of {@link TextQuestion TextQuestions} corresponding to the
     *         provided details.
     */
    public static List<TextQuestion> fromDetails(
            Collection<TextQuestionDetails> details) {

        List<TextQuestion> objects = new ArrayList<>(details.size());

        for (TextQuestionDetails detail : details) {

            objects.add(TextQuestion.fromDetails(detail));
        }

        return objects;
    }

}
