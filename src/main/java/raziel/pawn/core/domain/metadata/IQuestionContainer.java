/**
 * 
 */
package raziel.pawn.core.domain.metadata;


import java.util.Collection;
import java.util.List;


/**
 * @author Peter J. Radics
 *
 */
public interface IQuestionContainer {

    /**
     * Returns the {@link List} of {@link IQuestion Questions}.
     * 
     * @return the {@link List} of {@link IQuestion Questions}.
     */
    public abstract List<? extends IQuestion> getQuestions();

    /**
     * Adds the {@link IQuestion Question} to the {@link List}.
     * 
     * @param question
     *            the {@link IQuestion Question} to add.
     */
    public abstract  void addQuestion(final IQuestion question);

    /**
     * Adds the {@link IQuestion Questions} to the {@link List}.
     * 
     * @param questions
     *            the {@link IQuestion Questions} to add.
     */
    public abstract void addQuestions(
            final Collection<? extends IQuestion> questions);

    /**
     * Removes the {@link IQuestion Question} from the {@link List}.
     * 
     * @param question
     *            the {@link IQuestion Question} to remove.
     */
    public abstract void removeQuestion(final IQuestion question);

    /**
     * Removes the {@link IQuestion Questions} from the {@link List}.
     * 
     * @param questions
     *            the {@link IQuestion Question} to remove.
     */
    public abstract void removeQuestions(
            final Collection<? extends IQuestion> questions);


    /**
     * Removes all {@link IQuestion Questions} from the {@link List}.
     */
    public abstract void clearQuestions();

    /**
     * Replaces a {@link IQuestion Question} with another {@link IQuestion
     * Question}.
     * 
     * @param questionToReplace
     *            the {@link IQuestion Question} to replace.
     * @param replacement
     *            the replacement {@link IQuestion Question}.
     */
    public void replace(final IQuestion questionToReplace,
            final IQuestion replacement);

    /**
     * Swaps the two {@link IQuestion Questions}.
     * 
     * @param lhs
     *            the left-hand side {@link IQuestion Question}.
     * @param rhs
     *            the right-hand side {@link IQuestion Question}.
     */
    public void swap(final IQuestion lhs, final IQuestion rhs);


    /**
     * Re-numbers the {@link IQuestion Questions} in the {@link List} to span
     * from {@code 0} to {@link List#size()} while maintaining the order of
     * elements.
     */
    public void defragmentOrder();
}
