package raziel.pawn.core.domain.metadata.descriptors;


import raziel.pawn.core.domain.data.IDataSource;
import raziel.pawn.core.domain.metadata.IMetadata;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 * @param <S>
 *            the {@link IDataSource DataSource} type.
 * @param <M>
 *            the {@link IMetadata Description} type.
 */
public interface IDataPointDescriptor<S extends IDataSource, M extends IMetadata> {


    /**
     * Returns the {@link IMetadata Metadata}.
     * 
     * @return the {@link IMetadata Metadata}.
     */
    public abstract M getMetadata();

    /**
     * Sets the {@link IMetadata Metadata}.
     * 
     * @param metadata
     *            the {@link IMetadata Metadata}.
     */
    public abstract void setMetadata(final M metadata);

    /**
     * Extracts the {@link IMetadata Metadata} defined by this
     * {@link IDataPointDescriptor Descriptor} from the {@link IDataSource
     * DataSource}.
     * 
     * @param source
     *            the {@link IDataSource DataSource}.
     * @return the {@link IMetadata Metadata}.
     */
    public M extractMetadata(S source);


    /**
     * Returns the order.
     * 
     * @return the order.
     */
    public int getOrder();


    /**
     * Sets the order.
     * 
     * @param order
     *            the order.
     */
    public void setOrder(int order);
}
