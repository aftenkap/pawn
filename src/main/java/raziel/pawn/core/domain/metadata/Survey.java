package raziel.pawn.core.domain.metadata;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.dto.metadata.AnswerDetails;
import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;
import raziel.pawn.dto.metadata.SurveyDetails;
import raziel.pawn.dto.metadata.TextQuestionDetails;


/**
 * The {@link Survey} class models a survey.
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */

public class Survey
        extends MetadataSource {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;

    private List<IQuestion>   questions;
    private List<IAnswer>     answers;


    /**
     * Returns the questions.
     * 
     * @return the questions.
     */
    public List<IQuestion> getQuestions() {

        return Collections.unmodifiableList(this.questions);
    }

    /**
     * Sorts the questions according to their order.
     */
    public void sortQuestions() {

        Collections.sort(this.questions, new Comparator<IQuestion>() {

            @Override
            public int compare(IQuestion o1, IQuestion o2) {

                return Integer.compare(o1.getOrder(), o2.getOrder());
            }

        });
    }

    /**
     * Adds a question.
     * 
     * @param question
     *            the question to add.
     */
    public void addQuestion(final IQuestion question) {

        this.questions.add(question);
    }

    /**
     * Removes a question.
     * 
     * @param question
     *            the question to remove.
     */
    public void removeQuestion(final IQuestion question) {

        this.questions.remove(question);
    }



    /**
     * Clears the questions.
     */
    public void clearQuestions() {

        this.questions.clear();
    }

    /**
     * Returns the answers.
     * 
     * @return the answers.
     */
    public List<IAnswer> getAnswers() {

        return this.answers;
    }


    /**
     * Adds a answer.
     * 
     * @param answer
     *            the answer to add.
     */
    public void addAnswer(IAnswer answer) {

        this.answers.add(answer);
    }

    /**
     * Removes a answer.
     * 
     * @param answer
     *            the answer to remove.
     */
    public void removeAnswer(IAnswer answer) {

        this.answers.remove(answer);
    }



    /**
     * Clears the answers.
     */
    public void clearAnswers() {

        this.answers.clear();
    }

    /**
     * Creates a new instance of the {@link Survey} class.
     */
    protected Survey() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Survey} class.
     * 
     * @param name
     *            the name of the survey.
     */
    public Survey(String name) {

        super(name);

        this.questions = new LinkedList<>();
        this.answers = new LinkedList<>();
    }


    /**
     * Creates a new instance of the {@link Survey} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public Survey(SurveyDetails details) {

        super(details);
        this.questions = new LinkedList<>();
        this.answers = new LinkedList<>();

        if (details.getQuestions() != null) {

            for (QuestionDetails question : details.getQuestions()) {

                if (question instanceof TextQuestionDetails) {

                    this.questions.add(TextQuestion
                            .fromDetails((TextQuestionDetails) question));
                }
                else if (question instanceof ChoiceQuestionDetails) {

                    this.questions.add(ChoiceQuestion
                            .fromDetails((ChoiceQuestionDetails) question));
                }
                else if (question instanceof RankingQuestionDetails) {

                    this.questions.add(RankingQuestion
                            .fromDetails((RankingQuestionDetails) question));
                }
            }
        }

        if (details.getAnswers() != null) {

            for (AnswerDetails answer : details.getAnswers()) {

                this.answers.add(Answer.fromDetails(answer));
            }
        }
    }



    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public SurveyDetails toDTO() {

        SurveyDetails details = new SurveyDetails(super.toDTO());



        List<QuestionDetails> questionDetails = new ArrayList<>(
                this.questions.size());
        for (IQuestion question : this.questions) {

            questionDetails.add(question.toDTO());
        }
        details.setQuestions(questionDetails);


        List<AnswerDetails> answerDetails = new ArrayList<>(this.answers.size());
        for (IAnswer answer : this.answers) {

            answerDetails.add(answer.toDTO());
        }
        details.setAnswers(answerDetails);

        return details;
    }



    /**
     * Returns a {@link Survey} corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a {@link Survey} corresponding to the provided details.
     */
    public static Survey fromDetails(SurveyDetails details) {

        return new Survey(details);
    }


    /**
     * Returns a List of {@link Survey Surveys} corresponding to the provided
     * details.
     * 
     * @param details
     *            the {@link SurveyDetails}.
     * @return a List of {@link Survey Surveys} corresponding to the provided
     *         details.
     */
    public static List<Survey> fromDetails(Collection<SurveyDetails> details) {

        List<Survey> objects = new ArrayList<>(details.size());

        for (SurveyDetails detail : details) {

            objects.add(Survey.fromDetails(detail));
        }

        return objects;
    }
}
