/**
 * 
 */
package raziel.pawn.core.domain.metadata.descriptors;


import org.jutility.common.datatype.table.CellRange;

import raziel.pawn.core.domain.metadata.IParticipant;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 * 
 * 
 * @param <S>
 *            the concrete type.
 */
public abstract class TableParticipantDescriptorBase<S extends IParticipant>
        extends TableMetadataDescriptor<S> {



    /**
     * Creates a new instance of the {@link TableParticipantDescriptorBase}
     * class.
     * 
     * @param cellRange
     *            the cell range.
     */
    public TableParticipantDescriptorBase(final CellRange cellRange) {

        super(cellRange, "Participant", null);
    }

    @Override
    public String toString() {

        return "Participants in " + super.toString();
    }

}
