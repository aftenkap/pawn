package raziel.pawn.core.domain.data;


import java.net.URI;
import java.util.List;

import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.dto.data.DataSourceDetails;


/**
 *
 *
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */
public interface IDataSource
        extends IDomainObject {

    /**
     * Returns the location of the data source.
     *
     * @return the location of the data source.
     */
    public abstract URI getUri();

    /**
     * Sets the URI.
     *
     * @param uri
     *            the URI.
     */
    public abstract void setURI(final URI uri);


    /**
     * Returns the {@link IDataPoint DataPoints} of this {@link IDataSource}.
     *
     * @return the {@link IDataPoint DataPoints}.
     */
    public abstract List<? extends IDataPoint<?>> getDataPoints();

    @Override
    public abstract DataSourceDetails toDTO();
}
