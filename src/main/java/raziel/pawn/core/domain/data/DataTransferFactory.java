package raziel.pawn.core.domain.data;


import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.data.SpreadsheetDTO;


/**
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.3
 */
public abstract class DataTransferFactory {


    public static final IDataPoint<?> fromDTO(DataPointDTO dto) {

        return null;
    }


    /**
     * Returns a {@link SpreadsheetCell} corresponding to the provided DTO.
     * 
     * @param dto
     *            the dto.
     * @return a {@link SpreadsheetCell} corresponding to the provided DTO.
     */
    public static final SpreadsheetCell fromDTO(SpreadsheetCellDTO dto) {

        if (dto == null) {

            return null;
        }

        return new SpreadsheetCell(dto);
    }



    /**
     * Returns a List of {@link SpreadsheetCell SpreadsheetCells} corresponding
     * to the provided DTOs.
     * 
     * @param dtos
     *            the {@link SpreadsheetCellDTO}.
     * @return a List of {@link SpreadsheetCell SpreadsheetCells} corresponding
     *         to the provided DTOs.
     */
    public static final List<SpreadsheetCell> fromDTO(
            Collection<SpreadsheetCellDTO> dtos) {

        List<SpreadsheetCell> spreadsheetCells = new ArrayList<>();

        if (dtos != null) {

            for (SpreadsheetCellDTO dto : dtos) {

                spreadsheetCells.add(SpreadsheetCell.fromDTO(dto));
            }
        }

        return spreadsheetCells;
    }

    public static final Spreadsheet fromDTO(final SpreadsheetDTO dto) {

        if (dto == null) {

            return null;
        }
        Spreadsheet spreadsheet = new Spreadsheet(dto.getName());

        spreadsheet.setId(dto.getId());
        spreadsheet.setVersion(dto.getVersion());

        if (dto.getUri() != null) {

            spreadsheet.setURI(URI.create(dto.getUri()));
        }

        spreadsheet.addAll(SpreadsheetCell.fromDTO(dto.getCells()));

        return spreadsheet;
    }
}
