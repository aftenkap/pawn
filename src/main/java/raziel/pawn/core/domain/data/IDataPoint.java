package raziel.pawn.core.domain.data;


import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.dto.data.DataPointDTO;


/**
 *
 *
 * @author Peter J. Radics
 * @version 0.1
 * @param <T>
 *            the type of the data point.
 */

public interface IDataPoint<T>
        extends IDomainObject {

    /**
     * Returns the value of the data point.
     *
     * @return the value.
     */
    public T getValue();

    /**
     * Sets the value of the data point.
     *
     * @param value
     *            the value.
     */
    public abstract void setValue(final T value);

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.IDomainObject#toDetails()
     */
    @Override
    public DataPointDTO toDTO();
}
