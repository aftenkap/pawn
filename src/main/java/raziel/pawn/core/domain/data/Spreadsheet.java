package raziel.pawn.core.domain.data;


import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.jutility.common.datatype.table.AbstractTable;
import org.jutility.common.datatype.table.ITable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.data.SpreadsheetDTO;



/**
 * The generic {@link Spreadsheet} class models a two-dimensional table of
 * {@link String} data stored in {@link Spreadsheet Cells}.
 * <p/>
 * This class serves as a database wrapper of the {@link Spreadsheet} class and
 * provides integration of tables as {@link IDataSource DataSources}.
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.2
 */
public class Spreadsheet
        extends AbstractTable<SpreadsheetCell, String>
        implements IDataSource {


    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unused")
    private static Logger     LOG              = LoggerFactory
                                                       .getLogger(Spreadsheet.class);

    private Integer           id;
    private long              version;
    private String            name;

    private URI               uri;


    @Override
    public Integer getId() {

        return this.id;
    }

    @Override
    public void setId(int id) {

        this.id = id;

    }

    @Override
    public long getVersion() {

        return this.version;
    }

    @Override
    public void setVersion(long version) {

        this.version = version;

    }

    @Override
    public String getName() {

        return this.name;
    }

    @Override
    public void setName(String name) {

        this.name = name;
    }


    @Override
    public URI getUri() {

        return this.uri;
    }

    @Override
    public void setURI(URI uri) {

        this.uri = uri;
    }


    @Override
    public boolean add(int row, int column, String value) {

        return this.add(new SpreadsheetCell(row, column, value));
    }

    /**
     * Adds all the {@link SpreadsheetCell Cells} in the collection to the cells
     * of this {@code Spreadsheet}.
     * 
     * @param cells
     *            the {@link SpreadsheetCell Cells} to add.
     */
    public void addAll(Collection<SpreadsheetCell> cells) {

        for (SpreadsheetCell cell : cells) {

            this.add(cell);
        }
    }


    @Override
    public List<SpreadsheetCell> getDataPoints() {

        return new LinkedList<>(this.getCells());
    }

    /**
     * Creates a new instance of the {@link Spreadsheet} class.
     * 
     * @param name
     *            the name of the data table.
     */
    public Spreadsheet(String name) {

        super();
        this.id = null;
        this.version = 0;
        this.name = name;
        this.uri = null;
    }

    /**
     * Copy constructor. Creates a new instance of the {@link Spreadsheet} class
     * from the provided table.
     * 
     * @param name
     *            the name of the copy.
     * @param table
     *            the table to copy.
     */
    public Spreadsheet(String name, ITable<? extends String> table) {

        super(table);
        this.id = null;
        this.version = 0;
        this.name = name;
        this.uri = null;
    }

    /**
     * Creates a new instance of the {@link Spreadsheet} class from the provided
     * DTO.
     * 
     * @param dto
     *            the DTO.
     */
    protected Spreadsheet(final SpreadsheetDTO dto) {

        super();

        this.id = dto.getId();
        this.version = dto.getVersion();
        this.name = dto.getName();

        if (dto.getUri() != null) {

            this.uri = URI.create(dto.getUri());
        }
        else {

            this.uri = null;
        }

        this.addAll(SpreadsheetCell.fromDTO(dto.getCells()));
    }



    @Override
    public SpreadsheetDTO toDTO() {

        SpreadsheetDTO dto = new SpreadsheetDTO(this.id);

        dto.setVersion(this.version);
        dto.setName(this.name);

        if (this.uri != null) {

            dto.setUri(this.uri.toASCIIString());
        }

        List<SpreadsheetCellDTO> cellDTOs = new ArrayList<>(this.getCells()
                .size());

        for (SpreadsheetCell cell : this.getCells()) {

            cellDTOs.add(cell.toDTO());
        }
        dto.setCells(cellDTOs);

        return dto;
    }

    /**
     * Returns a {@link Spreadsheet} corresponding to the provided DTO.
     * 
     * @param dto
     *            the DTO.
     * @return a {@link Spreadsheet} corresponding to the provided DTO.
     */
    public static Spreadsheet fromDTO(final SpreadsheetDTO dto) {

        if (dto == null) {

            return null;
        }

        return new Spreadsheet(dto);
    }


    /**
     * Returns a {@link Spreadsheet} corresponding to the provided DTOs.
     * 
     * @param dtos
     *            the DTOs.
     * @return a {@link Spreadsheet} corresponding to the provided DTOs.
     */
    public static List<Spreadsheet> fromDTO(List<SpreadsheetDTO> dtos) {

        List<Spreadsheet> spreadsheets = new ArrayList<>();

        if (dtos != null) {

            for (SpreadsheetDTO dto : dtos) {

                spreadsheets.add(Spreadsheet.fromDTO(dto));
            }
        }

        return spreadsheets;
    }

    /**
     * Converts the provided generic {@link ITable} into a {@link Spreadsheet}.
     * 
     * @param name
     *            the name of the {@link Spreadsheet}.
     * @param table
     *            the generic {@link ITable} to convert.
     * @return the new {@link Spreadsheet}.
     */
    public static Spreadsheet convert(String name, ITable<String> table) {

        return new Spreadsheet(name, table);
    }



    /**
     * Returns a List of {@link Spreadsheet Tables} corresponding to the
     * provided DTOs.
     * 
     * @param dtos
     *            the {@link SpreadsheetDTO}.
     * @return a List of {@link Spreadsheet Tables} corresponding to the
     *         provided DTOs.
     */
    public static List<Spreadsheet> fromDTO(Collection<SpreadsheetDTO> dtos) {

        List<Spreadsheet> spreadsheets = new ArrayList<>();

        if (dtos != null) {

            for (SpreadsheetDTO dto : dtos) {

                spreadsheets.add(Spreadsheet.fromDTO(dto));
            }
        }

        return spreadsheets;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj != null && obj.getClass() == this.getClass()) {

            Spreadsheet otherTable = (Spreadsheet) obj;

            boolean bothNull = this.id == null && otherTable.getId() == null;

            if (!bothNull) {

                if (this.id != null) {

                    return this.id.equals(otherTable.getId());
                }
            }
        }

        return false;
    }

    @Override
    public int hashCode() {

        int hashcode = 7 * (this.id == null ? 0 : this.id);

        return hashcode;
    }
}
