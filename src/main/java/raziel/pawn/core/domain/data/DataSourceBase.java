package raziel.pawn.core.domain.data;


import java.net.URI;
import java.net.URISyntaxException;

import raziel.pawn.dto.data.DataSourceDetails;
import raziel.pawn.core.domain.DomainObject;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public abstract class DataSourceBase
        extends DomainObject
        implements IDataSource {

    /**
     * The serial version uid.
     */
    private static final long serialVersionUID = 1L;

    private URI               uri;


    @Override
    public URI getUri() {

        return this.uri;
    }

    @Override
    public void setURI(URI uri) {

        this.uri = uri;
    }


    /**
     * Creates a new instance of the {@link DataSourceBase} class.
     */
    public DataSourceBase() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DataSourceBase} class.
     * 
     * @param name
     *            the name of the data source.
     */
    public DataSourceBase(final String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link DataSourceBase} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     * @throws URISyntaxException
     *             if the URI provided in the details is invalid.
     */
    protected DataSourceBase(DataSourceDetails details)
            throws URISyntaxException {

        super(details);

        this.uri = new URI(details.getUri());
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.DomainObject#toDetails(boolean)
     */
    @Override
    public DataSourceDetails toDTO() {

        DataSourceDetails details = new DataSourceDetails(
                super.toDTO());

        details.setUri(this.uri.toASCIIString());

        return details;
    }
}
