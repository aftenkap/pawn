package raziel.pawn.core.domain.data;


import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.core.domain.DomainObject;


/**
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 * @param <T>
 *            the content type of the data point.
 */
public abstract class DataPointBase<T>
        extends DomainObject
        implements IDataPoint<T> {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link DataPointBase} class.
     */
    protected DataPointBase() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DataPointBase} class with the
     * provided name.
     * 
     * @param name
     *            the name.
     */
    public DataPointBase(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link DataPointBase} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public DataPointBase(DataPointDTO details) {

        super(details);
    }

    @Override
    public DataPointDTO toDTO() {

        return new DataPointDTO(super.toDTO());
    }
}
