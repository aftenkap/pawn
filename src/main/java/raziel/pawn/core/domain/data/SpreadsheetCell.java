package raziel.pawn.core.domain.data;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jutility.common.datatype.table.Cell;
import org.jutility.common.datatype.table.ICell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import raziel.pawn.dto.data.SpreadsheetCellDTO;



/**
 * The generic {@link SpreadsheetCell} class is a container class for
 * {@link String Strings} that are referenced by two indices.
 * 
 * 
 * @author Peter J. Radics
 * @version 0.1.0
 * @since 0.1.0
 */

public class SpreadsheetCell
        extends Cell<String>
        implements IDataPoint<String> {


    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unused")
    private static Logger     LOG              = LoggerFactory
                                                       .getLogger(SpreadsheetCell.class);

    private Integer           id;
    private long              version;
    private String            name;

    @Override
    public Integer getId() {

        return this.id;
    }

    @Override
    public void setId(int id) {

        this.id = id;

    }

    @Override
    public long getVersion() {

        return this.version;
    }

    @Override
    public void setVersion(long version) {

        this.version = version;

    }

    @Override
    public String getName() {

        return this.name;
    }

    @Override
    public void setName(String name) {

        this.name = name;
    }


    /**
     * Creates a new instance of the {@link SpreadsheetCell} class with the
     * provided values.
     * 
     * @param row
     *            the row.
     * @param column
     *            the column.
     * @param value
     *            the value.
     */
    public SpreadsheetCell(int row, int column, String value) {

        super(row, column, value);

        this.id = null;
        this.name = "Cell (" + row + ", " + column + ")";
        this.version = 0;
    }

    /**
     * Creates a new instance of the {@link SpreadsheetCell} class as a copy of
     * the provided {@link ICell Cell}. (Copy Constructor)
     * 
     * @param other
     *            the {@link ICell Cell} to copy.
     */
    public SpreadsheetCell(ICell<String> other) {

        this(other.getRow(), other.getColumn(), other.getValue());
    }

    /**
     * Creates a new instance of the {@link SpreadsheetCell} class from the
     * provided DTO.
     * 
     * @param dto
     *            the DTO.
     */
    protected SpreadsheetCell(SpreadsheetCellDTO dto) {

        super(dto.getRow(), dto.getColumn(), dto.getValue());

        this.id = dto.getId();
        this.version = dto.getVersion();
        this.name = dto.getName();
    }


    @Override
    public SpreadsheetCellDTO toDTO() {

        SpreadsheetCellDTO dto = new SpreadsheetCellDTO(this.getId());

        dto.setVersion(this.getVersion());
        dto.setName(this.getName());

        dto.setColumn(this.getColumn());
        dto.setRow(this.getRow());
        dto.setValue(this.getValue());

        return dto;
    }

    /**
     * Returns a SpreadsheetCell corresponding to the provided DTO.
     * 
     * @param dto
     *            the dto.
     * @return a SpreadsheetCell corresponding to the provided DTO.
     */
    public static SpreadsheetCell fromDTO(SpreadsheetCellDTO dto) {

        if (dto == null) {

            return null;
        }

        return new SpreadsheetCell(dto);
    }



    /**
     * Returns a List of {@link SpreadsheetCell TableCells} corresponding to the
     * provided DTOs.
     * 
     * @param dtos
     *            the {@link SpreadsheetCellDTO}.
     * @return a List of {@link SpreadsheetCell TableCells} corresponding to the
     *         provided DTOs.
     */
    public static List<SpreadsheetCell> fromDTO(
            Collection<SpreadsheetCellDTO> dtos) {

        List<SpreadsheetCell> spreadsheetCells = new ArrayList<>();

        if (dtos != null) {

            for (SpreadsheetCellDTO dto : dtos) {

                spreadsheetCells.add(SpreadsheetCell.fromDTO(dto));
            }
        }

        return spreadsheetCells;
    }
}
