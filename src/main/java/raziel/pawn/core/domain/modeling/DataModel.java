package raziel.pawn.core.domain.modeling;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.core.domain.modeling.assertions.Association;
import raziel.pawn.core.domain.modeling.assertions.Attribution;
import raziel.pawn.core.domain.modeling.assertions.Constant;
import raziel.pawn.core.domain.modeling.assertions.Entity;
import raziel.pawn.core.domain.modeling.assertions.Interaction;
import raziel.pawn.core.domain.modeling.terminology.AssociativeRole;
import raziel.pawn.core.domain.modeling.terminology.AttributiveRole;
import raziel.pawn.core.domain.modeling.terminology.Endurant;
import raziel.pawn.core.domain.modeling.terminology.InteractiveRole;
import raziel.pawn.core.domain.modeling.terminology.Perdurant;
import raziel.pawn.core.domain.modeling.time.DurationOrder;
import raziel.pawn.core.domain.modeling.time.SemiIntervalOrder;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AssociationDetails;
import raziel.pawn.dto.modeling.AssociativeRoleDetails;
import raziel.pawn.dto.modeling.AttributionDetails;
import raziel.pawn.dto.modeling.AttributiveRoleDetails;
import raziel.pawn.dto.modeling.ConstantDetails;
import raziel.pawn.dto.modeling.DataModelDetails;
import raziel.pawn.dto.modeling.DurationOrderDetails;
import raziel.pawn.dto.modeling.EndurantDetails;
import raziel.pawn.dto.modeling.EntityDetails;
import raziel.pawn.dto.modeling.InteractionDetails;
import raziel.pawn.dto.modeling.InteractiveRoleDetails;
import raziel.pawn.dto.modeling.PerdurantDetails;
import raziel.pawn.dto.modeling.SemiIntervalOrderDetails;
import raziel.pawn.dto.modeling.TermDTO;


/**
 * The {@link DataModel} class provides a container for elements of
 * {@link raziel.pawn.core.domain.modeling}.
 * 
 * @author Peter J. Radics
 * @version 0.1
 */
public class DataModel
        extends DomainObject {

    /**
     * Serial version uid.
     */
    private static final long       serialVersionUID = 1L;


    private List<Term<?>>           terms;

    private List<Assertion>         assertions;

    private List<DurationOrder>     durationOrders;

    private List<SemiIntervalOrder> semiIntervalOrders;


    /**
     * Returns the {@link Term Terms} of this {@link DataModel}.
     * 
     * @return the the {@link Term Terms} of this {@link DataModel}.
     */
    public List<Term<?>> getTerms() {

        return this.terms;
    }

    /**
     * Adds a {@link Term} to the terms.
     * 
     * @param term
     *            the {@link Term} to add.
     * @return {@code true}, if the operation changed the terms; {@code false}
     *         otherwise.
     */
    public boolean addTerm(Term<?> term) {

        return this.terms.add(term);
    }

    /**
     * Removes a {@link Term} from the terms.
     * 
     * @param term
     *            the {@link Term} to remove.
     * @return {@code true}, if the operation changed the terms; {@code false}
     *         otherwise.
     */
    public boolean removeConcept(Term<?> term) {

        return this.terms.remove(term);
    }

    /**
     * Clears the {@link Term Terms} of this {@link DataModel}.
     */
    public void clearTerms() {

        this.terms.clear();
    }

    /**
     * Returns the {@link Assertion Assertions} of this {@link DataModel}.
     * 
     * @return the the {@link Assertion Assertions} of this {@link DataModel}.
     */
    public List<Assertion> getAssertions() {

        return this.assertions;
    }

    /**
     * Adds an {@link Assertion} to the assertions.
     * 
     * @param assertion
     *            the {@link Assertion} to add.
     * @return {@code true}, if the operation changed the assertions;
     *         {@code false} otherwise.
     */
    public boolean addAssertion(Assertion assertion) {

        return this.assertions.add(assertion);
    }

    /**
     * Removes an {@link Assertion} from the assertions.
     * 
     * @param assertion
     *            the {@link Assertion} to remove.
     * @return {@code true}, if the operation changed the assertions;
     *         {@code false} otherwise.
     */
    public boolean removeAssertion(Assertion assertion) {

        return this.assertions.remove(assertion);
    }

    /**
     * Clears the {@link Assertion Assertions} of this {@link DataModel}.
     */
    public void clearAssertions() {

        this.assertions.clear();
    }

    /**
     * Returns the duration orders.
     * 
     * @return the duration orders.
     */
    public List<DurationOrder> getDurationOrders() {

        return this.durationOrders;
    }

    /**
     * Adds a duration order to the assertions.
     * 
     * @param durationOrder
     *            the duration order to add.
     * @return {@code true}, if the operation changed the assertions;
     *         {@code false} otherwise.
     */
    public boolean addDurationOrder(DurationOrder durationOrder) {

        return this.durationOrders.add(durationOrder);
    }

    /**
     * Removes a duration order from the assertions.
     * 
     * @param durationOrder
     *            the duration order to remove.
     * @return {@code true}, if the operation changed the assertions;
     *         {@code false} otherwise.
     */
    public boolean removeDurationOrder(DurationOrder durationOrder) {

        return this.durationOrders.remove(durationOrder);
    }

    /**
     * Clears the instances of this {@link DataModel}.
     */
    public void clearDurationOrders() {

        this.durationOrders.clear();
    }

    /**
     * Returns the semi-interval orders.
     * 
     * @return the semi-interval orders.
     */
    public List<SemiIntervalOrder> getSemiIntervalOrders() {

        return this.semiIntervalOrders;
    }


    /**
     * Adds a semi-interval order to the assertions.
     * 
     * @param semiIntervalOrder
     *            the semi-interval order to remove.
     * @return {@code true}, if the operation changed the assertions;
     *         {@code false} otherwise.
     */
    public boolean addSemiIntervalOrder(SemiIntervalOrder semiIntervalOrder) {

        return this.semiIntervalOrders.add(semiIntervalOrder);
    }

    /**
     * Removes a semi-interval order from the list.
     * 
     * @param semiIntervalOrder
     *            the semi-interval order to remove.
     * @return {@code true}, if the operation changed the list; {@code false}
     *         otherwise.
     */
    public boolean removeSemiIntervalOrder(SemiIntervalOrder semiIntervalOrder) {

        return this.semiIntervalOrders.remove(semiIntervalOrder);
    }

    /**
     * Clears the semi-interval orders of this {@link DataModel}.
     */
    public void clearSemiIntervalOrders() {

        this.semiIntervalOrders.clear();
    }

    /**
     * Creates a new instance of the {@link DataModel} class.
     */
    protected DataModel() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DataModel} class with the provided
     * name.
     * 
     * @param name
     *            the name.
     */
    public DataModel(String name) {

        super(name);

        this.terms = new LinkedList<>();
        this.assertions = new LinkedList<>();
        this.durationOrders = new LinkedList<>();
        this.semiIntervalOrders = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link DataModel} class. (Copy Constructor)
     * 
     * @param modelToCopy
     *            the model to copy.
     */
    public DataModel(final DataModel modelToCopy) {

        super(modelToCopy);

        this.terms = new LinkedList<>(modelToCopy.getTerms());
        this.assertions = new LinkedList<>(modelToCopy.getAssertions());
        this.durationOrders = new LinkedList<>(modelToCopy.getDurationOrders());
        this.semiIntervalOrders = new LinkedList<>(
                modelToCopy.getSemiIntervalOrders());

    }

    /**
     * Creates a new instance of the {@link DataModel} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    public DataModel(final DataModelDetails details) {

        super(details);

        this.terms = new LinkedList<>();
        this.assertions = new LinkedList<>();
        this.durationOrders = new LinkedList<>();
        this.semiIntervalOrders = new LinkedList<>();

        if (details.getTerms() != null) {

            for (TermDTO term : details.getTerms()) {

                if (term instanceof PerdurantDetails) {

                    this.terms.add(Perdurant
                            .fromDetails((PerdurantDetails) term));
                }
                else if (term instanceof EndurantDetails) {

                    this.terms
                            .add(Endurant.fromDetails((EndurantDetails) term));
                }
                else if (term instanceof AssociativeRoleDetails) {

                    this.terms.add(AssociativeRole
                            .fromDetails((AssociativeRoleDetails) term));
                }
                else if (term instanceof AttributiveRoleDetails) {

                    this.terms.add(AttributiveRole
                            .fromDetails((AttributiveRoleDetails) term));
                }
                else if (term instanceof InteractiveRoleDetails) {

                    this.terms.add(InteractiveRole
                            .fromDetails((InteractiveRoleDetails) term));
                }
            }
        }
        if (details.getAssertions() != null) {

            for (AssertionDetails assertion : details.getAssertions()) {

                if (assertion instanceof EntityDetails) {

                    this.assertions.add(Entity
                            .fromDetails((EntityDetails) assertion));
                }
                else if (assertion instanceof ConstantDetails) {

                    this.assertions.add(Constant
                            .fromDetails((ConstantDetails) assertion));
                }
                else if (assertion instanceof AssociationDetails) {

                    this.assertions.add(Association
                            .fromDetails((AssociationDetails) assertion));
                }
                else if (assertion instanceof AttributionDetails) {

                    this.assertions.add(Attribution
                            .fromDetails((AttributionDetails) assertion));
                }
                else if (assertion instanceof InteractionDetails) {

                    this.assertions.add(Interaction
                            .fromDetails((InteractionDetails) assertion));
                }
            }
        }
        if (details.getDurationOrders() != null) {

            for (DurationOrderDetails durationOrder : details
                    .getDurationOrders()) {

                this.durationOrders.add(DurationOrder
                        .fromDetails(durationOrder));
            }
        }
        if (details.getSemiIntervalOrders() != null) {

            for (SemiIntervalOrderDetails semiIntervalOrder : details
                    .getSemiIntervalOrders()) {

                this.semiIntervalOrders.add(SemiIntervalOrder
                        .fromDetails(semiIntervalOrder));
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails(boolean)
     */
    @Override
    public DataModelDetails toDTO() {

        DataModelDetails details = new DataModelDetails(
                super.toDTO());


            List<TermDTO> terms = new ArrayList<>(this.terms.size());
            for (Term<?> term : this.terms) {

                terms.add(term.toDTO());
            }
            details.setTerms(terms);

            List<AssertionDetails> assertions = new ArrayList<>(
                    this.assertions.size());
            for (Assertion assertion : this.assertions) {

                assertions.add(assertion.toDTO());
            }
            details.setAssertions(assertions);

            List<DurationOrderDetails> durationOrders = new ArrayList<>(
                    this.durationOrders.size());
            for (DurationOrder durationOrder : this.durationOrders) {

                durationOrders.add(durationOrder.toDTO());
            }
            details.setDurationOrders(durationOrders);

            List<SemiIntervalOrderDetails> semiIntervalOrders = new ArrayList<>(
                    this.semiIntervalOrders.size());
            for (SemiIntervalOrder semiIntervalOrder : this.semiIntervalOrders) {

                semiIntervalOrders
                        .add(semiIntervalOrder.toDTO());
            }
            details.setSemiIntervalOrders(semiIntervalOrders);

        return details;
    }

    /**
     * Returns a DataModel corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a DataModel corresponding to the provided details.
     */
    public static DataModel fromDetails(final DataModelDetails details) {

        return new DataModel(details);
    }
}
