/**
 * 
 */
package raziel.pawn.core.domain.modeling.assertions;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AssociationDetails;
import raziel.pawn.dto.modeling.ConstantDetails;



/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class Association
        extends PermanentRelationship<Constant, Constant> {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;



    /**
     * Creates a new instance of the {@link Association} class (Serialization
     * Constructor).
     */
    protected Association() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Association} class with the provided
     * name.
     * 
     * @param name
     *            the name.
     */
    public Association(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link PermanentRelationship} class. (Copy
     * Constructor)
     * 
     * @param associationToCopy
     *            the association to copy.
     */
    public Association(final Association associationToCopy) {

        super(associationToCopy);
    }

    /**
     * Creates a new instance of the {@link Association} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Association(final AssociationDetails details) {

        super(details);

        if (details.getDomainAssertions() != null) {

            for (AssertionDetails domain : details.getDomainAssertions()) {

                if (domain instanceof ConstantDetails) {

                    this.addDomainAssertion(Constant
                            .fromDetails((ConstantDetails) domain));
                }
            }
        }
        if (details.getRangeAssertions() != null) {

            for (AssertionDetails range : details.getRangeAssertions()) {

                if (range instanceof ConstantDetails) {

                    this.addRangeAssertion(Constant
                            .fromDetails((ConstantDetails) range));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.assertions.PermanentRelationship
     * #toDetails(boolean)
     */
    @Override
    public AssociationDetails toDTO() {

        AssociationDetails details = new AssociationDetails(super.toDTO());

        return details;
    }

    /**
     * Returns an Association corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an Association corresponding to the provided details.
     */
    public static Association fromDetails(final AssociationDetails details) {

        return new Association(details);
    }

    /**
     * Returns a List of {@link Association Associations} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link AssociationDetails}.
     * @return a List of {@link Association Associations} corresponding to the
     *         provided details.
     */
    public static List<Association> fromDetails(
            Collection<AssociationDetails> details) {

        List<Association> objects = new ArrayList<>(details.size());

        for (AssociationDetails detail : details) {

            objects.add(Association.fromDetails(detail));
        }

        return objects;
    }
}
