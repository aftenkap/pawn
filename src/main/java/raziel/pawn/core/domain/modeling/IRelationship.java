/**
 * 
 */
package raziel.pawn.core.domain.modeling;


import java.util.List;

import raziel.pawn.dto.modeling.RelationshipDetails;



/**
 * @author Peter J. Radics
 * @param <DOMAIN>
 *            the domain instance type.
 * @param <RANGE>
 *            the range instance type.
 * 
 */
public interface IRelationship<DOMAIN extends IAssertion, RANGE extends IAssertion>
        extends IAssertion {


    /**
     * Returns the {@link IAssertion Domain Assertions} of this
     * {@link IRelationship}.
     * 
     * @return the {@link IAssertion Domain Assertions} of this
     *         {@link IRelationship}.
     */
    public abstract List<DOMAIN> getDomainAssertions();

    /**
     * Adds an {@link IAssertion Assertion} to the domain instances of this
     * {@link IRelationship}.
     * 
     * @param domainAssertion
     *            the {@link IAssertion Assertion} to add.
     * @return {@code true}, if the domain instances changed as a result of the
     *         call.
     */
    public abstract boolean addDomainAssertion(DOMAIN domainAssertion);

    /**
     * Removes an {@link IAssertion Assertion} from the domain instances of this
     * {@link IRelationship}.
     * 
     * @param domainAssertion
     *            the {@link IAssertion Assertion} to remove.
     * @return {@code true}, if a domain instance was removed as a result of
     *         this call.
     */
    public abstract boolean removeDomainAssertion(DOMAIN domainAssertion);

    /**
     * Removes all {@link IAssertion Domain Assertions}.
     */
    public abstract void clearDomainAssertions();



    /**
     * Returns the {@link IAssertion Range Assertions} of this
     * {@link IRelationship}.
     * 
     * @return the {@link IAssertion Range Assertions} of this
     *         {@link IRelationship}.
     */
    public abstract List<RANGE> getRangeAssertions();

    /**
     * Adds an {@link IAssertion Assertion} to the range instances of this
     * {@link IRelationship}.
     * 
     * @param rangeAssertion
     *            the {@link IAssertion Assertion} to add.
     * @return {@code true}, if the range instances changed as a result of the
     *         call.
     */
    public abstract boolean addRangeAssertion(RANGE rangeAssertion);

    /**
     * Removes an {@link IAssertion Assertion} from the range instances of this
     * {@link IRelationship}.
     * 
     * @param rangeAssertion
     *            the {@link IAssertion Assertion} to remove.
     * @return {@code true}, if a range instance was removed as a result of this
     *         call.
     */
    public abstract boolean removeRangeAssertion(RANGE rangeAssertion);

    /**
     * Removes all {@link IAssertion Range Assertions}.
     */
    public abstract void clearRangeAssertions();

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.modeling.IAssertion#toDetails()
     */
    @Override
    public abstract RelationshipDetails toDTO();
}
