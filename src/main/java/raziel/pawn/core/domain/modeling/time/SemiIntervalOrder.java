package raziel.pawn.core.domain.modeling.time;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.dto.modeling.SemiIntervalOrderDetails;


/**
 * The {@link SemiIntervalOrder} class provides the capability of specifying an
 * order on {@link SemiInterval SemiIntervals}.
 *
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class SemiIntervalOrder
        extends DomainObject {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    private SemiInterval      firstOperand;


    private SemiInterval      secondOperand;


    private Operator          operator;


    /**
     * Returns the {@link SemiInterval first operand}.
     *
     * @return the first operand.
     */
    public SemiInterval getFirstOperand() {

        return firstOperand;
    }


    /**
     * Returns the {@link SemiInterval second operand}.
     *
     * @return the second operand.
     */
    public SemiInterval getSecondOperand() {

        return secondOperand;
    }

    /**
     * Returns the {@link Operator operator}.
     *
     * @return the operator.
     */
    public Operator getOperator() {

        return operator;
    }

    @SuppressWarnings("unused")
    private void setOperator(String operator) {

        this.operator = Operator.fromString(operator);
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class.
     */
    protected SemiIntervalOrder() {

        this("");
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class with the
     * provided name (Serialization Constructor).
     *
     * @param name
     *            the name.
     */
    private SemiIntervalOrder(String name) {

        this(name, null, null, null);
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class with the
     * provided name, operands, and operator.
     *
     * @param name
     *            the name.
     * @param firstOperand
     *            the first operand.
     * @param secondOperand
     *            the second operand.
     * @param operator
     *            the operator.
     */
    public SemiIntervalOrder(String name, SemiInterval firstOperand,
            SemiInterval secondOperand, Operator operator) {

        super(name);
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
        this.operator = operator;
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class. (Copy
     * Constructor)
     * 
     * @param orderToCopy
     *            the order to copy.
     */
    public SemiIntervalOrder(final SemiIntervalOrder orderToCopy) {

        super(orderToCopy);

        if (orderToCopy.getFirstOperand() != null) {

            this.firstOperand = new SemiInterval(orderToCopy.getFirstOperand());
        }

        if (orderToCopy.getSecondOperand() != null) {

            this.secondOperand = new SemiInterval(
                    orderToCopy.getSecondOperand());
        }

        this.operator = orderToCopy.getOperator();
    }

    /**
     * Creates a new instance of the {@link SemiIntervalOrder} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected SemiIntervalOrder(final SemiIntervalOrderDetails details) {

        super(details);

        if (details.getFirstOperand() != null) {

            this.firstOperand = SemiInterval.fromDetails(details
                    .getFirstOperand());
        }

        if (details.getSecondOperand() != null) {

            this.secondOperand = SemiInterval.fromDetails(details
                    .getSecondOperand());
        }

        if (details.getOperator() != null) {

            this.operator = Operator.fromString(details.getOperator());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public SemiIntervalOrderDetails toDTO() {

        SemiIntervalOrderDetails details = new SemiIntervalOrderDetails(
                super.toDTO());

        if (this.firstOperand != null) {

            details.setFirstOperand(this.firstOperand.toDTO());
        }
        if (this.secondOperand != null) {

            details.setSecondOperand(this.secondOperand.toDTO());
        }

        details.setOperator(this.operator.toString());

        return details;
    }

    /**
     * Returns a SemiIntervalOrder corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a SemiIntervalOrder corresponding to the provided details.
     */
    public static SemiIntervalOrder fromDetails(
            final SemiIntervalOrderDetails details) {

        return new SemiIntervalOrder(details);
    }

    /**
     * Returns a List of {@link SemiIntervalOrder SemiIntervalOrders}
     * corresponding to the provided details.
     * 
     * @param details
     *            the {@link SemiIntervalOrderDetails}.
     * @return a List of {@link SemiIntervalOrder SemiIntervalOrders}
     *         corresponding to the provided details.
     */
    public static List<SemiIntervalOrder> fromDetails(
            Collection<SemiIntervalOrderDetails> details) {

        List<SemiIntervalOrder> objects = new ArrayList<>(details.size());

        for (SemiIntervalOrderDetails detail : details) {

            objects.add(SemiIntervalOrder.fromDetails(detail));
        }

        return objects;
    }
}
