/**
 * 
 */
package raziel.pawn.core.domain.modeling.assertions;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AttributionDetails;
import raziel.pawn.dto.modeling.ConstantDetails;
import raziel.pawn.dto.modeling.EntityDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class Attribution
        extends TemporaryRelationship<Entity, Constant> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link Attribution} class (Serialization
     * Constructor).
     */
    protected Attribution() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Attribution} class with the provided
     * name.
     * 
     * @param name
     *            the name.
     */
    public Attribution(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link Attribution} class. (Copy
     * Constructor)
     * 
     * @param attributionToCopy
     *            the attribution to copy.
     */
    public Attribution(final Attribution attributionToCopy) {

        super(attributionToCopy);
    }

    /**
     * Creates a new instance of the {@link Association} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Attribution(final AttributionDetails details) {

        super(details);

        if (details.getDomainAssertions() != null) {

            for (AssertionDetails domain : details.getDomainAssertions()) {

                if (domain instanceof EntityDetails) {

                    this.addDomainAssertion(Entity
                            .fromDetails((EntityDetails) domain));
                }
            }
        }
        if (details.getRangeAssertions() != null) {

            for (AssertionDetails range : details.getRangeAssertions()) {

                if (range instanceof ConstantDetails) {

                    this.addRangeAssertion(Constant
                            .fromDetails((ConstantDetails) range));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.assertions.TemporaryRelationship
     * #toDetails()
     */
    @Override
    public AttributionDetails toDTO() {

        AttributionDetails details = new AttributionDetails(super.toDTO());

        return details;
    }

    /**
     * Returns an Attribution corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an Attribution corresponding to the provided details.
     */
    public static Attribution fromDetails(final AttributionDetails details) {

        return new Attribution(details);
    }

    /**
     * Returns a List of {@link Attribution Attributions} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link AttributionDetails}.
     * @return a List of {@link Attribution Attributions} corresponding to the
     *         provided details.
     */
    public static List<Attribution> fromDetails(
            Collection<AttributionDetails> details) {

        List<Attribution> objects = new ArrayList<>(details.size());

        for (AttributionDetails detail : details) {

            objects.add(Attribution.fromDetails(detail));
        }

        return objects;
    }
}
