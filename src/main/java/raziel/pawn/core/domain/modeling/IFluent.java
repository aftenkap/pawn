/**
 * 
 */
package raziel.pawn.core.domain.modeling;

import raziel.pawn.core.domain.modeling.time.Duration;





/**
 * @author peter
 * @version
 * @since
 *
 */
public interface IFluent {

    /**
     * Returns the {@link Duration duration} of this {@link IFluent}.
     * 
     * @return the {@link Duration duration}.
     */
    public abstract Duration getDuration();

    /**
     * Sets the {@link Duration duration} of this {@link IFluent}.
     * 
     * @param duration
     *            the {@link Duration duration}.
     */
    public abstract void setDuration(Duration duration);

}