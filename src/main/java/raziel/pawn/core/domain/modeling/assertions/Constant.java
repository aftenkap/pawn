/**
 * 
 */
package raziel.pawn.core.domain.modeling.assertions;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.modeling.Assertion;
import raziel.pawn.core.domain.modeling.IConstant;
import raziel.pawn.core.domain.modeling.IInstance;
import raziel.pawn.dto.modeling.ConstantDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class Constant
        extends Assertion
        implements IInstance, IConstant {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link Constant} class.
     */
    public Constant() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Constant} class with the provided
     * name.
     * 
     * @param name
     *            the name.
     */
    public Constant(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link Constant} class. (Copy Constructor)
     * 
     * @param constantToCopy
     *            the constant to copy.
     */
    public <T extends IInstance & IConstant> Constant(final T constantToCopy) {

        super(constantToCopy);
    }

    /**
     * Creates a new instance of the {@link Constant} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Constant(final ConstantDetails details) {

        super(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.Assertion#toDetails()
     */
    @Override
    public ConstantDetails toDTO() {

        ConstantDetails details = new ConstantDetails(super.toDTO());
        return details;
    }

    /**
     * Returns a Constant corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a Constant corresponding to the provided details.
     */
    public static Constant fromDetails(final ConstantDetails details) {

        return new Constant(details);
    }

    /**
     * Returns a List of {@link Constant Constants} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link ConstantDetails}.
     * @return a List of {@link Constant Constants} corresponding to the
     *         provided details.
     */
    public static List<Constant> fromDetails(Collection<ConstantDetails> details) {

        List<Constant> objects = new ArrayList<>(details.size());

        for (ConstantDetails detail : details) {

            objects.add(Constant.fromDetails(detail));
        }

        return objects;
    }
}
