package raziel.pawn.core.domain.modeling.assertions;



import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.core.domain.modeling.Assertion;
import raziel.pawn.core.domain.modeling.IAssertion;
import raziel.pawn.core.domain.modeling.IRelationship;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.RelationshipDetails;


/**
 * @param <DOMAIN>
 *            the domain instance type.
 * @param <RANGE>
 *            the range instance type.
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public abstract class Relationship<DOMAIN extends IAssertion, RANGE extends IAssertion>
        extends Assertion
        implements IRelationship<DOMAIN, RANGE> {


    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;


    private List<DOMAIN>      domainAssertions;

    private List<RANGE>       rangeAssertions;



    @Override
    public List<DOMAIN> getDomainAssertions() {

        return domainAssertions;
    }

    @Override
    public boolean addDomainAssertion(DOMAIN domainAssertion) {

        return this.domainAssertions.add(domainAssertion);
    }

    @Override
    public boolean removeDomainAssertion(DOMAIN domainAssertion) {

        return this.domainAssertions.remove(domainAssertion);
    }

    @Override
    public void clearDomainAssertions() {

        this.domainAssertions.clear();
    }



    @Override
    public List<RANGE> getRangeAssertions() {

        return this.rangeAssertions;
    }

    @Override
    public boolean addRangeAssertion(RANGE rangeAssertion) {

        return this.rangeAssertions.add(rangeAssertion);
    }

    @Override
    public boolean removeRangeAssertion(RANGE rangeAssertion) {

        return this.rangeAssertions.remove(rangeAssertion);
    }

    @Override
    public void clearRangeAssertions() {

        this.rangeAssertions.clear();
    }


    /**
     * Creates a new instance of the {@link Relationship} class (Serialization
     * Constructor).
     */
    protected Relationship() {

        this("");
    }


    /**
     * Creates a new instance of the {@link Relationship} class with the
     * provided name.
     * 
     * @param name
     *            the name.
     */
    public Relationship(String name) {

        super(name);

        this.domainAssertions = new LinkedList<>();
        this.rangeAssertions = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Relationship} class. (Copy
     * Constructor)
     * 
     * @param relationshipToCopy
     *            the relationship to copy.
     */
    public Relationship(IRelationship<DOMAIN, RANGE> relationshipToCopy) {

        super(relationshipToCopy);
        this.domainAssertions = new LinkedList<>(
                relationshipToCopy.getDomainAssertions());
        this.rangeAssertions = new LinkedList<>(
                relationshipToCopy.getRangeAssertions());
    }

    /**
     * Creates a new instance of the {@link Relationship} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public Relationship(RelationshipDetails details) {

        super(details);

        this.domainAssertions = new LinkedList<>();
        this.rangeAssertions = new LinkedList<>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.Assertion#toDetails()
     */
    @Override
    public RelationshipDetails toDTO() {

        RelationshipDetails details = new RelationshipDetails(super.toDTO());


        List<AssertionDetails> domainAssertions = new ArrayList<>(
                this.domainAssertions.size());
        for (IAssertion domain : this.domainAssertions) {

            domainAssertions.add(domain.toDTO());
        }
        details.setDomainAssertions(domainAssertions);

        List<AssertionDetails> rangeAssertions = new ArrayList<>(
                this.rangeAssertions.size());
        for (IAssertion range : this.rangeAssertions) {

            rangeAssertions.add(range.toDTO());
        }
        details.setRangeAssertions(rangeAssertions);

        return details;
    }


}
