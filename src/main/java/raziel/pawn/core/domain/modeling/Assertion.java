/**
 * 
 */
package raziel.pawn.core.domain.modeling;



import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.SpreadsheetCell;
import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.modeling.AssertionDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public abstract class Assertion
        extends DomainObject
        implements IAssertion {

    /**
     * The serial version UID.
     */
    private static final long   serialVersionUID = 1L;

    private List<IDataPoint<?>> support;



    @Override
    public List<IDataPoint<?>> getSupport() {

        return this.support;
    }

    @Override
    public boolean addSupport(IDataPoint<?> dataPoint) {

        return this.support.add(dataPoint);
    }

    @Override
    public boolean removeSupport(IDataPoint<?> dataPoint) {

        return this.support.remove(dataPoint);
    }

    @Override
    public void clearSupport() {

        this.support.clear();
    }


    /**
     * Creates a new instance of the {@link Assertion} class.
     */
    protected Assertion() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Assertion} class.
     * 
     * @param name
     */
    public Assertion(String name) {

        super(name);
        this.support = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Assertion} class.
     * 
     * @param assertionToCopy
     */
    public Assertion(IAssertion assertionToCopy) {

        super(assertionToCopy);
        this.support = new LinkedList<>(assertionToCopy.getSupport());
    }

    /**
     * Creates a new instance of the {@link Assertion} class.
     * 
     * @param details
     */
    protected Assertion(AssertionDetails details) {

        super(details);

        this.support = new LinkedList<>();
        if (details.getSupport() != null) {

            for (DataPointDTO dataPoint : details.getSupport()) {

                if (dataPoint instanceof SpreadsheetCellDTO) {

                    this.support.add(SpreadsheetCell
                            .fromDTO((SpreadsheetCellDTO) dataPoint));
                }
            }
        }
    }

    @Override
    public AssertionDetails toDTO() {

        AssertionDetails details = new AssertionDetails(super.toDTO());

        List<DataPointDTO> support = new ArrayList<>(this.support.size());
        for (IDataPoint<?> dataPoint : this.support) {

            support.add(dataPoint.toDTO());
        }
        details.setSupport(support);

        return details;
    }
}
