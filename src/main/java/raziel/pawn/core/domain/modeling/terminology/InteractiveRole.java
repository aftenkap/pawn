/**
 * 
 */
package raziel.pawn.core.domain.modeling.terminology;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.modeling.assertions.Interaction;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.InteractionDetails;
import raziel.pawn.dto.modeling.InteractiveRoleDetails;
import raziel.pawn.dto.modeling.PerdurantDetails;
import raziel.pawn.dto.modeling.RoleDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class InteractiveRole
        extends DynamicRole<Perdurant, Perdurant, Interaction> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link InteractiveRole} class
     * (Serialization Constructor).
     */
    protected InteractiveRole() {

        this("");
    }

    /**
     * Creates a new instance of the {@link InteractiveRole} class with the
     * provided name.
     * 
     * @param name
     *            the name of this {@link InteractiveRole}.
     */
    public InteractiveRole(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link InteractiveRole} class. (Copy
     * Constructor)
     * 
     * @param relationToCopy
     *            the relation to copy.
     */
    public InteractiveRole(InteractiveRole relationToCopy) {

        super(relationToCopy);
    }

    /**
     * Creates a new instance of the {@link DynamicRole} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public InteractiveRole(final InteractiveRoleDetails details) {

        super(details);
        if (details.getDomainConcepts() != null) {

            for (ConceptDetails domainConcept : details.getDomainConcepts()) {

                if (domainConcept instanceof PerdurantDetails) {

                    this.addDomain(Perdurant
                            .fromDetails((PerdurantDetails) domainConcept));
                }
            }
        }
        if (details.getRangeConcepts() != null) {

            for (ConceptDetails rangeConcept : details.getRangeConcepts()) {

                if (rangeConcept instanceof PerdurantDetails) {

                    this.addRange(Perdurant
                            .fromDetails((PerdurantDetails) rangeConcept));
                }
            }
        }
        if (details.getDescendants() != null) {

            for (RoleDetails descendant : details.getDescendants()) {
                if (descendant instanceof InteractiveRoleDetails) {

                    this.addDescendant(InteractiveRole
                            .fromDetails((InteractiveRoleDetails) descendant));
                }
            }
        }

        if (details.getInstances() != null) {

            for (AssertionDetails instance : details.getInstances()) {

                if (instance instanceof InteractionDetails) {

                    this.addInstance(Interaction
                            .fromDetails((InteractionDetails) instance));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.terminology.Role#toDetails()
     */
    @Override
    public InteractiveRoleDetails toDTO() {

        InteractiveRoleDetails details = new InteractiveRoleDetails(
                super.toDTO());
        return details;
    }

    /**
     * Returns an InteractiveRole corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an InteractiveRole corresponding to the provided details.
     */
    public static InteractiveRole fromDetails(
            final InteractiveRoleDetails details) {

        return new InteractiveRole(details);
    }

    /**
     * Returns a List of {@link InteractiveRole InteractiveRelations}
     * corresponding to the provided details.
     * 
     * @param details
     *            the {@link InteractiveRoleDetails}.
     * @return a List of {@link InteractiveRole InteractiveRelations}
     *         corresponding to the provided details.
     */
    public static List<InteractiveRole> fromDetails(
            Collection<InteractiveRoleDetails> details) {

        List<InteractiveRole> objects = new ArrayList<>(details.size());

        for (InteractiveRoleDetails detail : details) {

            objects.add(InteractiveRole.fromDetails(detail));
        }

        return objects;
    }
}
