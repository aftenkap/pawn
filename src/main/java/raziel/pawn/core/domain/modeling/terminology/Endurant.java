package raziel.pawn.core.domain.modeling.terminology;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.modeling.IConcept;
import raziel.pawn.core.domain.modeling.assertions.Constant;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.ConstantDetails;
import raziel.pawn.dto.modeling.EndurantDetails;


/**
 * The {@link Endurant} class represents a {@link Concept} that does not change
 * over time. As such, Endurants serve as the vocabulary for describing the
 * general, invariable relationships between {@link Concept Concepts}. Instances
 * of Endurants are {@link Constant Constants}.
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class Endurant
        extends Concept<Constant> {


    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link Endurant} class.
     */
    protected Endurant() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Endurant} class with the provided
     * name.
     * 
     * @param name
     *            the name of this {@link Endurant}.
     */
    public Endurant(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link Endurant} class. (Copy Constructor)
     * 
     * @param endurantToCopy
     *            the endurant to copy.
     */
    protected Endurant(final IConcept<Constant> endurantToCopy) {

        super(endurantToCopy);
    }

    /**
     * Creates a new instance of the {@link Endurant} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Endurant(final EndurantDetails details) {

        super(details);

        if (details.getInstances() != null) {

            for (AssertionDetails instance : details.getInstances()) {

                if (instance instanceof ConstantDetails) {

                    this.addInstance(Constant
                            .fromDetails((ConstantDetails) instance));
                }
            }
        }
        if (details.getDescendants() != null) {

            for (ConceptDetails descendant : details.getDescendants()) {

                if (descendant instanceof EndurantDetails) {

                    this.addDescendant(Endurant
                            .fromDetails((EndurantDetails) descendant));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.terminology.Concept#toDetails()
     */
    @Override
    public EndurantDetails toDTO() {

        EndurantDetails details = new EndurantDetails(super.toDTO());
        return details;
    }


    /**
     * Returns an Endurant corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an Endurant corresponding to the provided details.
     */
    public static Endurant fromDetails(final EndurantDetails details) {

        return new Endurant(details);
    }

    /**
     * Returns a List of {@link Endurant Endurants} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link EndurantDetails}.
     * @return a List of {@link Endurant Endurants} corresponding to the
     *         provided details.
     */
    public static List<Endurant> fromDetails(Collection<EndurantDetails> details) {

        List<Endurant> objects = new ArrayList<>(details.size());

        for (EndurantDetails detail : details) {

            objects.add(Endurant.fromDetails(detail));
        }

        return objects;
    }
}
