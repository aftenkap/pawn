package raziel.pawn.core.domain.modeling.terminology;



import raziel.pawn.core.domain.modeling.IConcept;
import raziel.pawn.core.domain.modeling.IRole;
import raziel.pawn.core.domain.modeling.IRelationship;
import raziel.pawn.core.domain.modeling.assertions.TemporaryRelationship;
import raziel.pawn.dto.modeling.DynamicRoleDetails;


/**
 * The {@link DynamicRole} class defines a connection between one or
 * multiple {@link IConcept Concepts}. Instances of Relations are
 * {@link IRelationship Relationships}.
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 * @param <DOMAIN>
 *            the domain type.
 * @param <RANGE>
 *            the range type.
 * @param <INSTANCE>
 *            the instance type.
 */
public abstract class DynamicRole<DOMAIN extends IConcept<?>, RANGE extends IConcept<?>, INSTANCE extends TemporaryRelationship<?, ?>>
        extends Role<DOMAIN, RANGE, INSTANCE> {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link DynamicRole} class
     * (Serialization Constructor).
     */
    protected DynamicRole() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DynamicRole} class with the
     * provided name.
     * 
     * @param name
     *            the name of this {@link DynamicRole}.
     */
    public DynamicRole(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link DynamicRole} class. (Copy
     * Constructor)
     * 
     * @param relationToCopy
     *            the relation to copy.
     */
    public DynamicRole(IRole<DOMAIN, RANGE, INSTANCE> relationToCopy) {

        super(relationToCopy);
    }

    /**
     * Creates a new instance of the {@link DynamicRole} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public DynamicRole(DynamicRoleDetails details) {

        super(details);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.terminology.Role#toDetails()
     */
    @Override
    public DynamicRoleDetails toDTO() {

        DynamicRoleDetails details = new DynamicRoleDetails(
                super.toDTO());
        return details;
    }
}
