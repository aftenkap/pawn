package raziel.pawn.core.domain.modeling.time;



/**
 * The {@link Operator} enum provides the operators for specifying an order on
 * {@link Duration Durations} and {@link SemiInterval SemiIntervals}.
 *
 * @author Peter J. Radics
 * @version 0.1
 *
 */
public enum Operator {

    /**
     * In respect to {@link Duration Durations}, the extent of the left-hand
     * side of the comparison is smaller than the extent of the right-hand side.
     * <p/>
     * In respect to {@link SemiInterval SemiIntervals}, the left-hand side of
     * the comparison takes place earlier than the right-hand side.
     */
    SMALLER("<"),
    /**
     * In respect to {@link Duration Durations}, the extent of the left-hand
     * side of the comparison is smaller than or equal to the extent of the
     * right-hand side.
     * <p/>
     * In respect to {@link SemiInterval SemiIntervals}, the left-hand side of
     * the comparison takes place earlier than or at the same time as the
     * right-hand side.
     */
    SMALLER_OR_EQUAL("<="),
    /**
     * In respect to {@link Duration Durations}, the extent of the left-hand
     * side of the comparison is equal to the extent of the right-hand side.
     * <p/>
     * In respect to {@link SemiInterval SemiIntervals}, the left-hand side of
     * the comparison takes place earlier than or at the same time as the
     * right-hand side.
     */
    EQUAL("="),
    /**
     * In respect to {@link Duration Durations}, the extent of the left-hand
     * side of the comparison is greater than or equal to the extent of the
     * right-hand side.
     * <p/>
     * In respect to {@link SemiInterval SemiIntervals}, the left-hand side of
     * the comparison takes place later than or at the same time as the
     * right-hand side.
     */
    GREATER_OR_EQUAL(">="),
    /**
     * In respect to {@link Duration Durations}, the extent of the left-hand
     * side of the comparison is greater than the extent of the right-hand side.
     * <p/>
     * In respect to {@link SemiInterval SemiIntervals}, the left-hand side of
     * the comparison takes place later than the right-hand side.
     */
    GREATER(">");


    private final String name;

    private Operator(String name) {

        this.name = name;
    }

    @Override
    public String toString() {

        return this.name;
    }

    /**
     * Returns the {@link Operator} corresponding to the provided String.
     *
     * @param value
     *            the string.
     * @return the corresponding {@link Operator} or {@code null}.
     */
    public static Operator fromString(String value) {

        for (Operator operator : Operator.values()) {

            if (operator.toString().equals(value)) {

                return operator;
            }
        }
        return null;
    }
}
