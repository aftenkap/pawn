package raziel.pawn.core.domain.modeling.time;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import raziel.pawn.dto.modeling.SemiIntervalDetails;
import raziel.pawn.core.domain.DomainObject;


/**
 * The {@link SemiInterval} class provides the capability of specifying the
 * start or end of a {@link Duration}. In its essence, a {@link SemiInterval} is
 * a time point that is either the beginning or the end of a time interval.
 * <p/>
 * While every {@link Duration} has a {@link SemiInterval Start} and an
 * {@link SemiInterval End}, the concrete <i>time</i> of these events does not
 * need to be specified.
 * <p/>
 * This also allows us to specify an {@link SemiIntervalOrder Order} on the
 * semi-intervals.
 *
 * @author Peter J. Radics
 * @version 0.1
 *
 */
public class SemiInterval
        extends DomainObject {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    private Date              time;


    /**
     * Returns the concrete {@link Date time point} of this {@link SemiInterval}
     * .
     *
     * @return the concrete {@link Date time point}
     */
    public Date getTime() {

        return this.time;
    }


    /**
     * Sets the concrete {@link Date time point} of this {@link SemiInterval}.
     *
     * @param time
     *            the concrete {@link Date time point}
     */
    public void setTime(Date time) {

        this.time = time;
    }


    /**
     * Creates a new instance of the {@link SemiInterval} class.
     */
    protected SemiInterval() {

        this("");
    }

    /**
     * Creates a new instance of the {@link SemiInterval} class with the
     * provided name.
     *
     * @param name
     *            the name.
     */
    public SemiInterval(String name) {

        super(name);
    }


    /**
     * Creates a new instance of the {@link SemiInterval} class.
     * 
     * @param semiIntervalToCopy
     */
    public SemiInterval(final SemiInterval semiIntervalToCopy) {

        super(semiIntervalToCopy);

        this.time = semiIntervalToCopy.getTime();
    }

    /**
     * Creates a new instance of the {@link SemiInterval} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected SemiInterval(final SemiIntervalDetails details) {

        super(details);

        if (details.getTime() != null) {

            this.time = new Date(details.getTime().getTime());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.DomainObject#toDetails()
     */
    @Override
    public SemiIntervalDetails toDTO() {

        SemiIntervalDetails details = new SemiIntervalDetails(super.toDTO());

        details.setTime(this.time);

        return details;
    }

    /**
     * Returns a SemiInterval corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a SemiInterval corresponding to the provided details.
     */
    public static SemiInterval fromDetails(final SemiIntervalDetails details) {

        return new SemiInterval(details);
    }

    /**
     * Returns a List of {@link SemiInterval SemiIntervals} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link SemiIntervalDetails}.
     * @return a List of {@link SemiInterval SemiIntervals} corresponding to the
     *         provided details.
     */
    public static List<SemiInterval> fromDetails(
            Collection<SemiIntervalDetails> details) {

        List<SemiInterval> objects = new ArrayList<>(details.size());

        for (SemiIntervalDetails detail : details) {

            objects.add(SemiInterval.fromDetails(detail));
        }

        return objects;
    }
}
