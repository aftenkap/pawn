package raziel.pawn.core.domain.modeling.time;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import raziel.pawn.core.domain.modeling.IFluent;
import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.dto.modeling.DurationDetails;


/**
 * The {@link Duration} class provides the capability to specify the time
 * interval of validity of {@link IFluent Fluents}.
 * <p/>
 * This also allows us to specify an {@link DurationOrder Order} on the
 * durations.
 * <p/>
 * {@link Duration Durations} need not be defined, explicitly defined by
 * specific {@link SemiInterval Start} and {@link SemiInterval End} points,
 * implicitly defined through a {@link Date concrete duration}, or any
 * combination thereof.
 *
 * @author Peter J. Radics
 * @version 0.1
 *
 */
public class Duration
        extends DomainObject {

    /**
     * The serial version id.
     */
    private static final long serialVersionUID = 1L;

    private SemiInterval      start;

    private SemiInterval      end;

    private Date              duration;


    /**
     * Returns the {@SemiInterval Start} of the interval.
     *
     * @return the start.
     */
    public SemiInterval getStart() {

        return start;
    }


    /**
     * Sets the {@SemiInterval Start} of the interval.
     *
     * @param start
     *            the start.
     */
    public void setStart(SemiInterval start) {

        this.start = start;
    }


    /**
     * Returns the {@SemiInterval End} of the interval.
     *
     * @return the end.
     */
    public SemiInterval getEnd() {

        return end;
    }


    /**
     * Sets the {@SemiInterval End} of the interval.
     *
     * @param end
     *            the end.
     */
    public void setEnd(SemiInterval end) {

        this.end = end;
    }


    /**
     * Returns the {@Date Duration} of the interval.
     *
     * @return the duration.
     */
    public Date getDuration() {

        return duration;
    }


    /**
     * Returns the {@Date Duration} of the interval.
     *
     * @param duration
     *            the duration.
     */
    public void setDuration(Date duration) {

        this.duration = duration;
    }


    /**
     * Creates a new instance of the {@link Duration} class.
     */
    protected Duration() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Duration} class with the provided
     * name.
     *
     * @param name
     *            the name.
     */
    public Duration(String name) {

        super(name);

        this.start = new SemiInterval(name + " (Start)");
        this.end = new SemiInterval(name + " (End)");
        this.duration = null;
    }


    /**
     * Creates a new instance of the {@link Duration} class. (Copy Constructor)
     * 
     * @param durationToCopy
     *            the duration to copy.
     */
    public Duration(final Duration durationToCopy) {

        super(durationToCopy);
        this.start = durationToCopy.getStart();
        this.end = durationToCopy.getEnd();
        this.duration = durationToCopy.getDuration();
    }

    /**
     * Creates a new instance of the {@link Duration} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Duration(final DurationDetails details) {

        super(details);

        if (details.getStart() != null) {

            this.start = SemiInterval.fromDetails(details.getStart());
        }

        if (details.getEnd() != null) {

            this.end = SemiInterval.fromDetails(details.getEnd());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.DomainObject#toDetails()
     */
    @Override
    public DurationDetails toDTO() {

        DurationDetails details = new DurationDetails(super.toDTO());


        if (this.start != null) {

            details.setStart(this.start.toDTO());
        }
        if (this.end != null) {

            details.setEnd(this.end.toDTO());
        }
        details.setDuration(this.duration);

        return details;
    }

    /**
     * Returns a Duration corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a Duration corresponding to the provided details.
     */
    public static Duration fromDetails(final DurationDetails details) {

        return new Duration(details);
    }


    /**
     * Returns a List of {@link Duration Durations} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link DurationDetails}.
     * @return a List of {@link Duration Durations} corresponding to the
     *         provided details.
     */
    public static List<Duration> fromDetails(Collection<DurationDetails> details) {

        List<Duration> objects = new ArrayList<>(details.size());

        for (DurationDetails detail : details) {

            objects.add(Duration.fromDetails(detail));
        }

        return objects;
    }
}
