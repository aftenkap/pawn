/**
 * 
 */
package raziel.pawn.core.domain.modeling;


import java.util.List;

import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.dto.modeling.AssertionDetails;


/**
 * @author peter
 * @version
 * @since
 *
 */
public interface IAssertion
        extends IDomainObject {

    /**
     * Returns the supporting {@link IDataPoint Data Points} of this
     * {@link IAssertion Assertion}.
     * 
     * @return the supporting {@link IDataPoint Data Points} of this
     *         {@link IAssertion Assertion}.
     */
    public List<IDataPoint<?>> getSupport();

    /**
     * Adds the provided {@link IDataPoint Data Point} to the support of this
     * {@link IAssertion Assertion}.
     * 
     * @param dataPoint
     *            the {@link IDataPoint Data Point} to be added.
     * @return {@code true}, if the collection was changed as a result of this
     *         operation; {@code false} otherwise.
     */
    public boolean addSupport(IDataPoint<?> dataPoint);

    /**
     * Removes the provided {@link IDataPoint Data Point} from the support of
     * this {@link IAssertion Assertion}.
     * 
     * @param dataPoint
     *            the {@link IDataPoint Data Point} to be removed.
     * @return {@code true}, if the collection was changed as a result of this
     *         operation; {@code false} otherwise.
     */
    public boolean removeSupport(IDataPoint<?> dataPoint);

    /**
     * Clears the support of this instance.
     */
    public void clearSupport();

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public AssertionDetails toDTO();
}
