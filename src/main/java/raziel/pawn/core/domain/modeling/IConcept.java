package raziel.pawn.core.domain.modeling;


import java.util.List;

import raziel.pawn.dto.modeling.ConceptDetails;



/**
 * 
 * @param <ASSERTION>
 *            the instance type.
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public interface IConcept<ASSERTION extends IInstance>
        extends ITerm<ASSERTION> {

    /**
     * Returns the list of Descendants of this {@link IConcept Concept}.
     * 
     * @return the list of Descendants of this {@link IConcept Concept}.
     */
    public abstract List<IConcept<ASSERTION>> descendants();

    /**
     * Adds the provided{@link IConcept Concept} to the descendants of this
     * {@link IConcept Concept}.
     * 
     * @param descendant
     *            the {@link IConcept Concept} to be added.
     * @return {@code true}, if the collection was changed as a result of this
     *         operation; {@code false} otherwise.
     */
    public abstract boolean addDescendant(final IConcept<ASSERTION> descendant);

    /**
     * Removes the provided{@link IConcept Concept} from the descendants of this
     * {@link IConcept Concept}.
     * 
     * @param descendant
     *            the {@link IConcept Concept} to be removed.
     * @return {@code true}, if the collection was changed as a result of this
     *         operation; {@code false} otherwise.
     */
    public abstract boolean removeDescendant(
            final IConcept<ASSERTION> descendant);

    /**
     * Clears the descendants of this {@link IConcept Concept}.
     */
    public abstract void clearDescendants();

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.IDomainObject#toDetails()
     */
    @Override
    public ConceptDetails toDTO();
}
