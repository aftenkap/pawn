/**
 * 
 */
package raziel.pawn.core.domain.modeling.terminology;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.modeling.assertions.Association;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AssociationDetails;
import raziel.pawn.dto.modeling.AssociativeRoleDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.EndurantDetails;
import raziel.pawn.dto.modeling.RoleDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class AssociativeRole
        extends StaticRole<Endurant, Endurant, Association> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of the {@link AssociativeRole} class
     * (Serialization Constructor).
     */
    protected AssociativeRole() {

        this("");
    }

    /**
     * Creates a new instance of the {@link AssociativeRole} class with the
     * provided name.
     * 
     * @param name
     *            the name of this {@link AssociativeRole}.
     */
    public AssociativeRole(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link AssociativeRole} class. (Copy
     * Constructor)
     * 
     * @param relationToCopy
     *            the relation to copy.
     */
    public AssociativeRole(AssociativeRole relationToCopy) {

        super(relationToCopy);
    }

    /**
     * Creates a new instance of the {@link AssociativeRole} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    public AssociativeRole(AssociativeRoleDetails details) {

        super(details);
        if (details.getDomainConcepts() != null) {

            for (ConceptDetails domainConcept : details.getDomainConcepts()) {

                if (domainConcept instanceof EndurantDetails) {

                    this.addDomain(Endurant
                            .fromDetails((EndurantDetails) domainConcept));
                }
            }
        }
        if (details.getRangeConcepts() != null) {

            for (ConceptDetails rangeConcept : details.getRangeConcepts()) {

                if (rangeConcept instanceof EndurantDetails) {

                    this.addRange(Endurant
                            .fromDetails((EndurantDetails) rangeConcept));
                }
            }
        }
        if (details.getDescendants() != null) {

            for (RoleDetails descendant : details.getDescendants()) {
                if (descendant instanceof AssociativeRoleDetails) {

                    this.addDescendant(AssociativeRole
                            .fromDetails((AssociativeRoleDetails) descendant));
                }
            }
        }
        if (details.getInstances() != null) {

            for (AssertionDetails instance : details.getInstances()) {

                if (instance instanceof AssociationDetails) {

                    this.addInstance(Association
                            .fromDetails((AssociationDetails) instance));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.terminology.Role#toDetails()
     */
    @Override
    public AssociativeRoleDetails toDTO() {

        AssociativeRoleDetails details = new AssociativeRoleDetails(
                super.toDTO());
        return details;
    }

    /**
     * Returns an InteractiveRole corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an InteractiveRole corresponding to the provided details.
     */
    public static AssociativeRole fromDetails(
            final AssociativeRoleDetails details) {

        return new AssociativeRole(details);
    }


    /**
     * Returns a List of {@link AssociativeRole AssociativeRelations}
     * corresponding to the provided details.
     * 
     * @param details
     *            the {@link AssociativeRoleDetails}.
     * @return a List of {@link AssociativeRole AssociativeRelations}
     *         corresponding to the provided details.
     */
    public static List<AssociativeRole> fromDetails(
            Collection<AssociativeRoleDetails> details) {

        List<AssociativeRole> objects = new ArrayList<>(details.size());

        for (AssociativeRoleDetails detail : details) {

            objects.add(AssociativeRole.fromDetails(detail));
        }

        return objects;
    }
}
