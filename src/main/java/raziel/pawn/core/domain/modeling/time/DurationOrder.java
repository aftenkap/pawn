package raziel.pawn.core.domain.modeling.time;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.dto.modeling.DurationOrderDetails;


/**
 * The {@link DurationOrder} class provides the capability of specifying an
 * order on {@link Duration Durations}.
 *
 * @author Peter J. Radics
 * @version 0.1
 *
 */
public class DurationOrder
        extends DomainObject {

    /**
     * Serial version uid.
     */
    private static final long serialVersionUID = 1L;


    private Duration          firstOperand;
    private Duration          secondOperand;

    private Operator          operator;


    /**
     * Returns the {@link Duration first operand}.
     *
     * @return the first operand.
     */
    public Duration getFirstOperand() {

        return firstOperand;
    }


    /**
     * Returns the {@link Duration second operand}.
     *
     * @return the second operand.
     */
    public Duration getSecondOperand() {

        return secondOperand;
    }

    /**
     * Returns the {@link Operator operator}.
     *
     * @return the operator.
     */
    public Operator getOperator() {

        return operator;
    }


    @SuppressWarnings("unused")
    private void setOperator(String operator) {

        this.operator = Operator.fromString(operator);
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class.
     */
    protected DurationOrder() {

        this("");
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class with the
     * provided name (Serialization Constructor).
     *
     * @param name
     *            the name.
     */
    private DurationOrder(String name) {

        this(name, null, null, null);
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class with the
     * provided name, operands, and operator.
     *
     * @param name
     *            the name.
     * @param firstOperand
     *            the first operand.
     * @param secondOperand
     *            the second operand.
     * @param operator
     *            the operator.
     */
    public DurationOrder(String name, Duration firstOperand,
            Duration secondOperand, Operator operator) {

        super(name);
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
        this.operator = operator;
    }


    /**
     * Creates a new instance of the {@link DurationOrder} class. (Copy
     * Constructor)
     * 
     * @param orderToCopy
     *            the order to copy.
     */
    public DurationOrder(final DurationOrder orderToCopy) {

        super(orderToCopy);
        if (orderToCopy.getFirstOperand() != null) {

            this.firstOperand = new Duration(orderToCopy.getFirstOperand());
        }
        if (orderToCopy.getSecondOperand() != null) {

            this.secondOperand = new Duration(orderToCopy.getSecondOperand());
        }
        this.operator = orderToCopy.getOperator();
    }

    /**
     * Creates a new instance of the {@link DurationOrder} class from the
     * provided details.
     * 
     * @param details
     *            the details.
     */
    protected DurationOrder(final DurationOrderDetails details) {

        super(details);
        if (details.getFirstOperand() != null) {

            this.firstOperand = Duration.fromDetails(details.getFirstOperand());
        }
        if (details.getSecondOperand() != null) {

            this.secondOperand = Duration.fromDetails(details
                    .getSecondOperand());
        }
        if (details.getOperator() != null) {

            this.operator = Operator.fromString(details.getOperator());
        }
    }


    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.DomainObject#toDetails()
     */
    @Override
    public DurationOrderDetails toDTO() {

        DurationOrderDetails details = new DurationOrderDetails(
                super.toDTO());

        if (this.firstOperand != null) {

            details.setFirstOperand(this.firstOperand.toDTO());
        }
        if (this.secondOperand != null) {

            details.setSecondOperand(this.secondOperand.toDTO());
        }

        details.setOperator(this.operator.toString());

        return details;
    }

    /**
     * Returns a DurationOrder corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return a DurationOrder corresponding to the provided details.
     */
    public static DurationOrder fromDetails(final DurationOrderDetails details) {

        return new DurationOrder(details);
    }

    /**
     * Returns a List of {@link DurationOrder DurationOrders} corresponding to
     * the provided details.
     * 
     * @param details
     *            the {@link DurationOrderDetails}.
     * @return a List of {@link DurationOrder DurationOrders} corresponding to
     *         the provided details.
     */
    public static List<DurationOrder> fromDetails(
            Collection<DurationOrderDetails> details) {

        List<DurationOrder> objects = new ArrayList<>(details.size());

        for (DurationOrderDetails detail : details) {

            objects.add(DurationOrder.fromDetails(detail));
        }

        return objects;
    }
}
