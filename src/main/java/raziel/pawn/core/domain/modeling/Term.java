/**
 * 
 */
package raziel.pawn.core.domain.modeling;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.core.domain.DomainObject;
import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.core.domain.data.SpreadsheetCell;
import raziel.pawn.dto.data.DataPointDTO;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.TermDTO;


/**
 * @param <INSTANCE>
 * 
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public abstract class Term<INSTANCE extends IAssertion>
        extends DomainObject
        implements ITerm<INSTANCE> {

    /**
     * The serial version UID.
     */
    private static final long   serialVersionUID = 1L;

    private List<IDataPoint<?>> support;

    private List<INSTANCE>      instances;


    @Override
    public List<IDataPoint<?>> getSupport() {

        return this.support;
    }

    @Override
    public boolean addSupport(final IDataPoint<?> dataPoint) {

        return this.support.add(dataPoint);
    }


    @Override
    public boolean removeSupport(final IDataPoint<?> dataPoint) {

        return this.support.remove(dataPoint);
    }

    @Override
    public void clearSupport() {

        this.support.clear();
    }

    @Override
    public List<INSTANCE> getInstances() {

        return this.instances;
    }

    @Override
    public boolean addInstance(final INSTANCE instance) {

        return this.instances.add(instance);
    }


    @Override
    public boolean removeInstance(final INSTANCE instance) {

        return this.support.remove(instance);
    }


    @Override
    public void clearInstances() {

        this.instances.clear();
    }



    /**
     * Creates a new instance of the {@link Term} class.
     */
    protected Term() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Term} class.
     * 
     * @param name
     */
    public Term(String name) {

        super(name);

        this.instances = new LinkedList<>();
        this.support = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Term} class.
     * 
     * @param termToCopy
     */
    public Term(ITerm<INSTANCE> termToCopy) {

        super(termToCopy);

        this.instances = new LinkedList<>(termToCopy.getInstances());
        this.support = new LinkedList<>(termToCopy.getSupport());
    }

    /**
     * Creates a new instance of the {@link Term} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Term(TermDTO details) {

        super(details);
        this.instances = new LinkedList<>();
        this.support = new LinkedList<>();


        for (DataPointDTO dataPoint : details.getSupport()) {

            if (dataPoint instanceof SpreadsheetCellDTO) {

                this.support.add(SpreadsheetCell
                        .fromDTO((SpreadsheetCellDTO) dataPoint));
            }
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.DomainObject#toDetails()
     */
    @Override
    public TermDTO toDTO() {

        TermDTO details = new TermDTO(super.toDTO());


        List<DataPointDTO> support = new ArrayList<>(this.support.size());
        for (IDataPoint<?> dataPoint : this.support) {

            support.add(dataPoint.toDTO());
        }
        details.setSupport(support);

        List<AssertionDetails> instances = new ArrayList<>(
                this.instances.size());
        for (IAssertion assertion : this.instances) {

            instances.add(assertion.toDTO());
        }
        details.setInstances(instances);

        return details;
    }
}
