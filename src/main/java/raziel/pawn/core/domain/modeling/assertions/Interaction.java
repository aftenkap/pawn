/**
 * 
 */
package raziel.pawn.core.domain.modeling.assertions;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.EntityDetails;
import raziel.pawn.dto.modeling.InteractionDetails;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class Interaction
        extends TemporaryRelationship<Entity, Entity> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of the {@link Interaction} class (Serialization
     * Constructor).
     */
    protected Interaction() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Interaction} class with the provided
     * name.
     * 
     * @param name
     *            the name.
     */
    public Interaction(String name) {

        super(name);
    }

    /**
     * Creates a new instance of the {@link Interaction} class. (Copy
     * Constructor)
     * 
     * @param interactionToCopy
     *            the interaction to copy.
     */
    public Interaction(final Interaction interactionToCopy) {

        super(interactionToCopy);
    }

    /**
     * Creates a new instance of the {@link Association} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Interaction(final InteractionDetails details) {

        super(details);

        if (details.getDomainAssertions() != null) {

            for (AssertionDetails domain : details.getDomainAssertions()) {

                if (domain instanceof EntityDetails) {

                    this.addDomainAssertion(Entity
                            .fromDetails((EntityDetails) domain));
                }
            }
        }
        if (details.getRangeAssertions() != null) {

            for (AssertionDetails range : details.getRangeAssertions()) {

                if (range instanceof EntityDetails) {

                    this.addRangeAssertion(Entity
                            .fromDetails((EntityDetails) range));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.assertions.TemporaryRelationship
     * #toDetails()
     */
    @Override
    public InteractionDetails toDTO() {

        InteractionDetails details = new InteractionDetails(super.toDTO());

        return details;
    }

    /**
     * Returns an Interaction corresponding to the provided details.
     * 
     * @param details
     *            the details.
     * @return an Interaction corresponding to the provided details.
     */
    public static Interaction fromDetails(final InteractionDetails details) {

        return new Interaction(details);
    }


    /**
     * Returns a List of {@link Interaction Interactions} corresponding to the
     * provided details.
     * 
     * @param details
     *            the {@link InteractionDetails}.
     * @return a List of {@link Interaction Interactions} corresponding to the
     *         provided details.
     */
    public static List<Interaction> fromDetails(
            Collection<InteractionDetails> details) {

        List<Interaction> objects = new ArrayList<>(details.size());

        for (InteractionDetails detail : details) {

            objects.add(Interaction.fromDetails(detail));
        }

        return objects;
    }
}
