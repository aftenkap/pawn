package raziel.pawn.core.domain.modeling.terminology;



import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import raziel.pawn.core.domain.modeling.IConcept;
import raziel.pawn.core.domain.modeling.IInstance;
import raziel.pawn.core.domain.modeling.Term;
import raziel.pawn.dto.modeling.ConceptDetails;


/**
 * @author Peter J. Radics
 * @param <T>
 *            the instance type.
 * 
 * @version 0.1
 * @since 0.1
 */
public abstract class Concept<T extends IInstance>
        extends Term<T>
        implements IConcept<T> {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private List<IConcept<T>> descendants;

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IConcept#getDescendants()
     */
    @Override
    public List<IConcept<T>> descendants() {

        return this.descendants;
    }


    /*
     * (non-Javadoc)
     * 
     * @see
     * raziel.pawn.persistence.domain.modeling.IConcept#addDescendant(raziel
     * .pawn.persistence.domain.modeling.IConcept)
     */
    @Override
    public boolean addDescendant(final IConcept<T> descendant) {

        return this.descendants.add(descendant);
    }


    /*
     * (non-Javadoc)
     * 
     * @see
     * 
     * raziel.pawn.persistence.domain.modeling.IConcept#removeDescendant(raziel
     * .pawn.persistence.domain.modeling.IConcept)
     */
    @Override
    public boolean removeDescendant(final IConcept<T> descendant) {

        return this.descendants.remove(descendant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.IConcept#clearDescendants()
     */
    @Override
    public void clearDescendants() {

        this.descendants.clear();
    }

    /**
     * Creates a new instance of the {@link Concept} class.
     */
    protected Concept() {

        this("");
    }

    /**
     * Creates a new instance of the {@link Concept} class with the provided
     * name.
     * 
     * @param name
     *            the name.
     */
    public Concept(String name) {

        super(name);

        this.descendants = new LinkedList<>();
    }

    /**
     * Creates a new instance of the {@link Concept} class. (Copy Constructor)
     * 
     * @param conceptToCopy
     *            the concept to copy.
     */
    public Concept(IConcept<T> conceptToCopy) {

        super(conceptToCopy);

        this.descendants = new LinkedList<>(conceptToCopy.descendants());
    }


    /**
     * Creates a new instance of the {@link Concept} class from the provided
     * details.
     * 
     * @param details
     *            the details.
     */
    protected Concept(ConceptDetails details) {

        super(details);
        this.descendants = new LinkedList<>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.persistence.domain.modeling.Term#toDetails()
     */
    @Override
    public ConceptDetails toDTO() {

        ConceptDetails details = new ConceptDetails(super.toDTO());


        List<ConceptDetails> descendants = new ArrayList<>(
                this.descendants.size());
        for (IConcept<T> descendant : this.descendants) {

            descendants.add(descendant.toDTO());
        }
        details.setDescendants(descendants);

        return details;
    }
}
