/**
 * 
 */
package raziel.pawn.core.domain.modeling;


import java.util.List;

import raziel.pawn.core.domain.IDomainObject;
import raziel.pawn.core.domain.data.IDataPoint;
import raziel.pawn.dto.modeling.TermDTO;



/**
 * @param <INSTANCE>
 * @author peter
 * @version
 * @since
 *
 */
public interface ITerm<INSTANCE extends IAssertion>
        extends IDomainObject {


    /**
     * Returns the supporting {@link IDataPoint Data Points} of this
     * {@link ITerm Term}.
     * 
     * @return the supporting {@link IDataPoint Data Points} of this
     *         {@link ITerm Term}.
     */
    public abstract List<IDataPoint<?>> getSupport();

    /**
     * Adds the provided {@link IDataPoint Data Point} to the support of this
     * {@link ITerm Term}.
     * 
     * @param dataPoint
     *            the {@link IDataPoint Data Point} to be added.
     * @return {@code true}, if the collection was changed as a result of this
     *         operation; {@code false} otherwise.
     */
    public abstract boolean addSupport(IDataPoint<?> dataPoint);

    /**
     * Removes the provided {@link IDataPoint Data Point} from the support of
     * this {@link ITerm Term}.
     * 
     * @param dataPoint
     *            the {@link IDataPoint Data Point} to be removed.
     * @return {@code true}, if the collection was changed as a result of this
     *         operation; {@code false} otherwise.
     */
    public abstract boolean removeSupport(IDataPoint<?> dataPoint);

    /**
     * Clears the support of this {@link ITerm Term}.
     */
    public abstract void clearSupport();

    /**
     * Returns the {@link IAssertion} instances of this {@link ITerm}.
     * 
     * @return the {@link IAssertion} instances of this {@link ITerm}.
     */
    public abstract List<INSTANCE> getInstances();

    /**
     * Adds the provided {@link IAssertion Assertion} to the instances of this
     * {@link ITerm}.
     * 
     * @param instance
     *            the {@link IAssertion Assertion} to add.
     * @return {@code true}, if the instances changed as a result of the call.
     */
    public abstract boolean addInstance(INSTANCE instance);

    /**
     * Removes the provided {@link IAssertion Assertion} from the instances of
     * this {@link ITerm}.
     * 
     * @param instance
     *            the {@link IAssertion Assertion} to remove.
     * @return {@code true}, if an instance was removed as a result of this
     *         call.
     */
    public abstract boolean removeInstance(INSTANCE instance);

    /**
     * Removes all instances.
     */
    public abstract void clearInstances();

    /*
     * (non-Javadoc)
     * 
     * @see raziel.pawn.core.domain.IDomainObject#toDetails()
     */
    @Override
    public abstract TermDTO toDTO();
}
