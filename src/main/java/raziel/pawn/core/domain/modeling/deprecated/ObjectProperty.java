package raziel.pawn.core.domain.modeling.deprecated;
//package raziel.pawn.core.domain.modeling.deprecated;
//
//
//import static javax.persistence.CascadeType.*;
//
//import javax.persistence.CascadeType;
//import javax.persistence.ConstraintMode;
//import javax.persistence.Entity;
//import javax.persistence.ForeignKey;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToOne;
//import javax.persistence.Table;
//
//import raziel.pawn.common.DatabaseObject;
//import raziel.pawn.core.domain.modeling.assertions.Fluent;
//import raziel.pawn.core.domain.modeling.time.Duration;
//
//
///**
// * The {@link ObjectProperty} class provides the capability of specifying simple
// * (temporally restricted) <i>relationships</i> of {@link Fluent Fluents}.
// * 
// * @author Peter J. Radics
// * @version 0.1
// * 
// */
//@Entity
//@Spreadsheet(name = "objectproperty")
//public class ObjectProperty
//        extends DomainObject {
//
//
//    /**
//     * Serial version uid.
//     */
//    private static final long serialVersionUID = 1L;
//
//
//    @OneToOne(cascade = { PERSIST, MERGE, REFRESH, DETACH })
//    @JoinColumn(name = "value", referencedColumnName = "_id",
//            foreignKey = @ForeignKey(name = "FK_objectproperty_value",
//                    value = ConstraintMode.CONSTRAINT))
//    private Fluent            value;
//
//
//    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true,
//            targetEntity = Duration.class)
//    @JoinColumn(name = "propertyDuration", referencedColumnName = "_id",
//            foreignKey = @ForeignKey(
//                    name = "FK_objectproperty_propertyDuration",
//                    value = ConstraintMode.CONSTRAINT))
//    private Duration          duration;
//
//
//    /**
//     * Returns the value of this {@link ObjectProperty}
//     * 
//     * @return the value.
//     */
//    public Fluent getValue() {
//
//        return this.value;
//    }
//
//
//    /**
//     * Sets the value of this {@link ObjectProperty}
//     * 
//     * @param value
//     *            the value.
//     */
//    public void setValue(Fluent value) {
//
//        this.value = value;
//    }
//
//
//    /**
//     * Returns the {@link Duration} of the validity of this
//     * {@link ObjectProperty}.
//     * 
//     * @return the duration.
//     */
//    public Duration getDuration() {
//
//        return this.duration;
//    }
//
//
//    /**
//     * Sets the {@link Duration} of the validity of this {@link ObjectProperty}.
//     * 
//     * @param duration
//     *            the duration.
//     */
//    public void setDuration(Duration duration) {
//
//        this.duration = duration;
//    }
//
//
//    /**
//     * Creates a new instance of the {@link ObjectProperty} class.
//     */
//    protected ObjectProperty() {
//
//        this("");
//    }
//
//    /**
//     * Creates a new instance of the {@link ObjectProperty} class with the
//     * provided name.
//     * 
//     * @param name
//     *            the name.
//     */
//    public ObjectProperty(String name) {
//
//        this(name, null);
//    }
//
//
//    /**
//     * Creates a new instance of the {@link ObjectProperty} class with the
//     * provided name and value. Its {@link Duration} is declared but not
//     * defined.
//     * 
//     * @param name
//     *            the name.
//     * @param value
//     *            the value.
//     */
//    public ObjectProperty(String name, Fluent value) {
//
//        super(name);
//        this.duration = new Duration(name + " - Duration");
//        this.value = value;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//
//        if (super.equals(obj) && obj instanceof ObjectProperty) {
//            ObjectProperty other = (ObjectProperty) obj;
//
//            boolean valuesEqual = this.getValue().equals(other.getValue());
//
//            return valuesEqual;
//        }
//
//        return false;
//    }
//
//    @Override
//    public int hashCode() {
//
//        return super.hashCode() + 81 * this.getValue().hashCode();
//    }
//}
