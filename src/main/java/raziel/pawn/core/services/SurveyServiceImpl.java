package raziel.pawn.core.services;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.metadata.AnswerDetails;
import raziel.pawn.dto.metadata.ChoiceDetails;
import raziel.pawn.dto.metadata.ChoiceQuestionDetails;
import raziel.pawn.dto.metadata.ChoiceSetDetails;
import raziel.pawn.dto.metadata.ParticipantDTO;
import raziel.pawn.dto.metadata.QuestionDetails;
import raziel.pawn.dto.metadata.RankingQuestionDetails;
import raziel.pawn.dto.metadata.SurveyDetails;
import raziel.pawn.dto.metadata.TextQuestionDetails;
import raziel.pawn.persistence.services.SurveyPersistenceService;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Service
@Transactional
public class SurveyServiceImpl
        implements SurveyService {

    private final SurveyPersistenceService surveyPersistenceService;

    /**
     * Creates a new instance of the {@link SurveyServiceImpl} class.
     * 
     * @param surveyPersistenceService
     *            the {@link SurveyPersistenceService}.
     */
    @Autowired
    public SurveyServiceImpl(
            final SurveyPersistenceService surveyPersistenceService) {

        this.surveyPersistenceService = surveyPersistenceService;
    }


    @Override
    public SurveyDetails createSurvey(SurveyDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.surveyPersistenceService.createSurvey(event);
    }



    @Override
    public List<SurveyDetails> retrieveAllSurveys()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveAllSurveys();
    }



    @Override
    public SurveyDetails retrieveSurvey(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveSurvey(id);
    }

    @Override
    public SurveyDetails updateSurvey(SurveyDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.updateSurvey(event);
    }


    @Override
    public SurveyDetails deleteSurvey(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.deleteSurvey(id);
    }

    @Override
    public SurveyDetails retrieveSurveyForQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveSurveyForQuestion(id);
    }

    @Override
    public SurveyDetails retrieveSurveyForAnswer(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {


        return this.surveyPersistenceService.retrieveSurveyForAnswer(id);
    }

    @Override
    public SurveyDetails retrieveSurveyForParticipant(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {


        return this.surveyPersistenceService.retrieveSurveyForParticipant(id);
    }

    @Override
    public TextQuestionDetails createTextQuestion(TextQuestionDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.surveyPersistenceService.createTextQuestion(event);
    }


    @Override
    public ChoiceQuestionDetails createChoiceQuestion(
            ChoiceQuestionDetails event)
            throws RequestDeniedException, RequestFailedException {


        return this.surveyPersistenceService.createChoiceQuestion(event);
    }



    @Override
    public RankingQuestionDetails createRankingQuestion(
            RankingQuestionDetails event)
            throws RequestDeniedException, RequestFailedException {


        return this.surveyPersistenceService.createRankingQuestion(event);
    }



    @Override
    public List<QuestionDetails> retrieveAllQuestions()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveAllQuestions();
    }


    @Override
    public QuestionDetails retrieveQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveQuestion(id);
    }

    @Override
    public TextQuestionDetails retrieveTextQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveTextQuestion(id);
    }

    @Override
    public ChoiceQuestionDetails retrieveChoiceQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveChoiceQuestion(id);
    }

    @Override
    public RankingQuestionDetails retrieveRankingQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveRankingQuestion(id);
    }


    @Override
    public TextQuestionDetails updateTextQuestion(TextQuestionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.updateTextQuestion(event);
    }

    @Override
    public ChoiceQuestionDetails updateChoiceQuestion(
            ChoiceQuestionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {


        return this.surveyPersistenceService.updateChoiceQuestion(event);
    }

    @Override
    public RankingQuestionDetails updateRankingQuestion(
            RankingQuestionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {


        return this.surveyPersistenceService.updateRankingQuestion(event);
    }

    @Override
    public QuestionDetails deleteQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.deleteQuestion(id);
    }


    @Override
    public AnswerDetails createAnswer(AnswerDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.surveyPersistenceService.createAnswer(event);
    }



    @Override
    public List<AnswerDetails> retrieveAllAnswers()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveAllAnswers();
    }

    @Override
    public AnswerDetails retrieveAnswer(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveAnswer(id);
    }

    @Override
    public AnswerDetails updateAnswer(AnswerDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.updateAnswer(event);
    }

    @Override
    public AnswerDetails deleteAnswer(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.deleteAnswer(id);
    }



    @Override
    public List<AnswerDetails> retrieveAllAnswersForQuestion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveAllAnswersForQuestion(id);
    }


    @Override
    public List<AnswerDetails> retrieveAllAnswersForParticipant(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService
                .retrieveAllAnswersForParticipant(id);
    }

    @Override
    public AnswerDetails retrieveAnswerToQuestionByParticipant(int questionId,
            int participantId)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService
                .retrieveAnswerToQuestionByParticipant(questionId,
                        participantId);
    }

    @Override
    public ParticipantDTO createParticipant(ParticipantDTO event)
            throws RequestDeniedException, RequestFailedException {


        return this.surveyPersistenceService.createParticipant(event);
    }


    @Override
    public List<ParticipantDTO> retrieveAllParticipants()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveAllParticipants();
    }

    @Override
    public ParticipantDTO retrieveParticipant(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveParticipant(id);
    }


    @Override
    @Transactional(readOnly = false)
    public ParticipantDTO updateParticipant(ParticipantDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.updateParticipant(event);
    }

    @Override
    public ParticipantDTO deleteParticipant(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.deleteParticipant(id);
    }


    @Override
    public ChoiceDetails createChoice(ChoiceDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.surveyPersistenceService.createChoice(event);
    }


    @Override
    public List<ChoiceDetails> retrieveAllChoices()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveAllChoices();
    }

    @Override
    public ChoiceDetails retrieveChoice(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveChoice(id);
    }

    @Override
    public ChoiceDetails retrieveChoiceByName(String name)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveChoiceByName(name);
    }

    @Override
    public ChoiceDetails updateChoice(ChoiceDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.updateChoice(event);
    }

    @Override
    public ChoiceDetails deleteChoice(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.deleteChoice(id);
    }



    @Override
    public ChoiceSetDetails createChoiceSet(ChoiceSetDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.surveyPersistenceService.createChoiceSet(event);
    }


    @Override
    public List<ChoiceSetDetails> retrieveAllChoiceSets()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveAllChoiceSets();
    }

    @Override
    public ChoiceSetDetails retrieveChoiceSet(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveChoiceSet(id);
    }

    @Override
    public ChoiceSetDetails retrieveChoiceSetByName(String name)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.retrieveChoiceSetByName(name);
    }

    @Override
    public ChoiceSetDetails updateChoiceSet(ChoiceSetDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.updateChoiceSet(event);
    }

    @Override
    public ChoiceSetDetails deleteChoiceSet(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.surveyPersistenceService.deleteChoiceSet(id);
    }
}
