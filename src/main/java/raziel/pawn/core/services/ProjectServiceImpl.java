package raziel.pawn.core.services;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.ProjectDetails;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.persistence.services.ProjectPersistenceService;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Service
@Transactional
public class ProjectServiceImpl
        implements ProjectService {

    private final ProjectPersistenceService projectPersistenceService;

    /**
     * Creates a new instance of the {@link ProjectServiceImpl} class.
     * 
     * @param projectPersistenceService
     *            the project persistence service.
     */
    @Autowired
    public ProjectServiceImpl(
            final ProjectPersistenceService projectPersistenceService) {

        this.projectPersistenceService = projectPersistenceService;
    }



    @Override
    public ProjectDetails createProject(ProjectDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.projectPersistenceService.createProject(event);
    }

    @Override
    public List<ProjectDetails> retrieveAllProjects()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.projectPersistenceService.retrieveAllProjects();
    }

    @Override
    public ProjectDetails retrieveProject(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.projectPersistenceService.retrieveProject(id);
    }

    @Override
    public ProjectDetails retrieveProjectForDataSource(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.projectPersistenceService.retrieveProjectForDataSource(id);
    }

    @Override
    public ProjectDetails retrieveProjectForMetadataSource(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.projectPersistenceService
                .retrieveProjectForMetadataSource(id);
    }

    @Override
    public ProjectDetails retrieveProjectForDataModel(final int id) {

        return this.retrieveProjectForDataModel(id);
    }


    @Override
    public ProjectDetails updateProject(ProjectDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.projectPersistenceService.updateProject(event);
    }

    @Override
    public ProjectDetails deleteProject(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.projectPersistenceService.deleteProject(id);
    }
}
