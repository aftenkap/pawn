package raziel.pawn.core.services;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.data.SpreadsheetCellDTO;
import raziel.pawn.dto.data.SpreadsheetDTO;
import raziel.pawn.persistence.services.SpreadsheetPersistenceService;


/**
 * @author Peter J. Radics
 * @version 0.1.3
 * @since 0.1.0
 *
 */
@Service
@Transactional
public class SpreadsheetServiceImpl
        implements SpreadsheetService {

    private final SpreadsheetPersistenceService spreadsheetPersistenceService;


    /**
     * Creates a new instance of the {@link SpreadsheetServiceImpl} class.
     * 
     * @param spreadsheetPersistenceService
     *            the {@link SpreadsheetPersistenceService}.
     */
    @Autowired
    public SpreadsheetServiceImpl(
            final SpreadsheetPersistenceService spreadsheetPersistenceService) {

        this.spreadsheetPersistenceService = spreadsheetPersistenceService;
    }



    @Override
    public SpreadsheetDTO createSpreadsheet(SpreadsheetDTO event)
            throws RequestDeniedException, RequestFailedException {

        return this.spreadsheetPersistenceService.createSpreadsheet(event);
    }

    @Override
    public List<SpreadsheetDTO> retrieveAllSpreadsheets()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.retrieveAllSpreadsheets();
    }

    @Override
    public SpreadsheetDTO retrieveSpreadsheet(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.retrieveSpreadsheet(id);
    }

    @Override
    public SpreadsheetDTO updateSpreadsheet(SpreadsheetDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.updateSpreadsheet(event);
    }


    @Override
    public SpreadsheetDTO deleteSpreadsheet(final SpreadsheetDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.deleteSpreadsheet(event);
    }

    @Override
    public SpreadsheetDTO deleteSpreadsheet(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.deleteSpreadsheet(id);
    }



    @Override
    public SpreadsheetDTO retrieveSpreadsheetForSpreadsheetCell(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService
                .retrieveSpreadsheetForSpreadsheetCell(id);
    }

    @Override
    public SpreadsheetCellDTO createSpreadsheetCell(SpreadsheetCellDTO event)
            throws RequestDeniedException, RequestFailedException {

        return this.spreadsheetPersistenceService.createSpreadsheetCell(event);
    }

    @Override
    public List<SpreadsheetCellDTO> retrieveAllSpreadsheetCells()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.retrieveAllSpreadsheetCells();
    }

    @Override
    public SpreadsheetCellDTO retrieveSpreadsheetCell(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.retrieveSpreadsheetCell(id);
    }

    @Override
    public SpreadsheetCellDTO updateSpreadsheetCell(SpreadsheetCellDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.updateSpreadsheetCell(event);
    }


    @Override
    public SpreadsheetCellDTO deleteSpreadsheetCell(
            final SpreadsheetCellDTO event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.deleteSpreadsheetCell(event);
    }

    @Override
    public SpreadsheetCellDTO deleteSpreadsheetCell(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.spreadsheetPersistenceService.deleteSpreadsheetCell(id);
    }
}
