package raziel.pawn.core.services;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AssociationDetails;
import raziel.pawn.dto.modeling.AssociativeRoleDetails;
import raziel.pawn.dto.modeling.AttributionDetails;
import raziel.pawn.dto.modeling.AttributiveRoleDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.ConstantDetails;
import raziel.pawn.dto.modeling.DataModelDetails;
import raziel.pawn.dto.modeling.DurationOrderDetails;
import raziel.pawn.dto.modeling.EndurantDetails;
import raziel.pawn.dto.modeling.EntityDetails;
import raziel.pawn.dto.modeling.InteractionDetails;
import raziel.pawn.dto.modeling.InteractiveRoleDetails;
import raziel.pawn.dto.modeling.PerdurantDetails;
import raziel.pawn.dto.modeling.RoleDetails;
import raziel.pawn.dto.modeling.SemiIntervalOrderDetails;
import raziel.pawn.persistence.services.DataModelPersistenceService;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 *
 */
@Service
@Transactional
public class DataModelServiceImpl
        implements DataModelService {

    private final DataModelPersistenceService dataModelPersistenceService;

    /**
     * Creates a new instance of the {@link DataModelServiceImpl} class.
     * 
     * @param dataModelPersistenceService
     *            the {@link DataModelPersistenceService}.
     */
    @Autowired
    public DataModelServiceImpl(
            final DataModelPersistenceService dataModelPersistenceService) {

        this.dataModelPersistenceService = dataModelPersistenceService;
    }



    @Override
    public DataModelDetails createDataModel(DataModelDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createDataModel(event);
    }

    @Override
    public List<DataModelDetails> retrieveAllDataModels()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveAllDataModels();
    }

    @Override
    public DataModelDetails retrieveDataModel(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveDataModel(id);
    }

    @Override
    public DataModelDetails updateDataModel(DataModelDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateDataModel(event);
    }

    @Override
    public DataModelDetails deleteDataModel(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.deleteDataModel(id);
    }



    @Override
    public EndurantDetails createEndurant(EndurantDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createEndurant(event);
    }

    @Override
    public PerdurantDetails createPerdurant(PerdurantDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createPerdurant(event);
    }



    @Override
    public List<ConceptDetails> retrieveAllConcepts()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveAllConcepts();
    }

    @Override
    public ConceptDetails retrieveConcept(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveConcept(id);
    }

    @Override
    public EndurantDetails updateEndurant(EndurantDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateEndurant(event);
    }

    @Override
    public PerdurantDetails updatePerdurant(PerdurantDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updatePerdurant(event);
    }

    @Override
    public ConceptDetails deleteConcept(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.deleteConcept(id);
    }



    @Override
    public AssociativeRoleDetails createAssociativeRole(
            AssociativeRoleDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createAssociativeRole(event);
    }


    @Override
    public AttributiveRoleDetails createAttributiveRole(
            AttributiveRoleDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createAttributiveRole(event);
    }

    @Override
    public InteractiveRoleDetails createInteractiveRole(
            InteractiveRoleDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createInteractiveRole(event);
    }

    @Override
    public List<RoleDetails> retrieveAllRoles()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveAllRoles();
    }

    @Override
    public RoleDetails retrieveRole(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveRole(id);
    }

    @Override
    public AssociativeRoleDetails updateAssociativeRole(
            AssociativeRoleDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateAssociativeRole(event);
    }

    @Override
    public AttributiveRoleDetails updateAttributiveRole(
            AttributiveRoleDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateAttributiveRole(event);
    }

    @Override
    public InteractiveRoleDetails updateInteractiveRole(
            InteractiveRoleDetails event) {

        return this.updateInteractiveRole(event);
    }

    @Override
    public RoleDetails deleteRole(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.deleteRole(id);
    }



    @Override
    public ConstantDetails createConstant(ConstantDetails event) {

        return this.createConstant(event);
    }

    @Override
    public EntityDetails createEntity(EntityDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createEntity(event);
    }

    @Override
    public AssociationDetails createAssociation(AssociationDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createAssociation(event);
    }

    @Override
    public AttributionDetails createAttribution(AttributionDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createAttribution(event);
    }

    @Override
    public InteractionDetails createInteraction(InteractionDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createInteraction(event);
    }

    @Override
    public List<AssertionDetails> retrieveAllAssertions()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveAllAssertions();
    }

    @Override
    public AssertionDetails retrieveAssertion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveAssertion(id);
    }

    @Override
    public ConstantDetails updateConstant(ConstantDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateConstant(event);
    }

    @Override
    public EntityDetails updateEntity(EntityDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateEntity(event);
    }

    @Override
    public AssociationDetails updateAssociation(AssociationDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateAssociation(event);
    }

    @Override
    public AttributionDetails updateAttribution(AttributionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateAttribution(event);
    }


    @Override
    public InteractionDetails updateInteraction(InteractionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateInteraction(event);
    }

    @Override
    public AssertionDetails deleteAssertion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.deleteAssertion(id);
    }



    @Override
    public DurationOrderDetails createDurationOrder(DurationOrderDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createDurationOrder(event);
    }

    @Override
    public List<DurationOrderDetails> retrieveAllDurationOrders()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveAllDurationOrders();
    }

    @Override
    public DurationOrderDetails retrieveDurationOrder(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveDurationOrder(id);
    }

    @Override
    public DurationOrderDetails updateDurationOrder(DurationOrderDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateDurationOrder(event);
    }

    @Override
    public DurationOrderDetails deleteDurationOrder(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.deleteDurationOrder(id);
    }



    @Override
    public SemiIntervalOrderDetails createSemiIntervalOrder(
            SemiIntervalOrderDetails event)
            throws RequestDeniedException, RequestFailedException {

        return this.dataModelPersistenceService.createSemiIntervalOrder(event);
    }

    @Override
    public List<SemiIntervalOrderDetails> retrieveAllSemiIntervalOrders()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveAllSemiIntervalOrders();
    }

    @Override
    public SemiIntervalOrderDetails retrieveSemiIntervalOrder(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.retrieveSemiIntervalOrder(id);
    }

    @Override
    public SemiIntervalOrderDetails updateSemiIntervalOrder(
            SemiIntervalOrderDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.updateSemiIntervalOrder(event);
    }

    @Override
    public SemiIntervalOrderDetails deleteSemiIntervalOrder(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        return this.dataModelPersistenceService.deleteSemiIntervalOrder(id);
    }
}
