package raziel.pawn.core.services;


import java.util.List;

import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;
import raziel.pawn.dto.modeling.AssertionDetails;
import raziel.pawn.dto.modeling.AssociationDetails;
import raziel.pawn.dto.modeling.AssociativeRoleDetails;
import raziel.pawn.dto.modeling.AttributionDetails;
import raziel.pawn.dto.modeling.AttributiveRoleDetails;
import raziel.pawn.dto.modeling.ConceptDetails;
import raziel.pawn.dto.modeling.ConstantDetails;
import raziel.pawn.dto.modeling.DataModelDetails;
import raziel.pawn.dto.modeling.DurationOrderDetails;
import raziel.pawn.dto.modeling.EndurantDetails;
import raziel.pawn.dto.modeling.EntityDetails;
import raziel.pawn.dto.modeling.InteractionDetails;
import raziel.pawn.dto.modeling.InteractiveRoleDetails;
import raziel.pawn.dto.modeling.PerdurantDetails;
import raziel.pawn.dto.modeling.RoleDetails;
import raziel.pawn.dto.modeling.SemiIntervalOrderDetails;


/**
 * @author Peter J. Radics
 * @version 0.0.1
 * @since 0.0.1
 */
public interface DataModelService {


    /**
     * Creates a data model.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public DataModelDetails createDataModel(DataModelDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all dataModels.
     * 
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<DataModelDetails> retrieveAllDataModels()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single DataModel.
     * 
     * @param id
     *            the id of the requested object.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public DataModelDetails retrieveDataModel(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a DataModel.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public DataModelDetails updateDataModel(DataModelDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a DataModel.
     * 
     * @param id
     *            the id of the object to delete.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public DataModelDetails deleteDataModel(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Creates a endurant.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public EndurantDetails createEndurant(EndurantDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates a perdurant.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public PerdurantDetails createPerdurant(PerdurantDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all Concepts.
     * 
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<ConceptDetails> retrieveAllConcepts()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single Concept.
     * 
     * @param id
     *            the id of the requested object.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ConceptDetails retrieveConcept(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an Endurant.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public EndurantDetails updateEndurant(EndurantDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a Perdurant.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public PerdurantDetails updatePerdurant(PerdurantDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a Concept.
     * 
     * @param id
     *            the id of the object to delete.
     * @return the result of the request.
     * 
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ConceptDetails deleteConcept(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Creates an associative relation
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public AssociativeRoleDetails createAssociativeRole(
            AssociativeRoleDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates an attributive relation.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public AttributiveRoleDetails createAttributiveRole(
            AttributiveRoleDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates an interactive relation.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public InteractiveRoleDetails createInteractiveRole(
            InteractiveRoleDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all Roles.
     * 
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<RoleDetails> retrieveAllRoles()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single Role.
     * 
     * @param id
     *            the id of the requested object.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public RoleDetails retrieveRole(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an AssociativeRole.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AssociativeRoleDetails updateAssociativeRole(
            AssociativeRoleDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an AttributiveRole.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AttributiveRoleDetails updateAttributiveRole(
            AttributiveRoleDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a InteractiveRole.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public InteractiveRoleDetails updateInteractiveRole(
            InteractiveRoleDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a Role.
     * 
     * @param id
     *            the id of the object to delete.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public RoleDetails deleteRole(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;


    /**
     * Creates a constant.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public ConstantDetails createConstant(ConstantDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates a entity.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public EntityDetails createEntity(EntityDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates an association.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public AssociationDetails createAssociation(AssociationDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates an attribution.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public AttributionDetails createAttribution(AttributionDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Creates an interaction.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public InteractionDetails createInteraction(InteractionDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all Assertions.
     * 
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<AssertionDetails> retrieveAllAssertions()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single Assertion.
     * 
     * @param id
     *            the id of the requested object.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AssertionDetails retrieveAssertion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a Constant.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public ConstantDetails updateConstant(ConstantDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an Entity.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public EntityDetails updateEntity(EntityDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an Association.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AssociationDetails updateAssociation(AssociationDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an Attribution.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AttributionDetails updateAttribution(AttributionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates an Interaction.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public InteractionDetails updateInteraction(InteractionDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes an Assertion.
     * 
     * @param id
     *            the id of the object to delete.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public AssertionDetails deleteAssertion(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Creates a duration order.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public DurationOrderDetails createDurationOrder(DurationOrderDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all DurationOrders.
     * 
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<DurationOrderDetails> retrieveAllDurationOrders()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single DurationOrder.
     * 
     * @param id
     *            the id of the requested object.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public DurationOrderDetails retrieveDurationOrder(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a DurationOrder.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public DurationOrderDetails updateDurationOrder(DurationOrderDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a DurationOrder.
     * 
     * @param id
     *            the id of the object to delete.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public DurationOrderDetails deleteDurationOrder(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;



    /**
     * Creates a SemiIntervalOrder.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     */
    public SemiIntervalOrderDetails createSemiIntervalOrder(
            SemiIntervalOrderDetails event)
            throws RequestDeniedException, RequestFailedException;

    /**
     * Retrieves all SemiIntervalOrders.
     * 
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public List<SemiIntervalOrderDetails> retrieveAllSemiIntervalOrders()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Retrieves a single SemiIntervalOrder.
     * 
     * @param id
     *            the id of the requested object.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SemiIntervalOrderDetails retrieveSemiIntervalOrder(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Updates a SemiIntervalOrder.
     * 
     * @param event
     *            the details of the request.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SemiIntervalOrderDetails updateSemiIntervalOrder(
            SemiIntervalOrderDetails event)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

    /**
     * Deletes a SemiIntervalOrder.
     * 
     * @param id
     *            the id of the object to delete.
     * @return the result of the request.
     * @throws RequestDeniedException
     *             if the request was denied.
     * @throws RequestFailedException
     *             if the request failed.
     * @throws ObjectNotFoundException
     *             if the object was not found.
     */
    public SemiIntervalOrderDetails deleteSemiIntervalOrder(final int id)
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException;

}
