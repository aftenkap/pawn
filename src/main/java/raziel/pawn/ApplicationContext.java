package raziel.pawn;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import raziel.pawn.config.CoreConfig;
import raziel.pawn.config.GuiConfig;
import raziel.pawn.config.JPAConfiguration;
import raziel.pawn.core.services.DataModelService;
import raziel.pawn.ui.controllers.ProjectController;
import raziel.pawn.ui.controllers.SurveyController;
import raziel.pawn.ui.controllers.SpreadsheetController;


/**
 * Wrapper to always return a reference to the Spring Application Context from
 * within non-Spring enabled beans. Unlike Spring MVC's
 * WebApplicationContextUtils we do not need a reference to the Servlet context
 * for this. All we need is for this bean to be initialized during application
 * startup.
 */
public class ApplicationContext {

    private static AnnotationConfigApplicationContext    instance;

    private static ApplicationListener<ApplicationEvent> applicationListener;

    private static BeanPostProcessor                     beanPostProcessor;

    private static Logger                                LOG = LoggerFactory
                                                                     .getLogger(ApplicationContext.class);



    /**
     * Sets the applicationListener.
     * 
     * @param applicationListener
     *            the applicationListener.
     */
    public static void setApplicationListener(
            ApplicationListener<ApplicationEvent> applicationListener) {

        ApplicationContext.applicationListener = applicationListener;
    }

    /**
     * Sets the beanPostProcessor.
     * 
     * @param beanPostProcessor
     *            the beanPostProcessor.
     */
    public static void setBeanPostProcessor(BeanPostProcessor beanPostProcessor) {

        ApplicationContext.beanPostProcessor = beanPostProcessor;
    }

    /**
     * Returns the Singleton Instance of the application context.
     * 
     * @return the Singleton Instance of the application context.
     */
    public static AnnotationConfigApplicationContext Instance() {

        if (ApplicationContext.instance == null) {

            LOG.debug("Creating application context.");
            ApplicationContext.instance = new AnnotationConfigApplicationContext();

            if (ApplicationContext.applicationListener != null) {

                LOG.debug("Setting application listener.");
                ApplicationContext.instance
                        .addApplicationListener(ApplicationContext.applicationListener);
            }

            if (ApplicationContext.beanPostProcessor != null) {

                LOG.debug("Setting bean post processor.");
                ApplicationContext.instance.getBeanFactory()
                        .addBeanPostProcessor(
                                ApplicationContext.beanPostProcessor);
            }

            LOG.debug("Registering configuration classes.");
            ApplicationContext.instance.register(JPAConfiguration.class,
                    GuiConfig.class, CoreConfig.class);

            LOG.debug("Refreshing context.");
            ApplicationContext.instance.refresh();
        }

        return ApplicationContext.instance;
    }



    /**
     * Returns the project controller.
     * 
     * @return the project controller.
     */
    public static ProjectController projectController() {

        return ApplicationContext.Instance().getBean(
                ProjectController.class);
    }

    /**
     * Returns the table controller.
     * 
     * @return the table controller.
     */
    public static SpreadsheetController spreadsheetController() {

        return ApplicationContext.Instance().getBean(
                SpreadsheetController.class);
    }

    /**
     * Returns the survey controller.
     * 
     * @return the survey controller.
     */
    public static SurveyController surveyController() {

        return ApplicationContext.Instance().getBean(
                SurveyController.class);
    }

    /**
     * Returns the data model service.
     * 
     * @return the data model service.
     */
    public static DataModelService getDataModelService() {

        return ApplicationContext.Instance().getBean(
                DataModelService.class);
    }
}
