package raziel.pawn;


import java.net.URI;
import java.util.List;

import raziel.pawn.core.domain.Project;
import raziel.pawn.core.services.ProjectService;
import raziel.pawn.dto.ObjectNotFoundException;
import raziel.pawn.dto.ProjectDetails;
import raziel.pawn.dto.RequestDeniedException;
import raziel.pawn.dto.RequestFailedException;


/**
 * @author Peter J. Radics
 * @version 0.1
 * @since 0.1
 */
public class WorkEnvironment {

    private static WorkEnvironment instance;
    private final ProjectService   service;

    /**
     * Returns the project service.
     * 
     * @return the project service.
     */
    public ProjectService service() {

        return this.service;
    }

    /**
     * Returns the {@link Project Projects} of this {@link WorkEnvironment}.
     * 
     * @return the {@link Project Projects} of this {@link WorkEnvironment}.
     * @throws ObjectNotFoundException
     *             if no {@link Project} is found.
     * @throws RequestFailedException
     *             if the request fails.
     * @throws RequestDeniedException
     *             if the request is denied.
     */
    public List<Project> getProjects()
            throws RequestDeniedException, RequestFailedException,
            ObjectNotFoundException {

        List<ProjectDetails> result = this.service.retrieveAllProjects();

        return Project.fromDetails(result);
    }

    /**
     * The singleton instance of the {@link WorkEnvironment} class.
     * 
     * @return the singleton instance of the {@link WorkEnvironment} class.
     */
    public static WorkEnvironment Instance() {

        if (WorkEnvironment.instance == null) {

            WorkEnvironment.instance = new WorkEnvironment();
        }

        return WorkEnvironment.instance;
    }


    /**
     * Creates a new instance of the {@link WorkEnvironment} class.
     */
    private WorkEnvironment() {


        this.service = ApplicationContext.Instance().getBean(
                ProjectService.class);
    }


    /**
     * Sets up a new database schema for a new work environment.
     * 
     * @param databaseServer
     *            the database server.
     * @param schema
     *            the database schema.
     * @param dbAdminName
     *            the username of a database administrator.
     * @param dbAdminPassword
     *            the password of the provided database administrator.
     * 
     * @param username
     *            the username.
     * @param password
     *            the password.
     */
    public void createNewWorkEnvironment(URI databaseServer, String schema,
            String dbAdminName, String dbAdminPassword, String username,
            String password) {

        throw new UnsupportedOperationException(
                "Creating a new work environment is currently not implemented!");
    }


}
