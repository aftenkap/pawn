package raziel.pawn;



import java.util.List;
import java.util.Optional;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Duration;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;

import raziel.pawn.core.domain.Project;
import raziel.pawn.ui.controls.WorkspaceGridPane;



/**
 * The {@link PawnGui} class provides the launcher for the Graphical User
 * Interface of the Privacy Work eNvironment.
 *
 * @author Peter J. Radics
 * @version 0.1.2
 * @since 0.1.0
 */
public class PawnGui
        extends Application {

    /**
     * The application icon.
     */
    public static final String APPLICATION_ICON = "images/pawn-icon-256.png";
    /**
     * The splash image.
     */
    public static final String SPLASH_IMAGE     = "images/pawn-logo.png";

    private Pane               splashLayout;
    private ProgressBar        loadProgress;
    private Label              progressText;
    private static final int   SPLASH_WIDTH     = 1116;
    private static final int   SPLASH_HEIGHT    = 540;

    private final String       title            = "Privacy Analyst Work eNvironment";

    private Stage              stage;
    private Scene              mainScene;

    private WorkspaceGridPane  workspacePane;



    /**
     * Main method.
     *
     * @param args
     *            optional arguments (unused)
     * @throws InterruptedException
     *             if the initialization task is interrupted.
     */
    public static void main(final String[] args)
            throws InterruptedException {

        Application.launch(args);
    }


    @Override
    public void init() {

        final ImageView splash = new ImageView(new Image(PawnGui.SPLASH_IMAGE));
        this.loadProgress = new ProgressBar();
        this.loadProgress.setPrefWidth(PawnGui.SPLASH_WIDTH);
        this.progressText = new Label("Initializing. . .");
        this.splashLayout = new VBox();
        this.splashLayout.getChildren().addAll(splash, this.loadProgress,
                this.progressText);
        this.progressText.setAlignment(Pos.CENTER);
        this.progressText.setStyle("-fx-backgroudn-color: white");
        this.splashLayout.setStyle("-fx-background-color: white; "
                + "-fx-border-width:5; " + "-fx-border-color: "
                + "linear-gradient(" + "to bottom, " + "white, "
                + "derive(-fx-color, 50%)" + ");");
        this.splashLayout.setEffect(new DropShadow());
    }


    @Override
    public void start(final Stage initStage)
            throws Exception {


        final Task<List<Project>> initTask = new InitializationTask();

        this.showSplash(initStage, initTask,
                () -> this.showMainStage(initTask.valueProperty()));
        new Thread(initTask).start();
    }

    private void showMainStage(
            final ReadOnlyObjectProperty<List<Project>> projects) {

        this.workspacePane = WorkspaceGridPane.Instance();
        this.workspacePane.projects().addAll(projects.get());

        this.stage = new Stage(StageStyle.DECORATED);
        this.stage.setTitle(this.title);


        this.mainScene = new Scene(this.workspacePane, 1440, 900,
                Color.LIGHTSLATEGREY);


        this.stage.getIcons().add(new Image(PawnGui.APPLICATION_ICON));

        this.stage.setScene(this.mainScene);
        this.stage.show();
    }

    private void showSplash(final Stage initStage, final Task<?> task,
            final InitCompletionHandler initCompletionHandler) {

        this.progressText.textProperty().bind(task.messageProperty());
        this.loadProgress.progressProperty().bind(task.progressProperty());
        task.stateProperty().addListener(
                (observableValue, oldState, newState) -> {
                    if (newState == Worker.State.SUCCEEDED) {
                        this.loadProgress.progressProperty().unbind();
                        this.loadProgress.setProgress(1);
                        initStage.toFront();


                        final FadeTransition fadeSplash = new FadeTransition(
                                Duration.seconds(1), this.splashLayout);
                        fadeSplash.setFromValue(1.0);
                        fadeSplash.setToValue(0.0);
                        fadeSplash.setOnFinished(actionEvent -> {
                            initStage.hide();

                            initCompletionHandler.complete();
                        });
                        fadeSplash.play();

                    } // todo add code to gracefully handle other task states.
                });

        final Scene splashScene = new Scene(this.splashLayout);
        initStage.initStyle(StageStyle.UNDECORATED);

        splashScene.setCursor(Cursor.WAIT);
        final Rectangle2D bounds = Screen.getPrimary().getBounds();
        initStage.setScene(splashScene);
        initStage.setX((bounds.getMinX() + (bounds.getWidth() / 2))
                - (PawnGui.SPLASH_WIDTH / 2));
        initStage.setY((bounds.getMinY() + (bounds.getHeight() / 2))
                - (PawnGui.SPLASH_HEIGHT / 2));
        initStage.show();
    }

    /**
     *
     * @author Peter J. Radics
     * @version 0.1
     * @since 0.1
     *
     */
    public interface InitCompletionHandler {

        /**
         * Called once initialization is completed.
         */
        public void complete();
    }


    private static class InitializationTask
            extends Task<List<Project>> {

        /**
         * Creates a new instance of the {@code InitializationTask} class.
         */
        public InitializationTask() {

            super();
        }

        @Override
        protected void updateMessage(final String message) {

            super.updateMessage(message);
        }

        @Override
        protected List<Project> call()
                throws InterruptedException {


            this.updateMessage("Creating Application Context . . .");
            ApplicationContext.setApplicationListener((event) -> {

                if (event instanceof ContextStartedEvent) {

                    this.updateMessage("Aplication Context Started");

                }
                else if (event instanceof ContextRefreshedEvent) {

                    this.updateMessage("Aplication Context Refreshed");

                }
            });

            ApplicationContext.setBeanPostProcessor(new BeanPostProcessor() {

                @Override
                public Object postProcessBeforeInitialization(
                        final Object bean, final String beanName)
                        throws BeansException {

                    InitializationTask.this.updateMessage("Initializing Bean "
                            + beanName + " . . .");
                    return bean;
                }

                @Override
                public Object postProcessAfterInitialization(final Object bean,
                        final String beanName)
                        throws BeansException {

                    InitializationTask.this.updateMessage("Initializing Bean "
                            + beanName + " . . . Done.");
                    return bean;
                }
            });

            ApplicationContext.Instance();

            this.updateProgress(0.8, 1);

            this.updateMessage("Loading Projects . . .");

            List<Project> projects = null;

            Optional<List<Project>> result = ApplicationContext
                    .projectController().retrieveAllProjects((Window) null);
            if (result.isPresent()) {

                projects = result.get();
                ApplicationContext.surveyController();
                ApplicationContext.spreadsheetController();
                this.updateMessage("Loading Projects . . . Done.");

                this.updateProgress(1, 1);
                return projects;
            }

            throw new InterruptedException("Loading projects failed!");
        }

    }

}
