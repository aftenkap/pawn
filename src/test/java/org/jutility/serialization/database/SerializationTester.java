//package org.jutility.serialization.database;
//
//
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.util.List;
//
//import raziel.pawn.core.domain.data.AssertionSet;
//import raziel.pawn.core.domain.data.Project;
////import raziel.pawn.data.Participant22;
////import raziel.pawn.data.Participant3;
//import raziel.pawn.core.domain.modeling.assertions.Fluent;
//import raziel.pawn.core.domain.modeling.time.DurationOrder;
//import raziel.pawn.core.domain.modeling.time.SemiIntervalOrder;
//import raziel.pawn.persistence.domain.DomainObject;
//import raziel.pawn.persistence.domain.IDomainObject;
//
//
//public class SerializationTester {
//
//    public static void main(String[] args)
//            throws URISyntaxException {
//
//
//        Project project = new Project("Privacy in Domestic Environments");
//
//        DataModel participant3 = new DataModel("Participant 3");
//        DataModel participant22 = new DataModel("Participant 22");
//
//        project.getDatasets().add(participant3);
//        project.getDatasets().add(participant22);
//
////        SerializationTester.parseKnowledgeBase(project, participant3,
////                Participant3.getKnowledgebase());
////        SerializationTester.parseKnowledgeBase(project, participant22,
////                Participant22.getKnowledgebase());
//
//
//        DBSerializer serializer = null;
//        try {
//            serializer = new DBSerializer();
//            serializer.setDatabaseServer(new URI("localhost:3306"));
//            serializer.setUsername("privacyworkbench");
//            serializer.setPassword("Pr1vacy$ystem");
//
//            serializer.serialize(project);
//
//        }
//        catch (DBSerializationException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
//
//    public static void parseKnowledgeBase(Project project, DataModel dataset,
//            List<DomainObject> knowledgebase) {
//
//        for (IDomainObject dbObject : knowledgebase) {
//
//            if (dbObject instanceof Fluent) {
//
//                Fluent fluent = (Fluent) dbObject;
//
//                project.addFluent(fluent, dataset);
//
//            }
//            else if (dbObject instanceof DurationOrder) {
//
//                DurationOrder durationOrder = (DurationOrder) dbObject;
//
//                project.addDurationOrder(durationOrder, dataset);
//            }
//            else if (dbObject instanceof SemiIntervalOrder) {
//
//                SemiIntervalOrder semiIntervalOrder = (SemiIntervalOrder) dbObject;
//
//                project.addSemiIntervalOrder(semiIntervalOrder, dataset);
//            }
//        }
//    }
//}
