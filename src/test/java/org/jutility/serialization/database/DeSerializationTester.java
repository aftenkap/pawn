//package org.jutility.serialization.database;
//
//
//import java.util.LinkedList;
//import java.util.List;
//
//import raziel.pawn.common.DatabaseObject;
//import raziel.pawn.data.Dataset;
//import raziel.pawn.data.Participant22;
//import raziel.pawn.data.Participant3;
//import raziel.pawn.data.Project;
//import raziel.pawn.core.domain.modeling.assertions.Action;
//import raziel.pawn.core.domain.modeling.assertions.DatatypeProperty;
//import raziel.pawn.core.domain.modeling.assertions.Duration;
//import raziel.pawn.core.domain.modeling.assertions.Fluent;
//import raziel.pawn.core.domain.modeling.assertions.SemiInterval;
//
//
//
//public class DeSerializationTester {
//
//    public static void main(String[] args) {
//
//        Project project = new Project("Privacy in Domestic Environments");
//        
//
//        DataModel participant3 = new DataModel("Participant 3");
//        DataModel participant22 = new DataModel("Participant 22");
//
//        project.getDatasets().add(participant3);
//        project.getDatasets().add(participant22);
//
//        SerializationTester.parseKnowledgeBase(project, participant3,
//                Participant3.getKnowledgebase());
//        SerializationTester.parseKnowledgeBase(project, participant22,
//                Participant22.getKnowledgebase());
//
//
//
//        List<Project> projects = null;
//
//        DBSerializer serializer = null;
//        try {
//            serializer = new DBSerializer();
//            serializer.setUsername("privacyworkbench");
//            serializer.setPassword("Pr1vacy$ystem");
//            projects = serializer.deserialize(Project.class);
//        }
//        catch (DBSerializationException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        System.out.println("\n\nOriginal list:\n");
//        for (Fluent fluent : project.getMasterDataset().getFluents()) {
//
//            DeSerializationTester.printFluent(fluent, "  ", true);
//        }
//
//
//        System.out.println("\n\nDeserialized list:\n");
//
//        for (Fluent fluent : projects.get(0).getMasterDataset().getFluents()) {
//
//            DeSerializationTester.printFluent(fluent, "  ", true);
//        }
//
//        System.out.println("Original project:");
//        System.out.println("\tFluents: "
//                + project.getMasterDataset().getFluents().size());
//        System.out.println("\tDurationOrders: "
//                + project.getMasterDataset().getDurationOrders().size());
//        System.out.println("\tSemiIntervalOrders: "
//                + project.getMasterDataset().getSemiIntervalOrders().size());
//        System.out.println("Deserialized project:");
//        System.out.println("\tFluents: "
//                + projects.get(0).getMasterDataset().getFluents().size());
//        System.out
//                .println("\tDurationOrders: "
//                        + projects.get(0).getMasterDataset()
//                                .getDurationOrders().size());
//        System.out.println("\tSemiIntervalOrders: "
//                + projects.get(0).getMasterDataset().getSemiIntervalOrders()
//                        .size());
//    }
//
//    private static void printFluent(Fluent fluent, String prefix,
//            boolean recurse) {
//
//        System.out.println(prefix + "Fluent " + fluent);
//        if (recurse) {
//
//            printAction(fluent.getAction(), prefix + "  ", false);
//            printDuration(fluent.getDuration(), prefix + "  ");
//            System.out.println(prefix + "  DatatypeProperties");
////            for (DatatypeProperty datatypeProperty : fluent
////                    .getDatatypeProperties()) {
////                printDatatypeProperty(datatypeProperty, prefix + "    ");
////            }
//        }
//
//    }
//
//    private static void printAction(Action action, String prefix,
//            boolean recurse) {
//
//        System.out.println(prefix + "Action " + action);
//        System.out.println(prefix + "  Subjects:");
//        for (Fluent subject : action.getSubjects()) {
//            printFluent(subject, prefix + "    ", recurse);
//        }
//        System.out.println(prefix + "  Objects:");
//        for (Fluent subject : action.getObjects()) {
//            printFluent(subject, prefix + "    ", recurse);
//        }
//    }
//
//    private static void printDuration(Duration duration, String prefix) {
//
//        System.out.println(prefix + "Duration " + duration);
//        System.out.println(prefix + "  Start");
//        printSemiInterval(duration.getStart(), prefix + "    ");
//        System.out.println(prefix + "  End");
//        printSemiInterval(duration.getEnd(), prefix + "    ");
//    }
//
//    private static void printSemiInterval(SemiInterval semiInterval,
//            String prefix) {
//
//        System.out.println(prefix + "SemiInterval " + semiInterval);
//        System.out.println(prefix + "  " + semiInterval.getTime());
//    }
//
//    private static void printDatatypeProperty(
//            DatatypeProperty datatypeProperty, String prefix) {
//
//        System.out.println(prefix + "DatatypeProperty " + datatypeProperty);
//        System.out.println(prefix + "  " + datatypeProperty.getValue());
//        printDuration(datatypeProperty.getDuration(), prefix + "  ");
//    }
//}
